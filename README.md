# Ryzom Forge Data

This repository is a clone of https://github.com/ryzom/ryzomcore_leveldesign/ with modifications for the specific mmorpg RYZOM.

## What you need
The current tools and processes have been tested only on Windows. A **windows 64bit** is therefore required.

First, you need [Git for Windows](https://gitforwindows.org/ "Git for Windows") which comes with the git tool and a bash terminal.

If you want to do 3D things, **3DSMax** is needed (a trial version works).

## How to install the ryzom tools
After installing git for Windows with all default options (git and bash commands are required), you can double-click on the **configure.sh** script.
The script will install all required packages and ask you to download additional packages.

**Landscapes for WorldEditor** are only needed if you are working with primitives (which are private data) or lansdcape for that matter.

**Pipeline exports** are already exported files (with pipeline process), only required if you need to work on landscape, collisions or very final things.

You can rerun configure to install/remove additional packages or reinstall a package if you have deleted the file.

