#!/bin/bash

SCRIPT_DIR=$(dirname -- "$0")
SCRIPT_DIR=$(realpath $SCRIPT_DIR)

CORE_DIR=$(realpath  ../../../ryzom-core)
cd $CORE_DIR/ryzom/client/src
git checkout main/rendor-staging
git diff --name-only main/rendor-staging main/atys-live > $SCRIPT_DIR/diff_atys_rendor.txt
REVISION=$(git rev-list HEAD --count .)

cd $SCRIPT_DIR


echo "REVISION = $REVISION"
cp $CORE_DIR/ryzom/client/data/gamedev/interfaces_v3/check_lua_versions.lua .
./setup_lua_versions.py $CORE_DIR $REVISION diff_atys_rendor.txt > $CORE_DIR/ryzom/client/data/gamedev/interfaces_v3/check_lua_versions.lua

