#!/usr/bin/env python3

import os, sys
import subprocess
import xml.etree.ElementTree as ET

def print_hash(shash):
	for i in range(5):
		s = shash[0+8*i:8+8*i]
		print(int(s, 16))

#print(os.stat("interfaces.bnp").st_size) # <_FileSize type="UINT32" value="18031502"/>
#print(os.path.getctime("interfaces.bnp")) # <_FileTime type="UINT32" value="1568735807"/> ????

#print(os.stat("patch_live/00750/exedll.bnp.lzma").st_size) # <_7ZFileSize type="UINT32" value="8461202"/>
#print(os.stat("patch_live/00750/exedll_00750.patch").st_size) # <_PatchSize type="UINT32" value="2528502"/>


renames = "cd ref_live\n"
tree = ET.parse("live.hist")
root = tree.getroot()
ignore = 1
for item in root.findall("_Files"):
	last_version = 0
	name = item.find("_FileName").attrib["value"]
	shortname = name[:-4]
	print("[ %s ]" % shortname)
	last_version = 0
	versions = []
	for vers in item.findall("_Versions"):
		for v in vers:
			if v.tag == "_VersionNumber":
				version = v.attrib["value"]
				if int(version) < 700:
					continue

				sys.stdout.write(version+" ")
				sys.stdout.flush()

				if last_version:
					if not os.path.isfile("tmp/"+name+"."+last_version):
						lzmafile = "patch_live/%.5d/%s.lzma" % (int(last_version), name)
						if os.path.isfile(lzmafile):
							os.system("7z -otmp x "+lzmafile+" > out.log 2>&1")
							if os.path.isfile("tmp/"+name):
								os.rename("tmp/"+name, "tmp/"+name+"."+last_version)
							else:
								last_version = 0
								with open("out.log") as f:
									print("ERR : %s" % f.read())
						else:
							print("\nNO FILE : %s" % lzmafile)
					os.system("7z -otmp x patch_live/%.5d/%s.lzma > /dev/null 2>&1" % (int(version), name))
					if os.path.isfile("tmp/"+name):
						os.rename("tmp/"+name, "tmp/"+name+"."+version)
					else:
						sys.stdout.write("!!!"+version+"!!! ")
						sys.stdout.flush()
						#continue
					if last_version:
						versions.append((last_version, version))
				last_version = version
	print()
	# Check patches
	for (last_version, version) in versions:
		os.system("xdelta patch patch_live/%.5d/%s_%.5d.patch tmp/%s.%s  tmp/%s.%s.B" % (int(version), shortname, int(version), name, last_version, name, version))
		md5A = subprocess.check_output("md5sum tmp/"+name+"."+version, shell=True).decode("utf-8").split(" ")[0]
		md5B = subprocess.check_output("md5sum tmp/"+name+"."+version+".B", shell=True).decode("utf-8").split(" ")[0]
		if md5A == md5B:
			print("%s => %s : OK" % (last_version, version))
		else:
			print("%s => %s : ERROR" % (last_version, version))
			os.system("xdelta delta tmp/"+name+"."+last_version+" tmp/"+name+"."+version+" tmp/"+shortname+"_00"+version+".patch")

