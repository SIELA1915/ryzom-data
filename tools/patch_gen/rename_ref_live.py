#!/usr/bin/env python3

import os, sys
import xml.etree.ElementTree as ET


renames = "cd ref_live\n"
tree = ET.parse("live.hist")
root = tree.getroot()
for item in root.findall("_Files"):
	last_version = 0
	name = item.find("_FileName").attrib["value"]
	for vers in item.findall("_Versions"):
		for v in vers:
			if v.tag == "_VersionNumber":
				version = int(v.attrib["value"])
				if version > last_version :
					last_version = version

	#renames += "mv "+name[:-4]+"*"+".bnp "+name+"\n"
	renames += "7z x ../patch_live/%05d/%s.lzma || exit\n" % (last_version, name) 
	renames += "mv %s %s_%05d%s\n" % (name, name[:-4], last_version, name[-4:])

open("update.sh", "w").write(renames)
