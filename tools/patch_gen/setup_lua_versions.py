#!/usr/bin/env python3
import os, sys, shutil
from datetime import date

if len(sys.argv) < 3:
	print("usage: %s path_to_ryzom_core version diff_file" % sys.argv[0])
	sys.exit()

path = sys.argv[1]+"/ryzom/client/data/gamedev/interfaces_v3/"
version = sys.argv[2]
diff_file = sys.argv[3]

with open(diff_file) as f:
	diffs = f.read().replace("ryzom/client/data/gamedev/interfaces_v3/", "").split("\n")

print("local ryzom_have_all_good_version = true")
for file in sorted(os.listdir(path)):
	if not file == "." and not file == "..":
		sfile = file.split(".", 2)
		if len(sfile) == 2:
			filebase = sfile[0].upper()
			if sfile[1] == "lua" and filebase != "BASE64" and filebase != "CHECK_LUA_VERSIONS":
				update = "wait"
				final = []
				with open(path+file, mode="r", encoding="utf-8") as f:
					lines = f.read().split("\n")
					for line in lines:
						if line.strip() == "-- VERSION --":
							update = "do"
							final.append(line)
						elif update == "do":
							if file in diffs:
								final.append("RYZOM_"+filebase+"_VERSION = "+version)
								file_version = version
							else:
								sline = line.split(" = ")
								file_version = sline[1]
								final.append(line)
							update = "done"
						else:
							final.append(line)
				if update == "wait":
					final.append("-- VERSION --")
					final.append("RYZOM_"+filebase+"_VERSION = "+version)

				with open(path+file, "w") as f:
					f.write("\n".join(final))
				print("if RYZOM_"+filebase+"_VERSION ~=", file_version, "then broadcastBadLuaVersions(RYZOM_"+filebase+"_VERSION, "+file_version+", \""+filebase.lower()+"\") end")

