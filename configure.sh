#!/bin/sh

SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do
	DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
	SOURCE=$(readlink "$SOURCE")
	[[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
BASE_PATH=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )

DL_URL="https://download.ryzom.com/tools"
export PATH="$PATH:$BASE_PATH/useful_stuff/_tools:$BASE_PATH/scripts/"

r_check.sh

Frameless $BASE_PATH/useful_stuff/_tools/ryzom_forge.png x=C y=C duration=00:00:02  trans=alpha

RYZOMCORE_PATH=""
if [[ -f "$BASE_PATH"/scripts/config.sh ]]
then
	source "$BASE_PATH"/scripts/config.sh
fi
echo  "$RYZOMCORE_PATH"
update_config=0

if [[ ! -d python27 ]]
then
	dl_install.sh $DL_URL/python27.7z
fi

mkdir -p tools/bin/
cd tools/bin/
if [[ ! -d nevrax ]]
then
	dl_install.sh $DL_URL/tools_win_x64.7z
	mv tools_win_x64 nevrax
fi
cd $BASE_PATH

if [[ ! -d graphics2 ]]
then
	dl_install.sh https://cdn.ryzom.dev/pub/assets/ryzomcore_graphics-rev5.7z graphics2
fi

cd leveldesign/landscape/
OPTIONS=""
for eco in desert jungle lacustre primes_racines
do
	if [[ ! -d $eco/collisionmap ]] || [[ ! -d $eco/pacs ]] || [[ ! -d $eco/zonebitmaps ]] || [[ ! -d $eco/zoneligos ]] || [[ ! -d $eco/zones ]]
	then
		OPTIONS="$OPTIONS FALSE $eco"
	else
		OPTIONS="$OPTIONS TRUE $eco"
	fi
done
selected_ecos=$(zenity --height 300 --list --checklist --title="Ecosystems" --text="Select <b>Ecosystems</b> for WorldEditor\n\n<span color='green'>If an ecosystem are checked but files missing, will be downloaded</span>\n<span color='red'>If an ecosystem are uncheck, files will be deleted</span>" --column="Use" --column="Ecosystem" $OPTIONS)
[[ "$?" != "0" ]] && exit 1

for eco in desert jungle lacustre primes_racines
do
	if [[ $selected_ecos == *"$eco"* ]]; then
		if [[ ! -d $eco/collisionmap ]] || [[ ! -d $eco/pacs ]] || [[ ! -d $eco/zonebitmaps ]] || [[ ! -d $eco/zoneligos ]] || [[ ! -d $eco/zones ]]
		then
			dl_install.sh $DL_URL/landscape/$eco.7z
		fi
	else
		rm -rf $eco/collisionmap $eco/pacs $eco/zonebitmaps $eco/zoneligos $eco/zones
	fi
done

cd ../..


want_install_pipeline=$(zenity --height 200 --list --checklist --title="Pipeline" --text="<span color='red'>Pipeline export take a lot of space</span>\nIt's only required if you work on landscape updates or create a new map\nDo you want install them?\nYou will able to select which parts install" --column="Your choice" --column="Action" FALSE "I want install pipeline export")
[[ "$?" != "0" ]] && exit 1

if [[ ! -z "$want_install_pipeline" ]]
then


	if [[ -z "$RYZOMCORE_PATH" ]]
	then
		zenity --height 100 --info --text="Please select location of <b>ryzom-core</b> repository\n\nIf you select a blank folder, repository will be cloned into &lt;location&gt;/ryzom-core"
		[[ "$?" != "0" ]] && exit 1
		selected_ryzomcore=$(zenity --file-selection --directory --title="Rycom-core Repository")
		[[ "$?" != "0" ]] && exit 1
		echo "$selected_ryzomcore"
		if [[ ! -d "$selected_ryzomcore/nel/tools/build_gamedata" ]]
		then
			if [[ -d "$selected_ryzomcore/ryzom-core" ]]
			then
				if [[ ! -d "$selected_ryzomcore/ryzom-core/nel/tools/build_gamedata" ]]
				then
					echo "OK subdir"
					RYZOMCORE_PATH="$selected_ryzomcore/ryzom-core/"
					update_config=1
				else
					zenity --error --text="This is not a ryzom-core repository and allready contain a ryzom-core folder.\n\nProccess canceled"
					exit 1
				fi
			else
				echo "CLONE"
				cd "$selected_ryzomcore"
				git config --global init.defaultBranch main
				git clone -b yubo https://gitlab.com/ryzom/ryzom-core.git
				RYZOMCORE_PATH="$selected_ryzomcore/ryzom-core/"
				update_config=1
				cd $BASE_PATH
			fi
		else
			echo "OK"
			RYZOMCORE_PATH="$selected_ryzomcore/"
			update_config=1
		fi
	fi

	mkdir -p pipeline
	cd pipeline
	if [[ ! -d pipeline/build_gamedata ]]
	then
		cp -r $RYZOMCORE_PATH/nel/tools/build_gamedata .
	fi

	mkdir -p export
	cd export
	mkdir -p ecosystems
	cd ecosystems

	OPTIONS=""
	for eco in desert jungle lacustre primes_racines
	do
		if [[ ! -d $eco ]]
		then
			OPTIONS="$OPTIONS FALSE $eco"
		else
			OPTIONS="$OPTIONS TRUE $eco"
		fi
	done
	selected_ecos=$(zenity --height 300 --list --checklist --title="Ecosystems" --text="Select <b>pipeline export Ecosystems</b> ot install\n\n<span color='green'>If an ecosystem are checked but files missing, will be downloaded</span>\n<span color='red'>If an ecosystem are uncheck, files will be deleted</span>" --column="Install" --column="PIPELINE Ecosystem" $OPTIONS)
	[[ "$?" != "0" ]] && exit 1

	for eco in desert jungle lacustre primes_racines
	do
		if [[ $selected_ecos == *"$eco"* ]]
		then
			if [[ ! -d $eco ]]
			then
				dl_install.sh $DL_URL/pipeline/$eco.7z
			fi
		else
			rm -rf $eco
		fi
	done

	echo "$selected_ecos"
	cd ..
	mkdir -p continents
	cd continents

	OPTIONS=""
	if [[ $selected_ecos == *"desert"* ]]
	then
		for continent in fyros fyros_island fyros_newbie r2_desert
		do
			if [[ ! -d $continent ]]
			then
				OPTIONS="$OPTIONS FALSE $continent"
			else
				OPTIONS="$OPTIONS TRUE $continent"
			fi
		done
	fi

	if [[ $selected_ecos == *"jungle"* ]]
	then
		for continent in corrupted_moor matis newbieland r2_forest zorai indoors matis_island nexus r2_jungle zorai_island
		do
			if [[ ! -d $continent ]]
			then
				OPTIONS="$OPTIONS FALSE $continent"
			else
				OPTIONS="$OPTIONS TRUE $continent"
			fi
		done
	fi

	if [[ $selected_ecos == *"lacustre"* ]]
	then
		for continent in r2_lakes tryker tryker_island tryker_newbie
		do
			if [[ ! -d $continent ]]
			then
				OPTIONS="$OPTIONS FALSE $continent"
			else
				OPTIONS="$OPTIONS TRUE $continent"
			fi
		done
	fi


	if [[ $selected_ecos == *"primes_racines"* ]]
	then
		for continent in bagne kitiniere r2_roots route_gouffre sources terre
		do
			if [[ ! -d $continent ]]
			then
				OPTIONS="$OPTIONS FALSE $continent"
			else
				OPTIONS="$OPTIONS TRUE $continent"
			fi
		done
	fi

	selected_continents=$(zenity --height 480 --list --checklist --title="Continents" --text="Select <b>pipeline export Continents</b> ot install\n\n<span color='green'>If an ecosystem are checked but files missing, will be downloaded</span>\n<span color='red'>If an ecosystem are uncheck, files will be deleted</span>" --column="Install" --column="PIPELINE Continents" $OPTIONS)
	[[ "$?" != "0" ]] && exit 1

	echo "$selected_continents"

	for continent in fyros fyros_island fyros_newbie r2_desert corrupted_moor matis newbieland r2_forest zorai indoors matis_island nexus r2_jungle zorai_island r2_lakes tryker tryker_island tryker_newbie bagne kitiniere r2_roots route_gouffre sources terre
	do
		if [[ $selected_continents == *"$continent"* ]]
		then
			if [[ ! -d $continent ]]
			then
				dl_install.sh $DL_URL/pipeline/$continent.7z
			fi
		else
			rm -rf $continent
		fi
	done

fi


if [[ "$update_config" == "1" ]]
then
	echo "RYZOMCORE_PATH=\"$RYZOMCORE_PATH\"" > "$BASE_PATH"/scripts/config.sh
fi

