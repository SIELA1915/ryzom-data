#!/usr/bin/env python3
import sys

phrases_wk = {}

for lang in ("wk", "de", "en", "es", "fr", "ru"):
	with open(sys.argv[1]+"/phrase_base_"+lang+".txt", "r") as f:
		phrases = f.read()

	new_phrase = False
	with open(sys.argv[1]+"/phrases_"+lang+".uxt", "r") as f:
		lines = f.read().split("\n")
		for line in lines:
			sline = line.split("\t")
			if len(sline) > 1:
				if sline[0] and sline[1][0] == "[":
					new_phrase = True
					name = sline[0].split(" ")
					params = " ".join(name[1:])
					if not params:
						params = "()"
					name = name[0].upper()
					value = "\t".join(sline[1:])
					if lang == "wk":
						phrases_wk[name] = value
					elif value == "[]":
							value = phrases_wk[name]
					phrases += name+" "+params+"\n{\n\t\t"+name.lower()+"\t"+value+"\n"
				else:
					phrases += line+"\n"
			elif line == "" and new_phrase:
				phrases += "}\n\n"
				new_phrase = False
			else:
				phrases += line+"\n"

	with open(sys.argv[1]+"/phrase_"+lang+".txt", "w") as f:
		f.write(phrases)

