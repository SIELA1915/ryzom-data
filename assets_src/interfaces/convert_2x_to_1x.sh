#!/bin/bash

function convert_image {
	CURRENT_DIR=$(pwd)
	echo $CURRENT_DIR
	cd $1
	for a in *.png
	do
		if [ ! -f $CURRENT_DIR/$2/$a ]
		then
			echo $a
			convert -resize 50% $a $CURRENT_DIR/$2/$a
		fi
	done
	cd $CURRENT_DIR
}

convert_image texture_interfaces_dxtc_2x/tags texture_interfaces_dxtc/tags
convert_image texture_interfaces_dxtc_2x texture_interfaces_dxtc
convert_image texture_interfaces_v3_2x texture_interfaces_v3


