#!/bin/bash

source /r/scripts/utils.sh
#/r/scripts/pipeline_setup.sh

rm /r/pipeline/export/continents/undernexus/ai_wmap/*
rm /r/pipeline/export/continents/undernexus/ai_wmap_tag/*
rm /r/pipeline/export/continents/undernexus/ig_land*/*
rm /r/pipeline/export/continents/undernexus/ig_elev_land*/*
rm /r/pipeline/export/continents/undernexus/ligo_ig_land/*
rm /r/pipeline/export/continents/undernexus/ligo_zones/*
rm /r/pipeline/export/continents/undernexus/rbank_output/*
rm /r/pipeline/export/continents/undernexus/rbank_smooth/*
rm /r/pipeline/export/continents/undernexus/zone*/*

read