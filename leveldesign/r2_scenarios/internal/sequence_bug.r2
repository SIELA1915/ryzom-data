scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_329]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 24,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_331]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    TextManager = 0,  
    RegionVertex = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    Position = 0,  
    Behavior = 0,  
    Npc = 0,  
    WayPoint = 0,  
    ActivityStep = 0
  },  
  Acts = {
    {
      Cost = 11,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client2_504]],  
        [[Client1_664]],  
        [[Client1_678]],  
        [[Client1_680]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_338]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_336]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              Position = {
                y = -2607.03125,  
                x = 26860.39063,  
                InstanceId = [[Client1_339]],  
                Class = [[Position]],  
                z = -13.90625
              },  
              Angle = -0.3125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_379]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_377]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              Position = {
                y = -2669.9375,  
                x = 26816.375,  
                InstanceId = [[Client2_380]],  
                Class = [[Position]],  
                z = -12.90625
              },  
              Angle = 6.203125,  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_387]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_385]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_546]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_547]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_461]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Weed God]],  
              Position = {
                y = -2631.890625,  
                x = 26896.42188,  
                InstanceId = [[Client2_388]],  
                Class = [[Position]],  
                z = -16.796875
              },  
              ActivitiesId = {
                [[Client2_546]]
              },  
              Angle = -0.421875,  
              Base = [[palette.entities.creatures.chrlf5]],  
              PlayerAttackable = 1
            },  
            {
              InheritPos = 1,  
              Name = [[Niobi_Area]],  
              InstanceId = [[Client2_461]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_460]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_463]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2595.234375,  
                    x = 26931.3125,  
                    InstanceId = [[Client2_464]],  
                    Class = [[Position]],  
                    z = -18.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_466]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2596.859375,  
                    x = 26898.92188,  
                    InstanceId = [[Client2_467]],  
                    Class = [[Position]],  
                    z = -16.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_469]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2638.0625,  
                    x = 26903.54688,  
                    InstanceId = [[Client2_470]],  
                    Class = [[Position]],  
                    z = -17.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_472]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2630.125,  
                    x = 26933.875,  
                    InstanceId = [[Client2_473]],  
                    Class = [[Position]],  
                    z = -18.03125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[People Praising Place 01]],  
              InstanceId = [[Client2_490]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_489]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_492]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2629.828125,  
                    x = 26997.21875,  
                    InstanceId = [[Client2_493]],  
                    Class = [[Position]],  
                    z = -18.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_495]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2637.984375,  
                    x = 26991.53125,  
                    InstanceId = [[Client2_496]],  
                    Class = [[Position]],  
                    z = -20.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_498]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2647.28125,  
                    x = 26983.79688,  
                    InstanceId = [[Client2_499]],  
                    Class = [[Position]],  
                    z = -19.71875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Niobi_Praising_Place]],  
              InstanceId = [[Client1_361]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_363]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2653.875,  
                    x = 26978.98438,  
                    InstanceId = [[Client1_364]],  
                    Class = [[Position]],  
                    z = -18.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_366]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2653.65625,  
                    x = 26979.15625,  
                    InstanceId = [[Client1_367]],  
                    Class = [[Position]],  
                    z = -18.796875
                  }
                }
              },  
              Position = {
                y = 1.34375,  
                x = 0.8125,  
                InstanceId = [[Client1_360]],  
                Class = [[Position]],  
                z = -0.1875
              }
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_373]],  
              ActivitiesId = {
              },  
              HairType = 5624110,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 13,  
              HandsModel = 0,  
              FeetColor = 1,  
              GabaritBreastSize = 2,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 13,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_371]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[summo]],  
                    InstanceId = [[Client1_381]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_382]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Praying]],  
                            InstanceId = [[Client1_383]],  
                            Who = [[Client1_373]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_387]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_389]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_390]],  
                            Who = [[Client1_373]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_391]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[prais]],  
                    InstanceId = [[Client1_407]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_408]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_409]],  
                            Who = [[Client1_373]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_411]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 0,  
              FeetModel = 5617710,  
              Angle = -2.3125,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              SheetClient = [[basic_zorai_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                y = -2627.953125,  
                x = 26999.4375,  
                InstanceId = [[Client1_374]],  
                Class = [[Position]],  
                z = -17.484375
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[Follower of the Weed]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              InstanceId = [[Client1_718]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_716]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]],  
              Position = {
                y = -2503.609375,  
                x = 26970.6875,  
                InstanceId = [[Client1_719]],  
                Class = [[Position]],  
                z = -18.921875
              },  
              Angle = 2.3125,  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Kamis Graveyard]],  
              InstanceId = [[Client1_731]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_730]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_736]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2502.40625,  
                    x = 26969.23438,  
                    InstanceId = [[Client1_737]],  
                    Class = [[Position]],  
                    z = -18.953125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_743]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_741]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 1]],  
              Position = {
                y = -2499.046875,  
                x = 26974.03125,  
                InstanceId = [[Client1_744]],  
                Class = [[Position]],  
                z = -19.03125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_747]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_745]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 1]],  
              Position = {
                y = -2498.3125,  
                x = 26968.625,  
                InstanceId = [[Client1_748]],  
                Class = [[Position]],  
                z = -18.96875
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_751]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_749]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 5 1]],  
              Position = {
                y = -2504.25,  
                x = 26974.875,  
                InstanceId = [[Client1_752]],  
                Class = [[Position]],  
                z = -19.078125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_755]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_753]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 3 1]],  
              Position = {
                y = -2507.3125,  
                x = 26969.89063,  
                InstanceId = [[Client1_756]],  
                Class = [[Position]],  
                z = -18.921875
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_759]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_757]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 4 1]],  
              Position = {
                y = -2499.703125,  
                x = 26971.03125,  
                InstanceId = [[Client1_760]],  
                Class = [[Position]],  
                z = -18.890625
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_763]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_761]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 2]],  
              Position = {
                y = -2502.78125,  
                x = 26965.39063,  
                InstanceId = [[Client1_764]],  
                Class = [[Position]],  
                z = -19.125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client2_502]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_504]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2638.59375,  
                    x = 27003.79688,  
                    InstanceId = [[Client2_505]],  
                    Class = [[Position]],  
                    z = -17.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_507]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2643,  
                    x = 26997.09375,  
                    InstanceId = [[Client2_508]],  
                    Class = [[Position]],  
                    z = -19.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_510]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2651.78125,  
                    x = 27004.8125,  
                    InstanceId = [[Client2_511]],  
                    Class = [[Position]],  
                    z = -18.828125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_501]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_333]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_332]],  
      ManualWeather = 0,  
      Events = {
      }
    },  
    {
      Cost = 21,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[D'ha s'Ploding Kamis]],  
      ActivitiesIds = {
        [[Client1_937]],  
        [[Client1_939]],  
        [[Client2_500]],  
        [[Client2_512]],  
        [[Client2_542]],  
        [[Client2_546]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_706]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_704]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 1]],  
              Position = {
                y = -2490.71875,  
                x = 26955.89063,  
                InstanceId = [[Client1_707]],  
                Class = [[Position]],  
                z = -19.921875
              },  
              Angle = -3,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_710]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_708]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 1]],  
              Position = {
                y = -2493.171875,  
                x = 26955.32813,  
                InstanceId = [[Client1_711]],  
                Class = [[Position]],  
                z = -19.984375
              },  
              Angle = -3,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_714]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_712]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_722]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 20,  
                        InstanceId = [[Client1_723]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_724]],  
                            Who = [[Client1_714]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_727]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 20,  
                        InstanceId = [[Client1_725]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_726]],  
                            Who = [[Client1_714]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_728]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[voice of Jena 1]],  
              Position = {
                y = -2503.6875,  
                x = 26970.78125,  
                InstanceId = [[Client1_715]],  
                Class = [[Position]],  
                z = -18.921875
              },  
              Angle = 2.515625,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_767]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_765]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burning Salamander 1]],  
              Position = {
                y = -2490.484375,  
                x = 26950.59375,  
                InstanceId = [[Client1_768]],  
                Class = [[Position]],  
                z = -20.125
              },  
              Angle = -0.46875,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_771]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_769]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ardent Genius 1]],  
              Position = {
                y = -2496.015625,  
                x = 26947.3125,  
                InstanceId = [[Client1_772]],  
                Class = [[Position]],  
                z = -20.375
              },  
              Angle = -0.046875,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_775]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_773]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Hairy Colossus 1]],  
              Position = {
                y = -2482.390625,  
                x = 26947.84375,  
                InstanceId = [[Client1_776]],  
                Class = [[Position]],  
                z = -19.390625
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_779]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_777]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 2]],  
              Position = {
                y = -2493.53125,  
                x = 26952.0625,  
                InstanceId = [[Client1_780]],  
                Class = [[Position]],  
                z = -20.171875
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_783]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_781]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 1]],  
              Position = {
                y = -2481.078125,  
                x = 26952.5625,  
                InstanceId = [[Client1_784]],  
                Class = [[Position]],  
                z = -19.796875
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_787]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_785]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 2]],  
              Position = {
                y = -2490.53125,  
                x = 26957.76563,  
                InstanceId = [[Client1_788]],  
                Class = [[Position]],  
                z = -19.84375
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_791]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_789]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 3]],  
              Position = {
                y = -2495.28125,  
                x = 26955.07813,  
                InstanceId = [[Client1_792]],  
                Class = [[Position]],  
                z = -19.96875
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_795]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_793]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 4]],  
              Position = {
                y = -2490.328125,  
                x = 26953.8125,  
                InstanceId = [[Client1_796]],  
                Class = [[Position]],  
                z = -20.03125
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_799]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_797]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 5]],  
              Position = {
                y = -2490.21875,  
                x = 26945.21875,  
                InstanceId = [[Client1_800]],  
                Class = [[Position]],  
                z = -20.109375
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_803]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_801]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 6]],  
              Position = {
                y = -2485.8125,  
                x = 26946.25,  
                InstanceId = [[Client1_804]],  
                Class = [[Position]],  
                z = -19.515625
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_807]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_805]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 7]],  
              Position = {
                y = -2486.109375,  
                x = 26951.84375,  
                InstanceId = [[Client1_808]],  
                Class = [[Position]],  
                z = -19.828125
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_811]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_809]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 8]],  
              Position = {
                y = -2497.375,  
                x = 26948.57813,  
                InstanceId = [[Client1_812]],  
                Class = [[Position]],  
                z = -20.359375
              },  
              Angle = 0.0625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_815]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_813]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 3]],  
              Position = {
                y = -2494.359375,  
                x = 26948.21875,  
                InstanceId = [[Client1_816]],  
                Class = [[Position]],  
                z = -20.34375
              },  
              Angle = 0.0625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_819]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_817]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 4]],  
              Position = {
                y = -2497.90625,  
                x = 26954.9375,  
                InstanceId = [[Client1_820]],  
                Class = [[Position]],  
                z = -20
              },  
              Angle = -0.375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_823]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_821]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 5]],  
              Position = {
                y = -2496.59375,  
                x = 26952.5,  
                InstanceId = [[Client1_824]],  
                Class = [[Position]],  
                z = -20.1875
              },  
              Angle = -0.125,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_827]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_825]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 6]],  
              Position = {
                y = -2500.328125,  
                x = 26950.21875,  
                InstanceId = [[Client1_828]],  
                Class = [[Position]],  
                z = -20.28125
              },  
              Angle = 0.265625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_831]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_829]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 7]],  
              Position = {
                y = -2501.03125,  
                x = 26943.75,  
                InstanceId = [[Client1_832]],  
                Class = [[Position]],  
                z = -20.109375
              },  
              Angle = 0.265625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_835]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_833]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 8]],  
              Position = {
                y = -2495.453125,  
                x = 26957.59375,  
                InstanceId = [[Client1_836]],  
                Class = [[Position]],  
                z = -19.75
              },  
              Angle = -0.109375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_703]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_702]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 1,  
        InstanceId = [[Client1_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_385]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh, please.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_386]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh, please.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_387]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh, please. Hear us oh mighty Lord of the Weed.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_388]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oh, please. Hear us oh mighty Lord of the Weed.]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[COME! And let us be with you. Come, let us praise you.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_410]],  
        Class = [[TextManagerEntry]],  
        Text = [[*smoookes* funnnnkyyyy]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_411]],  
        Class = [[TextManagerEntry]],  
        Text = [[*smoookes* funnnnkyyyy]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_727]],  
        Class = [[TextManagerEntry]],  
        Text = [[Kamis x'Ploding here please.]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_728]],  
        Class = [[TextManagerEntry]],  
        Text = [[This the way of Jena, kamis come explode here.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_729]],  
        Class = [[TextManagerEntry]],  
        Text = [[This the way of Jena, kamis come explode here.]]
      }
    },  
    InstanceId = [[Client1_330]]
  }
}