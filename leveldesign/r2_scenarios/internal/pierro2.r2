scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[The Botoga's Drop (Desert 01)]],  
      Season = [[Summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8656174,  
      Name = [[Plot item]],  
      InstanceId = [[Client1_123]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8638510,  
      Name = [[Plot item]],  
      InstanceId = [[Client1_135]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    },  
    {
      SheetId = 8634926,  
      Name = [[Plot item]],  
      InstanceId = [[Client1_136]],  
      Class = [[PlotItem]],  
      Comment = [[]],  
      Desc = [[]]
    }
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    EasterEgg = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LootSpawner = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    BossSpawner = 0,  
    TextManagerEntry = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    DefaultFeature = 0,  
    ChatSequence = 0,  
    ZoneTrigger = 1,  
    NpcCreature = 0,  
    Region = 0,  
    Road = 0,  
    ActivityStep = 1,  
    EventType = 0,  
    NpcCustom = 0,  
    RegionVertex = 0,  
    ActionStep = 1,  
    WayPoint = 0,  
    Position = 0,  
    Location = 1,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    PlotItem = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 4,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_29]],  
              Base = [[palette.entities.botobjects.construction_site]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_30]],  
                x = 21539.51563,  
                y = -1282.828125,  
                z = 74.984375
              },  
              Angle = -2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_27]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[building under construction 1]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_178]],  
              Name = [[Route 1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_180]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_181]],  
                    x = 21513.84375,  
                    y = -1284.921875,  
                    z = 75.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_183]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_184]],  
                    x = 21508.6875,  
                    y = -1289.890625,  
                    z = 76.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_186]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_187]],  
                    x = 21507.23438,  
                    y = -1293.84375,  
                    z = 77.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_189]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_190]],  
                    x = 21510.34375,  
                    y = -1298.8125,  
                    z = 79.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_192]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_193]],  
                    x = 21529.20313,  
                    y = -1304.734375,  
                    z = 76.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_195]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_196]],  
                    x = 21534.59375,  
                    y = -1293.015625,  
                    z = 74.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_198]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_199]],  
                    x = 21521.26563,  
                    y = -1280.375,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_201]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_202]],  
                    x = 21518.39063,  
                    y = -1281.796875,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_177]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_207]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_209]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_210]],  
                    x = 21523.60938,  
                    y = -1293.265625,  
                    z = 75.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_212]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_213]],  
                    x = 21530.75,  
                    y = -1292.03125,  
                    z = 74.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_215]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_216]],  
                    x = 21540.01563,  
                    y = -1299.640625,  
                    z = 75.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_218]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_219]],  
                    x = 21530.10938,  
                    y = -1306.734375,  
                    z = 77.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_221]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_222]],  
                    x = 21518.96875,  
                    y = -1297,  
                    z = 76.9375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_206]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_230]],  
              Base = [[palette.entities.botobjects.tomb_1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_231]],  
                x = 21525.85938,  
                y = -1297.5625,  
                z = 75.734375
              },  
              Angle = 2.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_228]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tomb stone 1]]
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 210,  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 21502.32813,  
                y = -1297.125,  
                z = 78.546875
              },  
              Angle = 1.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 13,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 7,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 27,  
              EyesColor = 4,  
              MorphTarget1 = 5,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 4,  
              MorphTarget6 = 1,  
              MorphTarget7 = 7,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Xarius]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_96]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_97]],  
                x = 21519.28125,  
                y = -1288.0625,  
                z = 75.390625
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_94]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_173]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_176]],  
                        Entity = r2.RefId([[]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_175]],  
                          Type = [[]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_174]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 2,  
              HairType = 2606,  
              HairColor = 3,  
              Tattoo = 23,  
              EyesColor = 2,  
              MorphTarget1 = 4,  
              MorphTarget2 = 0,  
              MorphTarget3 = 4,  
              MorphTarget4 = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              MorphTarget7 = 3,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Pila]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_100]],  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_101]],  
                x = 21515.92188,  
                y = -1291.0625,  
                z = 76.125
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_98]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_115]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_116]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 14,  
              HairType = 5623342,  
              HairColor = 4,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 7,  
              MorphTarget2 = 3,  
              MorphTarget3 = 7,  
              MorphTarget4 = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              JacketModel = 0,  
              TrouserModel = 5614126,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 4,  
              TrouserColor = 5,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Mac'Nary]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_104]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_105]],  
                x = 21520.57813,  
                y = -1291.78125,  
                z = 75.75
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_102]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_223]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_224]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_207]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_226]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_227]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 5,  
              HairType = 5622574,  
              HairColor = 5,  
              Tattoo = 10,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 6,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 4,  
              TrouserColor = 0,  
              FeetColor = 1,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Vicha]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_108]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_109]],  
                x = 21518.71875,  
                y = -1285.8125,  
                z = 75.234375
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_106]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_203]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_205]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_178]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Patrol]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 10,  
              HairType = 8238,  
              HairColor = 1,  
              Tattoo = 20,  
              EyesColor = 0,  
              MorphTarget1 = 0,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 4,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 5617710,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 4,  
              FeetColor = 5,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Steven Seagall]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_112]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_113]],  
                x = 21519.6875,  
                y = -1290.375,  
                z = 75.65625
              },  
              Angle = -2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_110]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 14,  
              HairType = 5422,  
              HairColor = 5,  
              Tattoo = 14,  
              EyesColor = 5,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              MorphTarget7 = 0,  
              MorphTarget8 = 7,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 5609774,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Ciero]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            },  
            {
              Class = [[NpcCreature]],  
              Base = [[palette.entities.creatures.cccdb7]],  
              Angle = -2.698976278,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_137]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_237]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_240]],  
                        Entity = r2.RefId([[]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_239]],  
                          Type = [[]],  
                          Value = r2.RefId([[]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_261]],  
                        Entity = r2.RefId([[]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_260]],  
                          Type = [[]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_238]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_140]],  
                x = 21525.125,  
                y = -1285.125,  
                z = 75.015625
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_139]],  
              Name = [[Goaketh]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_234]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_235]],  
                x = 21525.4375,  
                y = -1294.390625,  
                z = 75.4375
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_232]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_267]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_270]],  
                        Entity = r2.RefId([[]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_269]],  
                          Type = [[]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_268]],  
                      Type = [[desactivation]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 14,  
              HairType = 2606,  
              HairColor = 2,  
              Tattoo = 30,  
              EyesColor = 6,  
              MorphTarget1 = 0,  
              MorphTarget2 = 1,  
              MorphTarget3 = 4,  
              MorphTarget4 = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 3,  
              FeetColor = 3,  
              Speed = 0,  
              Level = 0,  
              Sex = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Decaon]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              PlayerAttackable = 1
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          InstanceId = [[Client1_31]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_59]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_62]],  
                    Entity = r2.RefId([[]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_61]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_60]],  
                  Type = [[On Player Arrived]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            InstanceId = [[Client1_32]]
          },  
          Cyclic = 1,  
          InheritPos = 1,  
          _Zone = [[Client1_35]],  
          Name = [[Zone trigger 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_33]],  
            x = 21499.42188,  
            y = -1296.734375,  
            z = 78.234375
          },  
          Base = [[palette.entities.botobjects.trigger_zone]],  
          Active = 1,  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_35]],  
              Name = [[Places 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_34]],  
                x = -0.5,  
                y = -0.078125,  
                z = -0.328125
              },  
              Deletable = 0,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_37]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_38]],  
                    x = 10.953125,  
                    y = -6.3125,  
                    z = 2
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_40]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_41]],  
                    x = 0.140625,  
                    y = 9.609375,  
                    z = -2.25
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_43]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_44]],  
                    x = -8.03125,  
                    y = 2.140625,  
                    z = -2.015625
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_46]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_47]],  
                    x = 1.484375,  
                    y = -12.34375,  
                    z = 3.546875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              }
            }
          },  
          Class = [[ZoneTrigger]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_54]],  
          Name = [[Dialog 1]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_55]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_56]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_52]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_53]],  
            x = 21503.39063,  
            y = -1298.21875,  
            z = 79
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          InstanceId = [[Client1_117]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_118]]
          },  
          Class = [[LootSpawner]],  
          Npc5Id = r2.RefId([[]]),  
          Npc1Id = r2.RefId([[Client1_100]]),  
          Npc3Id = r2.RefId([[]]),  
          Npc4Id = r2.RefId([[]]),  
          Base = [[palette.entities.botobjects.user_event]],  
          TriggerValue = 0,  
          _Seed = 1146751960,  
          InheritPos = 1,  
          EasterEggId = [[Client1_120]],  
          Npc2Id = r2.RefId([[]]),  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_119]],  
            x = 21515.01563,  
            y = -1293.453125,  
            z = 76.734375
          },  
          Ghosts = {
          },  
          Active = 1,  
          Components = {
            {
              Item2Id = r2.RefId([[]]),  
              Active = 1,  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_121]]
              },  
              Class = [[EasterEgg]],  
              ItemQty = 1,  
              Item1Qty = 1,  
              Item3Qty = 1,  
              Base = [[palette.entities.botobjects.chest_wisdom_std_sel]],  
              Item2Qty = 1,  
              Item1Id = r2.RefId([[Client1_136]]),  
              InheritPos = 0,  
              Name = [[Item Chest 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_122]],  
                x = 21515.71875,  
                y = -1291.1875,  
                z = 76.15625
              },  
              _Seed = 1146751960,  
              InstanceId = [[Client1_120]],  
              Components = {
              },  
              Item3Id = r2.RefId([[]])
            }
          },  
          Name = [[Loot spawner 1]]
        },  
        {
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_142]]
          },  
          Class = [[BossSpawner]],  
          Guard5Id = r2.RefId([[]]),  
          Base = [[palette.entities.botobjects.user_event]],  
          TriggerValue = 0,  
          Guard2Id = r2.RefId([[]]),  
          _Seed = 1146752113,  
          InheritPos = 1,  
          Ghosts = {
          },  
          BossId = r2.RefId([[Client1_139]]),  
          Guard3Id = r2.RefId([[]]),  
          Name = [[Boss Spawner 1]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_143]],  
            x = 21526.26563,  
            y = -1288.4375,  
            z = 75
          },  
          Guard1Id = r2.RefId([[Client1_96]]),  
          Guard4Id = r2.RefId([[]]),  
          Components = {
          },  
          InstanceId = [[Client1_141]]
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_168]],  
          Name = [[Dialog 2]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_169]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_170]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_166]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_167]],  
            x = 21524.73438,  
            y = -1289.1875,  
            z = 75
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        },  
        {
          Class = [[ChatSequence]],  
          InstanceId = [[Client1_243]],  
          Name = [[Dialog 3]],  
          Components = {
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_244]],  
              Time = 0,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_245]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_247]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_248]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_250]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_251]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            },  
            {
              Class = [[ChatStep]],  
              InstanceId = [[Client1_253]],  
              Time = 3,  
              Actions = {
                {
                  Class = [[ChatAction]],  
                  InstanceId = [[Client1_254]],  
                  Emote = [[]],  
                  Who = r2.RefId([[]]),  
                  Facing = r2.RefId([[]]),  
                  Says = [[]]
                }
              },  
              Name = [[]]
            }
          },  
          Type = [[None]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_241]]
          },  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_242]],  
            x = 21525.28125,  
            y = -1292.015625,  
            z = 75
          },  
          Base = [[palette.entities.botobjects.dialog]],  
          Repeating = 0
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_264]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_262]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_263]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 960,  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_265]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 2:Act 2]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_57]],  
        Count = 1,  
        Text = [[Bonjour ami ! derriere moi se trouvent plusieurs personnes, tu devras toutes les tuer afin de faire aparaitre un uber cadeau kipuxe ! Cependant ne te trompe pas et tue les bonnes personnes !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_171]],  
        Count = 1,  
        Text = [[Unleash the Mitch !!!!!! game over !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_246]],  
        Count = 1,  
        Text = [[I cant stop this feeling !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_249]],  
        Count = 1,  
        Text = [[Deep inside of meeeeee]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_252]],  
        Count = 1,  
        Text = [[girl you just don't realise ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_256]],  
        Count = 1,  
        Text = [[What you did to meeeeeee (P.S. kill me plz)]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}