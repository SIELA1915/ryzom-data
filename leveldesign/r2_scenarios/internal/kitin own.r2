scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 21,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 16,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_10]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -2501.59375,  
                x = 23467.29688,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = -5.125
              },  
              Angle = -1,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -2499.84375,  
                x = 23451.57813,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = -5.9375
              },  
              Angle = 2.625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -2492.421875,  
                x = 23444.28125,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = -5.328125
              },  
              Angle = 2.625,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 3]],  
              Position = {
                y = -2475.25,  
                x = 23462.28125,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = -5.046875
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_26]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 4]],  
              Position = {
                y = -2475.046875,  
                x = 23455.23438,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = -4.265625
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 5]],  
              Position = {
                y = -2478.046875,  
                x = 23455.23438,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = -4.515625
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 6]],  
              Position = {
                y = -2486.796875,  
                x = 23445.89063,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = -4.859375
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_38]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_36]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 7]],  
              Position = {
                y = -2471.59375,  
                x = 23457.85938,  
                InstanceId = [[Client1_39]],  
                Class = [[Position]],  
                z = -4.15625
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 8]],  
              Position = {
                y = -2480.390625,  
                x = 23448.28125,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = -4.21875
              },  
              Angle = -2.8125,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_73]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_75]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2461.140625,  
                    x = 23433.20313,  
                    InstanceId = [[Client1_76]],  
                    Class = [[Position]],  
                    z = 5.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2470.609375,  
                    x = 23429.42188,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = 3.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_81]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2493.734375,  
                    x = 23430.40625,  
                    InstanceId = [[Client1_82]],  
                    Class = [[Position]],  
                    z = -4.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_84]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2502.453125,  
                    x = 23433.59375,  
                    InstanceId = [[Client1_85]],  
                    Class = [[Position]],  
                    z = -4.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_87]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2512.484375,  
                    x = 23443.3125,  
                    InstanceId = [[Client1_88]],  
                    Class = [[Position]],  
                    z = -5.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_90]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2527.578125,  
                    x = 23452.85938,  
                    InstanceId = [[Client1_91]],  
                    Class = [[Position]],  
                    z = -12.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_93]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2513.328125,  
                    x = 23453.64063,  
                    InstanceId = [[Client1_94]],  
                    Class = [[Position]],  
                    z = -5.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_96]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2502.046875,  
                    x = 23465.76563,  
                    InstanceId = [[Client1_97]],  
                    Class = [[Position]],  
                    z = -6.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_99]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2492.25,  
                    x = 23456.89063,  
                    InstanceId = [[Client1_100]],  
                    Class = [[Position]],  
                    z = -49.375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_102]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2483.71875,  
                    x = 23449.0625,  
                    InstanceId = [[Client1_103]],  
                    Class = [[Position]],  
                    z = -4.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_105]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2477.921875,  
                    x = 23458.1875,  
                    InstanceId = [[Client1_106]],  
                    Class = [[Position]],  
                    z = -4.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_108]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2484.28125,  
                    x = 23470.98438,  
                    InstanceId = [[Client1_109]],  
                    Class = [[Position]],  
                    z = -6.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_111]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2472.625,  
                    x = 23466.29688,  
                    InstanceId = [[Client1_112]],  
                    Class = [[Position]],  
                    z = -3.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_114]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2465.171875,  
                    x = 23451.67188,  
                    InstanceId = [[Client1_115]],  
                    Class = [[Position]],  
                    z = -1.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_117]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2466.203125,  
                    x = 23439.64063,  
                    InstanceId = [[Client1_118]],  
                    Class = [[Position]],  
                    z = 2.671875
                  }
                }
              },  
              Position = {
                y = -3.734375,  
                x = 4.90625,  
                InstanceId = [[Client1_72]],  
                Class = [[Position]],  
                z = 42.859375
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_166]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_168]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2532.78125,  
                    x = 23435.14063,  
                    InstanceId = [[Client1_169]],  
                    Class = [[Position]],  
                    z = -14.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_171]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2532.78125,  
                    x = 23435.14063,  
                    InstanceId = [[Client1_172]],  
                    Class = [[Position]],  
                    z = -14.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_174]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2536.109375,  
                    x = 23455.21875,  
                    InstanceId = [[Client1_175]],  
                    Class = [[Position]],  
                    z = -14.5
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_177]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2528.359375,  
                    x = 23488.15625,  
                    InstanceId = [[Client1_178]],  
                    Class = [[Position]],  
                    z = -13.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_180]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2464.5,  
                    x = 23490.53125,  
                    InstanceId = [[Client1_181]],  
                    Class = [[Position]],  
                    z = -5.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_183]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2432.65625,  
                    x = 23472.46875,  
                    InstanceId = [[Client1_184]],  
                    Class = [[Position]],  
                    z = -0.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_186]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2441.8125,  
                    x = 23420.5,  
                    InstanceId = [[Client1_187]],  
                    Class = [[Position]],  
                    z = 5.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_189]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2458,  
                    x = 23395.26563,  
                    InstanceId = [[Client1_190]],  
                    Class = [[Position]],  
                    z = 5.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_192]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2492.796875,  
                    x = 23393.14063,  
                    InstanceId = [[Client1_193]],  
                    Class = [[Position]],  
                    z = 5.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_165]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_299]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_301]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2461.390625,  
                    x = 23386.6875,  
                    InstanceId = [[Client1_302]],  
                    Class = [[Position]],  
                    z = 6.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_304]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2465.234375,  
                    x = 23359.4375,  
                    InstanceId = [[Client1_305]],  
                    Class = [[Position]],  
                    z = 8.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_307]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2461.203125,  
                    x = 23347.60938,  
                    InstanceId = [[Client1_308]],  
                    Class = [[Position]],  
                    z = 10.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_310]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2449.671875,  
                    x = 23338.76563,  
                    InstanceId = [[Client1_311]],  
                    Class = [[Position]],  
                    z = 12.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_313]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2504.015625,  
                    x = 23347.42188,  
                    InstanceId = [[Client1_314]],  
                    Class = [[Position]],  
                    z = 7.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_316]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2494.96875,  
                    x = 23356.95313,  
                    InstanceId = [[Client1_317]],  
                    Class = [[Position]],  
                    z = 7.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_319]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2490.9375,  
                    x = 23366.73438,  
                    InstanceId = [[Client1_320]],  
                    Class = [[Position]],  
                    z = 8.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_322]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2497.734375,  
                    x = 23378.54688,  
                    InstanceId = [[Client1_323]],  
                    Class = [[Position]],  
                    z = 5.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_325]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2476.515625,  
                    x = 23379.76563,  
                    InstanceId = [[Client1_326]],  
                    Class = [[Position]],  
                    z = 7.65625
                  }
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_298]],  
                x = -0.609375,  
                y = 2.0625,  
                z = 0.765625
              }
            },  
            {
              InstanceId = [[Client2_21]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_19]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami watchtower 1]],  
              Position = {
                y = -2427.84375,  
                x = 23227.1875,  
                InstanceId = [[Client2_22]],  
                Class = [[Position]],  
                z = 10.140625
              },  
              Angle = 0.421875,  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client2_24]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_26]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2421.890625,  
                    x = 23241.60938,  
                    InstanceId = [[Client2_27]],  
                    Class = [[Position]],  
                    z = 8.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_29]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2433.828125,  
                    x = 23231.57813,  
                    InstanceId = [[Client2_30]],  
                    Class = [[Position]],  
                    z = 11.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_32]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2419.140625,  
                    x = 23223.46875,  
                    InstanceId = [[Client2_33]],  
                    Class = [[Position]],  
                    z = 11.078125
                  }
                }
              },  
              Position = {
                y = 0.671875,  
                x = 2.75,  
                InstanceId = [[Client2_23]],  
                Class = [[Position]],  
                z = -0.3125
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 4]],  
              InstanceId = [[Client1_417]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_419]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2524.765625,  
                    x = 23443.29688,  
                    InstanceId = [[Client1_420]],  
                    Class = [[Position]],  
                    z = -11.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_422]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2528.21875,  
                    x = 23459.54688,  
                    InstanceId = [[Client1_423]],  
                    Class = [[Position]],  
                    z = -11.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_425]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2521.921875,  
                    x = 23487.625,  
                    InstanceId = [[Client1_426]],  
                    Class = [[Position]],  
                    z = -12.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_428]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2526.234375,  
                    x = 23492.78125,  
                    InstanceId = [[Client1_429]],  
                    Class = [[Position]],  
                    z = -12.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_431]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2545.453125,  
                    x = 23473.20313,  
                    InstanceId = [[Client1_432]],  
                    Class = [[Position]],  
                    z = -14.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_434]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2548.890625,  
                    x = 23455.42188,  
                    InstanceId = [[Client1_435]],  
                    Class = [[Position]],  
                    z = -14.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_437]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2545.109375,  
                    x = 23440.51563,  
                    InstanceId = [[Client1_438]],  
                    Class = [[Position]],  
                    z = -15.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_440]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2537.984375,  
                    x = 23429.96875,  
                    InstanceId = [[Client1_441]],  
                    Class = [[Position]],  
                    z = -14.328125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_416]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_451]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_449]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -2597.40625,  
                x = 23444.53125,  
                InstanceId = [[Client1_452]],  
                Class = [[Position]],  
                z = -20.109375
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_455]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_453]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -2593.484375,  
                x = 23434.26563,  
                InstanceId = [[Client1_456]],  
                Class = [[Position]],  
                z = -20.21875
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_459]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_457]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -2599.328125,  
                x = 23438.40625,  
                InstanceId = [[Client1_460]],  
                Class = [[Position]],  
                z = -20.5
              },  
              Angle = 1.65625,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_463]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_461]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              Position = {
                y = -2606.203125,  
                x = 23441.51563,  
                InstanceId = [[Client1_464]],  
                Class = [[Position]],  
                z = -19.765625
              },  
              Angle = 1.6875,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_467]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_465]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -2603.6875,  
                x = 23431.39063,  
                InstanceId = [[Client1_468]],  
                Class = [[Position]],  
                z = -19.734375
              },  
              Angle = 0.859375,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_471]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_469]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -2596.703125,  
                x = 23429.09375,  
                InstanceId = [[Client1_472]],  
                Class = [[Position]],  
                z = -19.984375
              },  
              Angle = 3.328125,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client3_94]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_96]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2585.5625,  
                    x = 23442.84375,  
                    InstanceId = [[Client3_97]],  
                    Class = [[Position]],  
                    z = -18.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_99]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2573.953125,  
                    x = 23446.78125,  
                    InstanceId = [[Client3_100]],  
                    Class = [[Position]],  
                    z = -17.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_102]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2556.71875,  
                    x = 23441.71875,  
                    InstanceId = [[Client3_103]],  
                    Class = [[Position]],  
                    z = -17.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_105]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2553.703125,  
                    x = 23445.84375,  
                    InstanceId = [[Client3_106]],  
                    Class = [[Position]],  
                    z = -15.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_108]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2540.859375,  
                    x = 23456.23438,  
                    InstanceId = [[Client3_109]],  
                    Class = [[Position]],  
                    z = -15.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_111]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2536.359375,  
                    x = 23451,  
                    InstanceId = [[Client3_112]],  
                    Class = [[Position]],  
                    z = -15.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_114]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2524.984375,  
                    x = 23451.96875,  
                    InstanceId = [[Client3_115]],  
                    Class = [[Position]],  
                    z = -11.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_117]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2514.859375,  
                    x = 23450.125,  
                    InstanceId = [[Client3_118]],  
                    Class = [[Position]],  
                    z = -6.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_120]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2505.203125,  
                    x = 23449.53125,  
                    InstanceId = [[Client3_121]],  
                    Class = [[Position]],  
                    z = -5.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_123]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2482.78125,  
                    x = 23449.07813,  
                    InstanceId = [[Client3_124]],  
                    Class = [[Position]],  
                    z = -4.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_126]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2458.421875,  
                    x = 23424.39063,  
                    InstanceId = [[Client3_127]],  
                    Class = [[Position]],  
                    z = 4.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_129]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2461.5,  
                    x = 23408.85938,  
                    InstanceId = [[Client3_130]],  
                    Class = [[Position]],  
                    z = 5.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_132]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2458.921875,  
                    x = 23393.625,  
                    InstanceId = [[Client3_133]],  
                    Class = [[Position]],  
                    z = 5.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client3_135]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2462.734375,  
                    x = 23383.5625,  
                    InstanceId = [[Client3_136]],  
                    Class = [[Position]],  
                    z = 7.296875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client3_93]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      }
    },  
    {
      Cost = 34,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_119]],  
        [[Client1_121]],  
        [[Client1_123]],  
        [[Client1_125]],  
        [[Client1_127]],  
        [[Client1_129]],  
        [[Client1_196]],  
        [[Client1_327]],  
        [[Client1_329]],  
        [[Client1_331]],  
        [[Client1_333]],  
        [[Client1_335]],  
        [[Client1_337]],  
        [[Client2_34]],  
        [[Client1_414]],  
        [[Client1_443]],  
        [[Client1_445]],  
        [[Client1_447]],  
        [[Client3_137]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great  Kipesta]],  
              Position = {
                y = -2506.9375,  
                x = 23491.01563,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = -8.4375
              },  
              Angle = 2.765625,  
              Base = [[palette.entities.creatures.ckjdf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_127]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_128]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruesome Kiban]],  
              Position = {
                y = -2497.640625,  
                x = 23445.65625,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = -5.4375
              },  
              Angle = 2.59375,  
              Base = [[palette.entities.creatures.ckgdf1]],  
              ActivitiesId = {
                [[Client1_127]]
              }
            },  
            {
              InstanceId = [[Client1_54]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_52]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_125]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_126]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Killer  Kiban]],  
              Position = {
                y = -2498.515625,  
                x = 23436.53125,  
                InstanceId = [[Client1_55]],  
                Class = [[Position]],  
                z = -5.015625
              },  
              Angle = 1.578125,  
              Base = [[palette.entities.creatures.ckgdf3]],  
              ActivitiesId = {
                [[Client1_125]]
              }
            },  
            {
              InstanceId = [[Client1_58]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_121]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_122]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Killer  Kiban]],  
              Position = {
                y = -2476.53125,  
                x = 23445.39063,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = -3.0625
              },  
              Angle = -2.796875,  
              Base = [[palette.entities.creatures.ckgdf3]],  
              ActivitiesId = {
                [[Client1_121]]
              }
            },  
            {
              InstanceId = [[Client1_62]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_60]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_119]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_120]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Killer  Kiban]],  
              Position = {
                y = -2482.953125,  
                x = 23437,  
                InstanceId = [[Client1_63]],  
                Class = [[Position]],  
                z = -2.59375
              },  
              Angle = 1.96875,  
              Base = [[palette.entities.creatures.ckgdf3]],  
              ActivitiesId = {
                [[Client1_119]]
              }
            },  
            {
              InstanceId = [[Client1_66]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_64]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_129]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_130]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lacerating  Kiban]],  
              Position = {
                y = -2501.03125,  
                x = 23459.17188,  
                InstanceId = [[Client1_67]],  
                Class = [[Position]],  
                z = -6.4375
              },  
              Angle = 2.6875,  
              Base = [[palette.entities.creatures.ckgdf2]],  
              ActivitiesId = {
                [[Client1_129]]
              }
            },  
            {
              InstanceId = [[Client1_70]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_68]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_123]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_124]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_73]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kibakoo]],  
              Position = {
                y = -2500.859375,  
                x = 23447.71875,  
                InstanceId = [[Client1_71]],  
                Class = [[Position]],  
                z = -5.34375
              },  
              Angle = 2.25,  
              Base = [[palette.entities.creatures.ckgpf7]],  
              ActivitiesId = {
                [[Client1_123]]
              }
            },  
            {
              InstanceId = [[Client1_389]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_387]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_445]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_446]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_417]]),  
                        Name = [[Activity 1 : Wander Place 4 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lacerating Kincher]],  
              Position = {
                y = -2536.015625,  
                x = 23463.5625,  
                InstanceId = [[Client1_390]],  
                Class = [[Position]],  
                z = -13.1875
              },  
              Angle = -3.125,  
              Base = [[palette.entities.creatures.ckddf2]],  
              ActivitiesId = {
                [[Client1_445]]
              }
            },  
            {
              InstanceId = [[Client1_393]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_391]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_443]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_444]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_417]]),  
                        Name = [[Activity 1 : Wander Place 4 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Killer Kincher]],  
              Position = {
                y = -2528.6875,  
                x = 23437.25,  
                InstanceId = [[Client1_394]],  
                Class = [[Position]],  
                z = -12.703125
              },  
              Angle = -0.546875,  
              Base = [[palette.entities.creatures.ckddf3]],  
              ActivitiesId = {
                [[Client1_443]]
              }
            },  
            {
              InstanceId = [[Client1_397]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_395]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_447]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_448]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_417]]),  
                        Name = [[Activity 1 : Wander Place 4 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Killer Kincher]],  
              Position = {
                y = -2538.75,  
                x = 23475.34375,  
                InstanceId = [[Client1_398]],  
                Class = [[Position]],  
                z = -12.421875
              },  
              Angle = -2.625,  
              Base = [[palette.entities.creatures.ckddf3]],  
              ActivitiesId = {
                [[Client1_447]]
              }
            },  
            {
              InstanceId = [[Client1_401]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_399]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_414]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_415]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_442]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_417]]),  
                        Name = [[Activity 2 : Wander Place 4 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Kincher]],  
              Position = {
                y = -2531.140625,  
                x = 23447.34375,  
                InstanceId = [[Client1_402]],  
                Class = [[Position]],  
                z = -14.46875
              },  
              Angle = -1.09375,  
              Base = [[palette.entities.creatures.ckddf4]],  
              ActivitiesId = {
                [[Client1_414]]
              }
            },  
            {
              InstanceId = [[Client1_475]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_473]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Horned Faun 1]],  
              Position = {
                y = -2600.859375,  
                x = 23438.64063,  
                InstanceId = [[Client1_476]],  
                Class = [[Position]],  
                z = -20.4375
              },  
              Angle = 1.09375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_479]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_477]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 4]],  
              Position = {
                y = -2599.390625,  
                x = 23436.48438,  
                InstanceId = [[Client1_480]],  
                Class = [[Position]],  
                z = -20.484375
              },  
              Angle = 0.359375,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_f]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          InstanceId = [[Client1_164]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_163]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_153]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_196]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_197]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_166]]),  
                        Name = [[Activity 1 : Repeat Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Repeat Road]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruesome Kidinak]],  
              Position = {
                y = -2541.5625,  
                x = 23441.9375,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = -16.0625
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf1]],  
              ActivitiesId = {
                [[Client1_196]]
              }
            },  
            {
              InstanceId = [[Client1_149]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Kidinak]],  
              Position = {
                y = -2538,  
                x = 23439.5,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = -16
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruesome Kidinak]],  
              Position = {
                y = -2540.46875,  
                x = 23436.71875,  
                InstanceId = [[Client1_158]],  
                Class = [[Position]],  
                z = -15.609375
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_133]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_131]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Kidinak]],  
              Position = {
                y = -2546.46875,  
                x = 23442.95313,  
                InstanceId = [[Client1_134]],  
                Class = [[Position]],  
                z = -15.453125
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_141]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_139]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Kidinak]],  
              Position = {
                y = -2542.609375,  
                x = 23443.76563,  
                InstanceId = [[Client1_142]],  
                Class = [[Position]],  
                z = -16.09375
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_137]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_135]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Great Kidinak]],  
              Position = {
                y = -2544.84375,  
                x = 23447.67188,  
                InstanceId = [[Client1_138]],  
                Class = [[Position]],  
                z = -15.875
              },  
              Angle = 2.625,  
              Base = [[palette.entities.creatures.ckadf4]],  
              ActivitiesId = {
                [[Client1_194]]
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_162]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client2_18]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 5]],  
          Position = {
            y = -0.390625,  
            x = -0.484375,  
            InstanceId = [[Client2_17]],  
            Class = [[Position]],  
            z = -0.0625
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client2_10]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_8]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_34]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_35]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_24]]),  
                        Name = [[Activity 1 : Wander Place 3 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 2]],  
              Position = {
                y = -2422.375,  
                x = 23240.17188,  
                InstanceId = [[Client2_11]],  
                Class = [[Position]],  
                z = 9.09375
              },  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
                [[Client2_34]]
              }
            },  
            {
              InstanceId = [[Client2_6]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_4]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 1]],  
              Position = {
                y = -2427.984375,  
                x = 23236.65625,  
                InstanceId = [[Client2_7]],  
                Class = [[Position]],  
                z = 10.390625
              },  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_12]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obscure Faun 3]],  
              Position = {
                y = -2421.4375,  
                x = 23231.5,  
                InstanceId = [[Client2_15]],  
                Class = [[Position]],  
                z = 9.265625
              },  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client2_16]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client3_45]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 6]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client3_44]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client3_43]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client3_63]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 7]],  
          Position = {
            y = -0.453125,  
            x = -0.21875,  
            InstanceId = [[Client3_62]],  
            Class = [[Position]],  
            z = -0.09375
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client3_15]],  
              ActivitiesId = {
                [[Client3_137]]
              },  
              HairType = 5604398,  
              TrouserColor = 1,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 2,  
              HandsModel = 5606958,  
              FeetColor = 1,  
              GabaritBreastSize = 5,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 11,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_13]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client3_137]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client3_138]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client3_94]]),  
                        Name = [[Activity 1 : Follow Route Route 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5603886,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 27,  
              MorphTarget3 = 6,  
              MorphTarget7 = 5,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[desert guard 4]],  
              Position = {
                y = -2588.53125,  
                x = 23443.0625,  
                InstanceId = [[Client3_16]],  
                Class = [[Position]],  
                z = -18.828125
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client3_23]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 3,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_21]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 5603886,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 26,  
              MorphTarget3 = 5,  
              MorphTarget7 = 6,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[desert guard 6]],  
              Position = {
                y = -2590.34375,  
                x = 23442.76563,  
                InstanceId = [[Client3_24]],  
                Class = [[Position]],  
                z = -19.265625
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client3_35]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 4,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 2,  
              GabaritHeight = 3,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 13,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_33]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Angle = -1.90625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 19,  
              MorphTarget3 = 2,  
              MorphTarget7 = 3,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[desert guard 9]],  
              Position = {
                y = -2590.140625,  
                x = 23443.65625,  
                InstanceId = [[Client3_36]],  
                Class = [[Position]],  
                z = -19.140625
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client3_19]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 5,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 5,  
              HandsModel = 5606958,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_17]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 22,  
              MorphTarget3 = 5,  
              MorphTarget7 = 6,  
              Sex = 0,  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[desert guard 5]],  
              Position = {
                y = -2590.015625,  
                x = 23441.9375,  
                InstanceId = [[Client3_20]],  
                Class = [[Position]],  
                z = -19.265625
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client3_11]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 3,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 4,  
              HandsColor = 3,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_9]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 5606702,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 7,  
              MorphTarget3 = 6,  
              MorphTarget7 = 5,  
              Sex = 0,  
              WeaponRightHand = 5635886,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[desert guard 3]],  
              Position = {
                y = -2589.078125,  
                x = 23441.46875,  
                InstanceId = [[Client3_12]],  
                Class = [[Position]],  
                z = -19.046875
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client3_31]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 14,  
              HandsModel = 5604142,  
              FeetColor = 0,  
              GabaritBreastSize = 4,  
              GabaritHeight = 12,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_29]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5606702,  
              Angle = -1.90625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 23,  
              MorphTarget3 = 0,  
              MorphTarget7 = 5,  
              Sex = 0,  
              WeaponRightHand = 5635886,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[desert guard 8]],  
              Position = {
                y = -2589.453125,  
                x = 23444.25,  
                InstanceId = [[Client3_32]],  
                Class = [[Position]],  
                z = -18.96875
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client3_7]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 1,  
              GabaritBreastSize = 13,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 13,  
              HandsColor = 2,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_5]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 5603886,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 19,  
              MorphTarget3 = 7,  
              MorphTarget7 = 2,  
              Sex = 1,  
              WeaponRightHand = 5595694,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 5,  
              Name = [[desert guard 2]],  
              Position = {
                y = -2587.171875,  
                x = 23443.5625,  
                InstanceId = [[Client3_8]],  
                Class = [[Position]],  
                z = -18.53125
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client3_3]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 13,  
              HandsModel = 5604142,  
              FeetColor = 3,  
              GabaritBreastSize = 3,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 1,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_1]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 2,  
              FeetModel = 5603886,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 5,  
              MorphTarget3 = 7,  
              MorphTarget7 = 3,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[desert guard 1]],  
              Position = {
                y = -2587.75,  
                x = 23444.29688,  
                InstanceId = [[Client3_4]],  
                Class = [[Position]],  
                z = -18.671875
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client3_27]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 6,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 9,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_25]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 5,  
              FeetModel = 5603886,  
              Angle = -1.90625,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 30,  
              MorphTarget3 = 3,  
              MorphTarget7 = 7,  
              Sex = 0,  
              WeaponRightHand = 5635886,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[desert guard 7]],  
              Position = {
                y = -2588.59375,  
                x = 23444.5,  
                InstanceId = [[Client3_28]],  
                Class = [[Position]],  
                z = -18.828125
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client3_77]],  
              ActivitiesId = {
              },  
              HairType = 5604398,  
              TrouserColor = 1,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 8,  
              HandsModel = 5604142,  
              FeetColor = 2,  
              GabaritBreastSize = 8,  
              GabaritHeight = 5,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 5,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_75]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 28,  
              MorphTarget3 = 7,  
              MorphTarget7 = 4,  
              Sex = 1,  
              WeaponRightHand = 5595950,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[desert guard 12]],  
              Position = {
                y = -2588.3125,  
                x = 23441.65625,  
                InstanceId = [[Client3_78]],  
                Class = [[Position]],  
                z = -18.921875
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client3_73]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 5606958,  
              FeetColor = 4,  
              GabaritBreastSize = 2,  
              GabaritHeight = 10,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5607214,  
              GabaritLegsWidth = 12,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_71]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 5606702,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 11,  
              MorphTarget3 = 0,  
              MorphTarget7 = 1,  
              Sex = 1,  
              WeaponRightHand = 5595182,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 7,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[desert guard 11]],  
              Position = {
                y = -2587.71875,  
                x = 23442.01563,  
                InstanceId = [[Client3_74]],  
                Class = [[Position]],  
                z = -18.78125
              },  
              JacketModel = 5607726,  
              WeaponLeftHand = 0,  
              ArmModel = 5607470,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client3_69]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 5,  
              HandsModel = 5604142,  
              FeetColor = 5,  
              GabaritBreastSize = 4,  
              GabaritHeight = 3,  
              HairColor = 5,  
              EyesColor = 7,  
              TrouserModel = 5604654,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_67]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 5603886,  
              Angle = -0.078125,  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Tattoo = 1,  
              MorphTarget3 = 6,  
              MorphTarget7 = 0,  
              Sex = 0,  
              WeaponRightHand = 5595182,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[desert guard 10]],  
              Position = {
                y = -2587.140625,  
                x = 23442.67188,  
                InstanceId = [[Client3_70]],  
                Class = [[Position]],  
                z = -18.578125
              },  
              JacketModel = 5605166,  
              WeaponLeftHand = 0,  
              ArmModel = 5604910,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client3_61]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_6]],  
      Events = {
      }
    },  
    {
      Cost = 6,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client3_142]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client2_37]]
        },  
        {
          InstanceId = [[Client3_141]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 8]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client3_140]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client3_139]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client2_40]],  
              ActivitiesId = {
                [[Client3_142]]
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 6703150,  
              FeetColor = 5,  
              GabaritBreastSize = 6,  
              GabaritHeight = 2,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 6697774,  
              GabaritLegsWidth = 14,  
              HandsColor = 1,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_38]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client3_142]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client3_143]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_299]]),  
                        Name = [[Activity 1 : Wander Place 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 6702382,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.f_mage_celestial_curser_170]],  
              Tattoo = 4,  
              MorphTarget3 = 5,  
              MorphTarget7 = 4,  
              Sex = 0,  
              WeaponRightHand = 6934318,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 5,  
              Sheet = [[ring_magic_curser_fear_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[desert illusionist 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_41]],  
                x = 23357.60938,  
                y = -2476.125,  
                z = 5.625
              },  
              JacketModel = 6704686,  
              WeaponLeftHand = 0,  
              ArmModel = 6703918,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client2_60]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 4,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 2,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_58]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 6699310,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Tattoo = 0,  
              MorphTarget3 = 1,  
              MorphTarget7 = 1,  
              Sex = 0,  
              WeaponRightHand = 6754862,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              Sheet = [[ring_melee_tank_slash_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[desert warrior 3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_61]],  
                x = 23345.35938,  
                y = -2462.765625,  
                z = 11.234375
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 6773294,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_48]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 2,  
              HandsModel = 6716206,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 4,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6716718,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_46]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6715694,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.m_melee_dd_170]],  
              Tattoo = 15,  
              MorphTarget3 = 6,  
              MorphTarget7 = 4,  
              Sex = 1,  
              WeaponRightHand = 6760750,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              Sheet = [[ring_melee_damage_dealer_pierce_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[forest armsman 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_49]],  
                x = 23373.21875,  
                y = -2464.953125,  
                z = 8.921875
              },  
              JacketModel = 6712622,  
              WeaponLeftHand = 0,  
              ArmModel = 6717230,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client2_56]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 1,  
              HandsModel = 6700078,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 2,  
              HairColor = 1,  
              EyesColor = 2,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_54]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 0,  
              FeetModel = 6699566,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Tattoo = 7,  
              MorphTarget3 = 7,  
              MorphTarget7 = 4,  
              Sex = 1,  
              WeaponRightHand = 6754094,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_tank_slash_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[desert warrior 2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_57]],  
                x = 23349.23438,  
                y = -2494.296875,  
                z = 7.734375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 6773038,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client2_52]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 12,  
              GabaritHeight = 13,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 6700846,  
              GabaritLegsWidth = 14,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_50]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 6699310,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Tattoo = 27,  
              MorphTarget3 = 1,  
              MorphTarget7 = 1,  
              Sex = 0,  
              WeaponRightHand = 6754606,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_melee_tank_slash_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[desert warrior 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_53]],  
                x = 23368.32813,  
                y = -2487.953125,  
                z = 8.109375
              },  
              JacketModel = 6702126,  
              WeaponLeftHand = 6773038,  
              ArmModel = 6701358,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client2_44]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 5,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 1,  
              HairColor = 3,  
              EyesColor = 6,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 2,  
              HandsColor = 2,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_42]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 6702382,  
              Angle = 0.421875,  
              Base = [[palette.entities.npcs.bandits.f_mage_damage_dealer_170]],  
              Tattoo = 16,  
              MorphTarget3 = 4,  
              MorphTarget7 = 0,  
              Sex = 1,  
              WeaponRightHand = 6934318,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_magic_damage_dealer_acid_e2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[desert elementalist 1]],  
              Position = {
                y = -2479.703125,  
                x = 23356.46875,  
                InstanceId = [[Client2_45]],  
                Class = [[Position]],  
                z = 5.890625
              },  
              JacketModel = 6704430,  
              WeaponLeftHand = 0,  
              ArmModel = 6703918,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client2_36]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}