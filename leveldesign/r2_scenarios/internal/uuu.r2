scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    ChatSequence = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    ActivityStep = 1,  
    TimerFeature = 0,  
    NpcCustom = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    Behavior = 0,  
    EventType = 0,  
    ActionStep = 0,  
    LogicEntityReaction = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    Position = 0
  },  
  Acts = {
    {
      Cost = 6,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_279]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_285]],  
                Entity = r2.RefId([[Client1_282]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_284]],  
                  Type = [[Desactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_278]],  
              Type = [[On Scenario Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Title = [[Permanent]],  
      Name = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Version = 2,  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_22]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_23]],  
                x = 22394.39063,  
                y = -1540.71875,  
                z = 84.234375
              },  
              Angle = 2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_20]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_26]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_27]],  
                x = 22388.96875,  
                y = -1543.46875,  
                z = 83.96875
              },  
              Angle = 2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_24]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_30]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_31]],  
                x = 22388.45313,  
                y = -1537.953125,  
                z = 84
              },  
              Angle = 2.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_28]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_38]],  
              Base = [[palette.entities.creatures.chddb2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_39]],  
                x = 22387.3125,  
                y = -1541.46875,  
                z = 83.921875
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_36]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_205]],  
                    LogicEntityAction = [[Client1_202]],  
                    ActionStep = [[Client1_204]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_277]],  
                    LogicEntityAction = [[Client1_274]],  
                    ActionStep = [[Client1_276]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_190]],  
                    Name = [[Pizza]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_191]],  
                        Time = 1,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_192]],  
                            Emote = [[]],  
                            Who = [[Client1_38]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_193]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_212]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_214]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_213]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_216]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_211]],  
                      Type = [[death]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              PlayerAttackable = 0,  
              BotAttackable = 0,  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Paf le yubo]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_282]],  
              Base = [[palette.entities.creatures.ckhdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_283]],  
                x = 22384.32813,  
                y = -1542.890625,  
                z = 83.8125
              },  
              Angle = 1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_280]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_286]],  
                    LogicEntityAction = [[Client1_279]],  
                    ActionStep = [[Client1_285]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_292]],  
                    LogicEntityAction = [[Client1_241]],  
                    ActionStep = [[Client1_291]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_297]],  
                    LogicEntityAction = [[Client1_294]],  
                    ActionStep = [[Client1_296]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Hard Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_317]],  
              Base = [[palette.entities.botobjects.tomb_5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_318]],  
                x = 22400.25,  
                y = -1538.796875,  
                z = 84.84375
              },  
              Angle = 2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_315]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[tombe du precedent barbie yubo]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_325]],  
              Base = [[palette.entities.botobjects.ju_s3_bush_tree]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_326]],  
                x = 22398.51563,  
                y = -1540.234375,  
                z = 84.578125
              },  
              Angle = 2.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_323]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[olao 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_337]],  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_338]],  
                x = 22386.54688,  
                y = -1546.375,  
                z = 84.0625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_335]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo II 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_341]],  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_342]],  
                x = 22386.3125,  
                y = -1547.734375,  
                z = 84.15625
              },  
              Angle = -0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_339]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo II 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_345]],  
              Base = [[palette.entities.botobjects.ju_s3_bamboo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_346]],  
                x = 22385.26563,  
                y = -1547.34375,  
                z = 84.15625
              },  
              Angle = -2.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_343]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[bamboo II 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_349]],  
              Base = [[palette.entities.botobjects.kami_hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_350]],  
                x = 22393.6875,  
                y = -1545.53125,  
                z = 83.8125
              },  
              Angle = 2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_347]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_353]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_354]],  
                x = 22388.65625,  
                y = -1542.859375,  
                z = 83.90625
              },  
              Angle = 2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_351]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_357]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_358]],  
                x = 22394.0625,  
                y = -1540.09375,  
                z = 84.171875
              },  
              Angle = 1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_355]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_365]],  
              Base = [[palette.entities.botobjects.fx_ju_colibrisb]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_366]],  
                x = 22387.3125,  
                y = -1538.984375,  
                z = 83.9375
              },  
              Angle = 2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_363]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[hummingbird 1]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_5]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_294]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_296]],  
                Entity = r2.RefId([[Client1_282]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_295]],  
                  Type = [[Desactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_293]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_80]]
      },  
      Title = [[Act 1]],  
      Name = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Version = 2,  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_42]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_43]],  
                x = 22391.42188,  
                y = -1537.59375,  
                z = 84.21875
              },  
              Angle = 2.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_40]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_182]],  
                    LogicEntityAction = [[Client1_179]],  
                    ActionStep = [[Client1_181]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_64]],  
                    Name = [[Dis]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_65]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_66]],  
                            Emote = [[]],  
                            Who = [[Client1_42]],  
                            Facing = r2.RefId([[Client1_46]]),  
                            Says = [[Client1_272]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_172]],  
                    Name = [[mort]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_173]],  
                        Time = 5,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_174]],  
                            Emote = [[]],  
                            Who = [[Client1_42]],  
                            Facing = r2.RefId([[Client1_46]]),  
                            Says = [[Client1_175]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_166]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_168]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_167]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_68]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_165]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_64]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_202]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_204]],  
                        Entity = r2.RefId([[Client1_38]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_203]],  
                          Type = [[Kill]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_201]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_172]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_80]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_81]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_64]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 3,  
              HairType = 2862,  
              HairColor = 2,  
              Tattoo = 11,  
              EyesColor = 6,  
              MorphTarget1 = 2,  
              MorphTarget2 = 3,  
              MorphTarget3 = 4,  
              MorphTarget4 = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 3,  
              MorphTarget8 = 0,  
              Sex = 0,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 3,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Ken]],  
              ActivitiesId = {
                [[Client1_80]]
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_46]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 22389.3125,  
                y = -1535.078125,  
                z = 84.125
              },  
              Angle = -2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_44]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_169]],  
                    LogicEntityAction = [[Client1_166]],  
                    ActionStep = [[Client1_168]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_215]],  
                    LogicEntityAction = [[Client1_212]],  
                    ActionStep = [[Client1_214]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_68]],  
                    Name = [[ben]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_69]],  
                        Time = 2,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_70]],  
                            Emote = [[]],  
                            Who = [[Client1_46]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_82]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_83]],  
                    Name = [[Veto]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_84]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_85]],  
                            Emote = [[]],  
                            Who = [[Client1_46]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_86]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_216]],  
                    Name = [[ouais]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_217]],  
                        Time = 5,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_218]],  
                            Emote = [[]],  
                            Who = [[Client1_46]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_222]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_148]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_150]],  
                        Entity = r2.RefId([[Client1_46]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_149]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_83]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_147]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_68]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_179]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_181]],  
                        Entity = r2.RefId([[Client1_42]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_180]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_172]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_178]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_83]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 10,  
              HairType = 4910,  
              HairColor = 0,  
              Tattoo = 25,  
              EyesColor = 2,  
              MorphTarget1 = 3,  
              MorphTarget2 = 1,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Barbie va a la plage]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          Secondes = 40,  
          Behavior = {
            InstanceId = [[Client1_253]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Class = [[LogicEntityAction]],  
                InstanceId = [[Client1_256]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Class = [[ActionStep]],  
                    InstanceId = [[Client1_258]],  
                    Entity = r2.RefId([[Client1_14]]),  
                    Action = {
                      Class = [[ActionType]],  
                      InstanceId = [[Client1_257]],  
                      Type = [[Start Act]],  
                      Value = r2.RefId([[]])
                    }
                  }
                },  
                Event = {
                  Class = [[EventType]],  
                  InstanceId = [[Client1_255]],  
                  Type = [[On Trigger]],  
                  Value = r2.RefId([[]])
                },  
                Name = [[]]
              }
            },  
            Reactions = {
            }
          },  
          Class = [[TimerFeature]],  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Passage acte 2]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_254]],  
            x = 22384.20313,  
            y = -1537.8125,  
            z = 83.71875
          },  
          InstanceId = [[Client1_252]],  
          Components = {
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Cyclic = 0
        }
      },  
      Counters = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_9]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_12]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_274]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_276]],  
                Entity = r2.RefId([[Client1_38]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_275]],  
                  Type = [[Desactivate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_273]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
          {
            Class = [[LogicEntityReaction]],  
            InstanceId = [[Client1_259]],  
            LogicEntityAction = [[Client1_256]],  
            ActionStep = [[Client1_258]],  
            Name = [[]]
          }
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_228]]
      },  
      InstanceId = [[Client1_14]],  
      Title = [[Act 2]],  
      Version = 2,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 22390.79688,  
                y = -1537.390625,  
                z = 84.1875
              },  
              Angle = -2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_266]],  
                    LogicEntityAction = [[Client1_241]],  
                    ActionStep = [[Client1_265]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_260]],  
                    Name = [[burne]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_261]],  
                        Time = 4,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_262]],  
                            Emote = [[]],  
                            Who = [[Client1_50]],  
                            Facing = r2.RefId([[Client1_58]]),  
                            Says = [[Client1_263]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_268]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_270]],  
                        Entity = r2.RefId([[Client1_58]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_269]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_248]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_267]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_260]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 1,  
              HairType = 2350,  
              HairColor = 0,  
              Tattoo = 9,  
              EyesColor = 3,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 1,  
              HandsColor = 2,  
              TrouserColor = 0,  
              FeetColor = 0,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Ken]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_54]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_55]],  
                x = 22388.54688,  
                y = -1535.53125,  
                z = 84.078125
              },  
              Angle = -2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_52]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 10,  
              HairType = 5622318,  
              HairColor = 2,  
              Tattoo = 7,  
              EyesColor = 1,  
              MorphTarget1 = 6,  
              MorphTarget2 = 1,  
              MorphTarget3 = 1,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 5,  
              FeetColor = 3,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Barbie va a la plage]],  
              ActivitiesId = {
              },  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_58]],  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_59]],  
                x = 22384.67188,  
                y = -1538.265625,  
                z = 83.765625
              },  
              Angle = -0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_56]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_271]],  
                    LogicEntityAction = [[Client1_268]],  
                    ActionStep = [[Client1_270]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_230]],  
                    Name = [[probl]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_231]],  
                        Time = 5,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_232]],  
                            Emote = [[]],  
                            Who = [[Client1_58]],  
                            Facing = r2.RefId([[Client1_54]]),  
                            Says = [[Client1_233]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_248]],  
                    Name = [[Certe]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_249]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_250]],  
                            Emote = [[]],  
                            Who = [[Client1_58]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_251]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_241]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_265]],  
                        Entity = r2.RefId([[Client1_50]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_264]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_260]])
                        }
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_291]],  
                        Entity = r2.RefId([[Client1_282]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_290]],  
                          Type = [[Activate]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_240]],  
                      Type = [[end of chat sequence]],  
                      Value = r2.RefId([[Client1_230]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_228]],  
                    Name = [[]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_229]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[Client1_230]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 12,  
              GabaritBreastSize = 11,  
              HairType = 9006,  
              HairColor = 1,  
              Tattoo = 0,  
              EyesColor = 1,  
              MorphTarget1 = 3,  
              MorphTarget2 = 0,  
              MorphTarget3 = 6,  
              MorphTarget4 = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 0,  
              TrouserModel = 5618222,  
              FeetModel = 0,  
              HandsModel = 5617966,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 2,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 0,  
              Name = [[Le Barbie Vendeur de yubo]],  
              ActivitiesId = {
                [[Client1_228]]
              },  
              SheetClient = [[basic_zorai_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_15]]
        }
      },  
      Events = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_13]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Name = [[Act 2]],  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_63]],  
        Count = 12,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_67]],  
        Count = 4,  
        Text = [[Dis Barbie je crois que le yubo a bouff� un truc louhe ... ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_71]],  
        Count = 2,  
        Text = [[Oui Ken, j'ai cru comprendre j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_72]],  
        Count = 1,  
        Text = [[Oui Ken, j'ai cru comprendre j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_82]],  
        Count = 4,  
        Text = [[Oui Ken, j'ai cru comprendre ... j'ai march� dans un truc louche aussi ce matin ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_86]],  
        Count = 1,  
        Text = [[Bouge pas j'appelle le Barbie Veto ... *sort son portable rose et compose le numero du barbie veto*]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_175]],  
        Count = 3,  
        Text = [[Ha mon avis vu la tete du yubo, c'est plus un barbie veto qu'il lui faut mais un barbie taxidermiste ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_193]],  
        Count = 1,  
        Text = [[<J'aurais jamais du bouffer la derniere part de pizza tartiflette ... je crois que je vais ...>]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_219]],  
        Count = 1,  
        Text = [[...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_220]],  
        Count = 1,  
        Text = [[...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_221]],  
        Count = 1,  
        Text = [[...\n ha ouais ... \n effectivement il vient de claquer le con ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_222]],  
        Count = 2,  
        Text = [[...ha ouais ... effectivement il vient de claquer le con ... Bon ben faut appeler un barbie vendeur de yubo ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_233]],  
        Count = 4,  
        Text = [[Un probl�me de yubo ? Pas de probleme je vous le remplace immediatement !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_251]],  
        Count = 1,  
        Text = [[Bon okay il est pas tres ressemblant mais bon il est gratuit vous allez pas me casser les noix !]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_263]],  
        Count = 3,  
        Text = [[Heu ... j'crois qu'il y a une burne dans l'potage la quand meme ...]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_272]],  
        Count = 1,  
        Text = [[Dis Barbie je crois que le yubo a bouff� un truc louche ... ]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_301]],  
        Count = 1,  
        Text = [[*prout*]]
      }
    }
  }
}