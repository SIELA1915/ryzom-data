scenario = {
  Class = "Scenario",  
  AccessRules = "",  
  Description = {
    Class = "MapDescription",  
    Title = "tears",  
    ShortDescription = "",  
    OptimalNumberOfPlayer = 5,  
    InstanceId = "Client0_2",  
    LevelId = 0,  
    LocationId = 17,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    MaxPlayers = 5
    ,  Keys = {"Class",  "Title",  "ShortDescription",  "OptimalNumberOfPlayer",  "InstanceId",  "LevelId",  "LocationId",  "EntryPointId",  "MaxEntities",  "RuleId",  "MaxPlayers"}
  },  
  Texts = {
    Class = "TextManager",  
    InstanceId = "Client0_3",  
    Texts = {
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_61",  
        Text = "C'est l'heure de la patrouille !",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_64",  
        Text = "N'allons pas plus loin! On ne sait jamais.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_65",  
        Text = "J'espere que nous ne rencontrerons pas de Kittins !",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_66",  
        Text = "Moi non plus! J'ai horreur de cette vermine !!",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_68",  
        Text = "C'est bizzare.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_69",  
        Text = "Tout a l'air calme, et pourtant...",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_70",  
        Text = "J'ai un mauvais pressentiment.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_72",  
        Text = "Ce n'est rien, rentrons au camp.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_74",  
        Text = "Ha, un bon petit feu pour se r�chauffer!",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_100",  
        Text = "Revenez vite !",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_101",  
        Text = "J'espere qu'il ne leur arrivera rien de facheux !",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_102",  
        Text = "La petite derniere de Martha a dit avoir vu des Kittins pres de la colline, hier.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_103",  
        Text = "Mais a son �ge, on peut confondre un Yubo et un Kittin! ",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_104",  
        Text = "Esperons que tu aies raison! Ce n'est pas le moment d'avoir des ennuis!",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_107",  
        Text = "Alors, vous avez vu quelque chose ?",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_108",  
        Text = "On a rien vu, mais...",  
        Count = 1
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_109",  
        Text = "Je le sens pas.",  
        Count = 2
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_110",  
        Text = "Oui, c'est trop calme depuis trop longtemps...",  
        Count = 1
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_180",  
        Text = "C'est trop calme depuis trop longtemps...",  
        Count = 1
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_205",  
        Text = "Non, rien",  
        Count = 1
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      },  
      {
        Class = "TextManagerEntry",  
        InstanceId = "Client1_206",  
        Text = "On fera un nouveau tour d'ici peu.",  
        Count = 1
        ,  Keys = {"Class",  "InstanceId",  "Text",  "Count"}
      }
      ,  Keys = {}
    }
    ,  Keys = {"Class",  "InstanceId",  "Texts"}
  },  
  InstanceId = "Client0_4",  
  Acts = {
    {
      Class = "Act",  
      Features = {
        {
          Class = "DefaultFeature",  
          Components = {
            {
              Class = "Region",  
              InstanceId = "Client1_131",  
              Name = "",  
              Points = {
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_133",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_135",  
                    x = 21510,  
                    y = -1341,  
                    z = 81
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_136",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_138",  
                    x = 21508.5,  
                    y = -1338.39,  
                    z = 79
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_139",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_141",  
                    x = 21505.5,  
                    y = -1338.39,  
                    z = 79
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_142",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_144",  
                    x = 21504,  
                    y = -1341,  
                    z = 79
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_145",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_147",  
                    x = 21505.5,  
                    y = -1343.59,  
                    z = 81
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_148",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_150",  
                    x = 21508.5,  
                    y = -1343.59,  
                    z = 81
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_132",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_115",  
              Name = "aller",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_117",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_119",  
                    x = 21576,  
                    y = -1345,  
                    z = 0
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_120",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_122",  
                    x = 21510,  
                    y = -1351,  
                    z = 0
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_116",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Base = "palette.geom.road"
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position",  "Base"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_123",  
              Name = "aller",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_125",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_127",  
                    x = 21518.4,  
                    y = -1352.22,  
                    z = 80.5
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_128",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_130",  
                    x = 21577,  
                    y = -1347,  
                    z = 0
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_124",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Base = "palette.geom.road"
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position",  "Base"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_168",  
              Base = "palette.entities.botobjects.fires.fire4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_169",  
                x = 21584,  
                y = -1347,  
                z = 95
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_167",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Le feu"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Region",  
              InstanceId = "Client1_181",  
              Name = "",  
              Points = {
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_183",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_185",  
                    x = 21589,  
                    y = -1345,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_186",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_188",  
                    x = 21587.5,  
                    y = -1342.39,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_189",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_191",  
                    x = 21584.5,  
                    y = -1342.39,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_192",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_194",  
                    x = 21583,  
                    y = -1345,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_195",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_197",  
                    x = 21584.5,  
                    y = -1347.59,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_198",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_200",  
                    x = 21587.5,  
                    y = -1347.59,  
                    z = 95
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_182",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_208",  
              Base = "palette.entities.botobjects.totem_bird",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_210",  
                x = 21516,  
                y = -1344.45,  
                z = 80.9219
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.21875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_207",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "bird totem 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_220",  
              Base = "palette.entities.botobjects.tent_cosmetics",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_222",  
                x = 21580.5,  
                y = -1352.2,  
                z = 96.2188
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_219",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "cosmetics tent 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_224",  
              Base = "palette.entities.botobjects.tent_zorai",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_226",  
                x = 21590.8,  
                y = -1352.77,  
                z = 96.8125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.23438,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_223",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "zorai tent 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_228",  
              Base = "palette.entities.botobjects.tent_zorai",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_230",  
                x = 21594.5,  
                y = -1344.61,  
                z = 95.3438
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 3.53125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_227",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "zorai tent 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_232",  
              Base = "palette.entities.botobjects.tent_cosmetics",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_234",  
                x = 21589.5,  
                y = -1338.14,  
                z = 94.7031
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 7.34375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_231",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "cosmetics tent 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_236",  
              Base = "palette.entities.botobjects.chest_old",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_238",  
                x = 21592.5,  
                y = -1338.56,  
                z = 94.5781
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.92188,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_235",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "old chest 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_240",  
              Base = "palette.entities.botobjects.chest_old",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_242",  
                x = 21577.7,  
                y = -1350.7,  
                z = 95.7188
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.54688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_239",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "old chest 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_244",  
              Base = "palette.entities.botobjects.chest_old",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_246",  
                x = 21578.8,  
                y = -1349.44,  
                z = 95.8594
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 3.42188,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_243",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "old chest 3"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_248",  
              Base = "palette.entities.botobjects.pack_3",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_250",  
                x = 21585.5,  
                y = -1356.3,  
                z = 97
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.71875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_247",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "pack 3 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_252",  
              Base = "palette.entities.botobjects.pack_5",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_254",  
                x = 21581.3,  
                y = -1339.03,  
                z = 94.8906
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -3,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_251",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "pack 5 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_256",  
              Base = "palette.entities.botobjects.pack_2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_258",  
                x = 21579.3,  
                y = -1342.16,  
                z = 95.2344
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.65625,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_255",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "pack 2 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_260",  
              Base = "palette.entities.botobjects.street_lamp",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_262",  
                x = 21580.1,  
                y = -1349.03,  
                z = 96.0156
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -3,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_259",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "street lamp 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_264",  
              Base = "palette.entities.botobjects.street_lamp",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_266",  
                x = 21583.8,  
                y = -1352.67,  
                z = 96.5469
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.921875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_263",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "street lamp 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_268",  
              Base = "palette.entities.botobjects.street_lamp",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_270",  
                x = 21586.2,  
                y = -1338.56,  
                z = 94.8594
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.26563,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_267",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "street lamp 3"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_272",  
              Base = "palette.entities.botobjects.street_lamp",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_274",  
                x = 21590.4,  
                y = -1341.33,  
                z = 95.1719
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.26563,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_271",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "street lamp 4"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Region",  
              InstanceId = "Client1_287",  
              Name = "yubo feed zone",  
              Points = {
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_289",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_291",  
                    x = 21575.1,  
                    y = -1351.88,  
                    z = 95.1719
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_292",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_294",  
                    x = 21585.8,  
                    y = -1361.31,  
                    z = 97.5313
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_295",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_297",  
                    x = 21600.9,  
                    y = -1351.3,  
                    z = 96.2188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_298",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_300",  
                    x = 21607.4,  
                    y = -1379.11,  
                    z = 104.844
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_301",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_303",  
                    x = 21601.9,  
                    y = -1388.47,  
                    z = 106.188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_304",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_306",  
                    x = 21589.9,  
                    y = -1385.53,  
                    z = 103.234
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_307",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_309",  
                    x = 21557.7,  
                    y = -1357.86,  
                    z = 87.9688
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_310",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_312",  
                    x = 21573.9,  
                    y = -1339.63,  
                    z = 93.8281
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_288",  
                x = 1.375,  
                y = -0.859375,  
                z = 0.359375
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Region",  
              InstanceId = "Client1_343",  
              Name = "kipee zone",  
              Points = {
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_345",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_347",  
                    x = 21483.3,  
                    y = -1263.89,  
                    z = 62.3281
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_348",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_350",  
                    x = 21495.4,  
                    y = -1280.22,  
                    z = 61.6875
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_351",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_353",  
                    x = 21506.1,  
                    y = -1307.63,  
                    z = 68.625
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_354",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_356",  
                    x = 21503.3,  
                    y = -1323.53,  
                    z = 73.0781
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_357",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_359",  
                    x = 21482.2,  
                    y = -1334.91,  
                    z = 71.0625
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_360",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_362",  
                    x = 21455.5,  
                    y = -1338,  
                    z = 68.1719
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_363",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_365",  
                    x = 21447.9,  
                    y = -1327.42,  
                    z = 64.6719
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_366",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_368",  
                    x = 21356.8,  
                    y = -1374.39,  
                    z = 60.875
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_369",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_371",  
                    x = 21324.1,  
                    y = -1322.8,  
                    z = 57.5469
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_372",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_374",  
                    x = 21332.4,  
                    y = -1202.48,  
                    z = 47.8906
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_375",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_377",  
                    x = 21432.8,  
                    y = -1188.77,  
                    z = 60.0469
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_378",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_380",  
                    x = 21464.8,  
                    y = -1220.44,  
                    z = 59.9531
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_381",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_383",  
                    x = 21474,  
                    y = -1239.19,  
                    z = 64.75
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_344",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_413",  
              Base = "palette.entities.botobjects.spot_kitin",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_415",  
                x = 21320.6,  
                y = -1395.17,  
                z = 59.6563
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_412",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin mound 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_417",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_419",  
                x = 21338.7,  
                y = -1396.34,  
                z = 57.0781
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_416",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 1"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_421",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_423",  
                x = 21316.9,  
                y = -1381.72,  
                z = 59.125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_420",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_425",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_427",  
                x = 21320.1,  
                y = -1378.3,  
                z = 58.5313
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_424",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 3"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_429",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_431",  
                x = 21343.3,  
                y = -1396.58,  
                z = 56.6875
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_428",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 4"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_433",  
              Base = "palette.entities.botobjects.spot_kitin",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_435",  
                x = 21711.5,  
                y = -1109.3,  
                z = 58.8438
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_432",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin mound 2"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_437",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_439",  
                x = 21703.5,  
                y = -1096.67,  
                z = 58.4531
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_436",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 5"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_441",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_443",  
                x = 21701.7,  
                y = -1102.58,  
                z = 58.6563
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_440",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 6"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_445",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_447",  
                x = 21718.2,  
                y = -1116.95,  
                z = 59.9531
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_444",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 7"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_449",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_451",  
                x = 21713.6,  
                y = -1119.81,  
                z = 60.1094
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.84375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_448",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 8"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_500",  
              Name = "act 2 attack route",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_502",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_504",  
                    x = 21680,  
                    y = -1125.03,  
                    z = 60.3281
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_505",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_507",  
                    x = 21657.3,  
                    y = -1121.91,  
                    z = 60.1563
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_508",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_510",  
                    x = 21630.5,  
                    y = -1140.83,  
                    z = 60.0938
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_511",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_513",  
                    x = 21584.5,  
                    y = -1131.5,  
                    z = 61.25
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_514",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_516",  
                    x = 21571.1,  
                    y = -1141.64,  
                    z = 65.6406
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_517",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_519",  
                    x = 21571.2,  
                    y = -1185.02,  
                    z = 63.1406
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_520",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_522",  
                    x = 21588.6,  
                    y = -1229.98,  
                    z = 59.9531
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_523",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_525",  
                    x = 21582.1,  
                    y = -1325.83,  
                    z = 91.2813
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_526",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_528",  
                    x = 21585.5,  
                    y = -1342.02,  
                    z = 95.5
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_529",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_531",  
                    x = 21585.7,  
                    y = -1344.14,  
                    z = 95.8125
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_501",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_533",  
              Base = "palette.entities.botobjects.spot_kitin",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_535",  
                x = 21324.9,  
                y = -1005.03,  
                z = 44.4063
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_532",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin mound 3"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_537",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_539",  
                x = 21321.7,  
                y = -1018.09,  
                z = 42.9063
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_536",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 9"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_541",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_543",  
                x = 21321.5,  
                y = -1022.27,  
                z = 42.8125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_540",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 10"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_545",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_547",  
                x = 21343,  
                y = -995.656,  
                z = 44.375
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.92188,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_544",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 11"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_549",  
              Base = "palette.entities.botobjects.kitin_egg",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_551",  
                x = 21346.2,  
                y = -995.703,  
                z = 44.5313
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.92188,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_548",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "kitin egg 12"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_608",  
              Name = "Road 1",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_610",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_612",  
                    x = 21359.1,  
                    y = -1392.63,  
                    z = 58.2188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_613",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_615",  
                    x = 21407.5,  
                    y = -1395.09,  
                    z = 62.5
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_616",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_618",  
                    x = 21474.3,  
                    y = -1391.59,  
                    z = 90.1563
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_619",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_621",  
                    x = 21498.9,  
                    y = -1393.84,  
                    z = 90.7188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_622",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_624",  
                    x = 21589.7,  
                    y = -1362.55,  
                    z = 98.3594
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_625",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_627",  
                    x = 21600.9,  
                    y = -1356.23,  
                    z = 97.625
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_628",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_630",  
                    x = 21602,  
                    y = -1341.31,  
                    z = 94.2813
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_631",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_633",  
                    x = 21584.9,  
                    y = -1330.44,  
                    z = 92.9844
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_634",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_636",  
                    x = 21583.8,  
                    y = -1336.69,  
                    z = 94.4844
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_637",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_639",  
                    x = 21586.1,  
                    y = -1342.45,  
                    z = 95.5469
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_609",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_640",  
              Name = "Road 2",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_642",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_644",  
                    x = 21338.4,  
                    y = -1351.09,  
                    z = 60.4219
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_645",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_647",  
                    x = 21366,  
                    y = -1316.81,  
                    z = 59.2813
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_648",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_650",  
                    x = 21412.1,  
                    y = -1273.19,  
                    z = 60.0938
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_651",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_653",  
                    x = 21457.5,  
                    y = -1248.92,  
                    z = 62.0781
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_654",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_656",  
                    x = 21475.4,  
                    y = -1257.14,  
                    z = 64.9063
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_657",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_659",  
                    x = 21498.3,  
                    y = -1277.56,  
                    z = 61.3594
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_660",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_662",  
                    x = 21572.1,  
                    y = -1339.73,  
                    z = 93.1875
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_663",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_665",  
                    x = 21580.8,  
                    y = -1347.36,  
                    z = 96.0156
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_666",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_668",  
                    x = 21583.7,  
                    y = -1343.22,  
                    z = 95.7031
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_641",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Road",  
              InstanceId = "Client1_669",  
              Name = "Road 3",  
              Points = {
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_671",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_673",  
                    x = 21352,  
                    y = -1372.78,  
                    z = 61.1563
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_674",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_676",  
                    x = 21365.3,  
                    y = -1386.7,  
                    z = 59.6875
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_677",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_679",  
                    x = 21361.5,  
                    y = -1373.23,  
                    z = 60.7188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_680",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_682",  
                    x = 21368.4,  
                    y = -1384.81,  
                    z = 59.9375
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_683",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_685",  
                    x = 21365.5,  
                    y = -1372.53,  
                    z = 60.5313
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_686",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_688",  
                    x = 21350,  
                    y = -1371.59,  
                    z = 61.2188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_689",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_691",  
                    x = 21349.3,  
                    y = -1383.03,  
                    z = 60.2188
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_692",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_694",  
                    x = 21367.6,  
                    y = -1386.48,  
                    z = 59.7969
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_695",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_697",  
                    x = 21373,  
                    y = -1378.2,  
                    z = 60.1094
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_698",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_700",  
                    x = 21370.2,  
                    y = -1372.88,  
                    z = 60.25
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_701",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_703",  
                    x = 21384.9,  
                    y = -1382.83,  
                    z = 60.3281
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_704",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_706",  
                    x = 21385.9,  
                    y = -1372.81,  
                    z = 60.3594
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_707",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_709",  
                    x = 21391.7,  
                    y = -1365.25,  
                    z = 60.4375
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_710",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_712",  
                    x = 21420.2,  
                    y = -1349.83,  
                    z = 60.3906
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_713",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_715",  
                    x = 21456,  
                    y = -1349.69,  
                    z = 70.9688
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_716",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_718",  
                    x = 21528.9,  
                    y = -1358.84,  
                    z = 78.6563
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_719",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_721",  
                    x = 21564.4,  
                    y = -1354,  
                    z = 90.75
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_722",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_724",  
                    x = 21575.7,  
                    y = -1346.2,  
                    z = 95.1719
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "WayPoint",  
                  InstanceId = "Client1_725",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_727",  
                    x = 21579.9,  
                    y = -1347.06,  
                    z = 95.9063
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_670",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Region",  
              InstanceId = "Client1_728",  
              Name = "Region 1",  
              Points = {
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_730",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_732",  
                    x = 21591.8,  
                    y = -1347.73,  
                    z = 95.9844
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_733",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_735",  
                    x = 21585.5,  
                    y = -1352.2,  
                    z = 96.6094
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_736",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_738",  
                    x = 21580.8,  
                    y = -1348.67,  
                    z = 96.0469
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_739",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_741",  
                    x = 21580.2,  
                    y = -1343.97,  
                    z = 95.5781
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_742",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_744",  
                    x = 21581.8,  
                    y = -1341.67,  
                    z = 95.3594
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_745",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_747",  
                    x = 21584.5,  
                    y = -1340.67,  
                    z = 95.2656
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_748",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_750",  
                    x = 21586.3,  
                    y = -1341.25,  
                    z = 95.3438
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                },  
                {
                  Class = "RegionVertex",  
                  InstanceId = "Client1_751",  
                  Position = {
                    Class = "Position",  
                    InstanceId = "Client1_753",  
                    x = 21589.8,  
                    y = -1342.58,  
                    z = 95.3906
                    ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
                  },  
                  InheritPos = 1
                  ,  Keys = {"Class",  "InstanceId",  "Position",  "InheritPos"}
                }
                ,  Keys = {}
              },  
              InheritPos = 1,  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_729",  
                x = 0,  
                y = 0,  
                z = 0
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              }
              ,  Keys = {"Class",  "InstanceId",  "Name",  "Points",  "InheritPos",  "Position"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_755",  
              Base = "palette.entities.creatures.chdfb1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_757",  
                x = 21585.5,  
                y = -1350.27,  
                z = 96.4219
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.79688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_754",  
                Type = "wander",  
                ZoneId = "Client1_728",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "fido"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_759",  
              Base = "palette.entities.creatures.chdfb3",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_761",  
                x = 21590.3,  
                y = -1366.63,  
                z = 99.2344
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_758",  
                Type = "wander",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Scampering Yubo"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_763",  
              Base = "palette.entities.creatures.chdfb3",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_765",  
                x = 21589.1,  
                y = -1369.08,  
                z = 99.4375
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_762",  
                Type = "wander",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Scampering Yubo"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          InstanceId = "Client1_112"
          ,  Keys = {"Class",  "Components",  "InstanceId"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_151",  
          Name = "guards1",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_154",  
              Base = "palette.entities.npcs.cuthroats.cuthroat_b_melee_a_f_f",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_155",  
                x = 21585.3,  
                y = -1347,  
                z = 95
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 3.14063,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_153",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                  {
                    Class = "ActivitySequence",  
                    InstanceId = "Client1_170",  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_171",  
                        Activity = "Stand Still",  
                        ActivityZoneId = "",  
                        TimeLimit = "Few Sec",  
                        TimeLimitValue = "3",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_61"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 3
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      },  
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_172",  
                        Activity = "Follow Route",  
                        ActivityZoneId = "Client1_115",  
                        TimeLimit = "No Limit",  
                        TimeLimitValue = "",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "sally",  
                                  Says = "Client1_65"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 1
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_66"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                          "Client1_173"
                          ,  Keys = {}
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      },  
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_174",  
                        Activity = "Deploy",  
                        ActivityZoneId = "Client1_131",  
                        TimeLimit = "Few Sec",  
                        TimeLimitValue = "20",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_68"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 1
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_69"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_70"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      },  
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_175",  
                        Activity = "Follow Route",  
                        ActivityZoneId = "Client1_123",  
                        TimeLimit = "No Limit",  
                        TimeLimitValue = "",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_72"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 1
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                          "Client1_176"
                          ,  Keys = {}
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      },  
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_177",  
                        Activity = "Stand Still",  
                        ActivityZoneId = "",  
                        TimeLimit = "Few Sec",  
                        TimeLimitValue = "30",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_109"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 5
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_180"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                          "Client1_178",  
                          "Client1_179"
                          ,  Keys = {}
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      }
                      ,  Keys = {}
                    }
                    ,  Keys = {"Class",  "InstanceId",  "Repeating",  "Components"}
                  }
                  ,  Keys = {}
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "billy"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_157",  
              Base = "palette.entities.npcs.cuthroats.cuthroat_b_melee_a_f_f",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_158",  
                x = 21582.7,  
                y = -1347,  
                z = 95
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 6.28125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_156",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "sally"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_152",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_159",  
          Name = "guards1",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_162",  
              Base = "palette.entities.npcs.cuthroats.cuthroat_b_melee_a_f_f",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_163",  
                x = 21587.3,  
                y = -1345,  
                z = 95
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 3.14063,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_161",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                  {
                    Class = "ActivitySequence",  
                    InstanceId = "Client1_201",  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_202",  
                        Activity = "Wander",  
                        ActivityZoneId = "Client1_181",  
                        TimeLimit = "No Limit",  
                        TimeLimitValue = "",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "bobby",  
                                  Says = "Client1_100"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "judy",  
                                  Says = "Client1_101"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 3
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "bobby",  
                                  Says = "Client1_102"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 4
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "bobby",  
                                  Says = "Client1_103"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 3
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "bobby",  
                                  Says = "Client1_104"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 3
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      }
                      ,  Keys = {}
                    }
                    ,  Keys = {"Class",  "InstanceId",  "Repeating",  "Components"}
                  },  
                  {
                    Class = "ActivitySequence",  
                    InstanceId = "Client1_203",  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = "ActivityStep",  
                        InstanceId = "Client1_204",  
                        Activity = "Wander",  
                        ActivityZoneId = "Client1_181",  
                        TimeLimit = "No Limit",  
                        TimeLimitValue = "",  
                        Chat = {
                          Class = "ChatSequence",  
                          Type = "Non Repeating",  
                          Components = {
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "bobby",  
                                  Says = "Client1_107"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 1
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "sally",  
                                  Says = "Client1_205"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 2
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            },  
                            {
                              Class = "ChatStep",  
                              Actions = {
                                {
                                  Class = "ChatStep",  
                                  Actions = {
                                  },  
                                  Time = 0,  
                                  Emote = "",  
                                  Who = "billy",  
                                  Says = "Client1_206"
                                  ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
                                }
                                ,  Keys = {}
                              },  
                              Time = 3
                              ,  Keys = {"Class",  "Actions",  "Time"}
                            }
                            ,  Keys = {}
                          }
                          ,  Keys = {"Class",  "Type",  "Components"}
                        },  
                        EventsIds = {
                        }
                        ,  Keys = {"Class",  "InstanceId",  "Activity",  "ActivityZoneId",  "TimeLimit",  "TimeLimitValue",  "Chat",  "EventsIds"}
                      }
                      ,  Keys = {}
                    }
                    ,  Keys = {"Class",  "InstanceId",  "Repeating",  "Components"}
                  }
                  ,  Keys = {}
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "bobby"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_165",  
              Base = "palette.entities.npcs.cuthroats.cuthroat_b_melee_a_f_f",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_166",  
                x = 21584.7,  
                y = -1345,  
                z = 95
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 6.28125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_164",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "judy"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_160",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_774",  
          Name = "Group 6",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_771",  
              Base = "palette.entities.creatures.chcfb1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_773",  
                x = 21582.4,  
                y = -1368.89,  
                z = 97.3438
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_770",  
                Type = "wander",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Timorous Capryni"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_767",  
              Base = "palette.entities.creatures.chcfb1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_769",  
                x = 21580.1,  
                y = -1368.95,  
                z = 96.625
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_766",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Timorous Capryni"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_775",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_800",  
          Name = "Group 7",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_793",  
              Base = "palette.entities.creatures.chdfb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_795",  
                x = 21602.5,  
                y = -1385.59,  
                z = 105.734
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.53125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_792",  
                Type = "wander",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Roaming Yubo"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_797",  
              Base = "palette.entities.creatures.chdfb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_799",  
                x = 21600.2,  
                y = -1386.52,  
                z = 105.719
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.53125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_796",  
                Type = "",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Roaming Yubo"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_789",  
              Base = "palette.entities.creatures.chdfb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_791",  
                x = 21601.7,  
                y = -1387,  
                z = 105.938
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 2.53125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_788",  
                Type = "",  
                ZoneId = "Client1_287",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Roaming Yubo"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_801",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        }
        ,  Keys = {}
      },  
      Events = {
        {
          Class = "Event",  
          InstanceId = "Client1_173",  
          EventMode = "Only Once",  
          Condition = {
            Class = "EventCondition",  
            SourceType = "Entity or Group",  
            Type = "At Destination"
            ,  Keys = {"Class",  "SourceType",  "Type"}
          },  
          Activities = {
          },  
          Parameter = "",  
          Action = {
            Class = "ChatStep",  
            Actions = {
              {
                Class = "ChatStep",  
                Actions = {
                },  
                Time = 0,  
                Emote = "",  
                Who = "billy",  
                Says = "Client1_64"
                ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
              }
              ,  Keys = {}
            },  
            Time = 0
            ,  Keys = {"Class",  "Actions",  "Time"}
          }
          ,  Keys = {"Class",  "InstanceId",  "EventMode",  "Condition",  "Activities",  "Parameter",  "Action"}
        },  
        {
          Class = "Event",  
          InstanceId = "Client1_176",  
          EventMode = "Only Once",  
          Condition = {
            Class = "EventCondition",  
            SourceType = "Entity or Group",  
            Type = "At Destination"
            ,  Keys = {"Class",  "SourceType",  "Type"}
          },  
          Activities = {
          },  
          Parameter = "",  
          Action = {
            Class = "ChatStep",  
            Actions = {
              {
                Class = "ChatStep",  
                Actions = {
                },  
                Time = 0,  
                Emote = "",  
                Who = "sally",  
                Says = "Client1_74"
                ,  Keys = {"Class",  "Actions",  "Time",  "Emote",  "Who",  "Says"}
              }
              ,  Keys = {}
            },  
            Time = 0
            ,  Keys = {"Class",  "Actions",  "Time"}
          }
          ,  Keys = {"Class",  "InstanceId",  "EventMode",  "Condition",  "Activities",  "Parameter",  "Action"}
        },  
        {
          Class = "Event",  
          InstanceId = "Client1_178",  
          EventMode = "Only Once",  
          Condition = {
            Class = "EventCondition",  
            SourceType = "Entity or Group",  
            Type = "At Destination"
            ,  Keys = {"Class",  "SourceType",  "Type"}
          },  
          Activities = {
          },  
          Parameter = "",  
          Action = {
            Class = "ChatStep",  
            Actions = {
              {
                Class = "ScriptAction",  
                Who = "Client1_159",  
                Action = "Set Activity",  
                Parameter = "2"
                ,  Keys = {"Class",  "Who",  "Action",  "Parameter"}
              },  
              {
                Class = "ScriptAction",  
                Who = "",  
                Action = "Sit Down",  
                Parameter = ""
                ,  Keys = {"Class",  "Who",  "Action",  "Parameter"}
              }
              ,  Keys = {}
            },  
            Time = 0
            ,  Keys = {"Class",  "Actions",  "Time"}
          }
          ,  Keys = {"Class",  "InstanceId",  "EventMode",  "Condition",  "Activities",  "Parameter",  "Action"}
        },  
        {
          Class = "Event",  
          InstanceId = "Client1_179",  
          EventMode = "Only Once",  
          Condition = {
            Class = "EventCondition",  
            SourceType = "Entity or Group",  
            Type = "Leaves State",  
            ConditionParameter = "5"
            ,  Keys = {"Class",  "SourceType",  "Type",  "ConditionParameter"}
          },  
          Activities = {
          },  
          Parameter = "",  
          Action = {
            Class = "ChatStep",  
            Actions = {
              {
                Class = "ScriptAction",  
                Who = "",  
                Action = "Stand Up",  
                Parameter = ""
                ,  Keys = {"Class",  "Who",  "Action",  "Parameter"}
              }
              ,  Keys = {}
            },  
            Time = 0
            ,  Keys = {"Class",  "Actions",  "Time"}
          }
          ,  Keys = {"Class",  "InstanceId",  "EventMode",  "Condition",  "Activities",  "Parameter",  "Action"}
        }
        ,  Keys = {}
      },  
      PVP = 0,  
      Slider1 = 0,  
      Slider2 = 0,  
      ComboBox1 = 0,  
      ComboBox2 = 0,  
      Title = "",  
      Text1 = "",  
      Text2 = 0,  
      Cost = 43,  
      ActivitiesIds = {
        "Client1_170",  
        "Client1_201",  
        "Client1_203",  
        "Client1_203"
        ,  Keys = {}
      },  
      Counters = {
      },  
      InstanceId = "Client1_111"
      ,  Keys = {"Class",  "Features",  "Events",  "PVP",  "Slider1",  "Slider2",  "ComboBox1",  "ComboBox2",  "Title",  "Text1",  "Text2",  "Cost",  "ActivitiesIds",  "Counters",  "InstanceId"}
    },  
    {
      Class = "Act",  
      Features = {
        {
          Class = "DefaultFeature",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_276",  
              Base = "palette.entities.creatures.chjfb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_278",  
                x = 21588.5,  
                y = -1341.34,  
                z = 95.2969
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -1.92188,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_275",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Mektoub Packer"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_280",  
              Base = "palette.entities.creatures.chjfb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_282",  
                x = 21587.2,  
                y = -1340.56,  
                z = 95.2188
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 4.29688,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_279",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Mektoub Packer"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_284",  
              Base = "palette.entities.creatures.chifb2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_286",  
                x = 21580.6,  
                y = -1350.92,  
                z = 96.1719
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.90625,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_283",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Mektoub Mount"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_385",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_387",  
                x = 21420.3,  
                y = -1268.17,  
                z = 60.3125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_384",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_389",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_391",  
                x = 21412.7,  
                y = -1260.64,  
                z = 59.8438
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_388",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_393",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_395",  
                x = 21424,  
                y = -1256.59,  
                z = 60.0313
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_392",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_397",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_399",  
                x = 21432.9,  
                y = -1262.27,  
                z = 60.1094
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_396",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_401",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_403",  
                x = 21396.9,  
                y = -1245.91,  
                z = 59.1875
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_400",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_405",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_407",  
                x = 21396.2,  
                y = -1270.69,  
                z = 59.8281
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_404",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_409",  
              Base = "palette.entities.creatures.ckhfb4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_411",  
                x = 21393,  
                y = -1257.75,  
                z = 60.7344
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.1875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_408",  
                Type = "wander",  
                ZoneId = "Client1_343",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Hard Kipee"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          InstanceId = "Client1_114"
          ,  Keys = {"Class",  "Components",  "InstanceId"}
        }
        ,  Keys = {}
      },  
      Events = {
      },  
      PVP = 0,  
      Slider1 = 0,  
      Slider2 = 0,  
      ComboBox1 = 0,  
      ComboBox2 = 0,  
      Title = "Act 1",  
      Text1 = "",  
      Text2 = 0,  
      Cost = 10,  
      ActivitiesIds = {
      },  
      Counters = {
      },  
      InstanceId = "Client1_113"
      ,  Keys = {"Class",  "Features",  "Events",  "PVP",  "Slider1",  "Slider2",  "ComboBox1",  "ComboBox2",  "Title",  "Text1",  "Text2",  "Cost",  "ActivitiesIds",  "Counters",  "InstanceId"}
    },  
    {
      Class = "Act",  
      Features = {
        {
          Class = "DefaultFeature",  
          Components = {
          },  
          InstanceId = "Client1_453"
          ,  Keys = {"Class",  "Components",  "InstanceId"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_498",  
          Name = "Group 2",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_455",  
              Base = "palette.entities.creatures.ckgff5",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_457",  
                x = 21692.1,  
                y = -1118.13,  
                z = 60.0625
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_454",  
                Type = "follow_route",  
                ZoneId = "Client1_500",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Naka"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_491",  
              Base = "palette.entities.creatures.ckgff3",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_493",  
                x = 21704.7,  
                y = -1125.05,  
                z = 60.1094
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_490",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Killer  Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_459",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_461",  
                x = 21694.5,  
                y = -1124.78,  
                z = 60.1563
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_458",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_463",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_465",  
                x = 21694.9,  
                y = -1129,  
                z = 60.2969
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_462",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_479",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_481",  
                x = 21686.4,  
                y = -1125.34,  
                z = 60.2188
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_478",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_487",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_489",  
                x = 21684.5,  
                y = -1129.56,  
                z = 60.5313
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_486",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_475",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_477",  
                x = 21684.6,  
                y = -1121.08,  
                z = 60.125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_474",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_471",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_473",  
                x = 21683.7,  
                y = -1116.14,  
                z = 59.9844
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_470",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_467",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_469",  
                x = 21690,  
                y = -1110.94,  
                z = 59.625
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_466",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_495",  
              Base = "palette.entities.creatures.ckgff3",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_497",  
                x = 21695.6,  
                y = -1104.91,  
                z = 59.5781
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_494",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Killer  Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_483",  
              Base = "palette.entities.creatures.ckgff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_485",  
                x = 21680.1,  
                y = -1120.91,  
                z = 60.125
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_482",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kiban"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_499",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        }
        ,  Keys = {}
      },  
      Events = {
      },  
      PVP = 0,  
      Slider1 = 0,  
      Slider2 = 0,  
      ComboBox1 = 0,  
      ComboBox2 = 0,  
      Title = "Act 2",  
      Text1 = "",  
      Text2 = 0,  
      Cost = 11,  
      ActivitiesIds = {
      },  
      Counters = {
      },  
      InstanceId = "Client1_452",  
      States = {
      }
      ,  Keys = {"Class",  "Features",  "Events",  "PVP",  "Slider1",  "Slider2",  "ComboBox1",  "ComboBox2",  "Title",  "Text1",  "Text2",  "Cost",  "ActivitiesIds",  "Counters",  "InstanceId",  "States"}
    },  
    {
      Class = "Act",  
      Features = {
        {
          Class = "DefaultFeature",  
          Components = {
          },  
          InstanceId = "Client1_553"
          ,  Keys = {"Class",  "Components",  "InstanceId"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_586",  
          Name = "Group 3",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_583",  
              Base = "palette.entities.creatures.ckjff4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_585",  
                x = 21329.3,  
                y = -1355.69,  
                z = 61.0469
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.890625,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_582",  
                Type = "follow_route",  
                ZoneId = "Client1_640",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Great  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_579",  
              Base = "palette.entities.creatures.ckjff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_581",  
                x = 21327.5,  
                y = -1359.98,  
                z = 60.6719
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.625,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_578",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_575",  
              Base = "palette.entities.creatures.ckjff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_577",  
                x = 21324.3,  
                y = -1357.5,  
                z = 61.2969
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_574",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_571",  
              Base = "palette.entities.creatures.ckjff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_573",  
                x = 21324,  
                y = -1361.33,  
                z = 60.7344
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_570",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_567",  
              Base = "palette.entities.creatures.ckjff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_569",  
                x = 21324.5,  
                y = -1365.97,  
                z = 59.7969
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_566",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_587",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_588",  
          Name = "Group 4",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_555",  
              Base = "palette.entities.creatures.ckiff7",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_557",  
                x = 21341.2,  
                y = -1371.11,  
                z = 61.0156
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_554",  
                Type = "follow_route",  
                ZoneId = "Client1_669",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Kizokin"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_559",  
              Base = "palette.entities.creatures.ckiff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_561",  
                x = 21346.2,  
                y = -1366.61,  
                z = 61.0469
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_558",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kizoar"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_563",  
              Base = "palette.entities.creatures.ckiff2",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_565",  
                x = 21336.7,  
                y = -1370.91,  
                z = 60.5469
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 1.60938,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_562",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Lacerating  Kizoar"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_589",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        },  
        {
          Class = "NpcGrpFeature",  
          InstanceId = "Client1_606",  
          Name = "Group 5",  
          Components = {
            {
              Class = "Npc",  
              InstanceId = "Client1_591",  
              Base = "palette.entities.creatures.ckjff4",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_593",  
                x = 21349,  
                y = -1398.23,  
                z = 56.0938
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.796875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_590",  
                Type = "follow_route",  
                ZoneId = "Client1_608",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Great  Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_595",  
              Base = "palette.entities.creatures.ckjff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_597",  
                x = 21351.3,  
                y = -1386.77,  
                z = 59.1563
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.796875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_594",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_599",  
              Base = "palette.entities.creatures.ckjff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_601",  
                x = 21342.3,  
                y = -1400.38,  
                z = 58.7344
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.796875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_598",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            },  
            {
              Class = "Npc",  
              InstanceId = "Client1_603",  
              Base = "palette.entities.creatures.ckjff1",  
              Position = {
                Class = "Position",  
                InstanceId = "Client1_605",  
                x = 21355.5,  
                y = -1385.2,  
                z = 59.6875
                ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
              },  
              Angle = 0.796875,  
              Behavior = {
                Class = "Behavior",  
                InstanceId = "Client1_602",  
                Type = "",  
                ZoneId = "",  
                Activities = {
                }
                ,  Keys = {"Class",  "InstanceId",  "Type",  "ZoneId",  "Activities"}
              },  
              InheritPos = 1,  
              Name = "Gruesome Kipesta"
              ,  Keys = {"Class",  "InstanceId",  "Base",  "Position",  "Angle",  "Behavior",  "InheritPos",  "Name"}
            }
            ,  Keys = {}
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = "Position",  
            InstanceId = "Client1_607",  
            x = 0,  
            y = 0,  
            z = 0
            ,  Keys = {"Class",  "InstanceId",  "x",  "y",  "z"}
          }
          ,  Keys = {"Class",  "InstanceId",  "Name",  "Components",  "ActivitiesId",  "Cost",  "InheritPos",  "Position"}
        }
        ,  Keys = {}
      },  
      Events = {
      },  
      PVP = 0,  
      Slider1 = 0,  
      Slider2 = 0,  
      ComboBox1 = 0,  
      ComboBox2 = 0,  
      Title = "Act 3",  
      Text1 = "",  
      Text2 = 0,  
      Cost = 12,  
      ActivitiesIds = {
      },  
      Counters = {
      },  
      InstanceId = "Client1_552",  
      States = {
      }
      ,  Keys = {"Class",  "Features",  "Events",  "PVP",  "Slider1",  "Slider2",  "ComboBox1",  "ComboBox2",  "Title",  "Text1",  "Text2",  "Cost",  "ActivitiesIds",  "Counters",  "InstanceId",  "States"}
    }
    ,  Keys = {}
  }
  ,  Keys = {"Class",  "AccessRules",  "Description",  "Texts",  "InstanceId",  "Acts"}
}