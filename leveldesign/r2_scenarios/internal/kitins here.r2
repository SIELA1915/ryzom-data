scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 22,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    EventType = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    Npc = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    ChatSequence = 0,  
    Road = 0,  
    ActionStep = 0,  
    NpcCustom = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    Position = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Road]],  
              InstanceId = [[Client1_17]],  
              Name = [[Route 1]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_16]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_20]],  
                    x = 25059.89063,  
                    y = -2650.875,  
                    z = -2.28125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_22]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_23]],  
                    x = 25067.07813,  
                    y = -2647.953125,  
                    z = -3.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_25]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_26]],  
                    x = 25081.8125,  
                    y = -2642.703125,  
                    z = -5.4375
                  },  
                  InheritPos = 1
                }
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 14,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      Title = [[Act 1]],  
      ActivitiesIds = {
        [[Client1_27]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_10]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_11]],  
                x = 25058.32813,  
                y = -2652.875,  
                z = -1.265625
              },  
              Angle = 2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_8]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_36]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_38]],  
                        Entity = r2.RefId([[Client1_14]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_37]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_29]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_35]],  
                      Type = [[end of activity step]],  
                      Value = r2.RefId([[Client1_28]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_27]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_28]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 4,  
              HairType = 2606,  
              HairColor = 2,  
              Tattoo = 20,  
              EyesColor = 3,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 1,  
              MorphTarget4 = 6,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 3,  
              Sex = 0,  
              JacketModel = 5607726,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5604142,  
              ArmModel = 5607470,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 4,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5595182,  
              Name = [[desert guard 1]],  
              ActivitiesId = {
                [[Client1_27]]
              },  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.npcs.guards.m_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 25055.28125,  
                y = -2653.765625,  
                z = -1.84375
              },  
              Angle = 2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_29]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_30]],  
                        Time = 3,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_31]],  
                            Emote = [[]],  
                            Who = [[Client1_14]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_33]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 5,  
              HairType = 5422,  
              HairColor = 2,  
              Tattoo = 27,  
              EyesColor = 6,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 5,  
              MorphTarget4 = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5607982,  
              HandsModel = 5608238,  
              ArmModel = 5611566,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 3,  
              TrouserColor = 0,  
              FeetColor = 2,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 5597742,  
              Name = [[forest guard 1]],  
              ActivitiesId = {
              },  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_49]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_50]],  
                x = 25059.70313,  
                y = -2645.171875,  
                z = -6.234375
              },  
              Angle = 1.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_47]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_53]],  
              Base = [[palette.entities.creatures.ckhib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_54]],  
                x = 25066.89063,  
                y = -2650.015625,  
                z = -1.984375
              },  
              Angle = 1.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_51]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipee]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 25063.57813,  
                y = -2649.328125,  
                z = -2.78125
              },  
              Angle = 1.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.creatures.ckbib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 25056.03125,  
                y = -2649.5625,  
                z = -4.296875
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.creatures.ckdib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 25057.53125,  
                y = -2649.109375,  
                z = -4.28125
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_69]],  
              Base = [[palette.entities.creatures.ckeib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_70]],  
                x = 25063.75,  
                y = -2647.390625,  
                z = -4.3125
              },  
              Angle = 1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_67]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_73]],  
              Base = [[palette.entities.creatures.ckeib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_74]],  
                x = 25056.34375,  
                y = -2648.578125,  
                z = -4.78125
              },  
              Angle = 1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_71]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_77]],  
              Base = [[palette.entities.creatures.ckjib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_78]],  
                x = 25056,  
                y = -2646.71875,  
                z = -5.765625
              },  
              Angle = 2.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_75]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kipesta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_81]],  
              Base = [[palette.entities.creatures.ckiib4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_82]],  
                x = 25067.71875,  
                y = -2648.734375,  
                z = -2.828125
              },  
              Angle = 2.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_79]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Trooper Kizoar]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_85]],  
              Base = [[palette.entities.creatures.ckfrb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_86]],  
                x = 25066.98438,  
                y = -2648.28125,  
                z = -3.203125
              },  
              Angle = 2.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_83]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_89]],  
              Base = [[palette.entities.creatures.ckfrb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_90]],  
                x = 25053.375,  
                y = -2642.859375,  
                z = -5.875
              },  
              Angle = 2.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_87]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_93]],  
              Base = [[palette.entities.creatures.ckfrb3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_94]],  
                x = 25050.5625,  
                y = -2647.109375,  
                z = -5.21875
              },  
              Angle = 2.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_91]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Assault Kirosta]]
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 1,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      InstanceId = [[Client1_39]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
        [[Client1_45]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_43]],  
              Base = [[palette.entities.creatures.ckfrb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_44]],  
                x = 25054.1875,  
                y = -2654.828125,  
                z = -1.6875
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_41]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_45]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_46]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Assault Kirosta]],  
              ActivitiesId = {
                [[Client1_45]]
              }
            }
          },  
          InstanceId = [[Client1_40]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 1,  
      Events = {
      },  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_32]],  
        Count = 1,  
        Text = [[blablablabla]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_33]],  
        Count = 1,  
        Text = [[blablablabla]]
      }
    }
  }
}