scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_160]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_162]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_164]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_163]],  
      ManualWeather = 0,  
      WeatherValue = 0
    },  
    {
      Cost = 11,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_169]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_170]],  
                x = 30627.34375,  
                y = -2297.5,  
                z = 77.546875
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_173]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_174]],  
                x = 30614.95313,  
                y = -2280.921875,  
                z = 71.625
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_177]],  
              Base = [[palette.entities.creatures.chhfd3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_178]],  
                x = 30612.10938,  
                y = -2295.328125,  
                z = 73.75
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Rooting Mektoub]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_181]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_182]],  
                x = 30612.75,  
                y = -2296.0625,  
                z = 73.90625
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_185]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_186]],  
                x = 30621.32813,  
                y = -2287.859375,  
                z = 73.0625
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_183]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_189]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_190]],  
                x = 30627.23438,  
                y = -2302.421875,  
                z = 79.078125
              },  
              Angle = -0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_187]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_193]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_194]],  
                x = 30633.46875,  
                y = -2298.453125,  
                z = 77.609375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_191]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_197]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_198]],  
                x = 30631.125,  
                y = -2285.015625,  
                z = 72.609375
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_195]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_201]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_202]],  
                x = 30641.01563,  
                y = -2297.84375,  
                z = 75.5625
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_199]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_205]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_206]],  
                x = 30609.14063,  
                y = -2288.890625,  
                z = 72.6875
              },  
              Angle = 0.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_203]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_209]],  
              Base = [[palette.entities.creatures.chafd4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_210]],  
                x = 30614.42188,  
                y = -2302.984375,  
                z = 75.328125
              },  
              Angle = -3.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_207]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Scrounging Arma]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_166]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_165]],  
      ManualWeather = 0,  
      WeatherValue = 0
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_161]],  
    Texts = {
    }
  },  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  }
}