scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_298]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Forest21]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Passage of the Pit (Forests 21)]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_289]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_287]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_288]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 1,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_285]]
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    Npc = 0,  
    MapDescription = 0,  
    NpcGrpFeature = 1,  
    TextManager = 0,  
    Position = 0,  
    Location = 1,  
    NpcCustom = 0,  
    LogicEntityBehavior = 1,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_292]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_290]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_291]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.campfire_out]],  
              Angle = -3.129592657,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_299]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 26549.71094,  
                y = -12020.03711,  
                z = -21.44784164,  
                InstanceId = [[Client1_302]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_301]],  
              Name = [[dead camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.campfire_off]],  
              Angle = -3.129592657,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_303]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 26552.71289,  
                y = -12019.84473,  
                z = -21.27827835,  
                InstanceId = [[Client1_306]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_305]],  
              Name = [[dead camp fire 2]]
            }
          },  
          InstanceId = [[Client1_293]]
        }
      },  
      ManualWeather = 0,  
      LocationId = [[]]
    },  
    {
      InstanceId = [[Client1_296]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_294]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Version = 5,  
      Name = [[Act 1:Act 1]],  
      WeatherValue = 644,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_295]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_297]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Angle = -3.129592657,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_307]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 26552.0332,  
                y = -12022.61426,  
                z = -21.30405426,  
                InstanceId = [[Client1_310]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_309]],  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 1,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 2,  
              HairColor = 0,  
              Tattoo = 28,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 4,  
              MorphTarget3 = 5,  
              MorphTarget4 = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              HairType = 2862,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              JacketColor = 4,  
              TrouserColor = 0,  
              FeetColor = 2,  
              HandsColor = 1,  
              ArmColor = 5,  
              SheetClient = [[basic_fyros_female.creature]],  
              Name = [[Gaxius]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Angle = -3.129592657,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_311]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 26555.75195,  
                y = -12020.71973,  
                z = -21.14987373,  
                InstanceId = [[Client1_314]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_313]],  
              GabaritHeight = 7,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 9,  
              HairColor = 0,  
              Tattoo = 12,  
              EyesColor = 2,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              MorphTarget7 = 2,  
              MorphTarget8 = 3,  
              Sex = 0,  
              HairType = 4910,  
              JacketModel = 0,  
              TrouserModel = 5610030,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              JacketColor = 5,  
              TrouserColor = 2,  
              FeetColor = 0,  
              HandsColor = 1,  
              ArmColor = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Name = [[Gisse]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          Behavior = {
            Class = [[Behavior]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            InstanceId = [[Client1_315]]
          },  
          ActivitiesId = {
          },  
          Position = {
            Class = [[Position]],  
            x = 1.837890625,  
            y = 0.0908203125,  
            z = 0.06645965576,  
            InstanceId = [[Client1_316]]
          },  
          InheritPos = 1,  
          InstanceId = [[Client1_317]]
        }
      },  
      ManualWeather = 1,  
      LocationId = [[Client1_298]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_286]],  
    Texts = {
    }
  }
}