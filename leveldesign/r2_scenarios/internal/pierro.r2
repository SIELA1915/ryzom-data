scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[The Botoga's Drop (Desert 01)]],  
      Season = [[Summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2MiddleEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_4]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    Npc = 0,  
    MapDescription = 0,  
    TextManager = 0,  
    Position = 0,  
    Location = 1,  
    NpcCreature = 0,  
    LogicEntityBehavior = 1,  
    DefaultFeature = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 4,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_7]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_29]],  
              Base = [[palette.entities.botobjects.construction_site]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_30]],  
                x = 21537.64063,  
                y = -1281.59375,  
                z = 75
              },  
              Angle = -2.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_27]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[building under construction 1]]
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_11]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 210,  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCreature]],  
              Base = [[palette.entities.creatures.chcdb7]],  
              Angle = -2.777380705,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_15]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18]],  
                x = 21522.96875,  
                y = -1285.9375,  
                z = 75.09375
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_17]],  
              Name = [[Capryketh]]
            },  
            {
              Class = [[NpcCreature]],  
              Base = [[palette.entities.creatures.ckhdb4]],  
              Angle = 3.065368652,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_19]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_22]],  
                x = 21529.34375,  
                y = -1294.09375,  
                z = 75
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_21]],  
              Name = [[Hard Kipee]]
            },  
            {
              Class = [[NpcCreature]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Angle = -2.889505625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_23]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_26]],  
                x = 21512.01563,  
                y = -1284.015625,  
                z = 75.203125
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_25]],  
              Name = [[Assault Kirosta]]
            }
          },  
          InstanceId = [[Client1_13]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}