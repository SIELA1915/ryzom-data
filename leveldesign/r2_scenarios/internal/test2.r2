scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_24]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_26]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    Position = 0,  
    Region = 0,  
    NpcCustom = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Behavior = {
        InstanceId = [[Client1_27]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_28]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_342]],  
              Base = [[palette.entities.botobjects.counter]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_343]],  
                x = 22451.375,  
                y = -1258.65625,  
                z = 75.46875
              },  
              Angle = -1.591921806,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_340]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[counter 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_346]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_347]],  
                x = 22450.70313,  
                y = -1252.546875,  
                z = 75.984375
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_344]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[hut 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_350]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_351]],  
                x = 22453.39063,  
                y = -1256.453125,  
                z = 75.546875
              },  
              Angle = -0.796875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_348]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[3 jars 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_354]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_355]],  
                x = 22453.53125,  
                y = -1262.796875,  
                z = 75.046875
              },  
              Angle = 1.725033402,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_352]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_358]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_359]],  
                x = 22445.32813,  
                y = -1255.0625,  
                z = 75.984375
              },  
              Angle = -1.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_356]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[chariot 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_362]],  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_363]],  
                x = 22453.46875,  
                y = -1254.78125,  
                z = 75.671875
              },  
              Angle = -0.9231961966,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_360]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fallen jar 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_366]],  
              Base = [[palette.entities.botobjects.chest_old]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_367]],  
                x = 22453.15625,  
                y = -1250.921875,  
                z = 75.828125
              },  
              Angle = 0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_364]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[old chest 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_370]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_371]],  
                x = 22447.23438,  
                y = -1255.296875,  
                z = 75.921875
              },  
              Angle = -1.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_368]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[street lamp 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_378]],  
              Base = [[palette.entities.botobjects.merchant_armor_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_379]],  
                x = 22455.65625,  
                y = -1255.328125,  
                z = 75.5625
              },  
              Angle = -1.656910658,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_376]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros armor sign 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_382]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_383]],  
                x = 22458.84375,  
                y = -1269.921875,  
                z = 75.140625
              },  
              Angle = 2.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_380]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_386]],  
              Base = [[palette.entities.botobjects.totem_pachyderm]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_387]],  
                x = 22461.10938,  
                y = -1265.28125,  
                z = 75.484375
              },  
              Angle = 3.128487587,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_384]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[pachyderm totem 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_390]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_391]],  
                x = 22454.4375,  
                y = -1244.53125,  
                z = 75.9375
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_388]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_394]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_395]],  
                x = 22454.48438,  
                y = -1271.53125,  
                z = 74.671875
              },  
              Angle = 2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_392]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[street lamp 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_398]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_399]],  
                x = 22455.78125,  
                y = -1266.234375,  
                z = 74.953125
              },  
              Angle = 2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_396]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[street lamp 3]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_402]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_403]],  
                x = 22455.8125,  
                y = -1263.203125,  
                z = 75.078125
              },  
              Angle = -3.297535658,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_400]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 11,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 3,  
              HairType = 2606,  
              HairColor = 2,  
              Tattoo = 21,  
              EyesColor = 2,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 3,  
              MorphTarget4 = 6,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              MorphTarget7 = 6,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              BotAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_406]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_407]],  
                x = 22454.73438,  
                y = -1261.21875,  
                z = 75.171875
              },  
              Angle = -2.686670303,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_404]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 5,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 8,  
              EyesColor = 4,  
              MorphTarget1 = 5,  
              MorphTarget2 = 2,  
              MorphTarget3 = 7,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 6,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 2]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              BotAttackable = 1
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_410]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_411]],  
                x = 22451.4375,  
                y = -1257.390625,  
                z = 75.5625
              },  
              Angle = -1.899428368,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_408]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 8,  
              GabaritLegsWidth = 13,  
              GabaritBreastSize = 9,  
              HairType = 2606,  
              HairColor = 3,  
              Tattoo = 5,  
              EyesColor = 6,  
              MorphTarget1 = 6,  
              MorphTarget2 = 6,  
              MorphTarget3 = 4,  
              MorphTarget4 = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 3,  
              MorphTarget7 = 7,  
              MorphTarget8 = 1,  
              Sex = 0,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Popy]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b3.creature]],  
              BotAttackable = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_413]],  
              Name = [[Campement Fyros]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_415]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_416]],  
                    x = 22442.07813,  
                    y = -1264.4375,  
                    z = 74.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_418]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_419]],  
                    x = 22447.75,  
                    y = -1273.53125,  
                    z = 73.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_421]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_422]],  
                    x = 22458.84375,  
                    y = -1278.53125,  
                    z = 75.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_424]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_425]],  
                    x = 22465.53125,  
                    y = -1274,  
                    z = 76.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_427]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_428]],  
                    x = 22465.6875,  
                    y = -1262.9375,  
                    z = 76.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_430]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_431]],  
                    x = 22459.51563,  
                    y = -1241.796875,  
                    z = 76.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_433]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_434]],  
                    x = 22447.26563,  
                    y = -1240.078125,  
                    z = 76.484375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_436]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_437]],  
                    x = 22440.45313,  
                    y = -1250.859375,  
                    z = 76.140625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_412]],  
                x = 21.953125,  
                y = -39.109375,  
                z = 4.453125
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_440]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_441]],  
                x = 22440.6875,  
                y = -1249.59375,  
                z = 76.125
              },  
              Angle = 2.614859819,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_438]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_444]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_445]],  
                x = 22442.4375,  
                y = -1246.6875,  
                z = 76.15625
              },  
              Angle = 2.603889942,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_448]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_449]],  
                x = 22444.21875,  
                y = -1243.75,  
                z = 76.234375
              },  
              Angle = 2.603890181,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_446]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_452]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_453]],  
                x = 22446.01563,  
                y = -1240.84375,  
                z = 76.3125
              },  
              Angle = 2.583609819,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_450]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_456]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_457]],  
                x = 22440,  
                y = -1252.640625,  
                z = 75.90625
              },  
              Angle = -3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 5]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_460]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_461]],  
                x = 22440.45313,  
                y = -1255.890625,  
                z = 75.5625
              },  
              Angle = -2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 6]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_464]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_465]],  
                x = 22440.92188,  
                y = -1259.15625,  
                z = 75.078125
              },  
              Angle = -3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 7]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_468]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_469]],  
                x = 22441.35938,  
                y = -1262.46875,  
                z = 74.53125
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_466]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 8]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_472]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_473]],  
                x = 22448.53125,  
                y = -1239.625,  
                z = 76.484375
              },  
              Angle = 1.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_470]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 9]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_476]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_477]],  
                x = 22451.79688,  
                y = -1240.03125,  
                z = 76.4375
              },  
              Angle = 1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_474]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 10]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_480]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_481]],  
                x = 22455.125,  
                y = -1240.5625,  
                z = 76.34375
              },  
              Angle = 1.40625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_478]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 11]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_484]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_485]],  
                x = 22458.40625,  
                y = -1241.15625,  
                z = 76.3125
              },  
              Angle = 1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_482]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 12]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_488]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_489]],  
                x = 22460.4375,  
                y = -1243.03125,  
                z = 76.140625
              },  
              Angle = 0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_486]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 13]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_492]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_493]],  
                x = 22461.35938,  
                y = -1246.15625,  
                z = 75.890625
              },  
              Angle = 0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_490]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 14]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_496]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_497]],  
                x = 22462.28125,  
                y = -1249.34375,  
                z = 75.9375
              },  
              Angle = 0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_494]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 15]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_500]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_501]],  
                x = 22463.21875,  
                y = -1252.546875,  
                z = 76.0625
              },  
              Angle = 0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_498]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 16]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_504]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_505]],  
                x = 22464.20313,  
                y = -1255.734375,  
                z = 76.15625
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_502]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 17]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_508]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_509]],  
                x = 22465.21875,  
                y = -1258.875,  
                z = 76.328125
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_506]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 18]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_512]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_513]],  
                x = 22466.07813,  
                y = -1262.046875,  
                z = 76.390625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_510]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 19]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_516]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_517]],  
                x = 22466.40625,  
                y = -1265.34375,  
                z = 76.4375
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_514]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 20]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_520]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_521]],  
                x = 22466.3125,  
                y = -1268.703125,  
                z = 76.390625
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_518]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 21]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_524]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_525]],  
                x = 22466.26563,  
                y = -1272.0625,  
                z = 76.453125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_522]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 22]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_528]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_529]],  
                x = 22464.96875,  
                y = -1274.671875,  
                z = 76.265625
              },  
              Angle = -0.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_526]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 23]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_532]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_533]],  
                x = 22462.3125,  
                y = -1276.625,  
                z = 75.859375
              },  
              Angle = -0.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_530]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 24]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_536]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_537]],  
                x = 22459.59375,  
                y = -1278.53125,  
                z = 75.5625
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_534]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 25]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_540]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_541]],  
                x = 22456.89063,  
                y = -1278.59375,  
                z = 75.140625
              },  
              Angle = -2.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_538]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 26]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_544]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_545]],  
                x = 22453.98438,  
                y = -1276.8125,  
                z = 74.65625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_542]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 27]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_548]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_549]],  
                x = 22451.03125,  
                y = -1275.296875,  
                z = 74.265625
              },  
              Angle = -1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_546]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 28]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_552]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_553]],  
                x = 22447.89063,  
                y = -1273.921875,  
                z = 73.875
              },  
              Angle = -2.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_550]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 29]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_556]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_557]],  
                x = 22445.53125,  
                y = -1274.28125,  
                z = 73.625
              },  
              Angle = -2.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_554]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[stele 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_560]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_561]],  
                x = 22440.75,  
                y = -1264.46875,  
                z = 74.0625
              },  
              Angle = -5.88145256,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_558]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[stele 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_564]],  
              Base = [[palette.entities.botobjects.chest_old]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_565]],  
                x = 22457.59375,  
                y = -1273.515625,  
                z = 75.015625
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_562]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[old chest 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_568]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_569]],  
                x = 22458.46875,  
                y = -1273.625,  
                z = 75.140625
              },  
              Angle = -2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_566]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[3 jars 2]]
            }
          },  
          InstanceId = [[Client1_30]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_29]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_31]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
        [[Client1_324]]
      },  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Act 1]],  
      Version = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_32]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_34]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_33]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_25]],  
    Texts = {
    }
  }
}