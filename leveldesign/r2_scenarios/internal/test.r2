scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Name = [[pop]],  
      Time = 0,  
      InstanceId = [[Client1_12]],  
      Season = [[winter]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  PlotItems = {
  },  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    EventType = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    TextManager = 0,  
    ZoneTrigger = 0,  
    TimerFeature = 0,  
    Npc = 0,  
    RegionVertex = 0,  
    ActionStep = 0,  
    NpcCustom = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    BanditCampFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 4,  
    LocationId = 191,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 7,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          _BanditsLevel = 3,  
          InstanceId = [[Client1_465]],  
          Behavior = {
            InstanceId = [[Client1_466]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_801]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[member death]],  
                  InstanceId = [[Client1_802]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_807]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_808]],  
                    Entity = r2.RefId([[Client1_805]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_813]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_814]],  
                    Entity = r2.RefId([[Client1_811]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Desactivate]],  
                      InstanceId = [[Client1_815]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_816]],  
                    Entity = r2.RefId([[Client1_390]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[BanditCampFeature]],  
          _BanditsCount = 6,  
          _Zone = [[Client1_469]],  
          Race = [[Zorai]],  
          ZoneSize = [[nil]],  
          Base = [[palette.entities.botobjects.campfire]],  
          BanditsCount = [[6]],  
          _Race = 3,  
          BanditsLevel = [[170]],  
          InheritPos = 1,  
          Cycle = 999999,  
          Name = [[Bandit's camp 1]],  
          Position = {
            y = -1282.40625,  
            x = 21441.96875,  
            InstanceId = [[Client1_467]],  
            Class = [[Position]],  
            z = 75.0625
          },  
          _Seed = 1139395929,  
          Ghosts = {
          },  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Wander zone 1]],  
              InstanceId = [[Client1_469]],  
              Position = {
                y = -1245.921875,  
                x = 21413.92188,  
                InstanceId = [[Client1_468]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_471]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_472]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_474]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_475]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_477]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_478]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_480]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_481]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_483]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_484]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_486]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_487]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 0,  
              Class = [[Region]]
            }
          },  
          Cost = 6
        }
      },  
      Counters = {
      },  
      Name = [[Permanent]],  
      Title = [[]],  
      Events = {
      }
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[Client1_12]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Version = 3,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_205]],  
              ActivitiesId = {
              },  
              HairType = 5623086,  
              TrouserColor = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 5612334,  
              FeetColor = 5,  
              GabaritBreastSize = 5,  
              GabaritHeight = 2,  
              HairColor = 4,  
              EyesColor = 0,  
              AutoSpawn = 0,  
              TrouserModel = 5615406,  
              GabaritLegsWidth = 7,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_203]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_393]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[Start Act]],  
                          InstanceId = [[Client1_395]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_396]],  
                        Entity = r2.RefId([[Client1_162]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_394]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 5612078,  
              Angle = -0.015625,  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 5615662,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]],  
              Position = {
                y = -1246.796875,  
                x = 21414.35938,  
                InstanceId = [[Client1_206]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 2,  
              JacketModel = 5613358,  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[lakeland guard 1]],  
              Sex = 0,  
              WeaponRightHand = 5601070,  
              MorphTarget7 = 6,  
              MorphTarget3 = 1,  
              Tattoo = 24
            },  
            {
              InstanceId = [[Client1_281]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_279]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_287]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_289]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_290]],  
                        Entity = r2.RefId([[Client1_205]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_288]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Vulgar Izam]],  
              Position = {
                y = -1241.15625,  
                x = 21425.40625,  
                InstanceId = [[Client1_282]],  
                Class = [[Position]],  
                z = 74.59375
              },  
              Angle = 2.140625,  
              Base = [[palette.entities.creatures.cbbdb2]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          Secondes = 10,  
          Behavior = {
            InstanceId = [[Client1_212]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_219]],  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_220]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Kill]],  
                      InstanceId = [[Client1_221]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_222]],  
                    Entity = r2.RefId([[Client1_281]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[TimerFeature]],  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer 1]],  
          Position = {
            y = -1236.34375,  
            x = 21431.875,  
            InstanceId = [[Client1_213]],  
            Class = [[Position]],  
            z = 75
          },  
          InstanceId = [[Client1_211]],  
          Components = {
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Cyclic = 0
        }
      },  
      Counters = {
      },  
      Name = [[Act 1:pop]],  
      Title = [[]],  
      Events = {
      }
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_160]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      Name = [[Act 2:pop2]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_390]],  
              ActivitiesId = {
              },  
              HairType = 4910,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 11,  
              HandsModel = 0,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              GabaritHeight = 11,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 12,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_388]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5653038,  
              Angle = 0.4375,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 0,  
              Position = {
                y = -1249.890625,  
                x = 21403.53125,  
                InstanceId = [[Client1_391]],  
                Class = [[Position]],  
                z = 75.140625
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[matis-dressed civilian 1]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 4
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_805]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 1,  
              HandsModel = 5613870,  
              FeetColor = 5,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 3,  
              EyesColor = 4,  
              AutoSpawn = 0,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 5,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_803]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 5653550,  
              Angle = 2.140625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 22,  
              MorphTarget3 = 3,  
              MorphTarget7 = 7,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[tryker-dressed civilian 1]],  
              Position = {
                y = -1250.8125,  
                x = 21390.09375,  
                InstanceId = [[Client1_806]],  
                Class = [[Position]],  
                z = 76.234375
              },  
              JacketModel = 0,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_811]],  
              ActivitiesId = {
              },  
              HairType = 7214,  
              TrouserColor = 5,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 11,  
              HandsModel = 5613870,  
              FeetColor = 3,  
              GabaritBreastSize = 6,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 7,  
              AutoSpawn = 0,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 13,  
              HandsColor = 1,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_809]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 5653550,  
              Angle = 2.140625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 0,  
              MorphTarget3 = 0,  
              MorphTarget7 = 7,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[tryker-dressed civilian 2]],  
              Position = {
                y = -1248.734375,  
                x = 21389.92188,  
                InstanceId = [[Client1_812]],  
                Class = [[Position]],  
                z = 76
              },  
              JacketModel = 0,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_female.creature]]
            }
          },  
          InstanceId = [[Client1_163]]
        },  
        {
          InstanceId = [[Client1_574]],  
          Behavior = {
            InstanceId = [[Client1_575]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_592]],  
                Event = {
                  Type = [[On Player Arrived]],  
                  InstanceId = [[Client1_593]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Sit Down]],  
                      InstanceId = [[Client1_594]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_595]],  
                    Entity = r2.RefId([[Client1_390]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_822]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[On Player Left]],  
                  InstanceId = [[Client1_823]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_824]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_825]],  
                    Entity = r2.RefId([[Client1_805]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_910]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_911]],  
                    Entity = r2.RefId([[Client1_811]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[ZoneTrigger]],  
          InheritPos = 1,  
          Name = [[Trigger Zone 1]],  
          Position = {
            y = -1250.09375,  
            x = 21402,  
            InstanceId = [[Client1_576]],  
            Class = [[Position]],  
            z = 75.1875
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Trigger Zone 2]],  
              InstanceId = [[Client1_578]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_580]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_581]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_583]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 5,  
                    x = 0,  
                    InstanceId = [[Client1_584]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_586]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_587]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_589]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -5,  
                    x = 0,  
                    InstanceId = [[Client1_590]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Class = [[Region]],  
              Deletable = 0,  
              Position = {
                y = -0.59375,  
                x = -6.03125,  
                InstanceId = [[Client1_577]],  
                Class = [[Position]],  
                z = 0.359375
              }
            }
          },  
          _Zone = [[Client1_578]]
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_161]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_162]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_164]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      Name = [[Act 3:pop3]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 110,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_828]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_826]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              ActivitiesId = {
              },  
              Name = [[Capryketh]],  
              Position = {
                y = -1260.359375,  
                x = 21407.35938,  
                InstanceId = [[Client1_829]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              BotAttackable = 0,  
              Angle = 2.0625,  
              Base = [[palette.entities.creatures.chcdb7]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_835]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 1,  
              HandsModel = 6699822,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 1,  
              HairColor = 2,  
              EyesColor = 2,  
              AutoSpawn = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_833]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = 2.234375,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 2,  
              MorphTarget3 = 5,  
              MorphTarget7 = 2,  
              Sex = 0,  
              WeaponRightHand = 6756142,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 6,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[desert guard 1]],  
              Position = {
                y = -1242.453125,  
                x = 21398.48438,  
                InstanceId = [[Client1_836]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            }
          },  
          InstanceId = [[Client1_167]]
        },  
        {
          Secondes = 10,  
          Behavior = {
            InstanceId = [[Client1_831]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_838]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_839]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_840]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_841]],  
                    Entity = r2.RefId([[Client1_835]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            Reactions = {
            }
          },  
          Class = [[TimerFeature]],  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer 2]],  
          Position = {
            y = -1236.578125,  
            x = 21406.48438,  
            InstanceId = [[Client1_832]],  
            Class = [[Position]],  
            z = 73
          },  
          Cyclic = 0,  
          Base = [[palette.entities.botobjects.campfire]],  
          Components = {
          },  
          InstanceId = [[Client1_830]]
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_165]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_166]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_214]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      Name = [[Act 4:pop4]],  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 1022,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_217]]
        }
      },  
      Title = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_215]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      LocationId = [[]],  
      ManualWeather = 1,  
      InstanceId = [[Client1_216]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 6,  
        InstanceId = [[Client1_18]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_23]],  
        Class = [[TextManagerEntry]],  
        Text = [[blabla]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_24]],  
        Class = [[TextManagerEntry]],  
        Text = [[blabla]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_25]],  
        Class = [[TextManagerEntry]],  
        Text = [[blabla]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_26]],  
        Class = [[TextManagerEntry]],  
        Text = [[blabla]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}