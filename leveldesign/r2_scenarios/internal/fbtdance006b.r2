scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_648]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 3,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_650]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_652]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_651]]
    },  
    {
      Cost = 21,  
      Class = [[Act]],  
      WeatherValue = 0,  
      ActivitiesIds = {
        [[Client1_739]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_657]],  
              ActivitiesId = {
                [[Client1_739]]
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 12,  
              HandsModel = 6702894,  
              FeetColor = 5,  
              GabaritBreastSize = 8,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 11,  
              HandsColor = 5,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_655]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_920]],  
                    LogicEntityAction = [[Client1_790]],  
                    ActionStep = [[Client1_919]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[cINIT]],  
                    InstanceId = [[Client1_741]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_762]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_763]],  
                            Who = [[Client1_657]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_764]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_765]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_766]],  
                            Who = [[Client1_657]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_767]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_768]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_769]],  
                            Who = [[Client1_657]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_772]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_770]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_771]],  
                            Who = [[Client1_657]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_773]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 1,  
                        InstanceId = [[Client1_774]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_775]],  
                            Who = [[Client1_657]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_778]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_915]],  
                    Name = [[Chat2]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_916]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_917]],  
                            Emote = [[FBT]],  
                            Who = [[Client1_657]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_790]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_789]],  
                      Value = r2.RefId([[Client1_774]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_791]],  
                          Value = r2.RefId([[Client1_794]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_792]],  
                        Entity = r2.RefId([[Client1_681]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_806]],  
                          Value = r2.RefId([[Client1_797]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_807]],  
                        Entity = r2.RefId([[Client1_685]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_809]],  
                          Value = r2.RefId([[Client1_800]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_810]],  
                        Entity = r2.RefId([[Client1_689]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_812]],  
                          Value = r2.RefId([[Client1_803]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_813]],  
                        Entity = r2.RefId([[Client1_693]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_830]],  
                          Value = r2.RefId([[Client1_815]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_831]],  
                        Entity = r2.RefId([[Client1_661]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_833]],  
                          Value = r2.RefId([[Client1_818]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_834]],  
                        Entity = r2.RefId([[Client1_665]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_836]],  
                          Value = r2.RefId([[Client1_821]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_837]],  
                        Entity = r2.RefId([[Client1_669]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_839]],  
                          Value = r2.RefId([[Client1_824]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_840]],  
                        Entity = r2.RefId([[Client1_673]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_842]],  
                          Value = r2.RefId([[Client1_827]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_843]],  
                        Entity = r2.RefId([[Client1_677]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_864]],  
                          Value = r2.RefId([[Client1_861]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_865]],  
                        Entity = r2.RefId([[Client1_697]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_867]],  
                          Value = r2.RefId([[Client1_858]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_868]],  
                        Entity = r2.RefId([[Client1_701]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_870]],  
                          Value = r2.RefId([[Client1_854]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_871]],  
                        Entity = r2.RefId([[Client1_705]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_873]],  
                          Value = r2.RefId([[Client1_851]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_874]],  
                        Entity = r2.RefId([[Client1_709]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_876]],  
                          Value = r2.RefId([[Client1_848]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_877]],  
                        Entity = r2.RefId([[Client1_713]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_897]],  
                          Value = r2.RefId([[Client1_879]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_898]],  
                        Entity = r2.RefId([[Client1_717]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_900]],  
                          Value = r2.RefId([[Client1_882]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_901]],  
                        Entity = r2.RefId([[Client1_721]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_903]],  
                          Value = r2.RefId([[Client1_885]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_904]],  
                        Entity = r2.RefId([[Client1_725]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_906]],  
                          Value = r2.RefId([[Client1_888]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_907]],  
                        Entity = r2.RefId([[Client1_729]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_909]],  
                          Value = r2.RefId([[Client1_891]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_910]],  
                        Entity = r2.RefId([[Client1_733]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_912]],  
                          Value = r2.RefId([[Client1_894]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_913]],  
                        Entity = r2.RefId([[Client1_737]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_919]],  
                        Entity = r2.RefId([[Client1_657]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_918]],  
                          Type = [[begin chat sequence]],  
                          Value = r2.RefId([[Client1_915]])
                        }
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[sINIT]],  
                    InstanceId = [[Client1_739]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_740]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_741]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Angle = -0.71875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Position = {
                y = -1492.515625,  
                x = 25525.10938,  
                InstanceId = [[Client1_658]],  
                Class = [[Position]],  
                z = 71.359375
              },  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 2,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 1]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 25
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_661]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 4,  
              HandsModel = 6702894,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 13,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_659]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_832]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_831]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_815]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_816]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_817]],  
                            Who = [[Client1_661]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Angle = -0.71875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1493.515625,  
                x = 25522.26563,  
                InstanceId = [[Client1_662]],  
                Class = [[Position]],  
                z = 71.359375
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 6,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[fyros-dressed civilian 2]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 0,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_665]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 3,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 1,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 2,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 2,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_663]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_835]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_834]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_818]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_819]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_820]],  
                            Who = [[Client1_665]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 0,  
              FeetModel = 6702382,  
              Angle = -0.921875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1489.1875,  
                x = 25523.9375,  
                InstanceId = [[Client1_666]],  
                Class = [[Position]],  
                z = 70.859375
              },  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[fyros-dressed civilian 3]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 4,  
              Tattoo = 29
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_669]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 14,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 3,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_667]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_838]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_837]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_821]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_822]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_823]],  
                            Who = [[Client1_669]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 6705198,  
              Angle = -0.625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1494.828125,  
                x = 25519.46875,  
                InstanceId = [[Client1_670]],  
                Class = [[Position]],  
                z = 71.4375
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 4]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 5,  
              Tattoo = 5
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_673]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 2,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 14,  
              HandsColor = 5,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_671]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_841]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_840]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_824]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_825]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_826]],  
                            Who = [[Client1_673]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 4,  
              FeetModel = 6705198,  
              Angle = -0.625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1491.109375,  
                x = 25520.64063,  
                InstanceId = [[Client1_674]],  
                Class = [[Position]],  
                z = 70.734375
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[fyros-dressed civilian 5]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 0,  
              MorphTarget3 = 0,  
              Tattoo = 17
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_677]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 11,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 2,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 10,  
              HandsColor = 1,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_675]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_844]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_843]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_827]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_828]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_829]],  
                            Who = [[Client1_677]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 5,  
              FeetModel = 6705198,  
              Angle = -0.625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1485.796875,  
                x = 25522.29688,  
                InstanceId = [[Client1_678]],  
                Class = [[Position]],  
                z = 69.921875
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[fyros-dressed civilian 6]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 2,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_681]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 1,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 13,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 8,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_679]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_793]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_792]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_794]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_795]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_796]],  
                            Who = [[Client1_681]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 6702638,  
              Angle = -0.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Position = {
                y = -1496.71875,  
                x = 25516.48438,  
                InstanceId = [[Client1_682]],  
                Class = [[Position]],  
                z = 71.859375
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 7]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 0,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_685]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 3,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 3,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 12,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_683]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_808]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_807]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_797]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_798]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_799]],  
                            Who = [[Client1_685]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 7,  
              FeetModel = 6705198,  
              Angle = -0.359375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1492.6875,  
                x = 25517.48438,  
                InstanceId = [[Client1_686]],  
                Class = [[Position]],  
                z = 70.84375
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[fyros-dressed civilian 8]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 6,  
              MorphTarget3 = 7,  
              Tattoo = 23
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_689]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 6,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 0,  
              GabaritHeight = 5,  
              HairColor = 0,  
              EyesColor = 6,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 1,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_687]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_811]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_810]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_800]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_801]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_802]],  
                            Who = [[Client1_689]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6702382,  
              Angle = -0.359375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Position = {
                y = -1487.328125,  
                x = 25518.98438,  
                InstanceId = [[Client1_690]],  
                Class = [[Position]],  
                z = 69.53125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 9]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 6,  
              Tattoo = 5
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_693]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 8,  
              HandsColor = 2,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_691]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_814]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_813]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_803]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_804]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_805]],  
                            Who = [[Client1_693]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Angle = -0.359375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1482.328125,  
                x = 25520.40625,  
                InstanceId = [[Client1_694]],  
                Class = [[Position]],  
                z = 68.65625
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 6,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 10]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 22
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_697]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 9,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 5,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 6,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_695]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_866]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_865]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_861]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_862]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_863]],  
                            Who = [[Client1_697]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Position = {
                y = -1478.75,  
                x = 25518.39063,  
                InstanceId = [[Client1_698]],  
                Class = [[Position]],  
                z = 67.1875
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 11]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_701]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 14,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 12,  
              GabaritHeight = 12,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 9,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_699]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_869]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_868]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_858]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_859]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_860]],  
                            Who = [[Client1_701]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 6,  
              FeetModel = 6704942,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1484.234375,  
                x = 25516.60938,  
                InstanceId = [[Client1_702]],  
                Class = [[Position]],  
                z = 68.03125
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 12]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 2,  
              Tattoo = 16
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_705]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 5,  
              MorphTarget5 = 1,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 3,  
              HandsModel = 6702894,  
              FeetColor = 1,  
              GabaritBreastSize = 1,  
              GabaritHeight = 1,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_703]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_872]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_871]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_854]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_856]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_857]],  
                            Who = [[Client1_705]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 5,  
              FeetModel = 6705198,  
              Angle = 2.796875,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706734,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Position = {
                y = -1490.1875,  
                x = 25515.20313,  
                InstanceId = [[Client1_706]],  
                Class = [[Position]],  
                z = 69.84375
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 13]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 1,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_709]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 8,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 12,  
              HairColor = 4,  
              EyesColor = 0,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 0,  
              HandsColor = 2,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_707]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_875]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_874]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_851]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_852]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_853]],  
                            Who = [[Client1_709]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 6704942,  
              Angle = 1.40625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1494.3125,  
                x = 25514.42188,  
                InstanceId = [[Client1_710]],  
                Class = [[Position]],  
                z = 71.171875
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[fyros-dressed civilian 14]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 3,  
              Tattoo = 7
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_713]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 5,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 9,  
              HandsModel = 6702894,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 6,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_711]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_878]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_877]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_848]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_849]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_850]],  
                            Who = [[Client1_713]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 6702382,  
              Angle = 1.40625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1498.15625,  
                x = 25513.48438,  
                InstanceId = [[Client1_714]],  
                Class = [[Position]],  
                z = 72.203125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[fyros-dressed civilian 15]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 4,  
              Tattoo = 1
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_717]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 3,  
              HandsModel = 6702894,  
              FeetColor = 2,  
              GabaritBreastSize = 6,  
              GabaritHeight = 5,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 13,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_715]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_899]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_898]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_879]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_880]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_881]],  
                            Who = [[Client1_717]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6705198,  
              Angle = 1.59375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6703918,  
              WeaponLeftHand = 0,  
              JacketModel = 6706990,  
              Position = {
                y = -1499.359375,  
                x = 25510.03125,  
                InstanceId = [[Client1_718]],  
                Class = [[Position]],  
                z = 72.421875
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 0,  
              Name = [[fyros-dressed civilian 16]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 6,  
              Tattoo = 28
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_721]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 10,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 12,  
              GabaritHeight = 6,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 12,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_719]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_902]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_901]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_882]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_883]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_884]],  
                            Who = [[Client1_721]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Angle = 1.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1496.296875,  
                x = 25510.1875,  
                InstanceId = [[Client1_722]],  
                Class = [[Position]],  
                z = 71.703125
              },  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 2,  
              Name = [[fyros-dressed civilian 17]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 7,  
              Tattoo = 10
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_725]],  
              ActivitiesId = {
              },  
              HairType = 5621806,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 4,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 13,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 9,  
              HandsColor = 5,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_723]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_905]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_904]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_885]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_886]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_887]],  
                            Who = [[Client1_725]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6702638,  
              Angle = 1.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1492.109375,  
                x = 25510.89063,  
                InstanceId = [[Client1_726]],  
                Class = [[Position]],  
                z = 70.25
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[fyros-dressed civilian 18]],  
              Sex = 0,  
              InheritPos = 1,  
              MorphTarget7 = 3,  
              MorphTarget3 = 6,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_729]],  
              ActivitiesId = {
              },  
              HairType = 2350,  
              TrouserColor = 3,  
              MorphTarget5 = 0,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 6703150,  
              FeetColor = 5,  
              GabaritBreastSize = 1,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 0,  
              TrouserModel = 6705966,  
              GabaritLegsWidth = 2,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_727]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_908]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_907]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_888]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_889]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_890]],  
                            Who = [[Client1_729]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 7,  
              FeetModel = 6704942,  
              Angle = 1.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Position = {
                y = -1486.84375,  
                x = 25512.20313,  
                InstanceId = [[Client1_730]],  
                Class = [[Position]],  
                z = 68.140625
              },  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[fyros-dressed civilian 19]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 1,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_733]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 0,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 3,  
              GabaritHeight = 2,  
              HairColor = 2,  
              EyesColor = 0,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 5,  
              HandsColor = 5,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_731]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_911]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_910]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_891]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_892]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_893]],  
                            Who = [[Client1_733]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6702638,  
              Angle = 1.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706478,  
              WeaponLeftHand = 0,  
              JacketModel = 6704430,  
              Position = {
                y = -1480.75,  
                x = 25513.48438,  
                InstanceId = [[Client1_734]],  
                Class = [[Position]],  
                z = 66.078125
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 4,  
              Name = [[fyros-dressed civilian 20]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 2,  
              Tattoo = 14
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_737]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 13,  
              HandsModel = 6703150,  
              FeetColor = 2,  
              GabaritBreastSize = 9,  
              GabaritHeight = 7,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 10,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_735]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_790]],  
                    Name = [[]],  
                    InstanceId = [[Client1_914]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_913]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_894]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_895]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[FBT]],  
                            InstanceId = [[Client1_896]],  
                            Who = [[Client1_737]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 1,  
              FeetModel = 6702382,  
              Angle = 1.34375,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6706734,  
              WeaponLeftHand = 0,  
              JacketModel = 6707246,  
              Position = {
                y = -1473.8125,  
                x = 25515.14063,  
                InstanceId = [[Client1_738]],  
                Class = [[Position]],  
                z = 65.3125
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[fyros-dressed civilian 21]],  
              Sex = 1,  
              InheritPos = 1,  
              MorphTarget7 = 2,  
              MorphTarget3 = 5,  
              Tattoo = 6
            }
          },  
          InstanceId = [[Client1_654]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_653]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_649]],  
    Texts = {
      {
        Count = 66,  
        InstanceId = [[Client1_744]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_747]],  
        Class = [[TextManagerEntry]],  
        Text = [[10]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_750]],  
        Class = [[TextManagerEntry]],  
        Text = [[9]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_753]],  
        Class = [[TextManagerEntry]],  
        Text = [[8]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_756]],  
        Class = [[TextManagerEntry]],  
        Text = [[7]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_759]],  
        Class = [[TextManagerEntry]],  
        Text = [[6]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_760]],  
        Class = [[TextManagerEntry]],  
        Text = [[5]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_761]],  
        Class = [[TextManagerEntry]],  
        Text = [[5]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_764]],  
        Class = [[TextManagerEntry]],  
        Text = [[4]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_767]],  
        Class = [[TextManagerEntry]],  
        Text = [[3]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_772]],  
        Class = [[TextManagerEntry]],  
        Text = [[2]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_773]],  
        Class = [[TextManagerEntry]],  
        Text = [[1]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_778]],  
        Class = [[TextManagerEntry]],  
        Text = [[GO]]
      }
    }
  }
}