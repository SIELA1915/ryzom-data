scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1023]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes36]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp des guardes]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_1014]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_1012]]
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_1013]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    Location = 1,  
    TextManager = 0,  
    LogicEntityBehavior = 1,  
    DefaultFeature = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1010]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_1017]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1015]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1016]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1030]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1031]],  
                x = 34996.92188,  
                y = -3816.03125,  
                z = -18.421875
              },  
              Angle = 0.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1028]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[hut 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1034]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1035]],  
                x = 34976.14063,  
                y = -3839.21875,  
                z = -18.53125
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1032]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[paddock 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1038]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1039]],  
                x = 34999.92188,  
                y = -3741.59375,  
                z = -12.453125
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1036]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1046]],  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1047]],  
                x = 34971.39063,  
                y = -3784.84375,  
                z = -16.28125
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1044]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[bamboo I 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1050]],  
              Base = [[palette.entities.botobjects.campfire]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1051]],  
                x = 34987.42188,  
                y = -3810.125,  
                z = -18.6875
              },  
              Angle = 0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1048]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[camp fire 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1054]],  
              Base = [[palette.entities.botobjects.chariot]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1055]],  
                x = 34992.23438,  
                y = -3817.328125,  
                z = -18.5625
              },  
              Angle = 2.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1052]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[chariot 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = 0.4805663824,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1056]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34960.62109,  
                y = -3837.738037,  
                z = -19.02312851,  
                InstanceId = [[Client1_1059]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1058]],  
              Name = [[tryker tent 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = 2.894574165,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1060]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34994.39844,  
                y = -3826.52832,  
                z = -18.45736504,  
                InstanceId = [[Client1_1063]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1062]],  
              Name = [[tryker tent 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tr_s2_lokness_a]],  
              Angle = -1.415852427,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1064]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34944.64453,  
                y = -3756.464844,  
                z = -17.64704895,  
                InstanceId = [[Client1_1067]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1066]],  
              Name = [[weeding I 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tr_s3_trumpet_c]],  
              Angle = -1.089734912,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1072]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34996.00391,  
                y = -3768.727051,  
                z = -14.31607819,  
                InstanceId = [[Client1_1075]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1074]],  
              Name = [[trumperer III 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              Angle = -0.70068115,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1080]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34955.07813,  
                y = -3809.632813,  
                z = -18.33909416,  
                InstanceId = [[Client1_1083]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1082]],  
              Name = [[tryker tent 3]]
            },  
            {
              InstanceId = [[Client1_1086]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1087]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -3832.680176,  
                x = 34980.01953,  
                z = -19.38902283,  
                Class = [[Position]],  
                InstanceId = [[Client1_1088]]
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1091]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1092]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              Position = {
                y = -3811.060547,  
                x = 34964.75781,  
                z = -18.72202301,  
                Class = [[Position]],  
                InstanceId = [[Client1_1093]]
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Angle = -1.52012825,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1094]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34991.22266,  
                y = -3768.289795,  
                z = -14.95236015,  
                InstanceId = [[Client1_1097]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1096]],  
              Name = [[street lamp 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Angle = -1.52012825,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1098]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34981.95313,  
                y = -3766.603271,  
                z = -15.55923462,  
                InstanceId = [[Client1_1101]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1100]],  
              Name = [[street lamp 2]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Angle = 2.035871744,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1102]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34922.9375,  
                y = -3765.542969,  
                z = -19.51447487,  
                InstanceId = [[Client1_1105]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1104]],  
              Name = [[street lamp 3]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.street_lamp]],  
              Angle = 2.035871744,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1106]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34931.57422,  
                y = -3764.268311,  
                z = -19.5646286,  
                InstanceId = [[Client1_1109]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1108]],  
              Name = [[street lamp 4]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.jar_3]],  
              Angle = -1.205713391,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1114]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34966.82813,  
                y = -3807.66626,  
                z = -18.72891998,  
                InstanceId = [[Client1_1117]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1116]],  
              Name = [[3 jars 1]]
            },  
            {
              Class = [[Npc]],  
              Base = [[palette.entities.botobjects.barrels_3]],  
              Angle = 2.682293653,  
              Behavior = {
                Class = [[Behavior]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                InstanceId = [[Client1_1118]]
              },  
              ActivitiesId = {
              },  
              Position = {
                Class = [[Position]],  
                x = 34992.34766,  
                y = -3813.666016,  
                z = -18.52198792,  
                InstanceId = [[Client1_1121]]
              },  
              InheritPos = 1,  
              InstanceId = [[Client1_1120]],  
              Name = [[3 barrels 1]]
            }
          },  
          InstanceId = [[Client1_1018]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_1021]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1019]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1020]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 78,  
      LocationId = [[Client1_1023]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1042]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1043]],  
                x = 34968.17188,  
                y = -3744.34375,  
                z = -12.328125
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1040]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]]
            }
          },  
          InstanceId = [[Client1_1022]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_1011]]
  }
}