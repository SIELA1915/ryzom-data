scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 119,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 3,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Act 1]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pouf]],  
              Position = {
                y = -11127.46875,  
                x = 31298.40625,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = -8.40625
              },  
              Angle = 1.640625,  
              Base = [[palette.entities.creatures.chddb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_38]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_36]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pif]],  
              Position = {
                y = -11126.4375,  
                x = 31294.9375,  
                InstanceId = [[Client1_39]],  
                Class = [[Position]],  
                z = -7.9375
              },  
              Angle = 1.640625,  
              Base = [[palette.entities.creatures.chddb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Paf]],  
              Position = {
                y = -11125.53125,  
                x = 31292.20313,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = -7.640625
              },  
              Angle = 1.578125,  
              Base = [[palette.entities.creatures.chddb1]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}