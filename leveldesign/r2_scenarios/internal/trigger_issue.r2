scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 2,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 14,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_36]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_41]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1488.0625,  
                    x = 24249.92188,  
                    InstanceId = [[Client1_42]],  
                    Class = [[Position]],  
                    z = 82.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_47]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1512.78125,  
                    x = 24267.70313,  
                    InstanceId = [[Client1_48]],  
                    Class = [[Position]],  
                    z = 82.5
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_114]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_112]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 1]],  
              Position = {
                y = -1525.140625,  
                x = 24271.78125,  
                InstanceId = [[Client1_115]],  
                Class = [[Position]],  
                z = 83.21875
              },  
              Angle = 1.546875,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_118]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_116]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 2]],  
              Position = {
                y = -1515.484375,  
                x = 24281.29688,  
                InstanceId = [[Client1_119]],  
                Class = [[Position]],  
                z = 81.828125
              },  
              Angle = -2.984375,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_122]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_120]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 3]],  
              Position = {
                y = -1491.125,  
                x = 24274.40625,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 82.578125
              },  
              Angle = -2.525934219,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_126]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_124]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 4]],  
              Position = {
                y = -1483.71875,  
                x = 24279.42188,  
                InstanceId = [[Client1_127]],  
                Class = [[Position]],  
                z = 81.6875
              },  
              Angle = -3.029421806,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_130]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 5]],  
              Position = {
                y = -1484.359375,  
                x = 24231.75,  
                InstanceId = [[Client1_131]],  
                Class = [[Position]],  
                z = 82.09375
              },  
              Angle = -0.03507562354,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_134]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_132]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 6]],  
              Position = {
                y = -1492.078125,  
                x = 24237.89063,  
                InstanceId = [[Client1_135]],  
                Class = [[Position]],  
                z = 83.390625
              },  
              Angle = 1.10671711,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_138]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_136]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tent 7]],  
              Position = {
                y = -1472.96875,  
                x = 24235.96875,  
                InstanceId = [[Client1_139]],  
                Class = [[Position]],  
                z = 81.265625
              },  
              Angle = -0.9240413308,  
              Base = [[palette.entities.botobjects.tent]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_3]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_1]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 1 1]],  
              Position = {
                y = -1472.140625,  
                x = 24241.03125,  
                InstanceId = [[Client2_4]],  
                Class = [[Position]],  
                z = 80.140625
              },  
              Angle = -0.671875,  
              Base = [[palette.entities.botobjects.bag_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_7]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_5]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bag 2 1]],  
              Position = {
                y = -1470.71875,  
                x = 24240.01563,  
                InstanceId = [[Client2_8]],  
                Class = [[Position]],  
                z = 80.453125
              },  
              Angle = -0.671875,  
              Base = [[palette.entities.botobjects.bag_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_11]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_9]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1483.4375,  
                x = 24239.5,  
                InstanceId = [[Client2_12]],  
                Class = [[Position]],  
                z = 81.671875
              },  
              Angle = 1.15625,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_13]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fallen jar 1]],  
              Position = {
                y = -1478.765625,  
                x = 24234.39063,  
                InstanceId = [[Client2_16]],  
                Class = [[Position]],  
                z = 81.484375
              },  
              Angle = 1.375,  
              Base = [[palette.entities.botobjects.jar_fallen]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_146]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_144]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -1521.734375,  
                x = 24216.92188,  
                InstanceId = [[Client1_147]],  
                Class = [[Position]],  
                z = 82.71875
              },  
              Angle = 0.6875,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_19]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_17]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1479.578125,  
                x = 24233.78125,  
                InstanceId = [[Client2_20]],  
                Class = [[Position]],  
                z = 81.578125
              },  
              Angle = 0,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_162]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_160]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_163]],  
                x = 24271.92188,  
                y = -1506.296875,  
                z = 82.75
              },  
              Angle = 1.77859211,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 11,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_49]],  
        [[Client1_195]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_10]],  
              ActivitiesId = {
              },  
              HairType = 2862,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 0,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 2,  
              GabaritHeight = 5,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_223]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of activity step Activity 1 : Follow Route Route 1 without time limit' event of Hubert bouclier is here]],  
                    InstanceId = [[Client1_221]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_225]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_51]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_52]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_53]],  
                            Who = [[Client1_10]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_55]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) toto says ho l...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of chat step Chat 1 : (after  3sec) toto says ho l...' event]],  
                    InstanceId = [[Client1_228]],  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_227]],  
                      Value = [[Client1_52]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_229]],  
                          Value = [[Client1_232]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_230]],  
                        Entity = [[Client1_22]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 0,  
              FeetModel = 5605422,  
              Angle = -4.729535103,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1517.171875,  
                x = 24271.76563,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = 82.328125
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 2,  
              JacketModel = 5606446,  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[toto]],  
              Sex = 1,  
              WeaponRightHand = 0,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_14]],  
              ActivitiesId = {
              },  
              HairType = 5934,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 6,  
              HandsModel = 5609774,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 0,  
              HairColor = 2,  
              EyesColor = 7,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_184]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_185]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_186]],  
                            Who = [[Client1_22]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Hubert bouclier is here says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 0,  
              Angle = -3.672366619,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              Position = {
                y = -1514.6875,  
                x = 24274.57813,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 81.953125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[toto]],  
              Sex = 1,  
              WeaponRightHand = 0,  
              MorphTarget7 = 7,  
              MorphTarget3 = 4,  
              Tattoo = 12
            },  
            {
              InstanceId = [[Client1_142]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_140]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami banner 1]],  
              Position = {
                y = -1517.984375,  
                x = 24223.20313,  
                InstanceId = [[Client1_143]],  
                Class = [[Position]],  
                z = 83.1875
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.botobjects.banner_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_150]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_148]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ardent Genius 1]],  
              Position = {
                y = -1514.53125,  
                x = 24215.89063,  
                InstanceId = [[Client1_151]],  
                Class = [[Position]],  
                z = 82.40625
              },  
              Angle = 0.734375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_154]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_152]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ardent Genius 2]],  
              Position = {
                y = -1523.84375,  
                x = 24224.125,  
                InstanceId = [[Client1_155]],  
                Class = [[Position]],  
                z = 83.5625
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_158]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_156]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Burning Salamander 1]],  
              Position = {
                y = -1517.375,  
                x = 24221.75,  
                InstanceId = [[Client1_159]],  
                Class = [[Position]],  
                z = 83.046875
              },  
              Angle = 0.640625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_189]],  
              ActivitiesId = {
                [[Client1_195]]
              },  
              HairType = 5623086,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 2,  
              HandsModel = 6724654,  
              FeetColor = 5,  
              GabaritBreastSize = 3,  
              GabaritHeight = 3,  
              HairColor = 0,  
              EyesColor = 5,  
              TrouserModel = 6725166,  
              GabaritLegsWidth = 12,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_187]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_195]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_211]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_191]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_191]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_192]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_193]],  
                            Who = [[Client1_189]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_194]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) toto says dfgr...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 5,  
              FeetModel = 6724142,  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Tattoo = 14,  
              MorphTarget3 = 0,  
              MorphTarget7 = 3,  
              Sex = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 0,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[toto]],  
              Position = {
                y = -1511.828125,  
                x = 24275.125,  
                InstanceId = [[Client1_190]],  
                Class = [[Position]],  
                z = 81.90625
              },  
              JacketModel = 6725934,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_male.creature]]
            }
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          InstanceId = [[Client1_34]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -21.671875,  
            x = 23.15625,  
            InstanceId = [[Client1_33]],  
            Class = [[Position]],  
            z = -1.109375
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_32]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_22]],  
              ActivitiesId = {
              },  
              HairType = 8238,  
              TrouserColor = 5,  
              MorphTarget5 = 6,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 5608238,  
              FeetColor = 5,  
              GabaritBreastSize = 12,  
              GabaritHeight = 10,  
              HairColor = 5,  
              EyesColor = 1,  
              TrouserModel = 5611310,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_228]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of chat step Chat 1 : (after  3sec) toto says ho l...' event of toto]],  
                    InstanceId = [[Client1_231]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_230]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_232]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_233]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_234]],  
                            Who = [[Client1_10]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_235]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) toto says Oais...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 1 without time limit' event]],  
                    InstanceId = [[Client1_109]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_108]],  
                      Value = [[Client1_50]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'end of activity step Activity 1 : Follow Route Route 1 without time limit' event]],  
                    InstanceId = [[Client1_223]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_222]],  
                      Value = [[Client1_50]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_224]],  
                          Value = [[Client1_51]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_225]],  
                        Entity = [[Client1_10]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_49]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_50]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_36]],  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 0,  
              FeetModel = 5610798,  
              Speed = [[run]],  
              Angle = -0.671875,  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              SheetClient = [[basic_zorai_male.creature]],  
              ArmModel = 5610286,  
              WeaponLeftHand = 5637678,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]],  
              Position = {
                y = -1463.9375,  
                x = 24226.04688,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 83.171875
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Hubert bouclier is here]],  
              Sex = 0,  
              WeaponRightHand = 5596206,  
              MorphTarget7 = 1,  
              MorphTarget3 = 7,  
              Tattoo = 27
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_30]],  
              ActivitiesId = {
              },  
              HairType = 5422,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 14,  
              HandsModel = 5608238,  
              FeetColor = 2,  
              GabaritBreastSize = 14,  
              GabaritHeight = 0,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 5611310,  
              GabaritLegsWidth = 14,  
              HandsColor = 2,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 2,  
              FeetModel = 5610798,  
              Angle = -0.671875,  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmModel = 5610286,  
              WeaponLeftHand = 5637678,  
              Sheet = [[ring_melee_tank_pierce_b2.creature]],  
              Position = {
                y = -1462.53125,  
                x = 24227.54688,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = 82.953125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 5653294,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[malformed is here]],  
              Sex = 0,  
              WeaponRightHand = 5596974,  
              MorphTarget7 = 1,  
              MorphTarget3 = 5,  
              Tattoo = 21
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_18]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 8,  
              HandsModel = 5609774,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 7,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5659694,  
              GabaritLegsWidth = 3,  
              HandsColor = 3,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 5,  
              FeetModel = 5653038,  
              Angle = -0.671875,  
              Base = [[palette.entities.npcs.bandits.m_mage_aoe_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_magic_aoe_acid_b2.creature]],  
              Position = {
                y = -1464.5,  
                x = 24224.5,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = 83.234375
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[jmelapete.com]],  
              Sex = 1,  
              WeaponRightHand = 6934574,  
              MorphTarget7 = 4,  
              MorphTarget3 = 5,  
              Tattoo = 30
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_26]],  
              ActivitiesId = {
                [[Client1_49]]
              },  
              HairType = 5166,  
              TrouserColor = 2,  
              MorphTarget5 = 7,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 0,  
              HandsModel = 5608238,  
              FeetColor = 4,  
              GabaritBreastSize = 4,  
              GabaritHeight = 0,  
              HairColor = 0,  
              EyesColor = 2,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 8,  
              HandsColor = 1,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_24]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5610798,  
              Angle = -0.671875,  
              Base = [[palette.entities.npcs.bandits.m_melee_tank_20]],  
              SheetClient = [[basic_matis_female.creature]],  
              ArmModel = 5610286,  
              WeaponLeftHand = 5637678,  
              Sheet = [[ring_melee_tank_blunt_b2.creature]],  
              Position = {
                y = -1467,  
                x = 24224.10938,  
                InstanceId = [[Client1_27]],  
                Class = [[Position]],  
                z = 83.96875
              },  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 7,  
              JacketModel = 5610542,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[.]],  
              Sex = 1,  
              WeaponRightHand = 5596206,  
              MorphTarget7 = 6,  
              MorphTarget3 = 2,  
              Tattoo = 28
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 5,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_88]]
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_166]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_164]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 1]],  
              Position = {
                y = -1506.140625,  
                x = 24272.875,  
                InstanceId = [[Client1_167]],  
                Class = [[Position]],  
                z = 82.609375
              },  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_170]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_168]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 2]],  
              Position = {
                y = -1502.578125,  
                x = 24268.90625,  
                InstanceId = [[Client1_171]],  
                Class = [[Position]],  
                z = 83.671875
              },  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_174]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_172]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[guard 3]],  
              Position = {
                y = -1501.9375,  
                x = 24274.14063,  
                InstanceId = [[Client1_175]],  
                Class = [[Position]],  
                z = 82.65625
              },  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_178]],  
              ActivitiesId = {
              },  
              HairType = 6711342,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 6710574,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 0,  
              TrouserModel = 6711854,  
              GabaritLegsWidth = 6,  
              HandsColor = 2,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_176]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 6710318,  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.guards.m_guard_245]],  
              Tattoo = 13,  
              MorphTarget3 = 5,  
              MorphTarget7 = 0,  
              Sex = 1,  
              WeaponRightHand = 6761518,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 3,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              InheritPos = 1,  
              ArmColor = 4,  
              Name = [[forest guard 1]],  
              Position = {
                y = -1505.9375,  
                x = 24270.14063,  
                InstanceId = [[Client1_179]],  
                Class = [[Position]],  
                z = 83.0625
              },  
              JacketModel = 6712878,  
              WeaponLeftHand = 0,  
              ArmModel = 6712366,  
              SheetClient = [[basic_matis_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_182]],  
              ActivitiesId = {
              },  
              HairType = 6700334,  
              TrouserColor = 5,  
              MorphTarget5 = 2,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 7,  
              HandsModel = 6699822,  
              FeetColor = 4,  
              GabaritBreastSize = 1,  
              GabaritHeight = 4,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 3,  
              HandsColor = 2,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_180]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 7,  
              FeetModel = 6699310,  
              Angle = 1.953125,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Tattoo = 30,  
              MorphTarget3 = 6,  
              MorphTarget7 = 0,  
              Sex = 1,  
              WeaponRightHand = 6756910,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[desert guard 1]],  
              Position = {
                y = -1502.28125,  
                x = 24270.29688,  
                InstanceId = [[Client1_183]],  
                Class = [[Position]],  
                z = 83.421875
              },  
              JacketModel = 6701870,  
              WeaponLeftHand = 0,  
              ArmModel = 6701358,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client1_64]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_63]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 4,  
        InstanceId = [[Client1_54]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_55]],  
        Class = [[TextManagerEntry]],  
        Text = [[ho lalala jai peur]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_194]],  
        Class = [[TextManagerEntry]],  
        Text = [[dfgregreg]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_226]],  
        Class = [[TextManagerEntry]],  
        Text = [[Salut hubert ca va ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_235]],  
        Class = [[TextManagerEntry]],  
        Text = [[Oais ca va, mais je dois y aller la...]]
      }
    }
  }
}