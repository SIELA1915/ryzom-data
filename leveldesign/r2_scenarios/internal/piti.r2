scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Name = [[pop]],  
      Time = 0,  
      InstanceId = [[Client1_12]],  
      Season = [[winter]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts01]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  PlotItems = {
  },  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 4,  
    LocationId = 39,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_5]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_9]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 194,  
      LocationId = [[Client1_12]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_15]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_16]],  
                x = 21574.54688,  
                y = -1356.484375,  
                z = 110.546875
              },  
              Angle = -3.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_13]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 6,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 5,  
              HairType = 2350,  
              HairColor = 4,  
              Tattoo = 3,  
              EyesColor = 7,  
              MorphTarget1 = 2,  
              MorphTarget2 = 3,  
              MorphTarget3 = 7,  
              MorphTarget4 = 3,  
              MorphTarget5 = 7,  
              MorphTarget6 = 1,  
              MorphTarget7 = 4,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 3,  
              TrouserColor = 1,  
              FeetColor = 2,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:pop]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}