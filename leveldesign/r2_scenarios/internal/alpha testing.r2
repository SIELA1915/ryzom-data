scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_27]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 6,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_29]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_31]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_30]],  
      Events = {
      }
    },  
    {
      Cost = 6,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_36]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_37]],  
                x = 28922.59375,  
                y = -1418.71875,  
                z = 87.984375
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_34]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 6,  
              HairType = 5621806,  
              HairColor = 3,  
              Tattoo = 19,  
              EyesColor = 1,  
              MorphTarget1 = 5,  
              MorphTarget2 = 5,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 1,  
              MorphTarget6 = 0,  
              MorphTarget7 = 5,  
              MorphTarget8 = 6,  
              Sex = 1,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5604910,  
              JacketColor = 5,  
              ArmColor = 0,  
              HandsColor = 5,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 5595182,  
              WeaponLeftHand = 0,  
              Name = [[desert guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_40]],  
              Base = [[palette.entities.npcs.guards.f_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_41]],  
                x = 28930.46875,  
                y = -1416.234375,  
                z = 87.796875
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_38]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 13,  
              HairType = 2350,  
              HairColor = 3,  
              Tattoo = 4,  
              EyesColor = 3,  
              MorphTarget1 = 6,  
              MorphTarget2 = 7,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 6,  
              MorphTarget7 = 0,  
              MorphTarget8 = 1,  
              Sex = 0,  
              JacketModel = 5605166,  
              TrouserModel = 5607214,  
              FeetModel = 5606702,  
              HandsModel = 5606958,  
              ArmModel = 5607470,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 1,  
              FeetColor = 0,  
              WeaponRightHand = 5595694,  
              WeaponLeftHand = 0,  
              Name = [[desert guard 2]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_44]],  
              Base = [[palette.entities.npcs.guards.m_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_45]],  
                x = 28926.51563,  
                y = -1419.09375,  
                z = 87.921875
              },  
              Angle = -1.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_42]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 9,  
              GabaritBreastSize = 6,  
              HairType = 5622830,  
              HairColor = 0,  
              Tattoo = 22,  
              EyesColor = 7,  
              MorphTarget1 = 6,  
              MorphTarget2 = 5,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              MorphTarget7 = 1,  
              MorphTarget8 = 0,  
              Sex = 0,  
              JacketModel = 5653294,  
              TrouserModel = 5611310,  
              FeetModel = 5610798,  
              HandsModel = 5611054,  
              ArmModel = 5609006,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 0,  
              FeetColor = 5,  
              WeaponRightHand = 5598510,  
              WeaponLeftHand = 0,  
              Name = [[forest guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b4.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_48]],  
              Base = [[palette.entities.creatures.cbbdb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_49]],  
                x = 28925.14063,  
                y = -1426.09375,  
                z = 85.84375
              },  
              Angle = -1.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_46]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Gorged Izam]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_52]],  
              Base = [[palette.entities.creatures.cbbdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_53]],  
                x = 28921.73438,  
                y = -1425.203125,  
                z = 86.203125
              },  
              Angle = -1.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_50]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Prime Izam]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_56]],  
              Base = [[palette.entities.creatures.cbbdb4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_57]],  
                x = 28927.48438,  
                y = -1424.265625,  
                z = 86.234375
              },  
              Angle = -1.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_54]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Prime Izam]]
            }
          },  
          InstanceId = [[Client1_33]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_32]],  
      Events = {
      }
    },  
    {
      Cost = 5,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[Act 2]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_62]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_63]],  
                x = 28908.125,  
                y = -1442.484375,  
                z = 81.734375
              },  
              Angle = -0.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_60]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 10,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 7,  
              HairType = 2606,  
              HairColor = 4,  
              Tattoo = 4,  
              EyesColor = 0,  
              MorphTarget1 = 7,  
              MorphTarget2 = 4,  
              MorphTarget3 = 2,  
              MorphTarget4 = 1,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 2,  
              ArmColor = 1,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_66]],  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_67]],  
                x = 28910.46875,  
                y = -1438.40625,  
                z = 81.984375
              },  
              Angle = -0.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_64]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 13,  
              HairType = 4910,  
              HairColor = 4,  
              Tattoo = 8,  
              EyesColor = 7,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 5610542,  
              TrouserModel = 5610030,  
              FeetModel = 5653038,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 0,  
              ArmColor = 4,  
              HandsColor = 1,  
              TrouserColor = 5,  
              FeetColor = 0,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[matis-dressed civilian 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_matis_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_70]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_71]],  
                x = 28907.46875,  
                y = -1440.109375,  
                z = 81.75
              },  
              Angle = -0.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_68]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 10,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 5,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 2,  
              HairType = 2606,  
              HairColor = 5,  
              Tattoo = 14,  
              EyesColor = 4,  
              MorphTarget1 = 1,  
              MorphTarget2 = 0,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              MorphTarget7 = 4,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 5606446,  
              TrouserModel = 5605934,  
              FeetModel = 0,  
              HandsModel = 0,  
              ArmModel = 0,  
              JacketColor = 4,  
              ArmColor = 0,  
              HandsColor = 5,  
              TrouserColor = 0,  
              FeetColor = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[fyros-dressed civilian 2]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_74]],  
              Base = [[palette.entities.creatures.chflb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_75]],  
                x = 28910.78125,  
                y = -1433.328125,  
                z = 82.765625
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_72]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Timorous Messab]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_78]],  
              Base = [[palette.entities.creatures.chflb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_79]],  
                x = 28912.70313,  
                y = -1430.90625,  
                z = 83.53125
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_76]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Timorous Messab]]
            }
          },  
          InstanceId = [[Client1_59]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_58]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_28]]
  }
}