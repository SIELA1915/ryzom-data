scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 27,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 1,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_138]],  
        [[Client1_163]],  
        [[Client1_166]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_74]],  
              ActivitiesId = {
                [[Client1_369]]
              },  
              HairType = 5621550,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 0,  
              HandsModel = 6703150,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_72]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_369]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_382]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_314]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6705198,  
              Speed = [[run]],  
              Angle = -0.90625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmModel = 6704174,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              Position = {
                y = -2008.4375,  
                x = 29487.1875,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = -19.0625
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 1,  
              Name = [[News Girl]],  
              Sex = 0,  
              WeaponRightHand = 0,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 12
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_314]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_316]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2015.3125,  
                    x = 29494.64063,  
                    InstanceId = [[Client1_317]],  
                    Class = [[Position]],  
                    z = -18.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_319]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2015.3125,  
                    x = 29494.64063,  
                    InstanceId = [[Client1_320]],  
                    Class = [[Position]],  
                    z = -18.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_322]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2021.59375,  
                    x = 29501.76563,  
                    InstanceId = [[Client1_323]],  
                    Class = [[Position]],  
                    z = -19.8125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_313]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_221]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_222]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_223]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_224]],  
        Class = [[TextManagerEntry]],  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_225]],  
        Class = [[TextManagerEntry]],  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}