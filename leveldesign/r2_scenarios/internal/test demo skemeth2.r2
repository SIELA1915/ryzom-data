scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_1023]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Lakes36]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Camp des guardes]],  
      Season = [[Spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint01]]
    }
  },  
  InstanceId = [[Client1_1014]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_1012]]
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_1013]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TalkTo = 0,  
    NpcGrpFeature = 1,  
    TextManager = 0,  
    Position = 0,  
    Location = 1,  
    Npc = 0,  
    LogicEntityBehavior = 1,  
    EventType = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1010]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_1017]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1015]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 0,  
      InheritPos = 1,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_1016]],  
        Class = [[Position]],  
        z = 0
      },  
      Name = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1030]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1028]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -3816.03125,  
                x = 34996.92188,  
                InstanceId = [[Client1_1031]],  
                Class = [[Position]],  
                z = -18.421875
              },  
              Angle = 0.859375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1034]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1032]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -3839.21875,  
                x = 34976.14063,  
                InstanceId = [[Client1_1035]],  
                Class = [[Position]],  
                z = -18.53125
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1038]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1036]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -3741.59375,  
                x = 34999.92188,  
                InstanceId = [[Client1_1039]],  
                Class = [[Position]],  
                z = -12.453125
              },  
              Angle = 2.578125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1046]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1044]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo I 1]],  
              Position = {
                y = -3784.155762,  
                x = 34971.56641,  
                InstanceId = [[Client1_1047]],  
                Class = [[Position]],  
                z = -16.18996239
              },  
              Angle = 1.71875,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1050]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1048]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -3810.125,  
                x = 34987.42188,  
                InstanceId = [[Client1_1051]],  
                Class = [[Position]],  
                z = -18.6875
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1054]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1052]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[chariot 1]],  
              Position = {
                y = -3817.328125,  
                x = 34992.23438,  
                InstanceId = [[Client1_1055]],  
                Class = [[Position]],  
                z = -18.5625
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.botobjects.chariot]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1058]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1056]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 1]],  
              Position = {
                y = -3837.734375,  
                x = 34960.60938,  
                InstanceId = [[Client1_1059]],  
                Class = [[Position]],  
                z = -19.015625
              },  
              Angle = 0.46875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1062]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1060]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 2]],  
              Position = {
                y = -3826.515625,  
                x = 34994.39063,  
                InstanceId = [[Client1_1063]],  
                Class = [[Position]],  
                z = -18.453125
              },  
              Angle = 2.890625,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1066]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1064]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[weeding I 1]],  
              Position = {
                y = -3756.453125,  
                x = 34944.64063,  
                InstanceId = [[Client1_1067]],  
                Class = [[Position]],  
                z = -17.640625
              },  
              Angle = -1.40625,  
              Base = [[palette.entities.botobjects.tr_s2_lokness_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1074]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1072]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[trumperer III 1]],  
              Position = {
                y = -3768.71875,  
                x = 34996,  
                InstanceId = [[Client1_1075]],  
                Class = [[Position]],  
                z = -14.3125
              },  
              Angle = -1.078125,  
              Base = [[palette.entities.botobjects.tr_s3_trumpet_c]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1082]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1080]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tryker tent 3]],  
              Position = {
                y = -3809.625,  
                x = 34955.07813,  
                InstanceId = [[Client1_1083]],  
                Class = [[Position]],  
                z = -18.328125
              },  
              Angle = -0.6875,  
              Base = [[palette.entities.botobjects.tent_tryker]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1086]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1087]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 2]],  
              Position = {
                y = -3832.671875,  
                x = 34980.01563,  
                InstanceId = [[Client1_1088]],  
                Class = [[Position]],  
                z = -19.375
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1091]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1092]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 3]],  
              Position = {
                y = -3811.008301,  
                x = 34964.76563,  
                InstanceId = [[Client1_1093]],  
                Class = [[Position]],  
                z = -18.72164536
              },  
              Angle = 0.78125,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1096]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1094]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -3768.28125,  
                x = 34991.21875,  
                InstanceId = [[Client1_1097]],  
                Class = [[Position]],  
                z = -14.9375
              },  
              Angle = -1.515625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1100]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1098]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 2]],  
              Position = {
                y = -3766.59375,  
                x = 34981.95313,  
                InstanceId = [[Client1_1101]],  
                Class = [[Position]],  
                z = -15.546875
              },  
              Angle = -1.515625,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1104]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1102]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 3]],  
              Position = {
                y = -3765.53125,  
                x = 34922.9375,  
                InstanceId = [[Client1_1105]],  
                Class = [[Position]],  
                z = -19.5
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1108]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1106]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 4]],  
              Position = {
                y = -3764.265625,  
                x = 34931.5625,  
                InstanceId = [[Client1_1109]],  
                Class = [[Position]],  
                z = -19.5625
              },  
              Angle = 2.03125,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1116]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1114]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -3817.397949,  
                x = 34968.91797,  
                InstanceId = [[Client1_1117]],  
                Class = [[Position]],  
                z = -19.36590958
              },  
              Angle = -1.203125,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1120]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1118]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 barrels 1]],  
              Position = {
                y = -3813.65625,  
                x = 34992.34375,  
                InstanceId = [[Client1_1121]],  
                Class = [[Position]],  
                z = -18.515625
              },  
              Angle = 2.671875,  
              Base = [[palette.entities.botobjects.barrels_3]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_1018]]
        }
      },  
      Counters = {
      },  
      Version = 5,  
      Title = [[]],  
      Events = {
      }
    },  
    {
      InstanceId = [[Client1_1021]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_1019]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      ManualWeather = 1,  
      InheritPos = 1,  
      WeatherValue = 78,  
      LocationId = [[Client1_1023]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_1020]],  
        Class = [[Position]],  
        z = 0
      },  
      Name = [[Act 1:Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1042]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1040]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wind turbine 1]],  
              Position = {
                y = -3744.34375,  
                x = 34968.17188,  
                InstanceId = [[Client1_1043]],  
                Class = [[Position]],  
                z = -12.328125
              },  
              Angle = -1.25,  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1145]],  
              ActivitiesId = {
              },  
              Tattoo = 25,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 11,  
              HandsModel = 6721582,  
              FeetColor = 1,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 6,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 11,  
              HandsColor = 1,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1143]],  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1148]],  
                    Event = {
                      Type = [[targeted by player]],  
                      InstanceId = [[Client1_1149]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 7,  
              FeetModel = 6721070,  
              Speed = 0,  
              Level = 3,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              HairType = 6722094,  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmColor = 1,  
              WeaponLeftHand = 0,  
              Position = {
                y = -3831.960938,  
                x = 34977.02734,  
                z = -19.49832726,  
                Class = [[Position]],  
                InstanceId = [[Client1_1146]]
              },  
              WeaponRightHand = 6766126,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              JacketModel = 6723630,  
              InheritPos = 1,  
              ArmModel = 6723118,  
              Name = [[Be'Yle]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Angle = 1.448623279
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_1168]],  
              ActivitiesId = {
              },  
              Tattoo = 29,  
              TrouserColor = 3,  
              MorphTarget5 = 4,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 14,  
              HandsModel = 6699822,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 10,  
              HairColor = 1,  
              EyesColor = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 2,  
              HandsColor = 1,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1166]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6699310,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              HairType = 6700590,  
              SheetClient = [[basic_fyros_female.creature]],  
              ArmColor = 5,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6756910,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 2,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmModel = 6701614,  
              Name = [[Kridix]],  
              Position = {
                y = -3736.671875,  
                x = 35168.33594,  
                z = -14.07806587,  
                Class = [[Position]],  
                InstanceId = [[Client1_1169]]
              },  
              Sex = 1,  
              MorphTarget7 = 7,  
              MorphTarget3 = 0,  
              Angle = -2.415196896
            }
          },  
          InstanceId = [[Client1_1022]]
        },  
        {
          InstanceId = [[Client1_1132]],  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            ChatSequences = {
            },  
            InstanceId = [[Client1_1130]],  
            Class = [[Behavior]],  
            Actions = {
            },  
            Activities = {
            }
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            z = 0,  
            Class = [[Position]],  
            InstanceId = [[Client1_1131]]
          },  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_1124]],  
              ActivitiesId = {
              },  
              Tattoo = 22,  
              TrouserColor = 4,  
              MorphTarget5 = 6,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 3,  
              HandsModel = 6721582,  
              FeetColor = 4,  
              GabaritBreastSize = 0,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1122]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6721070,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              HairType = 6722094,  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Position = {
                y = -3830.37085,  
                x = 34971.82422,  
                z = -20.0474968,  
                Class = [[Position]],  
                InstanceId = [[Client1_1125]]
              },  
              WeaponRightHand = 6765870,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 1,  
              JacketModel = 6723630,  
              InheritPos = 1,  
              ArmModel = 6723118,  
              Name = [[Ba'Massey]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              MorphTarget7 = 7,  
              MorphTarget3 = 7,  
              Angle = 1.48972249
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_1128]],  
              ActivitiesId = {
              },  
              Tattoo = 15,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 5,  
              HandsModel = 6721326,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 3,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 6722350,  
              GabaritLegsWidth = 12,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1126]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 6720814,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              HairType = 6721838,  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -3832.57959,  
                x = 34974.01172,  
                z = -19.54600334,  
                Class = [[Position]],  
                InstanceId = [[Client1_1129]]
              },  
              WeaponRightHand = 6766638,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 4,  
              JacketModel = 6723374,  
              InheritPos = 1,  
              ArmModel = 6722862,  
              Name = [[Be'Daghan]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Angle = 1.48972249
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_1135]],  
              ActivitiesId = {
              },  
              Tattoo = 12,  
              TrouserColor = 0,  
              MorphTarget5 = 0,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 4,  
              HandsModel = 6721326,  
              FeetColor = 0,  
              GabaritBreastSize = 12,  
              GabaritHeight = 4,  
              HairColor = 0,  
              EyesColor = 7,  
              TrouserModel = 6722350,  
              GabaritLegsWidth = 1,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1133]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 0,  
              FeetModel = 6720814,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              HairType = 6721838,  
              SheetClient = [[basic_tryker_female.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -3833.711914,  
                x = 34971.32813,  
                z = -19.42586136,  
                Class = [[Position]],  
                InstanceId = [[Client1_1137]]
              },  
              WeaponRightHand = 6767150,  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 0,  
              JacketModel = 6723374,  
              InheritPos = 1,  
              ArmModel = 6722862,  
              Name = [[Be'Gany]],  
              Sex = 1,  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]],  
              MorphTarget7 = 2,  
              MorphTarget3 = 4,  
              Angle = 1.48972249
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_1140]],  
              ActivitiesId = {
              },  
              Tattoo = 30,  
              TrouserColor = 0,  
              MorphTarget5 = 1,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 13,  
              HandsModel = 6721582,  
              FeetColor = 0,  
              GabaritBreastSize = 1,  
              GabaritHeight = 9,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6722606,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                ChatSequences = {
                },  
                InstanceId = [[Client1_1138]],  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 7,  
              FeetModel = 6720814,  
              Speed = 0,  
              Level = 0,  
              Base = [[palette.entities.npcs.guards.t_guard_245]],  
              HairType = 6721838,  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmColor = 0,  
              WeaponLeftHand = 0,  
              Position = {
                y = -3832.09375,  
                x = 34969,  
                z = -19.8009758,  
                Class = [[Position]],  
                InstanceId = [[Client1_1142]]
              },  
              WeaponRightHand = 6765870,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              JacketModel = 6723374,  
              InheritPos = 1,  
              ArmModel = 6722862,  
              Name = [[Mac'Jorn]],  
              Sex = 0,  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]],  
              MorphTarget7 = 0,  
              MorphTarget3 = 1,  
              Angle = 1.48972249
            }
          },  
          ActivitiesId = {
          }
        },  
        {
          MissionText = [[Yo! on a besoin de potes pour attaquer un campement de bandits, va me chercher <mission target>]],  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_1157]]
          },  
          WaitValidationText = [[Bonjour homin.]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[<mission giver> veut vous voir au campement]],  
          MissionGiver = r2.RefId([[Client1_1145]]),  
          MissionTarget = r2.RefId([[Client1_1168]]),  
          InheritPos = 1,  
          MissionSucceedText = [[Ok, on arrive de suite!]],  
          Name = [[Mission: Talk to camp B]],  
          Position = {
            y = -3830.891602,  
            x = 34976.25781,  
            z = -19,  
            Class = [[Position]],  
            InstanceId = [[Client1_1158]]
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_1156]],  
          Components = {
          },  
          _Seed = 1146745385
        }
      },  
      Counters = {
      },  
      Version = 5,  
      Title = [[]],  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_1155]],  
        Class = [[TextManagerEntry]],  
        Text = [[hey mec, va nous cherchez des potes!]]
      }
    },  
    InstanceId = [[Client1_1011]]
  }
}