scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_331]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Jungle17]],  
      ShortDescription = [[]],  
      Time = 0,  
      Name = [[Promontories and Ridges (Jungle 17)]],  
      Season = [[Summer]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2EntryPoint06]]
    }
  },  
  InstanceId = [[Client1_322]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Name = [[New scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_320]]
  },  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    Class = [[Position]],  
    InstanceId = [[Client1_321]],  
    x = 0,  
    y = 0,  
    z = 0
  },  
  Versions = {
    Scenario = 2,  
    Act = 5,  
    MapDescription = 0,  
    Position = 0,  
    Location = 1,  
    TextManager = 0,  
    LogicEntityBehavior = 1,  
    DefaultFeature = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 3,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_318]]
  },  
  Acts = {
    {
      InstanceId = [[Client1_325]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_323]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_324]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_326]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Permanent]],  
      Version = 5
    },  
    {
      InstanceId = [[Client1_329]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_327]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_328]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[Client1_331]],  
      ManualWeather = 1,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_330]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Name = [[Act 1:Act 1]],  
      Version = 5
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_319]]
  }
}