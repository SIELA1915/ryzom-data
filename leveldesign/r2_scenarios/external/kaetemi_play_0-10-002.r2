scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_130]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_132]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ActivityStep = 1,  
    Position = 0,  
    NpcCustom = 0,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 36,  
      Behavior = {
        InstanceId = [[Client1_133]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_135]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_143]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_144]],  
                x = 21571.10938,  
                y = -1363.609375,  
                z = 109.3125
              },  
              Angle = 2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_141]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_147]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_148]],  
                x = 21568.9375,  
                y = -1362.328125,  
                z = 108.375
              },  
              Angle = 2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_145]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_150]],  
              Name = [[Spawn Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_152]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_153]],  
                    x = 21543.64063,  
                    y = -1329.953125,  
                    z = 95.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_155]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_156]],  
                    x = 21597.29688,  
                    y = -1342.625,  
                    z = 110.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_158]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_159]],  
                    x = 21601.54688,  
                    y = -1402.625,  
                    z = 123.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_161]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_162]],  
                    x = 21528.46875,  
                    y = -1387.296875,  
                    z = 102.703125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_149]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_165]],  
              Base = [[palette.entities.botobjects.karavan_mirador]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_166]],  
                x = 21579.59375,  
                y = -1376.65625,  
                z = 114.046875
              },  
              Angle = 2.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[#01 Mira Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_169]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_170]],  
                x = 21579.57813,  
                y = -1376.65625,  
                z = 114.015625
              },  
              Angle = 0.9424778223,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[#00 Kamivan]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_188]],  
              Name = [[Mira Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_190]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_191]],  
                    x = 21467.35938,  
                    y = -1134.828125,  
                    z = 73.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_193]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_194]],  
                    x = 21509.28125,  
                    y = -1075.375,  
                    z = 73.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_196]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_197]],  
                    x = 21448.5625,  
                    y = -1008.75,  
                    z = 61.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_199]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_200]],  
                    x = 21344.5625,  
                    y = -1038.078125,  
                    z = 61.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_202]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_203]],  
                    x = 21356.03125,  
                    y = -1121.53125,  
                    z = 62.375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_187]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_281]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_282]],  
                x = 21573.4375,  
                y = -1362.671875,  
                z = 110.28125
              },  
              Angle = 1.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_279]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 2,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 2,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 8,  
              HairType = 6700590,  
              HairColor = 5,  
              Tattoo = 11,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 7,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 2,  
              FeetColor = 0,  
              WeaponRightHand = 6756910,  
              WeaponLeftHand = 0,  
              Name = [[desert guard 2]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_285]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_286]],  
                x = 21574.71875,  
                y = -1362.65625,  
                z = 110.734375
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_283]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[emissary 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_289]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_290]],  
                x = 21574.40625,  
                y = -1361.46875,  
                z = 110.609375
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_287]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_293]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_294]],  
                x = 21574.0625,  
                y = -1360.65625,  
                z = 110.46875
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_291]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[protector 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_297]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_298]],  
                x = 21573.96875,  
                y = -1360.078125,  
                z = 110.40625
              },  
              Angle = 1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_295]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[voice of Jena 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_301]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_302]],  
                x = 21576.53125,  
                y = -1363.171875,  
                z = 111.375
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_299]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Ardent Genius 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_305]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_306]],  
                x = 21576.5,  
                y = -1362.625,  
                z = 111.34375
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_303]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Burning Salamander 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_309]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_310]],  
                x = 21575.76563,  
                y = -1361.34375,  
                z = 111.078125
              },  
              Angle = 2,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_307]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Diaphanous Salamander 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_313]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_314]],  
                x = 21581.48438,  
                y = -1361.046875,  
                z = 112.796875
              },  
              Angle = 2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_311]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Hairy Colossus 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_317]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_318]],  
                x = 21575.48438,  
                y = -1359.359375,  
                z = 110.96875
              },  
              Angle = 2.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_315]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Horned Faun 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_321]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_322]],  
                x = 21584.28125,  
                y = -1357.03125,  
                z = 113.0625
              },  
              Angle = 3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_319]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Mossy Colossus 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_325]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_326]],  
                x = 21574.625,  
                y = -1358.296875,  
                z = 110.65625
              },  
              Angle = 3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_323]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Obscure Faun 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_329]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_3_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_330]],  
                x = 21578.71875,  
                y = -1357.96875,  
                z = 111.890625
              },  
              Angle = 3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_327]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Spectral Genius 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_333]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_334]],  
                x = 21575.39063,  
                y = -1368.25,  
                z = 111.203125
              },  
              Angle = 1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_331]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[emissary 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_337]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_h_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_338]],  
                x = 21573.82813,  
                y = -1367.703125,  
                z = 110.609375
              },  
              Angle = 1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_335]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[guard 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_341]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_342]],  
                x = 21573.32813,  
                y = -1366.421875,  
                z = 110.328125
              },  
              Angle = 1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_339]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[protector 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_345]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_346]],  
                x = 21574.89063,  
                y = -1365.703125,  
                z = 110.890625
              },  
              Angle = 1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_343]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[voice of Jena 2]]
            }
          },  
          InstanceId = [[Client1_136]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_268]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_206]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_207]],  
                x = 21432.40625,  
                y = -1080.203125,  
                z = 66.296875
              },  
              Angle = 5.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_204]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_273]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_274]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kaeteketh]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_216]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_217]],  
                x = 21444.46875,  
                y = -1087.546875,  
                z = 69.359375
              },  
              Angle = -0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_214]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_212]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_213]],  
                x = 21434.9375,  
                y = -1092.59375,  
                z = 68.46875
              },  
              Angle = -0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_210]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_228]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_229]],  
                x = 21455.8125,  
                y = -1097.765625,  
                z = 73.15625
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_226]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_224]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_225]],  
                x = 21448.35938,  
                y = -1103.296875,  
                z = 72.96875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_222]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_220]],  
              Base = [[palette.entities.creatures.ckeif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_221]],  
                x = 21442.01563,  
                y = -1108.859375,  
                z = 73.421875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_218]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_244]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_245]],  
                x = 21462.48438,  
                y = -1104.25,  
                z = 74.5625
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_242]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_240]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_241]],  
                x = 21456.96875,  
                y = -1109.296875,  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_238]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_236]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_237]],  
                x = 21450.70313,  
                y = -1113.15625,  
                z = 74.578125
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_234]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_232]],  
              Base = [[palette.entities.creatures.ckeif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_233]],  
                x = 21444.76563,  
                y = -1117.578125,  
                z = 74.875
              },  
              Angle = -0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_230]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_264]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_265]],  
                x = 21470.40625,  
                y = -1108.15625,  
                z = 75.375
              },  
              Angle = -1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_262]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_260]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_261]],  
                x = 21465.07813,  
                y = -1114.484375,  
                z = 75.296875
              },  
              Angle = -1.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_258]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_256]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_257]],  
                x = 21457.82813,  
                y = -1117.90625,  
                z = 75.09375
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_252]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_253]],  
                x = 21452.20313,  
                y = -1122.125,  
                z = 75.125
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_248]],  
              Base = [[palette.entities.creatures.ckeif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_249]],  
                x = 21446.25,  
                y = -1124.9375,  
                z = 75.328125
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_246]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipucker]],  
              ActivitiesId = {
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_267]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_266]],  
            Type = [[]],  
            ZoneId = [[]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        }
      },  
      Events = {
      },  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_134]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Counters = {
      },  
      Version = 3,  
      Name = [[Permanent]],  
      Title = [[Permanent]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_131]]
  }
}