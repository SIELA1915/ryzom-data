scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_869]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_871]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    NpcCustom = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 22,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_872]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_878]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_876]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              Position = {
                y = -2013.53125,  
                x = 26956.1875,  
                InstanceId = [[Client1_879]],  
                Class = [[Position]],  
                z = -8.640625
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_882]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_880]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_883]],  
                x = 26958.20313,  
                y = -2021.859375,  
                z = -9.296875
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_886]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_884]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_887]],  
                x = 26953.98438,  
                y = -2005.234375,  
                z = -8.234375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_890]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_888]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_891]],  
                x = 26960.29688,  
                y = -2030.15625,  
                z = -9.4375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_894]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_892]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_895]],  
                x = 26952,  
                y = -1996.828125,  
                z = -7.703125
              },  
              Angle = -2.890625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_898]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_896]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 6]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_899]],  
                x = 26957.53125,  
                y = -2002.328125,  
                z = -8.390625
              },  
              Angle = -1.870344758,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_902]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_900]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_903]],  
                x = 26962.92188,  
                y = -2007.515625,  
                z = -8.671875
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_906]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_904]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 8]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_907]],  
                x = 26965.01563,  
                y = -2015.890625,  
                z = -9.109375
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_910]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_908]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 9]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_911]],  
                x = 26962.4375,  
                y = -2022.71875,  
                z = -9.78125
              },  
              Angle = 2.265625,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_918]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 0,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_916]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = -2.96875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756398,  
              ArmColor = 1,  
              Name = [[Guard]],  
              Position = {
                y = -2010.59375,  
                x = 26959.96875,  
                InstanceId = [[Client1_919]],  
                Class = [[Position]],  
                z = -8.828125
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              InstanceId = [[Client1_922]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_920]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 10]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_923]],  
                x = 26952.15625,  
                y = -1988.375,  
                z = -7.28125
              },  
              Angle = 2.836098909,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_926]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_924]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 11]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_927]],  
                x = 26955.3125,  
                y = -1980.3125,  
                z = -7.609375
              },  
              Angle = 2.699130297,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_930]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_928]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 12]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_931]],  
                x = 26966.8125,  
                y = -1967.78125,  
                z = -8.703125
              },  
              Angle = 2.109375,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_934]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_932]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[karavan big wall 13]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_935]],  
                x = 26960.125,  
                y = -1973.21875,  
                z = -8.546875
              },  
              Angle = 2.382973909,  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_950]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_951]],  
                x = 26978.60938,  
                y = -1964.21875,  
                z = -5.921875
              },  
              Angle = -1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_948]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_954]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_955]],  
                x = 26971.85938,  
                y = -2009.765625,  
                z = -7.078125
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_952]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan banner 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_958]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_959]],  
                x = 26961.35938,  
                y = -1992.5,  
                z = -7.84375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_956]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan banner 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_962]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_963]],  
                x = 26965.0625,  
                y = -2037.015625,  
                z = -9.515625
              },  
              Angle = -2.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_960]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 14]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_966]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_967]],  
                x = 26973.09375,  
                y = -2039.265625,  
                z = -9.203125
              },  
              Angle = -1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_964]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 15]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_970]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_971]],  
                x = 26981.67188,  
                y = -2037.6875,  
                z = -8.5625
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_968]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 16]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_974]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_975]],  
                x = 26989.98438,  
                y = -2035.234375,  
                z = -8.484375
              },  
              Angle = -1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_972]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 17]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_990]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_991]],  
                x = 27002.39063,  
                y = -2035.140625,  
                z = -5.90625
              },  
              Angle = -1.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_988]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]]
            }
          },  
          InstanceId = [[Client1_873]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_870]]
  }
}