scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 7,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Road To HGL 8]],  
              InstanceId = [[Client1_264]],  
              Class = [[Road]],  
              Position = {
                y = -0.53125,  
                x = 1.046875,  
                InstanceId = [[Client1_263]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_266]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.984375,  
                    x = 26734.03125,  
                    InstanceId = [[Client1_267]],  
                    Class = [[Position]],  
                    z = 90.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_269]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1317.125,  
                    x = 26729.51563,  
                    InstanceId = [[Client1_270]],  
                    Class = [[Position]],  
                    z = 90.890625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGR 8]],  
              InstanceId = [[Client1_429]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_431]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1311.640625,  
                    x = 26738.5625,  
                    InstanceId = [[Client1_432]],  
                    Class = [[Position]],  
                    z = 91.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_434]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1311.640625,  
                    x = 26738.5625,  
                    InstanceId = [[Client1_435]],  
                    Class = [[Position]],  
                    z = 91.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_437]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1310.8125,  
                    x = 26731.3125,  
                    InstanceId = [[Client1_438]],  
                    Class = [[Position]],  
                    z = 91.40625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 2.609375,  
                InstanceId = [[Client1_428]],  
                Class = [[Position]],  
                z = 0.03125
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGR 6]],  
              InstanceId = [[Client1_442]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_444]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1310.78125,  
                    x = 26733.90625,  
                    InstanceId = [[Client1_445]],  
                    Class = [[Position]],  
                    z = 91.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_450]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1308.65625,  
                    x = 26723.875,  
                    InstanceId = [[Client1_451]],  
                    Class = [[Position]],  
                    z = 91.703125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_441]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGR 4]],  
              InstanceId = [[Client1_454]],  
              Class = [[Road]],  
              Position = {
                y = 0.515625,  
                x = 0.203125,  
                InstanceId = [[Client1_453]],  
                Class = [[Position]],  
                z = 0.046875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_456]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1309.1875,  
                    x = 26723.65625,  
                    InstanceId = [[Client1_457]],  
                    Class = [[Position]],  
                    z = 91.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_459]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.265625,  
                    x = 26715.89063,  
                    InstanceId = [[Client1_460]],  
                    Class = [[Position]],  
                    z = 91.796875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGR 2]],  
              InstanceId = [[Client1_466]],  
              Class = [[Road]],  
              Position = {
                y = 0.5,  
                x = 0.125,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 0.046875
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_468]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.28125,  
                    x = 26715.95313,  
                    InstanceId = [[Client1_469]],  
                    Class = [[Position]],  
                    z = 91.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_471]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.28125,  
                    x = 26715.95313,  
                    InstanceId = [[Client1_472]],  
                    Class = [[Position]],  
                    z = 91.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_474]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1305.375,  
                    x = 26707.375,  
                    InstanceId = [[Client1_475]],  
                    Class = [[Position]],  
                    z = 91.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGL 6]],  
              InstanceId = [[Client1_490]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_492]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1316.578125,  
                    x = 26731.5625,  
                    InstanceId = [[Client1_493]],  
                    Class = [[Position]],  
                    z = 90.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_498]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1314.203125,  
                    x = 26723.28125,  
                    InstanceId = [[Client1_499]],  
                    Class = [[Position]],  
                    z = 91.046875
                  }
                }
              },  
              Position = {
                y = -1.09375,  
                x = -1.140625,  
                InstanceId = [[Client1_489]],  
                Class = [[Position]],  
                z = -0.03125
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGL 4]],  
              InstanceId = [[Client1_502]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_504]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1315.28125,  
                    x = 26722.14063,  
                    InstanceId = [[Client1_505]],  
                    Class = [[Position]],  
                    z = 91.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_507]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1315.28125,  
                    x = 26722.14063,  
                    InstanceId = [[Client1_508]],  
                    Class = [[Position]],  
                    z = 91.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_510]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1313.15625,  
                    x = 26714.15625,  
                    InstanceId = [[Client1_511]],  
                    Class = [[Position]],  
                    z = 91.1875
                  }
                }
              },  
              Position = {
                y = -0.015625,  
                x = 0,  
                InstanceId = [[Client1_501]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To HGL 2]],  
              InstanceId = [[Client1_514]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_516]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1313.15625,  
                    x = 26714,  
                    InstanceId = [[Client1_517]],  
                    Class = [[Position]],  
                    z = 91.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_519]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1313.15625,  
                    x = 26714,  
                    InstanceId = [[Client1_520]],  
                    Class = [[Position]],  
                    z = 91.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_522]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1311.015625,  
                    x = 26705.625,  
                    InstanceId = [[Client1_523]],  
                    Class = [[Position]],  
                    z = 91.3125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0.078125,  
                InstanceId = [[Client1_513]],  
                Class = [[Position]],  
                z = -0.015625
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To Priest Left]],  
              InstanceId = [[Client1_526]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_528]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1311,  
                    x = 26705.76563,  
                    InstanceId = [[Client1_529]],  
                    Class = [[Position]],  
                    z = 91.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_531]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1311,  
                    x = 26705.76563,  
                    InstanceId = [[Client1_532]],  
                    Class = [[Position]],  
                    z = 91.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_534]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1307.921875,  
                    x = 26699.5,  
                    InstanceId = [[Client1_535]],  
                    Class = [[Position]],  
                    z = 91.375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_525]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Road To Priest Right]],  
              InstanceId = [[Client1_538]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_540]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1304.859375,  
                    x = 26707.51563,  
                    InstanceId = [[Client1_541]],  
                    Class = [[Position]],  
                    z = 91.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_546]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1305.453125,  
                    x = 26699.85938,  
                    InstanceId = [[Client1_547]],  
                    Class = [[Position]],  
                    z = 91.359375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_537]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_885]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_887]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1214.984375,  
                    x = 26663.9375,  
                    InstanceId = [[Client1_888]],  
                    Class = [[Position]],  
                    z = 81.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_890]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1214.984375,  
                    x = 26663.9375,  
                    InstanceId = [[Client1_891]],  
                    Class = [[Position]],  
                    z = 81.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_893]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1233.859375,  
                    x = 26690.3125,  
                    InstanceId = [[Client1_894]],  
                    Class = [[Position]],  
                    z = 89.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_896]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1257.59375,  
                    x = 26698.04688,  
                    InstanceId = [[Client1_897]],  
                    Class = [[Position]],  
                    z = 90.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_899]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1298.625,  
                    x = 26697.70313,  
                    InstanceId = [[Client1_900]],  
                    Class = [[Position]],  
                    z = 91.15625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_884]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_4]]
    },  
    {
      Cost = 16,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client3_39]],  
        [[Client1_153]],  
        [[Client1_261]],  
        [[Client1_283]],  
        [[Client1_377]],  
        [[Client2_281]],  
        [[Client2_351]],  
        [[Client1_854]],  
        [[Client1_872]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1300.15625,  
                x = 26708.79688,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 91.703125
              },  
              Name = [[Honour Guard Right 2]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -1.8125,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1317.515625,  
                x = 26703.79688,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 90.921875
              },  
              Name = [[Honour Guard Left 1]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = 1.28125,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1301.921875,  
                x = 26717.29688,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 92.140625
              },  
              Name = [[Honour Guard Right 4]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -1.875,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_62]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_60]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1319.0625,  
                x = 26712.29688,  
                InstanceId = [[Client1_63]],  
                Class = [[Position]],  
                z = 90.828125
              },  
              Name = [[Honour Guard Left 2]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = 1.234375,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_70]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_68]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1320.640625,  
                x = 26720.89063,  
                InstanceId = [[Client1_71]],  
                Class = [[Position]],  
                z = 90.828125
              },  
              Name = [[Honour Guard Left 3]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = 1.296875,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_74]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_72]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1322.40625,  
                x = 26729.45313,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 90.78125
              },  
              Name = [[Honour Guard Left 4]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = 1.421875,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_86]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_84]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_594]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_595]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_596]],  
                            Who = [[Client1_86]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Honour Guard Right  6 says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1303.671875,  
                x = 26725.76563,  
                InstanceId = [[Client1_87]],  
                Class = [[Position]],  
                z = 91.96875
              },  
              Name = [[Honour Guard Right  6]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -1.90625,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_90]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_88]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_599]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of activity step Activity 1 : Follow Route Road To HGR 8 without time limit' event of Bride]],  
                    InstanceId = [[Client1_597]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_601]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_379]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 2,  
                        InstanceId = [[Client1_380]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_381]],  
                            Who = [[Client1_90]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  2sec) Honour Guard Right 8 says Merh...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_377]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_378]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[Client1_379]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Position = {
                y = -1306.25,  
                x = 26734.82813,  
                InstanceId = [[Client1_91]],  
                Class = [[Position]],  
                z = 91.625
              },  
              Name = [[Honour Guard Right 8]],  
              BotAttackable = 0,  
              PlayerAttackable = 0,  
              Angle = -1.734375,  
              Base = [[palette.entities.creatures.chbdc2]],  
              ActivitiesId = {
                [[Client1_377]]
              }
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_227]],  
              ActivitiesId = {
                [[Client1_261]]
              },  
              HairType = 5623598,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 4,  
              HandsModel = 6724398,  
              FeetColor = 5,  
              GabaritBreastSize = 0,  
              GabaritHeight = 12,  
              HairColor = 0,  
              EyesColor = 1,  
              TrouserModel = 6727470,  
              GabaritLegsWidth = 0,  
              HandsColor = 5,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_225]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_261]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_439]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_429]],  
                        Name = [[Activity 1 : Follow Route Road To HGR 8 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_452]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_442]],  
                        Name = [[Activity 2 : Follow Route Road To HGR 6 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_464]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_454]],  
                        Name = [[Activity 3 : Follow Route Road To HGR 4 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_476]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_466]],  
                        Name = [[Activity 4 : Follow Route Road To HGR 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_548]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_538]],  
                        Name = [[Activity 5 : Follow Route Road To Priest Right without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_415]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_416]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Love]],  
                            InstanceId = [[Client1_417]],  
                            Who = [[Client1_227]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_235]],  
                            Says = [[Client1_819]]
                          }
                        },  
                        Name = [[Chat 1 : (after  0sec) Bride says *kis...]]
                      }
                    }
                  },  
                  {
                    Name = [[Chat2]],  
                    InstanceId = [[Client1_919]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_920]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Wave]],  
                            InstanceId = [[Client1_921]],  
                            Who = [[Client1_227]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_351]],  
                            Says = [[Client1_922]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Bride says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 5 : Follow Route Road To Priest Right without time limit' event]],  
                    InstanceId = [[Client1_599]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_600]],  
                          Value = [[Client1_379]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_601]],  
                        Entity = [[Client1_90]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_598]],  
                      Value = [[Client1_548]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'end of activity step Activity 5 : Follow Route Road To Priest Right without time limit' event]],  
                    InstanceId = [[Client1_830]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_831]],  
                          Value = [[Client1_396]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_832]],  
                        Entity = [[Client1_351]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_829]],  
                      Value = [[Client1_548]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 3 : on 'end of activity step Activity 4 : Follow Route Road To HGR 2 without time limit' event]],  
                    InstanceId = [[Client1_876]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_875]],  
                      Value = [[Client1_476]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[Event 4 : on 'end of activity step Activity 4 : Follow Route Road To HGR 2 without time limit' event]],  
                    InstanceId = [[Client1_881]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_880]],  
                      Value = [[Client1_476]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_882]],  
                          Value = [[Client1_872]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_883]],  
                        Entity = [[Client1_835]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_419]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of chat step Chat 2 : (after  4sec) Voice Of Jena says you ...' event of Voice Of Jena]],  
                    InstanceId = [[Client1_422]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_421]]
                  },  
                  {
                    LogicEntityAction = [[Client1_926]],  
                    Name = [[Trigger 2 : Begin chat Chat2 on 'End of chat Chat1' event of Raj]],  
                    InstanceId = [[Client1_924]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_928]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 1,  
              FeetModel = 6726702,  
              Angle = -3.046875,  
              Base = [[palette.entities.npcs.civils.t_civil_220]],  
              Tattoo = 9,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Position = {
                y = -1312.765625,  
                x = 26746.3125,  
                InstanceId = [[Client1_228]],  
                Class = [[Position]],  
                z = 91.609375
              },  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 2,  
              JacketModel = 6728494,  
              InheritPos = 1,  
              Sex = 1,  
              Name = [[Bride]],  
              BotAttackable = 1,  
              ArmColor = 5,  
              ArmModel = 6727982,  
              SheetClient = [[basic_tryker_female.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_235]],  
              ActivitiesId = {
                [[Client1_283]]
              },  
              HairType = 5621806,  
              TrouserColor = 2,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 6702894,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 4,  
              HairColor = 3,  
              EyesColor = 4,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 2,  
              HandsColor = 5,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_233]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_283]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_284]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_264]],  
                        Name = [[Activity 1 : Follow Route Road To HGL 8 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_500]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_490]],  
                        Name = [[Activity 2 : Follow Route Road To HGL 6 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_512]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_502]],  
                        Name = [[Activity 3 : Follow Route Road To HGL 4 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_524]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_514]],  
                        Name = [[Activity 4 : Follow Route Road To HGL 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_536]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_526]],  
                        Name = [[Activity 5 : Follow Route Road To Priest Left without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_412]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_413]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Love]],  
                            InstanceId = [[Client1_414]],  
                            Who = [[Client1_235]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_227]],  
                            Says = [[Client1_819]]
                          }
                        },  
                        Name = [[Chat 1 : (after  0sec) Other Bride says *kis...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 3 : Follow Route Road To HGL 4 without time limit' event]],  
                    InstanceId = [[Client1_620]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_621]],  
                          Value = [[Client1_615]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_622]],  
                        Entity = [[Client1_613]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_619]],  
                      Value = [[Client1_512]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'end of activity step Activity 4 : Follow Route Road To HGL 2 without time limit' event]],  
                    InstanceId = [[Client1_625]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_626]],  
                          Value = [[Client1_608]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_627]],  
                        Entity = [[Client1_606]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_624]],  
                      Value = [[Client1_524]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 3 : on 'end of activity step Activity 2 : Follow Route Road To HGL 6 without time limit' event]],  
                    InstanceId = [[Client1_639]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_640]],  
                          Value = [[Client1_632]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_641]],  
                        Entity = [[Client1_630]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_638]],  
                      Value = [[Client1_500]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 4 : on 'end of activity step Activity 1 : Follow Route Road To HGL 8 without time limit' event]],  
                    InstanceId = [[Client1_653]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_654]],  
                          Value = [[Client1_646]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_655]],  
                        Entity = [[Client1_644]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_652]],  
                      Value = [[Client1_284]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[Event 5 : on 'end of activity step Activity 5 : Follow Route Road To Priest Left without time limit' event]],  
                    InstanceId = [[Client1_822]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_821]],  
                      Value = [[Client1_536]],  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_419]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of chat step Chat 2 : (after  4sec) Voice Of Jena says you ...' event of Voice Of Jena]],  
                    InstanceId = [[Client1_425]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_424]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6702382,  
              Angle = -3.65625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 3,  
              MorphTarget3 = 6,  
              MorphTarget7 = 0,  
              Position = {
                y = -1323.859375,  
                x = 26744.29688,  
                InstanceId = [[Client1_236]],  
                Class = [[Position]],  
                z = 90.578125
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 7,  
              JacketModel = 6704686,  
              InheritPos = 1,  
              Sex = 1,  
              Name = [[Other Bride]],  
              BotAttackable = 1,  
              ArmColor = 2,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_351]],  
              ActivitiesId = {
              },  
              HairType = 5622574,  
              TrouserColor = 2,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 6,  
              HandsModel = 0,  
              FeetColor = 2,  
              GabaritBreastSize = 13,  
              GabaritHeight = 10,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5610030,  
              GabaritLegsWidth = 7,  
              HandsColor = 2,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_349]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_396]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_397]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_398]],  
                            Who = [[Client1_351]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_400]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Voice Of Jena says Dear...]]
                      },  
                      {
                        Time = 4,  
                        InstanceId = [[Client1_408]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Rice]],  
                            InstanceId = [[Client1_409]],  
                            Who = [[Client1_351]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_410]]
                          }
                        },  
                        Name = [[Chat 2 : (after  4sec) Voice Of Jena says you ...]]
                      },  
                      {
                        Time = 10,  
                        InstanceId = [[Client1_863]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Alert]],  
                            InstanceId = [[Client1_864]],  
                            Who = [[Client1_351]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_865]]
                          }
                        },  
                        Name = [[Chat 3 : (after  10sec) Voice Of Jena says AAAA...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of chat step Chat 2 : (after  4sec) Voice Of Jena says you ...' event]],  
                    InstanceId = [[Client1_419]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_420]],  
                          Value = [[Client1_415]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_421]],  
                        Entity = [[Client1_227]],  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_423]],  
                          Value = [[Client1_412]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_424]],  
                        Entity = [[Client1_235]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Event = {
                      Type = [[end of chat step]],  
                      InstanceId = [[Client1_418]],  
                      Value = [[Client1_408]],  
                      Class = [[EventType]]
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_904]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[]],  
                      InstanceId = [[Client1_903]],  
                      Value = [[]],  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_830]],  
                    Name = [[Trigger 1 : Begin chat Chat1 on 'End of activity step Activity 5 : Follow Route Road To Priest Right without time limit' event of Bride]],  
                    InstanceId = [[Client1_828]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_832]]
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 5,  
              FeetModel = 5653038,  
              Angle = -0.3125,  
              Base = [[palette.entities.npcs.civils.m_civil_20]],  
              Tattoo = 0,  
              MorphTarget3 = 1,  
              MorphTarget7 = 3,  
              Position = {
                y = -1306.28125,  
                x = 26697.64063,  
                InstanceId = [[Client1_352]],  
                Class = [[Position]],  
                z = 91.453125
              },  
              GabaritTorsoWidth = 1,  
              MorphTarget2 = 4,  
              JacketModel = 0,  
              InheritPos = 1,  
              Sex = 1,  
              Name = [[Voice Of Jena]],  
              BotAttackable = 0,  
              ArmColor = 4,  
              ArmModel = 0,  
              SheetClient = [[basic_matis_female.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_606]],  
              ActivitiesId = {
              },  
              HairType = 6958,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 13,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 7,  
              GabaritHeight = 6,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 1,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_604]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_608]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_609]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_610]],  
                            Who = [[Client1_606]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Bodoc1 says Merh...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 2,  
              MorphTarget4 = 1,  
              FeetModel = 5653550,  
              Angle = -2.046875,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 15,  
              MorphTarget3 = 7,  
              MorphTarget7 = 6,  
              Position = {
                y = -1317.53125,  
                x = 26704.21875,  
                InstanceId = [[Client1_607]],  
                Class = [[Position]],  
                z = 90.90625
              },  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              JacketModel = 0,  
              InheritPos = 1,  
              Sex = 0,  
              Name = [[Bodoc1]],  
              BotAttackable = 0,  
              ArmColor = 2,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_male.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_613]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 2,  
              MorphTarget5 = 4,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 3,  
              HandsModel = 0,  
              FeetColor = 5,  
              GabaritBreastSize = 13,  
              GabaritHeight = 11,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 4,  
              HandsColor = 5,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_611]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_615]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_616]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_617]],  
                            Who = [[Client1_613]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Bodoc2 says Merh...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 3,  
              FeetModel = 5653550,  
              Angle = -1.40625,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 12,  
              MorphTarget3 = 2,  
              MorphTarget7 = 3,  
              Position = {
                y = -1319.171875,  
                x = 26712.39063,  
                InstanceId = [[Client1_614]],  
                Class = [[Position]],  
                z = 90.8125
              },  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 1,  
              JacketModel = 5614638,  
              InheritPos = 1,  
              Sex = 1,  
              Name = [[Bodoc2]],  
              BotAttackable = 0,  
              ArmColor = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_female.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_630]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 6,  
              HandsModel = 5613870,  
              FeetColor = 5,  
              GabaritBreastSize = 12,  
              GabaritHeight = 13,  
              HairColor = 0,  
              EyesColor = 3,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 11,  
              HandsColor = 1,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_628]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_632]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_633]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_634]],  
                            Who = [[Client1_630]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) tryker-dressed civilian 3 says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 0,  
              Angle = -1.484375,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 12,  
              MorphTarget3 = 7,  
              MorphTarget7 = 7,  
              Position = {
                y = -1320.6875,  
                x = 26721.125,  
                InstanceId = [[Client1_631]],  
                Class = [[Position]],  
                z = 90.8125
              },  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 1,  
              JacketModel = 5614638,  
              InheritPos = 1,  
              Sex = 0,  
              Name = [[Bodoc3]],  
              BotAttackable = 0,  
              ArmColor = 2,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_male.creature]],  
              PlayerAttackable = 0
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_644]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 12,  
              HandsModel = 5613870,  
              FeetColor = 3,  
              GabaritBreastSize = 0,  
              GabaritHeight = 4,  
              HairColor = 2,  
              EyesColor = 4,  
              TrouserModel = 5614126,  
              GabaritLegsWidth = 14,  
              HandsColor = 2,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_642]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_646]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 1,  
                        InstanceId = [[Client1_647]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_648]],  
                            Who = [[Client1_644]],  
                            Class = [[ChatAction]],  
                            Facing = [[]],  
                            Says = [[Client1_382]]
                          }
                        },  
                        Name = [[Chat 1 : (after  1sec) Bodoc4 says Merh...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Angle = -1.078125,  
              Base = [[palette.entities.npcs.civils.t_civil_20]],  
              Tattoo = 20,  
              MorphTarget3 = 2,  
              MorphTarget7 = 4,  
              Position = {
                y = -1322.359375,  
                x = 26729.39063,  
                InstanceId = [[Client1_645]],  
                Class = [[Position]],  
                z = 90.78125
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 5614638,  
              InheritPos = 1,  
              Sex = 0,  
              Name = [[Bodoc4]],  
              BotAttackable = 0,  
              ArmColor = 2,  
              ArmModel = 0,  
              SheetClient = [[basic_tryker_male.creature]],  
              PlayerAttackable = 0
            },  
            {
              InstanceId = [[Client1_835]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_833]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_881]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 4 : Follow Route Road To HGR 2 without time limit' event of Bride]],  
                    InstanceId = [[Client1_879]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_883]]
                  },  
                  {
                    LogicEntityAction = [[Client1_915]],  
                    Name = [[Trigger 2 :  on 'End of activity step Activity 1 : Follow Route Route 1 without time limit' event of Raj]],  
                    InstanceId = [[Client1_918]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_917]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_909]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_910]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Bow]],  
                            InstanceId = [[Client1_911]],  
                            Who = [[Client1_835]],  
                            Class = [[ChatAction]],  
                            Facing = [[Client1_351]],  
                            Says = [[Client1_912]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Raj says ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'End of activity step Activity 1 : Follow Route Route 1 without time limit' event]],  
                    InstanceId = [[Client1_915]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_914]],  
                      Value = [[Client1_907]],  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_916]],  
                          Value = [[Client1_909]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_917]],  
                        Entity = [[Client1_835]],  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_926]],  
                    Event = {
                      Type = [[end of chat sequence]],  
                      InstanceId = [[Client1_925]],  
                      Value = [[Client1_909]],  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin chat sequence]],  
                          InstanceId = [[Client1_927]],  
                          Value = [[Client1_919]],  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_928]],  
                        Entity = [[Client1_227]],  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_854]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_856]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 1 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client1_872]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_907]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[Client1_885]],  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_908]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = [[]],  
                        ActivityZoneId = [[]],  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Raj]],  
              Position = {
                y = -1213.359375,  
                x = 26644.90625,  
                InstanceId = [[Client1_836]],  
                Class = [[Position]],  
                z = 78.421875
              },  
              Angle = -0.28125,  
              Base = [[palette.entities.creatures.ccgpf5]],  
              ActivitiesId = {
                [[Client1_854]],  
                [[Client1_872]]
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_6]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 28,  
        InstanceId = [[Client1_158]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_161]],  
        Class = [[TextManagerEntry]],  
        Text = [[Moo]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_162]],  
        Class = [[TextManagerEntry]],  
        Text = [[Moo]]
      },  
      {
        Count = 20,  
        InstanceId = [[Client1_382]],  
        Class = [[TextManagerEntry]],  
        Text = [[Merh]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_383]],  
        Class = [[TextManagerEntry]],  
        Text = [[Merh]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Dearly beloved, yada yada]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_400]],  
        Class = [[TextManagerEntry]],  
        Text = [[Dearly beloved, yada yada]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_410]],  
        Class = [[TextManagerEntry]],  
        Text = [[you may now kiss the bride]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_411]],  
        Class = [[TextManagerEntry]],  
        Text = [[you may now kiss the bride]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_819]],  
        Class = [[TextManagerEntry]],  
        Text = [[*kiss*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_820]],  
        Class = [[TextManagerEntry]],  
        Text = [[*kiss*]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_861]],  
        Class = [[TextManagerEntry]],  
        Text = [[AAAAAAAAARGH! WE'RE UNDER ATTACK!!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_862]],  
        Class = [[TextManagerEntry]],  
        Text = [[AAAAAAAAARGH! WE'RE UNDER ATTACK!!!]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_865]],  
        Class = [[TextManagerEntry]],  
        Text = [[AAAAAAAARGH!!! WE'RE UNDER ATTACK!!!!!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_866]],  
        Class = [[TextManagerEntry]],  
        Text = [[AAAAAAAARGH!!! WE'RE UNDER ATTACK!!!!!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_912]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hi there! :D]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_913]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hi there! :D]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_922]],  
        Class = [[TextManagerEntry]],  
        Text = [[oh wait that's just Raj, my pet lizard]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_923]],  
        Class = [[TextManagerEntry]],  
        Text = [[oh wait that's just Raj, my pet lizard]]
      }
    }
  }
}