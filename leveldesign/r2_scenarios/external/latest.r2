scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts13]],  
      Time = 0,  
      Name = [[Dunes of Temperance (Desert 13)]],  
      Season = [[spring]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2NorthWestEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_3]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  Name = [[New scenario]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 1,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_1]]
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 5,  
    Behavior = 1,  
    UserTrigger = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManager = 0,  
    EventType = 0,  
    DefaultFeature = 0,  
    Timer = 0,  
    Fauna = 0,  
    Creature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    NpcCustom = 0,  
    ActionType = 0,  
    ActionStep = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 1,  
    ConditionType = 0,  
    Position = 0,  
    Location = 0,  
    NpcCreature = 0,  
    LogicEntityBehavior = 1,  
    ConditionStep = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_8]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_6]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Name = [[Permanent]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_36]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_38]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1927.890625,  
                    x = 28843.48438,  
                    InstanceId = [[Client1_39]],  
                    Class = [[Position]],  
                    z = 75.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_41]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1923.46875,  
                    x = 28840.92188,  
                    InstanceId = [[Client1_42]],  
                    Class = [[Position]],  
                    z = 75.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_44]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1926.90625,  
                    x = 28837.09375,  
                    InstanceId = [[Client1_45]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_47]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1938.0625,  
                    x = 28839.40625,  
                    InstanceId = [[Client1_48]],  
                    Class = [[Position]],  
                    z = 76.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_50]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1939.03125,  
                    x = 28844.84375,  
                    InstanceId = [[Client1_51]],  
                    Class = [[Position]],  
                    z = 76.234375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_55]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_57]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1927.21875,  
                    x = 28855.09375,  
                    InstanceId = [[Client1_58]],  
                    Class = [[Position]],  
                    z = 74.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_60]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1925.921875,  
                    x = 28847.5,  
                    InstanceId = [[Client1_61]],  
                    Class = [[Position]],  
                    z = 75.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_63]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1921.515625,  
                    x = 28843.25,  
                    InstanceId = [[Client1_64]],  
                    Class = [[Position]],  
                    z = 75.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_66]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1921.015625,  
                    x = 28846.29688,  
                    InstanceId = [[Client1_67]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_69]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1925.03125,  
                    x = 28855.32813,  
                    InstanceId = [[Client1_70]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_54]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_122]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_120]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[street lamp 1]],  
              Position = {
                y = -1926.046875,  
                x = 28825.23438,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 75.21875
              },  
              Angle = 0.921875,  
              Base = [[palette.entities.botobjects.street_lamp]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_12]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_10]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 689,  
      LocationId = [[Client1_14]],  
      Name = [[Act 1:Act 1]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_21]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_19]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_71]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_72]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_55]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Feed In Zone]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_31]],  
                    Conditions = {
                      {
                        InstanceId = [[Client1_82]],  
                        Entity = r2.RefId([[Client1_17]]),  
                        Class = [[ConditionStep]],  
                        Condition = {
                          Type = [[is in activity step]],  
                          InstanceId = [[Client1_81]],  
                          Value = r2.RefId([[Client1_80]]),  
                          Class = [[ConditionType]]
                        }
                      }
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_32]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[Activate]],  
                          InstanceId = [[Client1_33]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_34]],  
                        Entity = r2.RefId([[Client1_132]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              AutoSpawn = 0,  
              InheritPos = 1,  
              Name = [[Roaming Yubo]],  
              Position = {
                y = -1918.34375,  
                x = 28842.45313,  
                InstanceId = [[Client1_22]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              ActivitiesId = {
              },  
              Angle = 2.098390579,  
              Base = [[palette.entities.creatures.chdfb2]],  
              NoRespawn = 0
            },  
            {
              InstanceId = [[Client1_17]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_15]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_52]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_53]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_36]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Feed In Zone]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_79]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_80]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_36]],  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Rest In Zone]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_75]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_76]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_77]],  
                          Value = r2.RefId([[Client1_79]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_78]],  
                        Entity = r2.RefId([[Client1_17]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Ora]],  
              Position = {
                y = -1922.359375,  
                x = 28837.4375,  
                InstanceId = [[Client1_18]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              ActivitiesId = {
              },  
              Angle = 1.838390589,  
              Base = [[palette.entities.creatures.chdfb5]],  
              NoRespawn = 1
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_145]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 2,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -0.2626290321,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 16,  
              MorphTarget3 = 5,  
              MorphTarget7 = 1,  
              ArmModel = 6703918,  
              Position = {
                y = -1919.890625,  
                x = 28826.03125,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = 75
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              JacketModel = 6706990,  
              InheritPos = 1,  
              Level = 0,  
              Name = [[Ibithus]],  
              Sex = 0,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              WeaponLeftHand = 0,  
              ArmColor = 4,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_149]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 14,  
              HairColor = 2,  
              EyesColor = 2,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_150]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = -0.4022553563,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 13,  
              MorphTarget3 = 5,  
              MorphTarget7 = 1,  
              ArmModel = 6703918,  
              Position = {
                y = -1917.03125,  
                x = 28828.10938,  
                InstanceId = [[Client1_151]],  
                Class = [[Position]],  
                z = 75.03125
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              InheritPos = 1,  
              JacketModel = 6706990,  
              Name = [[Meron]],  
              Sex = 0,  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Level = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_154]],  
              ActivitiesId = {
              },  
              HairType = 5623598,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 8,  
              HandsModel = 6702894,  
              FeetColor = 4,  
              GabaritBreastSize = 5,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6703406,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6702382,  
              Speed = 0,  
              Angle = 0.15625,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              Tattoo = 3,  
              MorphTarget3 = 4,  
              MorphTarget7 = 0,  
              ArmModel = 6703918,  
              Position = {
                y = -1921.5,  
                x = 28827.5625,  
                InstanceId = [[Client1_156]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              Sheet = [[ring_civil_light_melee_blunt_f2.creature]],  
              InheritPos = 1,  
              JacketModel = 6706990,  
              Name = [[Be'Doyley]],  
              Sex = 0,  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              Level = 0,  
              SheetClient = [[basic_tryker_male.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          Secondes = 15,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_128]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Deactivate]],  
                      InstanceId = [[Client1_130]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_131]],  
                    Entity = r2.RefId([[Client1_21]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                },  
                Event = {
                  Type = [[On Trigger]],  
                  InstanceId = [[Client1_129]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]]
              }
            },  
            InstanceId = [[Client1_133]]
          },  
          Class = [[Timer]],  
          Active = 0,  
          InheritPos = 1,  
          Minutes = 0,  
          Name = [[Timer 1]],  
          Position = {
            y = -1918.296875,  
            x = 28845.65625,  
            InstanceId = [[Client1_134]],  
            Class = [[Position]],  
            z = 74.984375
          },  
          Base = [[palette.entities.botobjects.timer]],  
          InstanceId = [[Client1_132]],  
          Components = {
          },  
          Cyclic = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_138]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_136]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_137]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[Activate]],  
                      InstanceId = [[Client1_141]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_142]],  
                    Entity = r2.RefId([[Client1_21]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_139]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[yubo bouton]],  
          Position = {
            y = -1918.5,  
            x = 28844.20313,  
            InstanceId = [[Client1_140]],  
            Class = [[Position]],  
            z = 75
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 1
    },  
    {
      InstanceId = [[Client1_159]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_157]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      LocationId = [[Client1_14]],  
      ManualWeather = 1,  
      WeatherValue = 723,  
      InheritPos = 1,  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_160]]
        },  
        {
          CarnivoreRace = [[Scowling Gingo]],  
          Ghosts = {
          },  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_162]]
          },  
          Class = [[Fauna]],  
          CarnivoresCount = [[5]],  
          HerbivoreBase = [[palette.entities.creatures.cbadc2]],  
          _HerbCount = 7,  
          Base = [[palette.entities.botobjects.user_event]],  
          _CarnCount = 5,  
          _CarnId = [[Client1_227]],  
          HerbivoresCount = [[7]],  
          _Seed = 1145552412,  
          HerbivoreRace = [[Hungry Igara]],  
          InheritPos = 1,  
          _HerbId = [[Client1_224]],  
          Name = [[Fauna Feature 1]],  
          Position = {
            y = -1909.1875,  
            x = 28839.42188,  
            InstanceId = [[Client1_163]],  
            Class = [[Position]],  
            z = 75
          },  
          HerbivoresName = [[Hungry Igara]],  
          InstanceId = [[Client1_161]],  
          Components = {
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 1]],  
              InstanceId = [[Client1_165]],  
              Position = {
                y = -1909.1875,  
                x = 28849.42188,  
                InstanceId = [[Client1_164]],  
                Class = [[Position]],  
                z = 75
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_167]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_168]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_170]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_171]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_173]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_174]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_176]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_177]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_179]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_180]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_182]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_183]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Class = [[Region]]
            },  
            {
              InheritPos = 0,  
              Name = [[Sleep Zone 2]],  
              InstanceId = [[Client1_185]],  
              Position = {
                y = -1914.78125,  
                x = 28826.73438,  
                InstanceId = [[Client1_184]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_187]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_188]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_190]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_191]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_193]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_194]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_196]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_197]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_199]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_200]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_202]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_203]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Class = [[Region]]
            },  
            {
              InheritPos = 0,  
              Name = [[Food Zone 1]],  
              InstanceId = [[Client1_205]],  
              Position = {
                y = -1899.1875,  
                x = 28839.42188,  
                InstanceId = [[Client1_204]],  
                Class = [[Position]],  
                z = 75
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_207]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = 5,  
                    InstanceId = [[Client1_208]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_210]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = 2.5,  
                    InstanceId = [[Client1_211]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_213]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 4.328125,  
                    x = -2.484375,  
                    InstanceId = [[Client1_214]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_216]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = 0,  
                    x = -5,  
                    InstanceId = [[Client1_217]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_219]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = -2.5,  
                    InstanceId = [[Client1_220]],  
                    Class = [[Position]],  
                    z = 0
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_222]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -4.328125,  
                    x = 2.484375,  
                    InstanceId = [[Client1_223]],  
                    Class = [[Position]],  
                    z = 0
                  }
                }
              },  
              Deletable = 1,  
              Class = [[Region]]
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_224]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_225]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.cbadc2]],  
              RaceName = [[Hungry Igara]],  
              CrittersCount = [[7]],  
              _CrittersCount = 7,  
              SleepZone = [[Client1_165]],  
              FoodZone = [[Client1_205]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Herbivores]],  
              Position = {
                y = -1899.1875,  
                x = 28849.42188,  
                InstanceId = [[Client1_226]],  
                Class = [[Position]],  
                z = 75
              },  
              _Seed = 1145552412,  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              Ghosts = {
              }
            },  
            {
              FoodDuration = 30,  
              InstanceId = [[Client1_227]],  
              Behavior = {
                Class = [[LogicEntityBehavior]],  
                Actions = {
                },  
                InstanceId = [[Client1_228]]
              },  
              Class = [[Creature]],  
              RaceBase = [[palette.entities.creatures.ccadb3]],  
              RaceName = [[Scowling Gingo]],  
              CrittersCount = [[5]],  
              _CrittersCount = 5,  
              SleepZone = [[Client1_185]],  
              FoodZone = [[Client1_165]],  
              SleepDuration = 30,  
              InheritPos = 0,  
              Name = [[Carnivores]],  
              Position = {
                y = -1919.1875,  
                x = 28829.42188,  
                InstanceId = [[Client1_229]],  
                Class = [[Position]],  
                z = 75
              },  
              _Seed = 1145552412,  
              Base = [[palette.entities.botobjects.user_event]],  
              Components = {
              },  
              Ghosts = {
              }
            }
          },  
          CarnivoreBase = [[palette.entities.creatures.ccadb3]]
        },  
        {
          InstanceId = [[Client1_282]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_281]],  
            Class = [[Position]],  
            z = 0
          },  
          Components = {
            {
              InstanceId = [[Client1_294]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_292]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_302]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[320]],  
                        InstanceId = [[Client1_303]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_36]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Feed In Zone]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[320]],  
                        InstanceId = [[Client1_304]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = [[Client1_55]],  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Rest In Zone]]
                      }
                    }
                  }
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1932.515625,  
                x = 28852.0625,  
                InstanceId = [[Client1_296]],  
                Class = [[Position]],  
                z = 74.984375
              },  
              Angle = 2.210297346,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_274]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_272]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1934.890625,  
                x = 28853.51563,  
                InstanceId = [[Client1_275]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Angle = 1.781291008,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_278]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_276]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1934.28125,  
                x = 28851.34375,  
                InstanceId = [[Client1_279]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = 2.198297262,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_285]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_283]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1932.4375,  
                x = 28853.39063,  
                InstanceId = [[Client1_287]],  
                Class = [[Position]],  
                z = 74.890625
              },  
              Angle = 2.198297262,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_299]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_297]],  
                Activities = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                ChatSequences = {
                }
              },  
              Class = [[NpcCreature]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1935.578125,  
                x = 28856.0625,  
                InstanceId = [[Client1_301]],  
                Class = [[Position]],  
                z = 74.671875
              },  
              Angle = 2.210297346,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_280]],  
            Activities = {
            },  
            Class = [[Behavior]],  
            Actions = {
            },  
            ChatSequences = {
            }
          }
        }
      },  
      Version = 5,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_158]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Title = [[]],  
      Events = {
      },  
      Name = [[Act 2:Act 2]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
    }
  }
}