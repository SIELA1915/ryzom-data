scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 23,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    NpcCustom = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 45,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
        [[Client1_106]],  
        [[Client1_108]],  
        [[Client1_110]],  
        [[Client1_155]],  
        [[Client1_157]],  
        [[Client1_159]],  
        [[Client1_304]],  
        [[Client1_528]],  
        [[Client1_530]],  
        [[Client1_532]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_57]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_55]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              Position = {
                y = -2390.1875,  
                x = 26016.78125,  
                InstanceId = [[Client1_58]],  
                Class = [[Position]],  
                z = -9.609375
              },  
              Angle = 2.8125,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_61]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_59]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Totem]],  
              Position = {
                y = -2376.5625,  
                x = 26012.625,  
                InstanceId = [[Client1_62]],  
                Class = [[Position]],  
                z = -10.171875
              },  
              Angle = -2.390625,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_65]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_63]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Gateway]],  
              Position = {
                y = -2383.625,  
                x = 26014.73438,  
                InstanceId = [[Client1_66]],  
                Class = [[Position]],  
                z = -9.953125
              },  
              Angle = -2.875,  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae Kirosta]],  
              InstanceId = [[Client1_76]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_75]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_78]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2570.328125,  
                    x = 25971,  
                    InstanceId = [[Client1_79]],  
                    Class = [[Position]],  
                    z = -9.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_81]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2582.25,  
                    x = 26005.14063,  
                    InstanceId = [[Client1_82]],  
                    Class = [[Position]],  
                    z = -9.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_84]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2624.765625,  
                    x = 25978.34375,  
                    InstanceId = [[Client1_85]],  
                    Class = [[Position]],  
                    z = -11.203125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_96]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_94]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_108]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_109]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2584.78125,  
                x = 25982.75,  
                InstanceId = [[Client1_97]],  
                Class = [[Position]],  
                z = -10.625
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_108]]
              }
            },  
            {
              InstanceId = [[Client1_100]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_98]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_110]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_111]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2591.0625,  
                x = 25985.28125,  
                InstanceId = [[Client1_101]],  
                Class = [[Position]],  
                z = -10.4375
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_110]]
              }
            },  
            {
              InstanceId = [[Client1_104]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_102]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_106]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_107]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Assault Kirosta]],  
              Position = {
                y = -2593.0625,  
                x = 25977.82813,  
                InstanceId = [[Client1_105]],  
                Class = [[Position]],  
                z = -10.125
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.creatures.ckfrb1]],  
              ActivitiesId = {
                [[Client1_106]]
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae]],  
              InstanceId = [[Client1_124]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_126]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2624.765625,  
                    x = 25910.76563,  
                    InstanceId = [[Client1_127]],  
                    Class = [[Position]],  
                    z = -9.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_129]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2651.59375,  
                    x = 25911.59375,  
                    InstanceId = [[Client1_130]],  
                    Class = [[Position]],  
                    z = -10.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_132]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2673.125,  
                    x = 25893.60938,  
                    InstanceId = [[Client1_133]],  
                    Class = [[Position]],  
                    z = -9.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_135]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2654.453125,  
                    x = 25870.8125,  
                    InstanceId = [[Client1_136]],  
                    Class = [[Position]],  
                    z = -10.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_138]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2602.671875,  
                    x = 25871.70313,  
                    InstanceId = [[Client1_139]],  
                    Class = [[Position]],  
                    z = -8.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_141]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2599.6875,  
                    x = 25890.39063,  
                    InstanceId = [[Client1_142]],  
                    Class = [[Position]],  
                    z = -10.15625
                  }
                }
              }
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_145]],  
              ActivitiesId = {
                [[Client1_159]]
              },  
              HairType = 8494,  
              TrouserColor = 0,  
              MorphTarget5 = 2,  
              MorphTarget6 = 1,  
              GabaritArmsWidth = 4,  
              HandsModel = 5617966,  
              FeetColor = 0,  
              GabaritBreastSize = 5,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 1,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_143]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_159]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_160]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 5617710,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 6,  
              MorphTarget2 = 1,  
              JacketModel = 0,  
              WeaponRightHand = 0,  
              ArmColor = 3,  
              Name = [[Ni-Zo Long]],  
              Position = {
                y = -2625.40625,  
                x = 25880.98438,  
                InstanceId = [[Client1_146]],  
                Class = [[Position]],  
                z = -9.875
              },  
              Sex = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 5,  
              Tattoo = 31
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_149]],  
              ActivitiesId = {
                [[Client1_157]]
              },  
              HairType = 9006,  
              TrouserColor = 3,  
              MorphTarget5 = 1,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 3,  
              HandsModel = 5617966,  
              FeetColor = 5,  
              GabaritBreastSize = 10,  
              GabaritHeight = 11,  
              HairColor = 3,  
              EyesColor = 7,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_147]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_157]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_158]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 5,  
              MorphTarget4 = 7,  
              FeetModel = 0,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 0,  
              JacketModel = 5618734,  
              WeaponRightHand = 0,  
              ArmColor = 5,  
              Name = [[How-Long]],  
              Position = {
                y = -2628.21875,  
                x = 25886.32813,  
                InstanceId = [[Client1_150]],  
                Class = [[Position]],  
                z = -9.78125
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 2,  
              Tattoo = 9
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_153]],  
              ActivitiesId = {
                [[Client1_155]]
              },  
              HairType = 5624366,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 1,  
              HandsModel = 5617966,  
              FeetColor = 2,  
              GabaritBreastSize = 12,  
              GabaritHeight = 2,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_151]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_155]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_156]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 0,  
              Angle = 3.09375,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 5618734,  
              WeaponRightHand = 0,  
              ArmColor = 1,  
              Name = [[Ve-Ri Long]],  
              Position = {
                y = -2627.859375,  
                x = 25877.96875,  
                InstanceId = [[Client1_154]],  
                Class = [[Position]],  
                z = -9.890625
              },  
              Sex = 1,  
              MorphTarget7 = 5,  
              MorphTarget3 = 3,  
              Tattoo = 24
            },  
            {
              InstanceId = [[Client1_163]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_161]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Tower]],  
              Position = {
                y = -2639.640625,  
                x = 25916.75,  
                InstanceId = [[Client1_164]],  
                Class = [[Position]],  
                z = -12.90625
              },  
              Angle = 0.125,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_167]],  
              ActivitiesId = {
              },  
              HairType = 9006,  
              TrouserColor = 3,  
              MorphTarget5 = 2,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 0,  
              HandsModel = 5616430,  
              FeetColor = 2,  
              GabaritBreastSize = 1,  
              GabaritHeight = 5,  
              HairColor = 5,  
              EyesColor = 6,  
              TrouserModel = 5616942,  
              GabaritLegsWidth = 14,  
              HandsColor = 0,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_165]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 2,  
              FeetModel = 5616174,  
              Angle = 0.265625,  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_zorai_male.creature]],  
              InheritPos = 1,  
              ArmModel = 5619758,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 5617454,  
              WeaponRightHand = 5636654,  
              ArmColor = 1,  
              Name = [[Zo-Kae Guard]],  
              Position = {
                y = -2641.390625,  
                x = 25938.67188,  
                InstanceId = [[Client1_168]],  
                Class = [[Position]],  
                z = -12.03125
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 27
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_171]],  
              ActivitiesId = {
              },  
              HairType = 8750,  
              TrouserColor = 0,  
              MorphTarget5 = 6,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 14,  
              HandsModel = 5619246,  
              FeetColor = 2,  
              GabaritBreastSize = 2,  
              GabaritHeight = 7,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 5619502,  
              GabaritLegsWidth = 13,  
              HandsColor = 0,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_169]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 3,  
              MorphTarget4 = 4,  
              FeetModel = 5618990,  
              Angle = 0.046875,  
              Base = [[palette.entities.npcs.guards.z_guard_45]],  
              Sheet = [[ring_guard_melee_tank_pierce_b4.creature]],  
              SheetClient = [[basic_zorai_male.creature]],  
              InheritPos = 1,  
              ArmModel = 5619758,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 5,  
              MorphTarget2 = 2,  
              JacketModel = 5620014,  
              WeaponRightHand = 5636654,  
              ArmColor = 4,  
              Name = [[Zo-Kae Guard]],  
              Position = {
                y = -2630.390625,  
                x = 25937.01563,  
                InstanceId = [[Client1_172]],  
                Class = [[Position]],  
                z = -10.328125
              },  
              Sex = 0,  
              MorphTarget7 = 6,  
              MorphTarget3 = 1,  
              Tattoo = 10
            },  
            {
              InstanceId = [[Client1_179]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_177]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]],  
              Position = {
                y = -2629.59375,  
                x = 25866.73438,  
                InstanceId = [[Client1_180]],  
                Class = [[Position]],  
                z = -9.1875
              },  
              Angle = 0.109375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_183]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_181]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Guard]],  
              Position = {
                y = -2619.796875,  
                x = 25867.09375,  
                InstanceId = [[Client1_184]],  
                Class = [[Position]],  
                z = -8.421875
              },  
              Angle = 0.109375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_187]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_185]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Kae Kami Preacher]],  
              Position = {
                y = -2624.890625,  
                x = 25870.03125,  
                InstanceId = [[Client1_188]],  
                Class = [[Position]],  
                z = -9.03125
              },  
              Angle = 0,  
              Base = [[palette.entities.npcs.kami.kami_preacher_2_b]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Zo-Kae Kami]],  
              InstanceId = [[Client1_190]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_192]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2653.53125,  
                    x = 25869.25,  
                    InstanceId = [[Client1_193]],  
                    Class = [[Position]],  
                    z = -10.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_195]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2648.4375,  
                    x = 25843.70313,  
                    InstanceId = [[Client1_196]],  
                    Class = [[Position]],  
                    z = -8.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_198]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2615.15625,  
                    x = 25842.6875,  
                    InstanceId = [[Client1_199]],  
                    Class = [[Position]],  
                    z = -6.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_201]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2600.8125,  
                    x = 25851.26563,  
                    InstanceId = [[Client1_202]],  
                    Class = [[Position]],  
                    z = -8.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_204]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2597.6875,  
                    x = 25869.89063,  
                    InstanceId = [[Client1_205]],  
                    Class = [[Position]],  
                    z = -9.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_189]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_208]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_206]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo Source]],  
              Position = {
                y = -2465.53125,  
                x = 26177.84375,  
                InstanceId = [[Client1_209]],  
                Class = [[Position]],  
                z = -5.0625
              },  
              Angle = -1.671875,  
              Base = [[palette.entities.botobjects.spot_goo]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_212]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_210]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Goo]],  
              Position = {
                y = -2531.5625,  
                x = 26033.42188,  
                InstanceId = [[Client1_213]],  
                Class = [[Position]],  
                z = -10.5
              },  
              Angle = -0.421875,  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Ki-Teen Region]],  
              InstanceId = [[Client1_215]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_217]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2565.421875,  
                    x = 26146.34375,  
                    InstanceId = [[Client1_218]],  
                    Class = [[Position]],  
                    z = -14.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_220]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2538.4375,  
                    x = 26151.57813,  
                    InstanceId = [[Client1_221]],  
                    Class = [[Position]],  
                    z = -13.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_223]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2503.734375,  
                    x = 26117.75,  
                    InstanceId = [[Client1_224]],  
                    Class = [[Position]],  
                    z = -13.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_226]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2443.875,  
                    x = 26114.32813,  
                    InstanceId = [[Client1_227]],  
                    Class = [[Position]],  
                    z = -14.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_229]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2396.28125,  
                    x = 26170.29688,  
                    InstanceId = [[Client1_230]],  
                    Class = [[Position]],  
                    z = -14.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_232]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2409.734375,  
                    x = 26175.5625,  
                    InstanceId = [[Client1_233]],  
                    Class = [[Position]],  
                    z = -12.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_235]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2427.25,  
                    x = 26208.46875,  
                    InstanceId = [[Client1_236]],  
                    Class = [[Position]],  
                    z = -12.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_238]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2509.21875,  
                    x = 26211.14063,  
                    InstanceId = [[Client1_239]],  
                    Class = [[Position]],  
                    z = -13.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_241]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2515.5625,  
                    x = 26203.17188,  
                    InstanceId = [[Client1_242]],  
                    Class = [[Position]],  
                    z = -13.609375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_244]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2544.953125,  
                    x = 26174.85938,  
                    InstanceId = [[Client1_245]],  
                    Class = [[Position]],  
                    z = -14.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_247]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2563.359375,  
                    x = 26173.42188,  
                    InstanceId = [[Client1_248]],  
                    Class = [[Position]],  
                    z = -14.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_214]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_494]],  
              Name = [[Zo-Ki-Teen Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_496]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_497]],  
                    x = 26013.78125,  
                    y = -2420.625,  
                    z = -7.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_499]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_500]],  
                    x = 25995.75,  
                    y = -2421.703125,  
                    z = -6.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_502]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_503]],  
                    x = 25985.64063,  
                    y = -2398.78125,  
                    z = -10.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_505]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_506]],  
                    x = 26012.71875,  
                    y = -2377.109375,  
                    z = -10.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_508]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_509]],  
                    x = 26020.92188,  
                    y = -2367.828125,  
                    z = -10.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_511]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_512]],  
                    x = 26053.98438,  
                    y = -2365.78125,  
                    z = -6.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_514]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_515]],  
                    x = 26056.70313,  
                    y = -2386.28125,  
                    z = -6.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_517]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_518]],  
                    x = 26054.03125,  
                    y = -2391.8125,  
                    z = -6.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_520]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_521]],  
                    x = 26015.39063,  
                    y = -2390.875,  
                    z = -9.640625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_523]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_524]],  
                    x = 26011.07813,  
                    y = -2395.765625,  
                    z = -10.296875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_493]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_5]]
        },  
        {
          InstanceId = [[Client1_303]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Ki-Teen]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_302]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_301]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_263]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_261]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_304]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_525]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_215]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Ki-Teen Leader]],  
              Position = {
                y = -2516.84375,  
                x = 26174.21875,  
                InstanceId = [[Client1_264]],  
                Class = [[Position]],  
                z = -12.625
              },  
              Angle = -1.828125,  
              Base = [[palette.entities.creatures.ckepf7]],  
              ActivitiesId = {
                [[Client1_304]]
              }
            },  
            {
              InstanceId = [[Client1_275]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_273]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]],  
              Position = {
                y = -2522.796875,  
                x = 26162.1875,  
                InstanceId = [[Client1_276]],  
                Class = [[Position]],  
                z = -12.9375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_283]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_281]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Poisoned Kipucka]],  
              Position = {
                y = -2526.6875,  
                x = 26167.67188,  
                InstanceId = [[Client1_284]],  
                Class = [[Position]],  
                z = -12.84375
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_287]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_285]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2537.25,  
                x = 26164.15625,  
                InstanceId = [[Client1_288]],  
                Class = [[Position]],  
                z = -13.296875
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_291]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_289]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2535.671875,  
                x = 26168.98438,  
                InstanceId = [[Client1_292]],  
                Class = [[Position]],  
                z = -13.03125
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_271]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_269]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2531.546875,  
                x = 26170.79688,  
                InstanceId = [[Client1_272]],  
                Class = [[Position]],  
                z = -12.875
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_267]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_265]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2530.546875,  
                x = 26164.01563,  
                InstanceId = [[Client1_268]],  
                Class = [[Position]],  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_295]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_293]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2532.625,  
                x = 26160.4375,  
                InstanceId = [[Client1_296]],  
                Class = [[Position]],  
                z = -12.890625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_299]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_297]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Kipucka]],  
              Position = {
                y = -2535.796875,  
                x = 26159.5625,  
                InstanceId = [[Client1_300]],  
                Class = [[Position]],  
                z = -13.0625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_279]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_277]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Morbid Kipucka]],  
              Position = {
                y = -2540.109375,  
                x = 26161.125,  
                InstanceId = [[Client1_280]],  
                Class = [[Position]],  
                z = -13.765625
              },  
              Angle = -1.9375,  
              Base = [[palette.entities.creatures.ckegb3]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_446]],  
          Name = [[Kinrey]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_418]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_419]],  
                x = 26032.79688,  
                y = -2385.046875,  
                z = -8.078125
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_416]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_532]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_533]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_532]]
              },  
              InheritPos = 1,  
              Name = [[Kipurey Leader]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_434]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_435]],  
                x = 26001.21875,  
                y = -2403.71875,  
                z = -12.078125
              },  
              Angle = -3.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_432]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_430]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_431]],  
                x = 26007.73438,  
                y = -2403.71875,  
                z = -10.953125
              },  
              Angle = 3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_428]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_438]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_439]],  
                x = 26000.20313,  
                y = -2412.03125,  
                z = -11.28125
              },  
              Angle = 2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_436]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_426]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_427]],  
                x = 26006.46875,  
                y = -2412.59375,  
                z = -10
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_424]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_442]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_443]],  
                x = 25996.6875,  
                y = -2418.359375,  
                z = -8.328125
              },  
              Angle = 2.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_440]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_445]],  
            x = 3.0625,  
            y = -1.546875,  
            z = -0.375
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_444]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_469]],  
          Name = [[Kizarak]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_422]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_423]],  
                x = 26037.90625,  
                y = -2376.109375,  
                z = -8.34375
              },  
              Angle = -2.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_420]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_530]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_531]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_530]]
              },  
              InheritPos = 1,  
              Name = [[Kipurak Leader]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_453]],  
              Base = [[palette.entities.creatures.ckcif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_454]],  
                x = 26005.28125,  
                y = -2394.90625,  
                z = -11.515625
              },  
              Angle = -2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_451]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_465]],  
              Base = [[palette.entities.creatures.ckcif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_466]],  
                x = 25999.9375,  
                y = -2400.21875,  
                z = -13.828125
              },  
              Angle = -2.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_463]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_461]],  
              Base = [[palette.entities.creatures.ckcif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_462]],  
                x = 25996.98438,  
                y = -2395.875,  
                z = -12.34375
              },  
              Angle = -2.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_459]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_449]],  
              Base = [[palette.entities.creatures.ckcif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_450]],  
                x = 25995.0625,  
                y = -2390.734375,  
                z = -11.453125
              },  
              Angle = -2.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_447]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_457]],  
              Base = [[palette.entities.creatures.ckcif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_458]],  
                x = 25989.76563,  
                y = -2394.40625,  
                z = -11.3125
              },  
              Angle = -1.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_455]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kizarak]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_468]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_467]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_492]],  
          Name = [[Kipesta]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_414]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_415]],  
                x = 26027.45313,  
                y = -2379.78125,  
                z = -9.140625
              },  
              Angle = -2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_412]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_528]],  
                    Name = [[Seq1]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_529]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_494]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_528]]
              },  
              InheritPos = 1,  
              Name = [[Kipusta Leader]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_472]],  
              Base = [[palette.entities.creatures.ckjif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_473]],  
                x = 25999.3125,  
                y = -2410.5,  
                z = -11.890625
              },  
              Angle = 0.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_470]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipesta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_476]],  
              Base = [[palette.entities.creatures.ckjif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_477]],  
                x = 25994.15625,  
                y = -2400.40625,  
                z = -13.453125
              },  
              Angle = -2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_474]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipesta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_488]],  
              Base = [[palette.entities.creatures.ckjif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_489]],  
                x = 25995.21875,  
                y = -2411.984375,  
                z = -12.1875
              },  
              Angle = 2.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_486]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_484]],  
              Base = [[palette.entities.creatures.ckjif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_485]],  
                x = 25990.57813,  
                y = -2407.8125,  
                z = -11.21875
              },  
              Angle = -2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_482]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_480]],  
              Base = [[palette.entities.creatures.ckjif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_481]],  
                x = 25992.04688,  
                y = -2409.78125,  
                z = -11.46875
              },  
              Angle = -2.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_478]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kipesta]]
            }
          },  
          ActivitiesId = {
            [[Client1_526]],  
            [[Client1_527]]
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_491]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_490]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[ACT 0: Permanent]]
    },  
    {
      Cost = 2,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_6]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_396]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_394]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Wall]],  
              Position = {
                y = -2425.390625,  
                x = 25974.10938,  
                InstanceId = [[Client1_397]],  
                Class = [[Position]],  
                z = -8.515625
              },  
              Angle = -2.9375,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_408]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_406]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Zo-Ki-Teen Wall]],  
              Position = {
                y = -2417.203125,  
                x = 25970.92188,  
                InstanceId = [[Client1_409]],  
                Class = [[Position]],  
                z = -10.484375
              },  
              Angle = -1.796875,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act I: The Ki-Teens]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      InstanceId = [[Client1_410]],  
      WeatherValue = 0,  
      Version = 1,  
      ActivitiesIds = {
      },  
      Title = [[Act II: Zo-Ki-Teen]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_411]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}