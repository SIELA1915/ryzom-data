scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_12]],  
      Season = [[summer]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts14]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    Position = 0,  
    Location = 0,  
    TextManager = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 4,  
    LocationId = 51,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[fgdsfdsf]],  
    Title = [[]],  
    MaxPlayers = 100
  },  
  Acts = {
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_6]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_5]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[]],  
      ManualWeather = 0,  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 4,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[sdfsdfsdf]],  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_10]],  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_9]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      WeatherValue = 0,  
      LocationId = [[Client1_12]],  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_23]],  
              Base = [[palette.entities.npcs.bandits.t_mage_aoe_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_24]],  
                x = 30630.78125,  
                y = -2303.125,  
                z = 95.859375
              },  
              Angle = 1.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_21]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 9,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 10,  
              GabaritBreastSize = 6,  
              HairType = 7214,  
              HairColor = 4,  
              Tattoo = 18,  
              EyesColor = 6,  
              MorphTarget1 = 0,  
              MorphTarget2 = 6,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 7,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6725934,  
              TrouserModel = 6719278,  
              FeetModel = 6723886,  
              HandsModel = 6724398,  
              ArmModel = 6725422,  
              JacketColor = 1,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 4,  
              WeaponRightHand = 6935854,  
              WeaponLeftHand = 0,  
              Name = [[lakeland devastator 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_female.creature]],  
              Sheet = [[ring_magic_aoe_acid_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_31]],  
              Base = [[palette.entities.npcs.bandits.f_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_32]],  
                x = 30631.14063,  
                y = -2299.71875,  
                z = 95.125
              },  
              Angle = 0.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_29]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 13,  
              GabaritTorsoWidth = 1,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 8,  
              GabaritBreastSize = 6,  
              HairType = 5623086,  
              HairColor = 3,  
              Tattoo = 23,  
              EyesColor = 6,  
              MorphTarget1 = 1,  
              MorphTarget2 = 2,  
              MorphTarget3 = 7,  
              MorphTarget4 = 7,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              MorphTarget7 = 4,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 6706990,  
              TrouserModel = 6705966,  
              FeetModel = 6705198,  
              HandsModel = 6705454,  
              ArmModel = 6706478,  
              JacketColor = 3,  
              ArmColor = 4,  
              HandsColor = 5,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponRightHand = 6755118,  
              WeaponLeftHand = 0,  
              Name = [[desert armsman 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_35]],  
              Base = [[palette.entities.npcs.bandits.z_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_36]],  
                x = 30632.15625,  
                y = -2300.34375,  
                z = 95.109375
              },  
              Angle = 1.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_33]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 11,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 3,  
              GabaritBreastSize = 8,  
              HairType = 6958,  
              HairColor = 2,  
              Tattoo = 18,  
              EyesColor = 6,  
              MorphTarget1 = 5,  
              MorphTarget2 = 6,  
              MorphTarget3 = 7,  
              MorphTarget4 = 6,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              MorphTarget7 = 3,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6739502,  
              TrouserModel = 6738222,  
              FeetModel = 6731822,  
              HandsModel = 6737966,  
              ArmModel = 6738990,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 3,  
              FeetColor = 3,  
              WeaponRightHand = 6770734,  
              WeaponLeftHand = 0,  
              Name = [[jungle armsman 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_melee_damage_dealer_blunt_f2.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_39]],  
              Base = [[palette.entities.npcs.bandits.z_light_melee_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_40]],  
                x = 30630.73438,  
                y = -2297.84375,  
                z = 94.625
              },  
              Angle = 0.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_37]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 8,  
              HairType = 6958,  
              HairColor = 4,  
              Tattoo = 11,  
              EyesColor = 4,  
              MorphTarget1 = 4,  
              MorphTarget2 = 4,  
              MorphTarget3 = 4,  
              MorphTarget4 = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              MorphTarget7 = 0,  
              MorphTarget8 = 4,  
              Sex = 0,  
              JacketModel = 6739502,  
              TrouserModel = 6738478,  
              FeetModel = 6734894,  
              HandsModel = 6737966,  
              ArmModel = 6738990,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6768430,  
              WeaponLeftHand = 6768430,  
              Name = [[jungle milicia 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_light_melee_pierce_f2.creature]]
            }
          },  
          InstanceId = [[Client1_11]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Act 1:sdfsdf]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}