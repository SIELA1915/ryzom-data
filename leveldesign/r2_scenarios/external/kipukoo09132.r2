scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_869]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_871]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    NpcCustom = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    TextManagerEntry = 0,  
    ActivityStep = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 46,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_872]],  
      ActivitiesIds = {
        [[Client1_1046]],  
        [[Client1_1052]],  
        [[Client1_1057]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_878]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_879]],  
                x = 26956.1875,  
                y = -2013.53125,  
                z = -8.640625
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_876]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_882]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_883]],  
                x = 26958.20313,  
                y = -2021.859375,  
                z = -9.296875
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_880]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_886]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_887]],  
                x = 26953.98438,  
                y = -2005.234375,  
                z = -8.234375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_890]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_891]],  
                x = 26960.29688,  
                y = -2030.15625,  
                z = -9.4375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_888]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_894]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_895]],  
                x = 26952,  
                y = -1996.828125,  
                z = -7.703125
              },  
              Angle = -2.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_892]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_898]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_899]],  
                x = 26957.53125,  
                y = -2002.328125,  
                z = -8.390625
              },  
              Angle = -1.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_896]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_902]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_903]],  
                x = 26962.92188,  
                y = -2007.515625,  
                z = -8.671875
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_900]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_906]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_907]],  
                x = 26965.01563,  
                y = -2015.890625,  
                z = -9.109375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_904]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_910]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_911]],  
                x = 26962.4375,  
                y = -2022.71875,  
                z = -9.78125
              },  
              Angle = 2.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_908]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 9]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_922]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_923]],  
                x = 26952.21875,  
                y = -1988.453125,  
                z = -7.328125
              },  
              Angle = 2.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_920]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 10]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_926]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_927]],  
                x = 26955.26563,  
                y = -1980.453125,  
                z = -7.671875
              },  
              Angle = 2.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_924]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 11]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_930]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_931]],  
                x = 26966.8125,  
                y = -1967.78125,  
                z = -8.703125
              },  
              Angle = 2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_928]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 12]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_934]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_935]],  
                x = 26960.125,  
                y = -1973.21875,  
                z = -8.546875
              },  
              Angle = 2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_932]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 13]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_950]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_951]],  
                x = 26978.60938,  
                y = -1964.21875,  
                z = -5.921875
              },  
              Angle = -1.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_948]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_954]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_955]],  
                x = 26971.85938,  
                y = -2009.765625,  
                z = -7.078125
              },  
              Angle = -2.21875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_952]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan banner 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_958]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_959]],  
                x = 26961.35938,  
                y = -1992.5,  
                z = -7.84375
              },  
              Angle = -0.6875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_956]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan banner 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_962]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_963]],  
                x = 26965.0625,  
                y = -2037.015625,  
                z = -9.515625
              },  
              Angle = -2.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_960]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 14]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_966]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_967]],  
                x = 26973.09375,  
                y = -2039.265625,  
                z = -9.203125
              },  
              Angle = -1.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_964]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 15]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_970]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_971]],  
                x = 26981.67188,  
                y = -2037.6875,  
                z = -8.5625
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_968]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 16]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_974]],  
              Base = [[palette.entities.botobjects.karavan_big_wall]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_975]],  
                x = 26989.98438,  
                y = -2035.234375,  
                z = -8.484375
              },  
              Angle = -1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_972]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan big wall 17]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_994]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_995]],  
                x = 27006.5,  
                y = -2035.5,  
                z = -3.609375
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_992]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_998]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_999]],  
                x = 27000.53125,  
                y = -2035.15625,  
                z = -6.6875
              },  
              Angle = -2.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_996]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1002]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1003]],  
                x = 27006.67188,  
                y = -2046.828125,  
                z = -8.609375
              },  
              Angle = -2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1000]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1006]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1007]],  
                x = 26995.20313,  
                y = -2038.6875,  
                z = -8.578125
              },  
              Angle = -2.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1004]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 5]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1010]],  
              Base = [[palette.entities.botobjects.banner_karavan]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1011]],  
                x = 26970.92188,  
                y = -2029.25,  
                z = -10.234375
              },  
              Angle = -2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1008]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[karavan banner 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1014]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1015]],  
                x = 26967.89063,  
                y = -1965.578125,  
                z = -8.0625
              },  
              Angle = -3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1012]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 6]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1018]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1019]],  
                x = 26975.82813,  
                y = -1972.1875,  
                z = -9.25
              },  
              Angle = -2.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1016]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 7]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1022]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1023]],  
                x = 26982.67188,  
                y = -1968.796875,  
                z = -7.078125
              },  
              Angle = -1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1020]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 8]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1033]],  
              Name = [[Place 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1035]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1036]],  
                    x = 26961.78125,  
                    y = -1986.453125,  
                    z = -7.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1038]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1039]],  
                    x = 26989.34375,  
                    y = -1980.53125,  
                    z = -5.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1041]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1042]],  
                    x = 27003.54688,  
                    y = -2021.65625,  
                    z = -0.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1044]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1045]],  
                    x = 26975.875,  
                    y = -2031.6875,  
                    z = -9.578125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1032]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1050]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1051]],  
                x = 27075.875,  
                y = -2028.375,  
                z = 14.171875
              },  
              Angle = 2.1875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1048]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1052]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1053]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kipukoo]],  
              ActivitiesId = {
                [[Client1_1052]]
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1074]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1075]],  
                x = 26959.03125,  
                y = -2009.21875,  
                z = -8.78125
              },  
              Angle = -2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1072]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[protector 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1078]],  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1079]],  
                x = 26960.28125,  
                y = -2012.6875,  
                z = -8.890625
              },  
              Angle = -2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1076]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[voice of Jena 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1082]],  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1083]],  
                x = 26961.03125,  
                y = -2016.3125,  
                z = -9.171875
              },  
              Angle = -2.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1080]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[protector 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1086]],  
              Base = [[palette.entities.creatures.chcdb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1087]],  
                x = 26942.48438,  
                y = -2055.796875,  
                z = -7.40625
              },  
              Angle = 1.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1084]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1090]],  
              Base = [[palette.entities.creatures.chgdb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1091]],  
                x = 26954.35938,  
                y = -2052.3125,  
                z = -8.453125
              },  
              Angle = 2.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1088]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Ibakus]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1094]],  
              Base = [[palette.entities.creatures.cccdb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1095]],  
                x = 26912.53125,  
                y = -1936.625,  
                z = -3.484375
              },  
              Angle = 2.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1092]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goaketh]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1098]],  
              Base = [[palette.entities.creatures.chbfb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1099]],  
                x = 26959.85938,  
                y = -2062.6875,  
                z = -9.75
              },  
              Angle = 2.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1096]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Bodokin]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1102]],  
              Base = [[palette.entities.creatures.chdfb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1103]],  
                x = 26941.375,  
                y = -2061.578125,  
                z = -6.171875
              },  
              Angle = 1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1100]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Ora]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1106]],  
              Base = [[palette.entities.creatures.cbajb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1107]],  
                x = 26950.59375,  
                y = -2060.421875,  
                z = -9.015625
              },  
              Angle = 1.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1104]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Igakya]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1110]],  
              Base = [[palette.entities.creatures.chfjb5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1111]],  
                x = 26946.26563,  
                y = -2064.546875,  
                z = -6.9375
              },  
              Angle = 1.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1108]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Zoann]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1114]],  
              Base = [[palette.entities.creatures.ccejb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1115]],  
                x = 26885.54688,  
                y = -2001.765625,  
                z = -9.90625
              },  
              Angle = 2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1112]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Rakya]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1118]],  
              Base = [[palette.entities.creatures.chflb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1119]],  
                x = 26952.45313,  
                y = -2059.765625,  
                z = -9.25
              },  
              Angle = 2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1116]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Messakan]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1122]],  
              Base = [[palette.entities.creatures.chcdb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1123]],  
                x = 26941.21875,  
                y = -2058.21875,  
                z = -7.140625
              },  
              Angle = 2.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1120]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1126]],  
              Base = [[palette.entities.creatures.chcdb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1127]],  
                x = 26946.84375,  
                y = -2055.453125,  
                z = -8.03125
              },  
              Angle = 2.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1124]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1130]],  
              Base = [[palette.entities.creatures.cccdb7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1131]],  
                x = 26974.03125,  
                y = -1932.90625,  
                z = -4.625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1128]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goaketh]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_873]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1056]],  
          Name = [[Group 1]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_918]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_919]],  
                x = 26914.84375,  
                y = -1791.296875,  
                z = -23.3125
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_916]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1057]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1058]],  
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 6700590,  
              HairColor = 1,  
              Tattoo = 0,  
              EyesColor = 3,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 1,  
              HandsColor = 1,  
              TrouserColor = 1,  
              FeetColor = 1,  
              WeaponLeftHand = 0,  
              WeaponRightHand = 6756398,  
              Name = [[Guard]],  
              ActivitiesId = {
                [[Client1_1057]]
              },  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1026]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1027]],  
                x = 26928.14063,  
                y = -1991.234375,  
                z = -5.234375
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1024]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1046]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1047]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[kipukoo]],  
              ActivitiesId = {
                [[Client1_1046]]
              }
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1055]],  
            x = 58.53125,  
            y = -15.703125,  
            z = 3.609375
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1054]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_1067]],  
        Count = 1,  
        Text = [[]]
      }
    },  
    InstanceId = [[Client1_870]]
  }
}