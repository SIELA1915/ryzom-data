scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_693]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 2,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_695]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    TextManager = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Npc = 0,  
    Position = 0,  
    ActionStep = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    LogicEntityReaction = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Behavior = {
        InstanceId = [[Client1_696]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      InstanceId = [[Client1_698]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_697]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_714]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_715]],  
                x = 22792.1875,  
                y = -1288.6875,  
                z = 74.453125
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_712]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_718]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_719]],  
                x = 22789.9375,  
                y = -1289.265625,  
                z = 74.515625
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_716]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_722]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_723]],  
                x = 22769.28125,  
                y = -1294.3125,  
                z = 71.8125
              },  
              Angle = 0.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_720]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[runic circle 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_726]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_727]],  
                x = 22780.64063,  
                y = -1269.5625,  
                z = 75.140625
              },  
              Angle = -0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_724]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_730]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_731]],  
                x = 22788.21875,  
                y = -1265.28125,  
                z = 75.109375
              },  
              Angle = -1.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_728]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_733]],  
              Name = [[Local Town Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_735]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_736]],  
                    x = 22780.45313,  
                    y = -1290.171875,  
                    z = 74.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_738]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_739]],  
                    x = 22802.26563,  
                    y = -1283.203125,  
                    z = 75.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_741]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_742]],  
                    x = 22797.5,  
                    y = -1264.90625,  
                    z = 75.09375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_732]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_749]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_750]],  
                x = 22799.25,  
                y = -1250.65625,  
                z = 75.109375
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_747]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_753]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_754]],  
                x = 22803.375,  
                y = -1262.953125,  
                z = 75.125
              },  
              Angle = 0.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_751]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_757]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_758]],  
                x = 22805.78125,  
                y = -1273.59375,  
                z = 75.09375
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_755]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_761]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_762]],  
                x = 22808.5,  
                y = -1288.140625,  
                z = 74.796875
              },  
              Angle = 0.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_759]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[barrier 4]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_765]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_766]],  
                x = 22807.53125,  
                y = -1280.703125,  
                z = 75.078125
              },  
              Angle = 0.140625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_763]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1047]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1046]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 3,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 4,  
              GabaritLegsWidth = 11,  
              GabaritBreastSize = 8,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 31,  
              EyesColor = 7,  
              MorphTarget1 = 7,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 6,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 5,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_769]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_770]],  
                x = 22804.64063,  
                y = -1268.4375,  
                z = 75.09375
              },  
              Angle = 0.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_767]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1050]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1049]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 1,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 4,  
              Tattoo = 28,  
              EyesColor = 4,  
              MorphTarget1 = 0,  
              MorphTarget2 = 0,  
              MorphTarget3 = 2,  
              MorphTarget4 = 3,  
              MorphTarget5 = 6,  
              MorphTarget6 = 1,  
              MorphTarget7 = 1,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 0,  
              HandsColor = 2,  
              TrouserColor = 2,  
              FeetColor = 3,  
              WeaponRightHand = 6755630,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_773]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_774]],  
                x = 22801.0625,  
                y = -1256.828125,  
                z = 75.078125
              },  
              Angle = 0.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_771]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1053]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1052]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 7,  
              GabaritBreastSize = 14,  
              HairType = 6700334,  
              HairColor = 5,  
              Tattoo = 5,  
              EyesColor = 3,  
              MorphTarget1 = 5,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 1,  
              MorphTarget5 = 5,  
              MorphTarget6 = 1,  
              MorphTarget7 = 3,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6700846,  
              FeetModel = 6699566,  
              HandsModel = 6699822,  
              ArmModel = 6701358,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 1,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_777]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_778]],  
                x = 22809,  
                y = -1299.265625,  
                z = 72.25
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_775]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1056]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1055]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 5,  
              GabaritBreastSize = 2,  
              HairType = 6700334,  
              HairColor = 1,  
              Tattoo = 10,  
              EyesColor = 0,  
              MorphTarget1 = 3,  
              MorphTarget2 = 4,  
              MorphTarget3 = 0,  
              MorphTarget4 = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 0,  
              MorphTarget7 = 3,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 5,  
              ArmColor = 1,  
              HandsColor = 3,  
              TrouserColor = 3,  
              FeetColor = 0,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_f4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_781]],  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_782]],  
                x = 22795.40625,  
                y = -1244.28125,  
                z = 74.8125
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_779]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1059]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1058]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 4,  
              GabaritTorsoWidth = 4,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 4,  
              GabaritBreastSize = 13,  
              HairType = 6700590,  
              HairColor = 3,  
              Tattoo = 0,  
              EyesColor = 5,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 4,  
              MorphTarget4 = 2,  
              MorphTarget5 = 1,  
              MorphTarget6 = 1,  
              MorphTarget7 = 0,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 1,  
              ArmColor = 5,  
              HandsColor = 0,  
              TrouserColor = 5,  
              FeetColor = 1,  
              WeaponRightHand = 6755374,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_blunt_f4.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_785]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_786]],  
                x = 22801.76563,  
                y = -1300.234375,  
                z = 72.328125
              },  
              Angle = 2.4375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_783]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_789]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_790]],  
                x = 22800.54688,  
                y = -1288.5,  
                z = 74.34375
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_787]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_793]],  
              Base = [[palette.entities.botobjects.tent]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_794]],  
                x = 22789.71875,  
                y = -1300.21875,  
                z = 73.875
              },  
              Angle = 1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_791]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tent 3]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_829]],  
              Name = [[Right Attack Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_831]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_832]],  
                    x = 22880.23438,  
                    y = -1308.90625,  
                    z = 74.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_834]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_835]],  
                    x = 22927.07813,  
                    y = -1251.203125,  
                    z = 72.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_837]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_838]],  
                    x = 22959.92188,  
                    y = -1282.625,  
                    z = 75.796875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_828]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_840]],  
              Name = [[Left Attack Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_842]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_843]],  
                    x = 22890.01563,  
                    y = -1189.25,  
                    z = 72.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_845]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_846]],  
                    x = 22845.92188,  
                    y = -1148.109375,  
                    z = 75.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_848]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_849]],  
                    x = 22916.25,  
                    y = -1089.453125,  
                    z = 76.96875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_839]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_851]],  
              Name = [[Kitin Home Region]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_853]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_854]],  
                    x = 22989.45313,  
                    y = -1101.015625,  
                    z = 76.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_856]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_857]],  
                    x = 23069.15625,  
                    y = -1170.84375,  
                    z = 75.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_859]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_860]],  
                    x = 23065.32813,  
                    y = -1096.359375,  
                    z = 75.546875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_850]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_944]],  
              Name = [[Kitin Front Defense]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_946]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_947]],  
                    x = 23068.76563,  
                    y = -1172.5625,  
                    z = 75.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_949]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_950]],  
                    x = 23067.375,  
                    y = -1194.078125,  
                    z = 75.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_952]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_953]],  
                    x = 22960.46875,  
                    y = -1094.78125,  
                    z = 76.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_955]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_956]],  
                    x = 22986.29688,  
                    y = -1100.8125,  
                    z = 76.828125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_943]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_699]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_825]],  
          Name = [[Local Town Fighters]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_821]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_822]],  
                x = 22799.98438,  
                y = -1279.21875,  
                z = 75.109375
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_819]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_826]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_930]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_931]],  
                    Name = [[Kitin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_932]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1062]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1061]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1065]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1064]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1068]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1067]],  
                    Name = [[]]
                  }
                }
              },  
              GabaritHeight = 8,  
              GabaritTorsoWidth = 5,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 1,  
              GabaritBreastSize = 5,  
              HairType = 6700334,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 5,  
              MorphTarget3 = 5,  
              MorphTarget4 = 7,  
              MorphTarget5 = 0,  
              MorphTarget6 = 6,  
              MorphTarget7 = 6,  
              MorphTarget8 = 3,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699566,  
              HandsModel = 6700078,  
              ArmModel = 6701614,  
              JacketColor = 1,  
              ArmColor = 3,  
              HandsColor = 5,  
              TrouserColor = 2,  
              FeetColor = 5,  
              WeaponRightHand = 6756398,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_809]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_810]],  
                x = 22800.46875,  
                y = -1281.71875,  
                z = 75.046875
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_807]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 6,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 12,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 1,  
              HairType = 6700334,  
              HairColor = 0,  
              Tattoo = 20,  
              EyesColor = 1,  
              MorphTarget1 = 0,  
              MorphTarget2 = 7,  
              MorphTarget3 = 2,  
              MorphTarget4 = 6,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              MorphTarget7 = 2,  
              MorphTarget8 = 1,  
              Sex = 1,  
              JacketModel = 6701870,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6699822,  
              ArmModel = 6701614,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 0,  
              TrouserColor = 0,  
              FeetColor = 3,  
              WeaponRightHand = 6755886,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_pierce_e4.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_817]],  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_818]],  
                x = 22799.03125,  
                y = -1276.859375,  
                z = 75.078125
              },  
              Angle = 0.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_815]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 2,  
              GabaritArmsWidth = 6,  
              GabaritLegsWidth = 6,  
              GabaritBreastSize = 9,  
              HairType = 6700334,  
              HairColor = 4,  
              Tattoo = 23,  
              EyesColor = 3,  
              MorphTarget1 = 2,  
              MorphTarget2 = 7,  
              MorphTarget3 = 7,  
              MorphTarget4 = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 3,  
              MorphTarget7 = 1,  
              MorphTarget8 = 7,  
              Sex = 1,  
              JacketModel = 6702126,  
              TrouserModel = 6701102,  
              FeetModel = 6699310,  
              HandsModel = 6700078,  
              ArmModel = 6701358,  
              JacketColor = 3,  
              ArmColor = 2,  
              HandsColor = 2,  
              TrouserColor = 4,  
              FeetColor = 0,  
              WeaponRightHand = 6756654,  
              WeaponLeftHand = 0,  
              Name = [[Kaethia Fighter]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_e4.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_824]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_823]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_927]],  
          Name = [[Kitin Patrol Kincher]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_887]],  
              Base = [[palette.entities.creatures.ckdif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_888]],  
                x = 23030.34375,  
                y = -1149.203125,  
                z = 74.40625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_885]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_928]],  
                    Name = [[Kitin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_929]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_933]],  
                    Name = [[Homin]],  
                    Repeating = 0,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_934]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_733]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1071]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1070]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1074]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1073]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1077]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1076]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1080]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1079]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1083]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1082]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1086]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1085]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1089]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1088]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1092]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1091]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1095]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1094]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1098]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1097]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_895]],  
              Base = [[palette.entities.creatures.ckdif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_896]],  
                x = 23035.35938,  
                y = -1148.71875,  
                z = 74.875
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_893]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_903]],  
              Base = [[palette.entities.creatures.ckdif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_904]],  
                x = 23031.03125,  
                y = -1140.296875,  
                z = 74.53125
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_901]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_899]],  
              Base = [[palette.entities.creatures.ckdif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_900]],  
                x = 23025.4375,  
                y = -1136.671875,  
                z = 74.1875
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_897]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_907]],  
              Base = [[palette.entities.creatures.ckdif3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_908]],  
                x = 23040.29688,  
                y = -1147.25,  
                z = 75.0625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_905]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Great Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_911]],  
              Base = [[palette.entities.creatures.ckdif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_912]],  
                x = 23022.1875,  
                y = -1130.046875,  
                z = 74.3125
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_909]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_915]],  
              Base = [[palette.entities.creatures.ckdif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_916]],  
                x = 23030.89063,  
                y = -1134.09375,  
                z = 74.5625
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_913]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_919]],  
              Base = [[palette.entities.creatures.ckdif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_920]],  
                x = 23038.15625,  
                y = -1139.09375,  
                z = 74.875
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_917]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_923]],  
              Base = [[palette.entities.creatures.ckdif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_924]],  
                x = 23044.85938,  
                y = -1143.078125,  
                z = 75.015625
              },  
              Angle = -1.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_921]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kincher]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_891]],  
              Base = [[palette.entities.creatures.ckdif2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_892]],  
                x = 23026.32813,  
                y = -1143.0625,  
                z = 74.0625
              },  
              Angle = -2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_889]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Power Overlord Kincher]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_926]],  
            x = 7.046875,  
            y = 11.1875,  
            z = 0.0625
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_925]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1011]],  
          Name = [[Defense Elite Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_991]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_992]],  
                x = 23021.40625,  
                y = -1121.046875,  
                z = 75
              },  
              Angle = -2.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_989]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1035]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1036]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1101]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1100]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1104]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1103]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1107]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1106]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1110]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1109]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_983]],  
              Base = [[palette.entities.creatures.ckcif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_984]],  
                x = 23013.14063,  
                y = -1114.359375,  
                z = 75.734375
              },  
              Angle = -2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_981]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_995]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_996]],  
                x = 23025.5625,  
                y = -1127.859375,  
                z = 74.5625
              },  
              Angle = -2.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_993]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_987]],  
              Base = [[palette.entities.creatures.ckcif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_988]],  
                x = 23048.3125,  
                y = -1146.921875,  
                z = 75.21875
              },  
              Angle = -2.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_985]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kizarak]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1010]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1009]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1014]],  
          Name = [[Defense Boss Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_937]],  
              Base = [[palette.entities.creatures.ckepf7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_938]],  
                x = 23035.45313,  
                y = -1110.65625,  
                z = 74.78125
              },  
              Angle = -2.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_935]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1037]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1038]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_851]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1113]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1112]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1116]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1115]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1119]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1118]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1122]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1121]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kipukoo]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_999]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1000]],  
                x = 23023.34375,  
                y = -1112.71875,  
                z = 75.375
              },  
              Angle = -2.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_997]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1003]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1004]],  
                x = 23033.54688,  
                y = -1120.015625,  
                z = 75.0625
              },  
              Angle = -2.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1001]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1007]],  
              Base = [[palette.entities.creatures.ckeif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1008]],  
                x = 23044.60938,  
                y = -1123.546875,  
                z = 74.78125
              },  
              Angle = -2.359375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1005]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kipucker]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1013]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1012]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1017]],  
          Name = [[Front Defense Left]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_959]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_960]],  
                x = 22986.3125,  
                y = -1113.125,  
                z = 75.84375
              },  
              Angle = -2.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_957]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1031]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1032]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_944]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1125]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1124]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1128]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1127]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1131]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1130]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_963]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_964]],  
                x = 22986.26563,  
                y = -1107.875,  
                z = 76.890625
              },  
              Angle = -2.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_961]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_967]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_968]],  
                x = 22993.67188,  
                y = -1112.328125,  
                z = 76.21875
              },  
              Angle = -2.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_965]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1016]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1015]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1020]],  
          Name = [[Front Defense Right]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_971]],  
              Base = [[palette.entities.creatures.ckbif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_972]],  
                x = 23038.65625,  
                y = -1161.921875,  
                z = 75.1875
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_969]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1033]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1034]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_944]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1134]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1133]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1137]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1136]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1140]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1139]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_975]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_976]],  
                x = 23036.90625,  
                y = -1156.21875,  
                z = 75.03125
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_973]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_979]],  
              Base = [[palette.entities.creatures.ckbif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_980]],  
                x = 23045.375,  
                y = -1161.390625,  
                z = 75.25
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_977]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1019]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1018]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1023]],  
          Name = [[Front Attack Left]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_863]],  
              Base = [[palette.entities.creatures.ckfif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_864]],  
                x = 22873.39063,  
                y = -1166.484375,  
                z = 75.09375
              },  
              Angle = 3.924025774,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_861]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1029]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1030]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_840]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1143]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1142]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1146]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1145]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1149]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1148]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_867]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_868]],  
                x = 22878.26563,  
                y = -1159.359375,  
                z = 75.109375
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_865]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_871]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_872]],  
                x = 22879.57813,  
                y = -1167.96875,  
                z = 75.109375
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_869]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1022]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1021]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1026]],  
          Name = [[Front Attack Right]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_875]],  
              Base = [[palette.entities.creatures.ckfif4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_876]],  
                x = 22903.70313,  
                y = -1290.140625,  
                z = 75.4375
              },  
              Angle = 2.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_873]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1027]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1028]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_829]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1152]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1151]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1155]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1154]],  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_1158]],  
                    LogicEntityAction = [[Client1_1044]],  
                    ActionStep = [[Client1_1157]],  
                    Name = [[]]
                  }
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Elite Overlord Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_879]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_880]],  
                x = 22915.78125,  
                y = -1287.28125,  
                z = 75.46875
              },  
              Angle = 2.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_877]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_883]],  
              Base = [[palette.entities.creatures.ckfif1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_884]],  
                x = 22911.0625,  
                y = -1292.1875,  
                z = 76.140625
              },  
              Angle = 2.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_881]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1025]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1024]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      Name = [[Permanent]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_1039]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
          {
            Class = [[LogicEntityAction]],  
            InstanceId = [[Client1_1044]],  
            Conditions = {
            },  
            Actions = {
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1046]],  
                Entity = r2.RefId([[Client1_765]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1045]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1049]],  
                Entity = r2.RefId([[Client1_769]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1048]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1052]],  
                Entity = r2.RefId([[Client1_773]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1051]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1055]],  
                Entity = r2.RefId([[Client1_777]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1054]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1058]],  
                Entity = r2.RefId([[Client1_781]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1057]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1061]],  
                Entity = r2.RefId([[Client1_821]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1060]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1064]],  
                Entity = r2.RefId([[Client1_809]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1063]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1067]],  
                Entity = r2.RefId([[Client1_817]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1066]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1070]],  
                Entity = r2.RefId([[Client1_887]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1069]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1073]],  
                Entity = r2.RefId([[Client1_895]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1072]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1076]],  
                Entity = r2.RefId([[Client1_903]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1075]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1079]],  
                Entity = r2.RefId([[Client1_899]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1078]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1082]],  
                Entity = r2.RefId([[Client1_907]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1081]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1085]],  
                Entity = r2.RefId([[Client1_911]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1084]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1088]],  
                Entity = r2.RefId([[Client1_915]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1087]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1091]],  
                Entity = r2.RefId([[Client1_919]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1090]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1094]],  
                Entity = r2.RefId([[Client1_923]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1093]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1097]],  
                Entity = r2.RefId([[Client1_891]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1096]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1100]],  
                Entity = r2.RefId([[Client1_991]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1099]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1103]],  
                Entity = r2.RefId([[Client1_983]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1102]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1106]],  
                Entity = r2.RefId([[Client1_995]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1105]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1109]],  
                Entity = r2.RefId([[Client1_987]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1108]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1112]],  
                Entity = r2.RefId([[Client1_937]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1111]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1115]],  
                Entity = r2.RefId([[Client1_999]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1114]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1118]],  
                Entity = r2.RefId([[Client1_1003]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1117]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1121]],  
                Entity = r2.RefId([[Client1_1007]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1120]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1124]],  
                Entity = r2.RefId([[Client1_959]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1123]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1127]],  
                Entity = r2.RefId([[Client1_963]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1126]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1130]],  
                Entity = r2.RefId([[Client1_967]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1129]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1133]],  
                Entity = r2.RefId([[Client1_971]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1132]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1136]],  
                Entity = r2.RefId([[Client1_975]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1135]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1139]],  
                Entity = r2.RefId([[Client1_979]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1138]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1142]],  
                Entity = r2.RefId([[Client1_863]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1141]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1145]],  
                Entity = r2.RefId([[Client1_867]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1144]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1148]],  
                Entity = r2.RefId([[Client1_871]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1147]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1151]],  
                Entity = r2.RefId([[Client1_875]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1150]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1154]],  
                Entity = r2.RefId([[Client1_879]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1153]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              },  
              {
                Class = [[ActionStep]],  
                InstanceId = [[Client1_1157]],  
                Entity = r2.RefId([[Client1_883]]),  
                Action = {
                  Class = [[ActionType]],  
                  InstanceId = [[Client1_1156]],  
                  Type = [[Activate]],  
                  Value = r2.RefId([[]])
                }
              }
            },  
            Event = {
              Class = [[EventType]],  
              InstanceId = [[Client1_1043]],  
              Type = [[On Act Started]],  
              Value = r2.RefId([[]])
            },  
            Name = [[]]
          }
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act I: Reset Scenario]],  
      InstanceId = [[Client1_1041]],  
      ManualWeather = 0,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        Class = [[Position]],  
        InstanceId = [[Client1_1040]],  
        x = 0,  
        y = 0,  
        z = 0
      },  
      Title = [[Act I: Reset Scenario]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_1042]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      Version = 3,  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_694]]
  }
}