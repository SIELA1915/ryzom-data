scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client1_3]],  
      IslandName = [[uiR2_Deserts33]],  
      Class = [[Location]],  
      Season = [[fall]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client1_1]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Description = {
    InstanceId = [[Client1_2]],  
    Class = [[MapDescription]],  
    LevelId = 3,  
    LocationId = 70,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    Title = [[errorthingy]],  
    RuleId = 0,  
    ShortDescription = [[gives an error]],  
    MaxEntities = 50,  
    MaxPlayers = 100
  },  
  Versions = {
    TextManagerEntry = 0,  
    Scenario = 1,  
    Act = 3,  
    Location = 0,  
    TextManager = 0,  
    DefaultFeature = 0,  
    MapDescription = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      ManualWeather = 0,  
      States = {
      }
    }
  },  
}