scenario = {
  AccessRules = [[strict]],  
  Locations = {
    {
      InstanceId = [[Client1_5980]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts03]],  
      Time = 0,  
      Name = [[The Bodoc Rest (Desert 03)]],  
      Season = [[summer]],  
      ManualSeason = 0,  
      EntryPoint = [[uiR2NorthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5971]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    InstanceId = [[Client1_5969]]
  },  
  VersionName = [[0.1.0]],  
  InheritPos = 1,  
  PlotItems = {
    {
      SheetId = 8669230,  
      Name = [[<RI_##>Objet 01]],  
      InstanceId = [[Client1_6036]],  
      Class = [[PlotItem]],  
      Comment = [[Objet requis pour <RI_##>. Demandé par <NPC_NAME>]],  
      Desc = [[<RI_##>Description Objet 01]]
    }
  },  
  Name = [[ENORIL MACROS]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_5970]],  
    Class = [[Position]],  
    z = 0
  },  
  Description = {
    ShortDescription = [[Cette carte regroupe, sur diff�rents acts, des macro types pouvant etre utilis� dans d'autre carte.
Regardez la description de chaque act pour avoir plus d'informations]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 0,  
    Class = [[MapDescription]],  
    InstanceId = [[Client1_5967]]
  },  
  Versions = {
    ConditionStep = 0,  
    Scenario = 2,  
    Act = 5,  
    ChatSequence = 0,  
    UserTrigger = 0,  
    EventType = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    TextManager = 0,  
    EasterEgg = 0,  
    PlotItem = 0,  
    ChatStep = 0,  
    RequestItem = 0,  
    ConditionType = 0,  
    ChatAction = 0,  
    TalkTo = 0,  
    Behavior = 1,  
    ActionStep = 0,  
    Position = 0,  
    Location = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 1,  
    NpcCustom = 0
  },  
  Acts = {
    {
      InstanceId = [[Client1_5974]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_5972]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 0,  
      LocationId = [[]],  
      Name = [[Permanent]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_5975]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5973]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    },  
    {
      InstanceId = [[Client1_5978]],  
      Behavior = {
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        InstanceId = [[Client1_5976]]
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[Ce scenario contient un pnj qui demande de maniere interactive une mission de type "Give Item"
- 1) Un pnj demande en boucle de l'aide.
- 2) Une fois que le joueur clique sur le menu contextuel ("Je peux vous aider ?") le pnj commence l'explication de sa mission.
- 3) Le joueur a - � la fin du speech explicatif - deux choix: Accepter la mission ou Refuser la mission
- Si le joueur refuse, le pnj sort un discours de circonstance et recommence plus tard ses demandes a l'aide. (on repart a l'�tape 1)
- Si le joueur accepte, la mission "Give item" est automatiquement ajout� dans les taches du joueur. 
4) Si le joueur revient sur le pnj une fois la mission "Give item" activ� et clique dessus, on peut redemander des explications de la mission.
5) Une fois que le joueur revient avec les items demand�s, le pnj le remercie et la fin du remerciement peut etre utilis� pour d�clencher la fin de l'act, un autre evenement etc...]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      Title = [[]],  
      Version = 5,  
      WeatherValue = 611,  
      LocationId = [[Client1_5980]],  
      Name = [[NPC_Request_Item (RI)]],  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_5987]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 6,  
              HandsModel = 6703150,  
              FeetColor = 4,  
              GabaritBreastSize = 13,  
              Notes = [[## NPC_Request_Item Macro ##
Version 1.0 par Enoril (20/04/2006)

Ce PNJ donne de maniere interactive une mission "Demande d'objet" (REQUEST ITEM ou RI).
-> L'apparition de la mission se fait via le d�clenchement du controle "<RI_##>Dialogue 01: Demande d'aide (Boucle)". Le pnj va alors demander de l'aide et le joueur pourra si il le souhaite l'aider.

Le succes de la mission est detectable via l'evenement "End dialog" du controle "<RI_##>Dialogue 05: Le NPC remercie le joueur pour son aide"

Texte � modifier:
- Nom de ce pnj,
- Le contenu des controles "Dialogue 01->05"
- Les champs "Contextual Text" et "Mission succeeded Text" des controles "Menu_Contextuel" 
Attention: il ne faut au moins laisser au moins un espace dans les champs des Menu_Contextuel sous peine de plantage.
- Si vous voulez utiliser plusieurs mission de ce type, modifiez le texte <RI_##> pr�sent devant chaques controle afin d'�viter les m�langes.]],  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 5,  
              AutoSpawn = 1,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 6,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_5985]],  
                ChatSequences = {
                },  
                Class = [[Behavior]],  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 3,  
              FeetModel = 6702638,  
              Speed = 0,  
              Angle = 4.602199554,  
              Base = [[palette.entities.npcs.civils.f_civil_220]],  
              SheetClient = [[basic_fyros_male.creature]],  
              ArmColor = 4,  
              WeaponLeftHand = 0,  
              JacketModel = 6704686,  
              Sex = 1,  
              InheritPos = 1,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 3,  
              Sheet = [[ring_civil_light_melee_blunt_f4.creature]],  
              WeaponRightHand = 0,  
              Level = 3,  
              Name = [[<RI_##><NPC_NAME>]],  
              Position = {
                y = -1105.78125,  
                x = 22894.9375,  
                InstanceId = [[Client1_5988]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              ArmModel = 6704174,  
              MorphTarget7 = 3,  
              MorphTarget3 = 3,  
              Tattoo = 20
            }
          },  
          InstanceId = [[Client1_5979]]
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_5991]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6118]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_6119]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_6120]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6121]],  
                    Entity = r2.RefId([[Client1_6037]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_5989]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[<RI_##>Dialog 01: NPC Request Help (Loop)]],  
          Position = {
            y = -1101.28125,  
            x = 22883.8125,  
            InstanceId = [[Client1_5990]],  
            Class = [[Position]],  
            z = 73.796875
          },  
          Repeating = 1,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_5992]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_5993]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6226]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 19,  
              InstanceId = [[Client1_5995]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_5996]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6227]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 19,  
              InstanceId = [[Client1_5998]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_5999]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6228]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 19,  
              InstanceId = [[Client1_6001]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6002]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6229]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 19,  
              InstanceId = [[Client1_6003]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6004]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6230]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_6008]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6130]],  
                Conditions = {
                  {
                    InstanceId = [[Client1_6137]],  
                    Entity = r2.RefId([[Client1_6033]]),  
                    Class = [[ConditionStep]],  
                    Condition = {
                      Type = [[is inactive]],  
                      InstanceId = [[Client1_6136]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ConditionType]]
                    }
                  }
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[start of dialog]],  
                  InstanceId = [[Client1_6131]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_6132]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6133]],  
                    Entity = r2.RefId([[Client1_6040]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_6134]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6135]],  
                    Entity = r2.RefId([[Client1_6102]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_6006]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[<RI_##>Dialog 02: Mission Explanation]],  
          Position = {
            y = -1101.15625,  
            x = 22887.4375,  
            InstanceId = [[Client1_6007]],  
            Class = [[Position]],  
            z = 74.0625
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_6009]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6010]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6233]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 3,  
              InstanceId = [[Client1_6017]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6018]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6232]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_6022]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_6020]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[<RI_##>Dialog 03: Player Accept Mission]],  
          Position = {
            y = -1101.03125,  
            x = 22894.8125,  
            InstanceId = [[Client1_6021]],  
            Class = [[Position]],  
            z = 74.796875
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_6030]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6031]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6237]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_6025]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6197]],  
                Class = [[LogicEntityAction]],  
                Conditions = {
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6199]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6200]],  
                    Entity = r2.RefId([[Client1_5991]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_6198]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                }
              }
            },  
            InstanceId = [[Client1_6023]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[<RI_##>Dialog 04: Player Reject Mission]],  
          Position = {
            y = -1101.15625,  
            x = 22891.1875,  
            InstanceId = [[Client1_6024]],  
            Class = [[Position]],  
            z = 74.40625
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_6026]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6027]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6238]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          MissionText = [[ ]],  
          Item2Id = r2.RefId([[]]),  
          MissionSucceedText = [[ ]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6174]],  
                Event = {
                  Type = [[wait validation]],  
                  InstanceId = [[Client1_6175]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Sit Down]],  
                      InstanceId = [[Client1_6176]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6177]],  
                    Entity = r2.RefId([[Client1_5987]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_6201]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6202]],  
                    Entity = r2.RefId([[Client1_6105]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              },  
              {
                Name = [[]],  
                InstanceId = [[Client1_6179]],  
                Event = {
                  Type = [[succeeded]],  
                  InstanceId = [[Client1_6180]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6181]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6182]],  
                    Entity = r2.RefId([[Client1_6110]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6183]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6184]],  
                    Entity = r2.RefId([[Client1_6033]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6203]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6204]],  
                    Entity = r2.RefId([[Client1_6105]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[Stand Up]],  
                      InstanceId = [[Client1_6205]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6206]],  
                    Entity = r2.RefId([[Client1_5987]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_6034]]
          },  
          WaitValidationText = [[ ]],  
          Item1Qty = 1,  
          Item3Qty = 0,  
          Base = [[palette.entities.botobjects.bot_request_item]],  
          Repeatable = 0,  
          Item2Qty = 0,  
          ContextualText = [[<I_HAVE_FIND_YOUR_ITEM>]],  
          MissionGiver = r2.RefId([[Client1_5987]]),  
          Item1Id = r2.RefId([[Client1_6036]]),  
          _Seed = 1145563662,  
          InheritPos = 1,  
          InstanceId = [[Client1_6033]],  
          Name = [[<RI_##>Mission Objective: ITEM ASK BY NPC_NAME]],  
          Position = {
            y = -1103.65625,  
            x = 22894.6875,  
            InstanceId = [[Client1_6035]],  
            Class = [[Position]],  
            z = 74.828125
          },  
          Active = 0,  
          Class = [[RequestItem]],  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        },  
        {
          MissionText = [[  ]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6123]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[succeeded]],  
                  InstanceId = [[Client1_6124]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[stops dialog]],  
                      InstanceId = [[Client1_6125]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6126]],  
                    Entity = r2.RefId([[Client1_5991]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6127]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6128]],  
                    Entity = r2.RefId([[Client1_6008]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6138]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6139]],  
                    Entity = r2.RefId([[Client1_6037]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_6038]]
          },  
          WaitValidationText = [[ ]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[<TEXT_MAY_BE_I_CAN_HELP_YOU_?>]],  
          MissionGiver = r2.RefId([[Client1_5987]]),  
          MissionTarget = r2.RefId([[Client1_5987]]),  
          InheritPos = 1,  
          MissionSucceedText = [[<TEXT_INTRODUCE_MISSION_EXPLANATION_DIALOG>]],  
          Name = [[<RI_##>Contextual_Menu 01: May be I Can Help You ?]],  
          Position = {
            y = -1098.28125,  
            x = 22883.6875,  
            InstanceId = [[Client1_6039]],  
            Class = [[Position]],  
            z = 73.71875
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_6037]],  
          Components = {
          },  
          _Seed = 1145563954
        },  
        {
          MissionText = [[ ]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6158]],  
                Event = {
                  Type = [[succeeded]],  
                  InstanceId = [[Client1_6159]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[activate]],  
                      InstanceId = [[Client1_6160]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6161]],  
                    Entity = r2.RefId([[Client1_6033]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6162]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6163]],  
                    Entity = r2.RefId([[Client1_6040]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6164]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6165]],  
                    Entity = r2.RefId([[Client1_6102]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_6041]]
          },  
          WaitValidationText = [[ ]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[<TEXT_ACCEPT_MISSION>]],  
          MissionGiver = r2.RefId([[Client1_5987]]),  
          MissionTarget = r2.RefId([[Client1_5987]]),  
          InheritPos = 1,  
          MissionSucceedText = [[<TEXT_START_RESEARCH_NOW_MY_ITEM>]],  
          Name = [[<RI_##>Contextual_Menu 02: Accept Mission]],  
          Position = {
            y = -1098.40625,  
            x = 22894.9375,  
            InstanceId = [[Client1_6042]],  
            Class = [[Position]],  
            z = 74.859375
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_6040]],  
          Components = {
          },  
          _Seed = 1145564054
        },  
        {
          MissionText = [[ ]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6149]],  
                Event = {
                  Type = [[succeeded]],  
                  InstanceId = [[Client1_6150]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6151]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6152]],  
                    Entity = r2.RefId([[Client1_6025]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6153]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6154]],  
                    Entity = r2.RefId([[Client1_6040]]),  
                    Class = [[ActionStep]]
                  },  
                  {
                    Action = {
                      Type = [[deactivate]],  
                      InstanceId = [[Client1_6155]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6156]],  
                    Entity = r2.RefId([[Client1_6102]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_6103]]
          },  
          WaitValidationText = [[ ]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[<TEXT_REJECT_MISSION>]],  
          MissionGiver = r2.RefId([[Client1_5987]]),  
          MissionTarget = r2.RefId([[Client1_5987]]),  
          InheritPos = 1,  
          MissionSucceedText = [[<TEXT_INTRODUCE_MISSION_REJECTED_DIALOG>]],  
          Name = [[<RI_##>Contextual_Menu 03: Reject Mission]],  
          Position = {
            y = -1098.453125,  
            x = 22891.42188,  
            InstanceId = [[Client1_6104]],  
            Class = [[Position]],  
            z = 74.40625
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_6102]],  
          Components = {
          },  
          _Seed = 1145564500
        },  
        {
          MissionText = [[ ]],  
          Active = 0,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6167]],  
                Event = {
                  Type = [[succeeded]],  
                  InstanceId = [[Client1_6168]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6169]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6170]],  
                    Entity = r2.RefId([[Client1_6008]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                  {
                    InstanceId = [[Client1_6172]],  
                    Entity = r2.RefId([[Client1_6033]]),  
                    Class = [[ConditionStep]],  
                    Condition = {
                      Type = [[is active]],  
                      InstanceId = [[Client1_6171]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ConditionType]]
                    }
                  }
                }
              }
            },  
            InstanceId = [[Client1_6106]]
          },  
          WaitValidationText = [[ ]],  
          Base = [[palette.entities.botobjects.bot_chat]],  
          Repeatable = 0,  
          ContextualText = [[<TEXT_RESTART_MISSION_EXPLANATION>]],  
          MissionGiver = r2.RefId([[Client1_5987]]),  
          MissionTarget = r2.RefId([[Client1_5987]]),  
          InheritPos = 1,  
          MissionSucceedText = [[<TEXT_INTRODUCE_RESTART_MISSION_EXPLANATION_DIALOG>]],  
          Name = [[<RI_##>Contextual_Menu 04: Restart Mission Explanation]],  
          Position = {
            y = -1098.34375,  
            x = 22887.8125,  
            InstanceId = [[Client1_6107]],  
            Class = [[Position]],  
            z = 74.09375
          },  
          Class = [[TalkTo]],  
          InstanceId = [[Client1_6105]],  
          Components = {
          },  
          _Seed = 1145564859
        },  
        {
          Type = [[None]],  
          InstanceId = [[Client1_6110]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6188]],  
                Event = {
                  Type = [[end of dialog]],  
                  InstanceId = [[Client1_6189]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Class = [[LogicEntityAction]],  
                Actions = {
                  {
                    Action = {
                      Type = [[Start Act]],  
                      InstanceId = [[Client1_6190]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6191]],  
                    Entity = r2.RefId([[Client1_5978]]),  
                    Class = [[ActionStep]]
                  }
                },  
                Conditions = {
                }
              }
            },  
            InstanceId = [[Client1_6108]]
          },  
          Class = [[ChatSequence]],  
          InheritPos = 1,  
          Name = [[<RI_##>Dialog 05: MISSION SUCCEED - NPC THANKS]],  
          Position = {
            y = -1101.171875,  
            x = 22898.01563,  
            InstanceId = [[Client1_6109]],  
            Class = [[Position]],  
            z = 75.1875
          },  
          Repeating = 0,  
          Base = [[palette.entities.botobjects.dialog]],  
          Components = {
            {
              Time = 9,  
              InstanceId = [[Client1_6111]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6112]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6239]]
                }
              },  
              Name = [[]]
            },  
            {
              Time = 9,  
              InstanceId = [[Client1_6114]],  
              Class = [[ChatStep]],  
              Actions = {
                {
                  Emote = [[]],  
                  InstanceId = [[Client1_6115]],  
                  Who = r2.RefId([[Client1_5987]]),  
                  Class = [[ChatAction]],  
                  Facing = r2.RefId([[]]),  
                  Says = [[Client1_6240]]
                }
              },  
              Name = [[]]
            }
          },  
          Active = 0
        },  
        {
          Description = [[]],  
          InstanceId = [[Client1_6140]],  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
              {
                Name = [[]],  
                InstanceId = [[Client1_6144]],  
                Conditions = {
                },  
                Class = [[LogicEntityAction]],  
                Event = {
                  Type = [[triggered]],  
                  InstanceId = [[Client1_6145]],  
                  Value = r2.RefId([[]]),  
                  Class = [[EventType]]
                },  
                Actions = {
                  {
                    Action = {
                      Type = [[starts dialog]],  
                      InstanceId = [[Client1_6146]],  
                      Value = r2.RefId([[]]),  
                      Class = [[ActionType]]
                    },  
                    InstanceId = [[Client1_6147]],  
                    Entity = r2.RefId([[Client1_5991]]),  
                    Class = [[ActionStep]]
                  }
                }
              }
            },  
            InstanceId = [[Client1_6141]]
          },  
          Class = [[UserTrigger]],  
          InheritPos = 1,  
          Name = [[<TEMP> START_MISSION_AVAILABILITY]],  
          Position = {
            y = -1111.484375,  
            x = 22889.60938,  
            InstanceId = [[Client1_6142]],  
            Class = [[Position]],  
            z = 74.765625
          },  
          Components = {
          },  
          Base = [[palette.entities.botobjects.user_event]]
        },  
        {
          Item2Id = r2.RefId([[]]),  
          Active = 1,  
          Behavior = {
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            InstanceId = [[Client1_6223]]
          },  
          Class = [[EasterEgg]],  
          ItemQty = 1,  
          Item1Qty = 1,  
          Item3Qty = 1,  
          Base = [[palette.entities.botobjects.chest_wisdom_std_sel]],  
          Item2Qty = 1,  
          Item1Id = r2.RefId([[Client1_6036]]),  
          InheritPos = 1,  
          Name = [[<TEMP> ITEM REQUEST]],  
          Position = {
            y = -1109.359375,  
            x = 22889.71875,  
            InstanceId = [[Client1_6224]],  
            Class = [[Position]],  
            z = 74.640625
          },  
          _Seed = 1145567963,  
          InstanceId = [[Client1_6222]],  
          Components = {
          },  
          Item3Id = r2.RefId([[]])
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5977]],  
        Class = [[Position]],  
        z = 0
      },  
      InheritPos = 1,  
      ManualWeather = 0
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_5968]],  
    Texts = {
      {
        Count = 6,  
        InstanceId = [[Client1_5994]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Texte Demande Aide 01>]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_5997]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Texte Demande Aide 02>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6000]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Texte Demande Aide 03>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6005]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Texte Demande Aide 05>]]
      },  
      {
        Count = 13,  
        InstanceId = [[Client1_6012]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Explication Mission>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6019]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Fin des explications>]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_6029]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Réponse du pnj suite au refus du joueur>]]
      },  
      {
        Count = 6,  
        InstanceId = [[Client1_6032]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Réponse du npc suite à l'aide du joueur>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6113]],  
        Class = [[TextManagerEntry]],  
        Text = [[<Texte de remerciement du pnj>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6116]],  
        Class = [[TextManagerEntry]],  
        Text = [[<A la fin de ce dialog on peut déclencher l'evenement qui cloturera tout cet ensemble (changement d'act, disparition de pnj etc)>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6225]],  
        Class = [[TextManagerEntry]],  
        Text = [[< A la fin de ce dialogue on peut declencher l'evenement qui cloturera la mission (changement d'act, disparition de pnj etc... ici je le tue :p) >]]
      },  
      {
        Count = 9,  
        InstanceId = [[Client1_6226]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_HELP_01>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6227]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_HELP_02>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6228]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_HELP_03>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6229]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_HELP_04>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6230]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_HELP_05>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6232]],  
        Class = [[TextManagerEntry]],  
        Text = [[<END_OF_MISSION_EXPLANATION>]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_6233]],  
        Class = [[TextManagerEntry]],  
        Text = [[<START_OF_MISSION_EXPLANATION>]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_6234]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_FOLLOWING_THE_MISSION_ACCEPTATION_BY_PLAYER>]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_6235]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_FOLLOWING_THE_REJECT_<Reponse du pnj suite au refus du joueur>]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_6236]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_FOLLOWING_THE_REJECT>]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_6237]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_FOLLOWING_THE_ACCEPT_MISSION>]]
      },  
      {
        Count = 4,  
        InstanceId = [[Client1_6238]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_FOLLOWING_THE_REJECT_MISSION>]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client1_6239]],  
        Class = [[TextManagerEntry]],  
        Text = [[<TEXT_MISSION_SUCCEED___NPC_THANKS>]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_6240]],  
        Class = [[TextManagerEntry]],  
        Text = [[<END OF MISSION REQUEST ITEM>]]
      }
    }
  }
}