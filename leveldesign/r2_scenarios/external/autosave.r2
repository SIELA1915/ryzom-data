scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_541]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 0,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_543]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.3]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    ActivityStep = 1,  
    Region = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 12,  
      Behavior = {
        InstanceId = [[Client1_544]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Permanent]],  
      Version = 3,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[Permanent]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_545]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_616]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_618]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_619]],  
                    x = 21570.79688,  
                    y = -1353,  
                    z = 109.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_621]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_622]],  
                    x = 21579.21875,  
                    y = -1366.359375,  
                    z = 112.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_624]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_625]],  
                    x = 21563.6875,  
                    y = -1374.53125,  
                    z = 107.9375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_627]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_628]],  
                    x = 21561.375,  
                    y = -1360.0625,  
                    z = 105.21875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_615]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_547]]
        },  
        {
          InstanceId = [[Client1_614]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_613]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_612]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_610]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_608]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_629]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_630]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_616]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1364.859375,  
                x = 21565.32813,  
                InstanceId = [[Client1_611]],  
                Class = [[Position]],  
                z = 107.109375
              },  
              Angle = -3.125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_582]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_580]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1361.6875,  
                x = 21563.82813,  
                InstanceId = [[Client1_583]],  
                Class = [[Position]],  
                z = 106.28125
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_606]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_604]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1362.515625,  
                x = 21565.04688,  
                InstanceId = [[Client1_607]],  
                Class = [[Position]],  
                z = 106.828125
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_598]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_596]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1362.875,  
                x = 21567,  
                InstanceId = [[Client1_599]],  
                Class = [[Position]],  
                z = 107.609375
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_602]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_600]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1364.765625,  
                x = 21567.82813,  
                InstanceId = [[Client1_603]],  
                Class = [[Position]],  
                z = 108.0625
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_590]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_588]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1360.375,  
                x = 21566.71875,  
                InstanceId = [[Client1_591]],  
                Class = [[Position]],  
                z = 107.359375
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_578]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_576]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1361.234375,  
                x = 21568.32813,  
                InstanceId = [[Client1_579]],  
                Class = [[Position]],  
                z = 108.078125
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_586]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_584]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1361.953125,  
                x = 21570.03125,  
                InstanceId = [[Client1_587]],  
                Class = [[Position]],  
                z = 108.875
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_594]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_592]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1362.296875,  
                x = 21571.59375,  
                InstanceId = [[Client1_595]],  
                Class = [[Position]],  
                z = 109.515625
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_574]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_572]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1358.859375,  
                x = 21571.03125,  
                InstanceId = [[Client1_575]],  
                Class = [[Position]],  
                z = 109.25
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_566]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_564]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1358.765625,  
                x = 21572.51563,  
                InstanceId = [[Client1_567]],  
                Class = [[Position]],  
                z = 109.890625
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_570]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_568]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Stinking Cute]],  
              Position = {
                y = -1360.359375,  
                x = 21573.15625,  
                InstanceId = [[Client1_571]],  
                Class = [[Position]],  
                z = 110.109375
              },  
              Angle = -2.78125,  
              Base = [[palette.entities.creatures.cdagb2]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_546]]
    },  
    {
      Cost = 0,  
      Behavior = {
        InstanceId = [[Client1_548]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Name = [[Act 1]],  
      Version = 3,  
      WeatherValue = 0,  
      InheritPos = 1,  
      Events = {
      },  
      Title = [[Act 1]],  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_549]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_551]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_550]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_542]],  
    Texts = {
    }
  }
}