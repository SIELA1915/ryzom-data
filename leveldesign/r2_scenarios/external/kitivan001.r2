scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 23,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    Position = 0,  
    TextManager = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 10,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_10]],  
              Base = [[palette.entities.botobjects.tomb_5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_11]],  
                x = 26026.625,  
                y = -2531.765625,  
                z = -9.984375
              },  
              Angle = 1.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_8]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tomb stone 5 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_14]],  
              Base = [[palette.entities.botobjects.tomb_2]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_15]],  
                x = 26029.96875,  
                y = -2525.265625,  
                z = -9.359375
              },  
              Angle = 3.593411922,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_12]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[tomb stone 2 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_18]],  
              Base = [[palette.entities.botobjects.chariot_working]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19]],  
                x = 26029.125,  
                y = -2531.546875,  
                z = -10.40625
              },  
              Angle = 2.578125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_16]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[working chariot 1]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_29]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_31]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_32]],  
                    x = 26054.35938,  
                    y = -2519.28125,  
                    z = -10.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_34]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_35]],  
                    x = 26057.59375,  
                    y = -2463.984375,  
                    z = -12.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_37]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_38]],  
                    x = 26091.29688,  
                    y = -2477.71875,  
                    z = -13.078125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_28]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_41]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_42]],  
                x = 26065.125,  
                y = -2494.734375,  
                z = -11.0625
              },  
              Angle = 0.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_39]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_45]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_46]],  
                x = 26064.53125,  
                y = -2490.296875,  
                z = -11.25
              },  
              Angle = 0.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_43]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_49]],  
              Base = [[palette.entities.creatures.ckfrb1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_50]],  
                x = 26067.85938,  
                y = -2490.0625,  
                z = -12.5625
              },  
              Angle = 0.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_47]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Assault Kirosta]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_57]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_58]],  
                x = 26016.78125,  
                y = -2390.1875,  
                z = -9.609375
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_55]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kitin totem 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_61]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_62]],  
                x = 26012.625,  
                y = -2376.5625,  
                z = -10.171875
              },  
              Angle = -2.390625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_59]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kitin totem 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_65]],  
              Base = [[palette.entities.botobjects.karavan_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_66]],  
                x = 26014.73438,  
                y = -2383.625,  
                z = -9.953125
              },  
              Angle = -2.882313251,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_63]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[karavan gateway 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_69]],  
              Base = [[palette.entities.botobjects.vegetable_gateway]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_70]],  
                x = 26029.03125,  
                y = -2528.484375,  
                z = -10.3125
              },  
              Angle = 2.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_67]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[living gateway 1]]
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[ACT 0: Permanent]]
    },  
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_6]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act I: Nothing]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}