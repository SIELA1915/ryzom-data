scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_2181]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_2183]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    ChatSequence = 0,  
    ChatAction = 0,  
    ChatStep = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    ActivityStep = 0,  
    TextManager = 0,  
    Position = 0,  
    Behavior = 0,  
    EventType = 0,  
    LogicEntityReaction = 0,  
    Npc = 0,  
    ActionType = 0,  
    MapDescription = 0,  
    ActionStep = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_2185]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2184]]
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Title = [[Act 1]],  
      ActivitiesIds = {
        [[Client1_2192]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_2190]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_2188]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_2202]],  
                    LogicEntityAction = [[Client1_2199]],  
                    ActionStep = [[Client1_2201]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                  {
                    Class = [[ChatSequence]],  
                    InstanceId = [[Client1_2194]],  
                    Name = [[Chat1]],  
                    Components = {
                      {
                        Class = [[ChatStep]],  
                        InstanceId = [[Client1_2195]],  
                        Time = 0,  
                        Actions = {
                          {
                            Class = [[ChatAction]],  
                            InstanceId = [[Client1_2196]],  
                            Emote = [[]],  
                            Who = [[Client1_2190]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[]]
                          }
                        },  
                        Name = [[]]
                      }
                    },  
                    Repeating = 1
                  }
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_2199]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_2201]],  
                        Entity = r2.RefId([[Client1_2190]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_2200]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_2192]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_2198]],  
                      Type = [[end of chat step]],  
                      Value = r2.RefId([[Client1_2195]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_2192]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_2193]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Capryketh]],  
              Position = {
                y = -2018.0625,  
                x = 26944.10938,  
                InstanceId = [[Client1_2191]],  
                Class = [[Position]],  
                z = -2.953125
              },  
              Angle = 0.28125,  
              Base = [[palette.entities.creatures.chcdb7]],  
              ActivitiesId = {
                [[Client1_2192]]
              }
            }
          },  
          InstanceId = [[Client1_2187]]
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_2186]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2182]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_2197]],  
        Count = 1,  
        Text = [[]]
      }
    }
  }
}