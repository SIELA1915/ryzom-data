scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 15,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    Road = 0,  
    TextManager = 0,  
    Npc = 0,  
    ActivityStep = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    EventType = 0,  
    Position = 0,  
    ActionStep = 0,  
    ActionType = 0,  
    WayPoint = 0,  
    LogicEntityReaction = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_116]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_115]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_118]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2029.265625,  
                    x = 26155.875,  
                    InstanceId = [[Client1_119]],  
                    Class = [[Position]],  
                    z = -3.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_121]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2035.421875,  
                    x = 26211.01563,  
                    InstanceId = [[Client1_122]],  
                    Class = [[Position]],  
                    z = -4.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_124]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1994.28125,  
                    x = 26230,  
                    InstanceId = [[Client1_125]],  
                    Class = [[Position]],  
                    z = -5.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_127]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2013.546875,  
                    x = 26294.01563,  
                    InstanceId = [[Client1_128]],  
                    Class = [[Position]],  
                    z = 0.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_130]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2017.390625,  
                    x = 26334.9375,  
                    InstanceId = [[Client1_131]],  
                    Class = [[Position]],  
                    z = 4.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_133]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2022.515625,  
                    x = 26381.125,  
                    InstanceId = [[Client1_134]],  
                    Class = [[Position]],  
                    z = -3.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_136]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1969.265625,  
                    x = 26446.1875,  
                    InstanceId = [[Client1_137]],  
                    Class = [[Position]],  
                    z = 8.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_164]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_166]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1997.375,  
                    x = 26266.1875,  
                    InstanceId = [[Client1_167]],  
                    Class = [[Position]],  
                    z = -1.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_169]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1992.09375,  
                    x = 26250.5625,  
                    InstanceId = [[Client1_170]],  
                    Class = [[Position]],  
                    z = -2.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_172]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2013.234375,  
                    x = 26243.40625,  
                    InstanceId = [[Client1_173]],  
                    Class = [[Position]],  
                    z = -4.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_175]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2023.28125,  
                    x = 26265.8125,  
                    InstanceId = [[Client1_176]],  
                    Class = [[Position]],  
                    z = -3.078125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_178]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2005.828125,  
                    x = 26279.1875,  
                    InstanceId = [[Client1_179]],  
                    Class = [[Position]],  
                    z = -2.046875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_163]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    },  
    {
      Cost = 11,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_6]],  
      ActivitiesIds = {
        [[Client1_97]],  
        [[Client1_138]],  
        [[Client1_180]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_7]]
        },  
        {
          InstanceId = [[Client1_70]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -3.5,  
            x = 4.390625,  
            InstanceId = [[Client1_69]],  
            Class = [[Position]],  
            z = -1.375
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_113]],  
              ActivitiesId = {
                [[Client1_138]]
              },  
              HairType = 5166,  
              TrouserColor = 0,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6727214,  
              FeetColor = 0,  
              GabaritBreastSize = 0,  
              GabaritHeight = 7,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_111]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_187]],  
                    LogicEntityAction = [[Client1_189]],  
                    ActionStep = [[Client1_191]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_183]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_185]],  
                        Entity = r2.RefId([[Client1_162]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_184]],  
                          Type = [[begin activity sequence]],  
                          Value = r2.RefId([[Client1_180]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_182]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  },  
                  {
                    Class = [[LogicEntityAction]],  
                    InstanceId = [[Client1_189]],  
                    Conditions = {
                    },  
                    Actions = {
                      {
                        Class = [[ActionStep]],  
                        InstanceId = [[Client1_191]],  
                        Entity = r2.RefId([[Client1_113]]),  
                        Action = {
                          Class = [[ActionType]],  
                          InstanceId = [[Client1_190]],  
                          Type = [[]],  
                          Value = r2.RefId([[]])
                        }
                      }
                    },  
                    Event = {
                      Class = [[EventType]],  
                      InstanceId = [[Client1_188]],  
                      Type = [[]],  
                      Value = r2.RefId([[]])
                    },  
                    Name = [[]]
                  }
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_138]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[0]],  
                        InstanceId = [[Client1_139]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_116]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              Angle = -1.421875,  
              Base = [[palette.entities.npcs.bandits.t_light_melee_220]],  
              SheetClient = [[basic_matis_male.creature]],  
              ArmModel = 0,  
              WeaponLeftHand = 0,  
              Sheet = [[ring_light_melee_pierce_f2.creature]],  
              Position = {
                y = -1965.671875,  
                x = 26160.71875,  
                InstanceId = [[Client1_114]],  
                Class = [[Position]],  
                z = 1.234375
              },  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 6728750,  
              InheritPos = 1,  
              ArmColor = 2,  
              Name = [[Chouly]],  
              Sex = 0,  
              WeaponRightHand = 6764078,  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 0
            },  
            {
              InstanceId = [[Client1_10]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_97]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_98]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gibbakya]],  
              Position = {
                y = -1957.671875,  
                x = 26152.28125,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = 3.5
              },  
              Angle = -0.375,  
              Base = [[palette.entities.creatures.cdbje7]],  
              ActivitiesId = {
                [[Client1_97]]
              }
            },  
            {
              InstanceId = [[Client1_34]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_32]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1953.234375,  
                x = 26147.60938,  
                InstanceId = [[Client1_35]],  
                Class = [[Position]],  
                z = 4.3125
              },  
              Angle = -0.203125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1956.421875,  
                x = 26145.75,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 4.21875
              },  
              Angle = -0.203125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1960.21875,  
                x = 26145.23438,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 4.078125
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_54]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_52]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1962.9375,  
                x = 26147.15625,  
                InstanceId = [[Client1_55]],  
                Class = [[Position]],  
                z = 4.140625
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_58]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_56]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1962.328125,  
                x = 26141.45313,  
                InstanceId = [[Client1_59]],  
                Class = [[Position]],  
                z = 4.125
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_62]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_60]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1958.625,  
                x = 26141.04688,  
                InstanceId = [[Client1_63]],  
                Class = [[Position]],  
                z = 4.171875
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_66]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_64]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Pustulous Gibbai]],  
              Position = {
                y = -1953.640625,  
                x = 26142.73438,  
                InstanceId = [[Client1_67]],  
                Class = [[Position]],  
                z = 4.6875
              },  
              Angle = -0.03125,  
              Base = [[palette.entities.creatures.cdbge1]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_68]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_162]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_161]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_158]],  
              ActivitiesId = {
                [[Client1_180]]
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 4,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 6,  
              HandsModel = 6724654,  
              FeetColor = 1,  
              GabaritBreastSize = 6,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 6727726,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 0,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_156]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    Class = [[LogicEntityReaction]],  
                    InstanceId = [[Client1_186]],  
                    LogicEntityAction = [[Client1_183]],  
                    ActionStep = [[Client1_185]],  
                    Name = [[]]
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_180]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_181]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_164]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6726702,  
              PlayerAttackable = 1,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.civils.t_civil_170]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 6728238,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmColor = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 6728750,  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              Name = [[Tur'Luth]],  
              Sex = 1,  
              Position = {
                y = -2005.90625,  
                x = 26261.40625,  
                InstanceId = [[Client1_159]],  
                Class = [[Position]],  
                z = -2.890625
              },  
              MorphTarget7 = 0,  
              MorphTarget3 = 4,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_154]],  
              ActivitiesId = {
              },  
              HairType = 5623342,  
              TrouserColor = 1,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 6,  
              HandsModel = 6724398,  
              FeetColor = 1,  
              GabaritBreastSize = 6,  
              GabaritHeight = 6,  
              HairColor = 1,  
              EyesColor = 3,  
              TrouserModel = 6724910,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_152]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 3,  
              FeetModel = 6723886,  
              PlayerAttackable = 1,  
              Angle = -2.140625,  
              Base = [[palette.entities.npcs.civils.t_civil_120]],  
              SheetClient = [[basic_tryker_male.creature]],  
              ArmModel = 6725422,  
              WeaponLeftHand = 0,  
              BotAttackable = 1,  
              ArmColor = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 4,  
              JacketModel = 6725934,  
              InheritPos = 1,  
              Sheet = [[ring_civil_light_melee_blunt_d2.creature]],  
              Name = [[Tur'Luth]],  
              Sex = 1,  
              Position = {
                y = -2004.84375,  
                x = 26260.98438,  
                InstanceId = [[Client1_155]],  
                Class = [[Position]],  
                z = -2.984375
              },  
              MorphTarget7 = 4,  
              MorphTarget3 = 4,  
              Tattoo = 13
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_160]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}