scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 10,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 1,  
    TextManager = 0,  
    ChatAction = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ChatStep = 0,  
    Position = 0,  
    ChatSequence = 0,  
    EventType = 0,  
    LogicEntityBehavior = 0,  
    Npc = 0
  },  
  Acts = {
    {
      Cost = 20,  
      Behavior = {
        InstanceId = [[Client1_4]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Title = [[Permanent]],  
      Name = [[Permanent]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_18]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              Position = {
                y = -1288.140625,  
                x = 29703.76563,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = 74.5625
              },  
              Angle = -2.15625,  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_30]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_28]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[cosmetics tent 1]],  
              Position = {
                y = -1295.609375,  
                x = 29707.45313,  
                InstanceId = [[Client1_31]],  
                Class = [[Position]],  
                z = 73.609375
              },  
              Angle = -6.15625,  
              Base = [[palette.entities.botobjects.tent_cosmetics]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_40]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              Position = {
                y = -1282.953125,  
                x = 29696.59375,  
                InstanceId = [[Client1_43]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              Angle = -1.796875,  
              Base = [[palette.entities.botobjects.paddock]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_46]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_44]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 1 1]],  
              Position = {
                y = -1299.578125,  
                x = 29707.60938,  
                InstanceId = [[Client1_47]],  
                Class = [[Position]],  
                z = 72.9375
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.pack_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_50]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_48]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3 1]],  
              Position = {
                y = -1280.890625,  
                x = 29689.70313,  
                InstanceId = [[Client1_51]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -1.203125,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_62]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_60]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1297.625,  
                x = 29690.78125,  
                InstanceId = [[Client1_63]],  
                Class = [[Position]],  
                z = 74.9375
              },  
              Angle = -2.515625,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_98]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_96]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bird totem 1]],  
              Position = {
                y = -1282.21875,  
                x = 29714.40625,  
                InstanceId = [[Client1_99]],  
                Class = [[Position]],  
                z = 75.0625
              },  
              Angle = -2.125,  
              Base = [[palette.entities.botobjects.totem_bird]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_136]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_138]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1289.8125,  
                    x = 29698.35938,  
                    InstanceId = [[Client1_139]],  
                    Class = [[Position]],  
                    z = 75.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_141]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1288.1875,  
                    x = 29699.39063,  
                    InstanceId = [[Client1_142]],  
                    Class = [[Position]],  
                    z = 75.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_144]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1287,  
                    x = 29701.03125,  
                    InstanceId = [[Client1_145]],  
                    Class = [[Position]],  
                    z = 76.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_147]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1288.921875,  
                    x = 29704.78125,  
                    InstanceId = [[Client1_148]],  
                    Class = [[Position]],  
                    z = 75.703125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_153]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    Activity = [[Stand Still]],  
                    InstanceId = [[Client1_154]],  
                    Class = [[Position]],  
                    y = -1291.65625,  
                    x = 29706.42188,  
                    z = 75.28125,  
                    TimeLimit = [[No Limit]],  
                    TimeLimitValue = [[]],  
                    ActivityZoneId = r2.RefId([[]])
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_156]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1293.796875,  
                    x = 29704.0625,  
                    InstanceId = [[Client1_157]],  
                    Class = [[Position]],  
                    z = 74.9375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_159]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1294.03125,  
                    x = 29702.54688,  
                    InstanceId = [[Client1_160]],  
                    Class = [[Position]],  
                    z = 74.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_162]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1292.09375,  
                    x = 29700.40625,  
                    InstanceId = [[Client1_163]],  
                    Class = [[Position]],  
                    z = 75.4375
                  }
                }
              },  
              Position = {
                y = -1.078125,  
                x = -2.203125,  
                InstanceId = [[Client1_135]],  
                Class = [[Position]],  
                z = -1.359375
              }
            },  
            {
              InstanceId = [[Client1_414]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_412]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olansi I 1]],  
              Position = {
                y = -1288.828125,  
                x = 29722.54688,  
                InstanceId = [[Client1_415]],  
                Class = [[Position]],  
                z = 75.390625
              },  
              Angle = -2.828125,  
              Base = [[palette.entities.botobjects.fy_s2_palmtree_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_434]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_432]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga III 1]],  
              Position = {
                y = -1250.03125,  
                x = 29700.26563,  
                InstanceId = [[Client1_435]],  
                Class = [[Position]],  
                z = 73.296875
              },  
              Angle = -2.171875,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_c]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_438]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_436]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[botoga I 1]],  
              Position = {
                y = -1298.9375,  
                x = 29644.5,  
                InstanceId = [[Client1_439]],  
                Class = [[Position]],  
                z = 79.34375
              },  
              Angle = 0.265625,  
              Base = [[palette.entities.botobjects.fy_s1_baobab_a]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_887]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_886]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_889]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1195.953125,  
                    x = 29648.95313,  
                    InstanceId = [[Client1_890]],  
                    Class = [[Position]],  
                    z = 73.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_892]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1212.015625,  
                    x = 29656.5625,  
                    InstanceId = [[Client1_893]],  
                    Class = [[Position]],  
                    z = 72.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_895]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1221.578125,  
                    x = 29676.14063,  
                    InstanceId = [[Client1_896]],  
                    Class = [[Position]],  
                    z = 76.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_898]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1216.421875,  
                    x = 29680.28125,  
                    InstanceId = [[Client1_899]],  
                    Class = [[Position]],  
                    z = 75.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_901]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1192.078125,  
                    x = 29669.03125,  
                    InstanceId = [[Client1_902]],  
                    Class = [[Position]],  
                    z = 71.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_904]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1188.078125,  
                    x = 29654.10938,  
                    InstanceId = [[Client1_905]],  
                    Class = [[Position]],  
                    z = 72.75
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1047]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1045]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[olash I 1]],  
              Position = {
                y = -1282.421875,  
                x = 29703,  
                InstanceId = [[Client1_1048]],  
                Class = [[Position]],  
                z = 75.015625
              },  
              Angle = -3.328109503,  
              Base = [[palette.entities.botobjects.fy_s2_coconuts_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1051]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1049]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[papalexi I 1]],  
              Position = {
                y = -1245.984375,  
                x = 29685.76563,  
                InstanceId = [[Client1_1052]],  
                Class = [[Position]],  
                z = 75.125
              },  
              Angle = -2.335100174,  
              Base = [[palette.entities.botobjects.fy_s2_papaleaf_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1055]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1053]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[papalexi II 1]],  
              Position = {
                y = -1236,  
                x = 29692.98438,  
                InstanceId = [[Client1_1056]],  
                Class = [[Position]],  
                z = 76.390625
              },  
              Angle = -2.304019213,  
              Base = [[palette.entities.botobjects.fy_s2_papaleaf_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1135]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1133]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin mound 1]],  
              Position = {
                y = -1426.015625,  
                x = 29525.09375,  
                InstanceId = [[Client1_1136]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1139]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1137]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 1]],  
              Position = {
                y = -1401.96875,  
                x = 29559.79688,  
                InstanceId = [[Client1_1140]],  
                Class = [[Position]],  
                z = 78.359375
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1143]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1141]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 2]],  
              Position = {
                y = -1403.375,  
                x = 29556.125,  
                InstanceId = [[Client1_1144]],  
                Class = [[Position]],  
                z = 78.375
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1147]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1145]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 3]],  
              Position = {
                y = -1406.109375,  
                x = 29559.73438,  
                InstanceId = [[Client1_1148]],  
                Class = [[Position]],  
                z = 78.78125
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1151]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1149]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin egg 4]],  
              Position = {
                y = -1406.28125,  
                x = 29557.4375,  
                InstanceId = [[Client1_1152]],  
                Class = [[Position]],  
                z = 78.703125
              },  
              Angle = 0.84375,  
              Base = [[palette.entities.botobjects.kitin_egg]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 3]],  
              InstanceId = [[Client1_1158]],  
              Class = [[Region]],  
              Position = {
                y = -4.984375,  
                x = 0.296875,  
                InstanceId = [[Client1_1157]],  
                Class = [[Position]],  
                z = 0.625
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1160]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1413.984375,  
                    x = 29544.0625,  
                    InstanceId = [[Client1_1161]],  
                    Class = [[Position]],  
                    z = 76.03125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1163]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1427.15625,  
                    x = 29549.42188,  
                    InstanceId = [[Client1_1164]],  
                    Class = [[Position]],  
                    z = 75.890625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1166]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1430.78125,  
                    x = 29558.5,  
                    InstanceId = [[Client1_1167]],  
                    Class = [[Position]],  
                    z = 75.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1169]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1411.46875,  
                    x = 29564.89063,  
                    InstanceId = [[Client1_1170]],  
                    Class = [[Position]],  
                    z = 76.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1172]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1401.8125,  
                    x = 29563.26563,  
                    InstanceId = [[Client1_1173]],  
                    Class = [[Position]],  
                    z = 77.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1175]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1394.609375,  
                    x = 29561.10938,  
                    InstanceId = [[Client1_1176]],  
                    Class = [[Position]],  
                    z = 77.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1178]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1397.65625,  
                    x = 29549.82813,  
                    InstanceId = [[Client1_1179]],  
                    Class = [[Position]],  
                    z = 76.6875
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_1254]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1252]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[insect carrion 1]],  
              Position = {
                y = -1408.359375,  
                x = 29534.0625,  
                InstanceId = [[Client1_1255]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Angle = -0.00331853074,  
              Base = [[palette.entities.botobjects.carrion_insect]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1262]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1260]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[smoke 1]],  
              Position = {
                y = -1400.234375,  
                x = 29548.21875,  
                InstanceId = [[Client1_1263]],  
                Class = [[Position]],  
                z = 76.71875
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.botobjects.fx_de_vapeurs]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_7]]
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_5]],  
        Class = [[Position]],  
        z = 0
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_6]]
    },  
    {
      Cost = 24,  
      Behavior = {
        InstanceId = [[Client1_8]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Version = 3,  
      Events = {
      },  
      WeatherValue = 0,  
      InheritPos = 1,  
      Title = [[Act 1]],  
      Name = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
          },  
          InstanceId = [[Client1_11]]
        },  
        {
          InstanceId = [[Client1_134]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 30.875,  
            x = 21.625,  
            InstanceId = [[Client1_133]],  
            Class = [[Position]],  
            z = 0.828125
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_122]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 3,  
              GabaritArmsWidth = 10,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              Aggro = 1,  
              EyesColor = 3,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 10,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_120]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_617]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_618]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = -1.625,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 28,  
              MorphTarget3 = 4,  
              MorphTarget7 = 1,  
              InheritPos = 1,  
              Sex = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 2,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756398,  
              ArmColor = 3,  
              Name = [[guard3]],  
              Position = {
                y = -1305.40625,  
                x = 29690.875,  
                InstanceId = [[Client1_123]],  
                Class = [[Position]],  
                z = 74.1875
              },  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_172]],  
              ActivitiesId = {
              },  
              HairType = 2606,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 1,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 5,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_170]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Angle = -1.296875,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 24,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              Sex = 1,  
              WeaponRightHand = 6754862,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard5]],  
              Position = {
                y = -1306.5625,  
                x = 29690.1875,  
                InstanceId = [[Client1_173]],  
                Class = [[Position]],  
                z = 74.078125
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 7,  
              InstanceId = [[Client1_168]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 6,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 4,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_166]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6699566,  
              Angle = -1.359375,  
              Base = [[palette.entities.npcs.bandits.f_melee_tank_170]],  
              Tattoo = 7,  
              MorphTarget3 = 2,  
              MorphTarget7 = 5,  
              Sheet = [[ring_melee_tank_slash_e1.creature]],  
              Sex = 1,  
              WeaponRightHand = 6754350,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 1,  
              JacketModel = 6702126,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard2]],  
              Position = {
                y = -1304.6875,  
                x = 29689.07813,  
                InstanceId = [[Client1_169]],  
                Class = [[Position]],  
                z = 74.03125
              },  
              WeaponLeftHand = 6773294,  
              PlayerAttackable = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_176]],  
              ActivitiesId = {
              },  
              HairType = 6702,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_174]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Angle = -1.375,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 21,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sheet = [[ring_light_melee_blunt_e4.creature]],  
              Sex = 1,  
              WeaponRightHand = 6752302,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[guard4]],  
              Position = {
                y = -1307.265625,  
                x = 29692.39063,  
                InstanceId = [[Client1_177]],  
                Class = [[Position]],  
                z = 74.203125
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_130]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 7,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 7,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_908]],  
                    Class = [[LogicEntityAction]],  
                    Conditions = {
                    },  
                    Actions = {
                    },  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_907]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 6699566,  
              Angle = -2.453125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 19,  
              MorphTarget3 = 7,  
              MorphTarget7 = 3,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 0,  
              JacketModel = 6702126,  
              WeaponRightHand = 6755886,  
              ArmColor = 3,  
              Name = [[guard6]],  
              Position = {
                y = -1307.96875,  
                x = 29690.42188,  
                InstanceId = [[Client1_131]],  
                Class = [[Position]],  
                z = 74.25
              },  
              Sheet = [[ring_guard_melee_tank_pierce_e1.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_132]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client1_468]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = -1.609375,  
            x = -12.859375,  
            InstanceId = [[Client1_467]],  
            Class = [[Position]],  
            z = -0.03125
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 5,  
              InstanceId = [[Client1_444]],  
              ActivitiesId = {
              },  
              HairType = 6700590,  
              TrouserColor = 4,  
              MorphTarget5 = 1,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 7,  
              HandsModel = 6700078,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 4,  
              Aggro = 1,  
              EyesColor = 3,  
              AutoSpawn = 1,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 7,  
              HandsColor = 4,  
              MorphTarget1 = 7,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_442]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1211]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1212]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 7,  
              FeetModel = 6699566,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.guards.f_guard_195]],  
              Tattoo = 2,  
              MorphTarget3 = 5,  
              MorphTarget7 = 2,  
              InheritPos = 1,  
              Sex = 1,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 3,  
              Name = [[squad5]],  
              Position = {
                y = -1280.015625,  
                x = 29722.85938,  
                InstanceId = [[Client1_445]],  
                Class = [[Position]],  
                z = 75.09375
              },  
              Sheet = [[ring_guard_melee_tank_slash_e1.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6701614,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_460]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 0,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 11,  
              GabaritHeight = 14,  
              HairColor = 3,  
              EyesColor = 5,  
              AutoSpawn = 1,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_458]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 1,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 24,  
              MorphTarget3 = 5,  
              MorphTarget7 = 1,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              Sex = 1,  
              WeaponRightHand = 6754862,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad4]],  
              Position = {
                y = -1276.796875,  
                x = 29724.46875,  
                InstanceId = [[Client1_461]],  
                Class = [[Position]],  
                z = 75.171875
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_456]],  
              ActivitiesId = {
              },  
              HairType = 5622062,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 0,  
              EyesColor = 2,  
              AutoSpawn = 1,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_454]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 15,  
              MorphTarget3 = 5,  
              MorphTarget7 = 0,  
              Sheet = [[ring_light_melee_slash_e4.creature]],  
              Sex = 1,  
              WeaponRightHand = 6754094,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 5,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad2]],  
              Position = {
                y = -1278.375,  
                x = 29724.59375,  
                InstanceId = [[Client1_457]],  
                Class = [[Position]],  
                z = 75.109375
              },  
              WeaponLeftHand = 0,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_464]],  
              ActivitiesId = {
              },  
              HairType = 6958,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 2,  
              GabaritArmsWidth = 7,  
              HandsModel = 6705710,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 1,  
              AutoSpawn = 1,  
              TrouserModel = 6706222,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_462]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6704942,  
              Angle = 0.53125,  
              Base = [[palette.entities.npcs.bandits.f_light_melee_170]],  
              Tattoo = 8,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Sheet = [[ring_light_melee_pierce_e4.creature]],  
              Sex = 1,  
              WeaponRightHand = 6753326,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              JacketModel = 6707246,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[squad3]],  
              Position = {
                y = -1279.1875,  
                x = 29719.73438,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              WeaponLeftHand = 6753326,  
              PlayerAttackable = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_female.creature]]
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_466]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_719]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 5]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_718]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              MorphTarget8 = 6,  
              InstanceId = [[Client1_711]],  
              ActivitiesId = {
              },  
              HairType = 7470,  
              TrouserColor = 4,  
              MorphTarget5 = 3,  
              MorphTarget6 = 7,  
              GabaritArmsWidth = 7,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 5,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_709]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_935]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_936]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[Alert]],  
                            InstanceId = [[Client1_937]],  
                            Who = [[Client1_711]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_938]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1287]],  
                    Actions = {
                    },  
                    Conditions = {
                    },  
                    Event = {
                      Type = [[]],  
                      InstanceId = [[Client1_1286]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]]
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_930]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_931]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_933]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_934]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_935]]),  
                        ActivityZoneId = r2.RefId([[Client1_887]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1034]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1035]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_1036]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 2,  
              FeetModel = 6702638,  
              Speed = [[run]],  
              Angle = -1.875,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 9,  
              MorphTarget3 = 0,  
              MorphTarget7 = 0,  
              Position = {
                y = -1288.5,  
                x = 29694.17188,  
                InstanceId = [[Client1_712]],  
                Class = [[Position]],  
                z = 74.90625
              },  
              Sex = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 3,  
              JacketModel = 6704686,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Aedan]],  
              BotAttackable = 1,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_tryker_male.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_715]],  
              ActivitiesId = {
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 4,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 7,  
              HandsModel = 6703150,  
              FeetColor = 3,  
              GabaritBreastSize = 7,  
              GabaritHeight = 8,  
              HairColor = 5,  
              EyesColor = 4,  
              TrouserModel = 6703662,  
              GabaritLegsWidth = 7,  
              HandsColor = 3,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_713]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6702638,  
              Angle = -3.046875,  
              Base = [[palette.entities.npcs.civils.f_civil_170]],  
              Tattoo = 30,  
              MorphTarget3 = 4,  
              MorphTarget7 = 4,  
              Position = {
                y = -1301.78125,  
                x = 29707.5,  
                InstanceId = [[Client1_716]],  
                Class = [[Position]],  
                z = 72.5
              },  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 4,  
              JacketModel = 6704686,  
              InheritPos = 1,  
              ArmColor = 3,  
              Name = [[Aenia]],  
              BotAttackable = 1,  
              Sheet = [[ring_civil_light_melee_blunt_e2.creature]],  
              WeaponLeftHand = 0,  
              ArmModel = 6704174,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              InstanceId = [[Client1_118]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_116]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Voracious Mektoub]],  
              Position = {
                y = -1289.046875,  
                x = 29689.32813,  
                InstanceId = [[Client1_119]],  
                Class = [[Position]],  
                z = 75.15625
              },  
              PlayerAttackable = 0,  
              Angle = -0.6875,  
              Base = [[palette.entities.creatures.chhpf4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_106]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_104]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Obstinate Mektoub]],  
              Position = {
                y = -1284.421875,  
                x = 29697.9375,  
                InstanceId = [[Client1_107]],  
                Class = [[Position]],  
                z = 74.96875
              },  
              PlayerAttackable = 0,  
              Angle = -2.640625,  
              Base = [[palette.entities.creatures.chhdd2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_102]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_100]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Gruff Mektoub]],  
              Position = {
                y = -1283.46875,  
                x = 29694.78125,  
                InstanceId = [[Client1_103]],  
                Class = [[Position]],  
                z = 75.046875
              },  
              PlayerAttackable = 0,  
              Angle = -1.265625,  
              Base = [[palette.entities.creatures.chhdd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_114]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_112]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Rooting Mektoub]],  
              Position = {
                y = -1281.453125,  
                x = 29696.90625,  
                InstanceId = [[Client1_115]],  
                Class = [[Position]],  
                z = 75.078125
              },  
              PlayerAttackable = 0,  
              Angle = -1.84375,  
              Base = [[palette.entities.creatures.chhdd3]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_717]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          }
        },  
        {
          InstanceId = [[Client1_1196]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 6]],  
          Position = {
            y = 6.734375,  
            x = 13.421875,  
            InstanceId = [[Client1_1195]],  
            Class = [[Position]],  
            z = -2.296875
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1194]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_1282]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1280]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_1284]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1285]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_136]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Kipekoo]],  
              Position = {
                y = -1396.109375,  
                x = 29555.14063,  
                InstanceId = [[Client1_1283]],  
                Class = [[Position]],  
                z = 76.859375
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckjpf7]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1188]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1186]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              Position = {
                y = -1400.65625,  
                x = 29547.64063,  
                InstanceId = [[Client1_1189]],  
                Class = [[Position]],  
                z = 76.625
              },  
              Angle = 2.509633064,  
              Base = [[palette.entities.creatures.ckaif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1192]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1190]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              Position = {
                y = -1402.421875,  
                x = 29541.375,  
                InstanceId = [[Client1_1193]],  
                Class = [[Position]],  
                z = 79.25
              },  
              Angle = 1.671875,  
              Base = [[palette.entities.creatures.ckaif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1184]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1182]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              Position = {
                y = -1405.3125,  
                x = 29543.76563,  
                InstanceId = [[Client1_1185]],  
                Class = [[Position]],  
                z = 79.9375
              },  
              Angle = 1.863861203,  
              Base = [[palette.entities.creatures.ckaif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1215]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1213]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1412.75,  
                x = 29553.5,  
                InstanceId = [[Client1_1216]],  
                Class = [[Position]],  
                z = 79.75
              },  
              Angle = 2.203125,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1266]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1264]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1405.359375,  
                x = 29558.5,  
                InstanceId = [[Client1_1267]],  
                Class = [[Position]],  
                z = 78.015625
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1270]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1268]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kinrey]],  
              Position = {
                y = -1407.640625,  
                x = 29554.89063,  
                InstanceId = [[Client1_1271]],  
                Class = [[Position]],  
                z = 78.796875
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckbif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1278]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1276]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1402.796875,  
                x = 29554.57813,  
                InstanceId = [[Client1_1279]],  
                Class = [[Position]],  
                z = 78.125
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_1274]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1272]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Overlord Kirosta]],  
              Position = {
                y = -1405.390625,  
                x = 29552.0625,  
                InstanceId = [[Client1_1275]],  
                Class = [[Position]],  
                z = 79.28125
              },  
              Angle = 0.609375,  
              Base = [[palette.entities.creatures.ckfif1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      Counters = {
      },  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_9]],  
        Class = [[Position]],  
        z = 0
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_10]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Count = 7,  
        InstanceId = [[Client1_938]],  
        Class = [[TextManagerEntry]],  
        Text = [[Save the mektoubs!!!!!]]
      }
    }
  }
}