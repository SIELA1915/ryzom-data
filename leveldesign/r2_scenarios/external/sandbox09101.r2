scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_547]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 16,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_549]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_550]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_556]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_557]],  
                x = 26951.15625,  
                y = -1994.6875,  
                z = -7.625
              },  
              Angle = -2.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_554]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Spawn Decoration]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_564]],  
              Base = [[palette.entities.botobjects.spot_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_565]],  
                x = 27076.71875,  
                y = -2032.734375,  
                z = 14.96875
              },  
              Angle = 2.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_562]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Kitin Sandbox]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_568]],  
              Base = [[palette.entities.botobjects.totem_kami]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_569]],  
                x = 26935.59375,  
                y = -2012.46875,  
                z = -3.125
              },  
              Angle = 0.703125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_566]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kami totem 1]]
            }
          },  
          InstanceId = [[Client1_551]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_552]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_560]],  
              Base = [[palette.entities.botobjects.roadsign]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_561]],  
                x = 26955.90625,  
                y = -2018.46875,  
                z = -8.65625
              },  
              Angle = 2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_558]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[This Is Spawn]]
            }
          },  
          InstanceId = [[Client1_553]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_548]]
  }
}