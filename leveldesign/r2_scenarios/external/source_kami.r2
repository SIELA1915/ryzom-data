scenario = {
  AccessRules = [[Kalhaans Action]],  
  Description = {
    InstanceId = [[Client1_1]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 6,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_3]],  
  Class = [[Scenario]],  
  Acts = {
    {
      Cost = 25,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client2_90]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_10]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_8]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 1]],  
              Position = {
                y = -1317.3125,  
                x = 28829.42188,  
                InstanceId = [[Client1_11]],  
                Class = [[Position]],  
                z = 60.375
              },  
              Angle = 1.578125,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_14]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_12]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 2]],  
              Position = {
                y = -1332.140625,  
                x = 28848.53125,  
                InstanceId = [[Client1_15]],  
                Class = [[Position]],  
                z = 55.984375
              },  
              Angle = 1.90625,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_18]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_16]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 3]],  
              Position = {
                y = -1352.65625,  
                x = 28834.6875,  
                InstanceId = [[Client1_19]],  
                Class = [[Position]],  
                z = 53.046875
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_22]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_20]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami totem 4]],  
              Position = {
                y = -1337.03125,  
                x = 28815.5,  
                InstanceId = [[Client1_23]],  
                Class = [[Position]],  
                z = 56.234375
              },  
              Angle = 2.0625,  
              Base = [[palette.entities.botobjects.totem_kami]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_90]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_88]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 1]],  
              Position = {
                y = -1328.875,  
                x = 28820.78125,  
                InstanceId = [[Client1_91]],  
                Class = [[Position]],  
                z = 56.5625
              },  
              Angle = -1.125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_94]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_92]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 2]],  
              Position = {
                y = -1322.3125,  
                x = 28825.54688,  
                InstanceId = [[Client1_95]],  
                Class = [[Position]],  
                z = 58.296875
              },  
              Angle = -1.125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_98]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_96]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 3]],  
              Position = {
                y = -1322.9375,  
                x = 28836.32813,  
                InstanceId = [[Client1_99]],  
                Class = [[Position]],  
                z = 59.1875
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_102]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_100]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 4]],  
              Position = {
                y = -1326.71875,  
                x = 28840.9375,  
                InstanceId = [[Client1_103]],  
                Class = [[Position]],  
                z = 57.78125
              },  
              Angle = -2.90625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_106]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_104]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 5]],  
              Position = {
                y = -1340.203125,  
                x = 28844.59375,  
                InstanceId = [[Client1_107]],  
                Class = [[Position]],  
                z = 52.390625
              },  
              Angle = 2.21875,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_110]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_108]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 6]],  
              Position = {
                y = -1346.65625,  
                x = 28839.98438,  
                InstanceId = [[Client1_111]],  
                Class = [[Position]],  
                z = 51.984375
              },  
              Angle = 0.9375,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_114]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_112]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 7]],  
              Position = {
                y = -1348.5,  
                x = 28826.875,  
                InstanceId = [[Client1_115]],  
                Class = [[Position]],  
                z = 52.5625
              },  
              Angle = 0.046875,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_126]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_124]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 10]],  
              Position = {
                y = -1335.078125,  
                x = 28816.625,  
                InstanceId = [[Client1_127]],  
                Class = [[Position]],  
                z = 56.21875
              },  
              Angle = -0.78125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_130]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_128]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 11]],  
              Position = {
                y = -1319.328125,  
                x = 28829.01563,  
                InstanceId = [[Client1_131]],  
                Class = [[Position]],  
                z = 59.5
              },  
              Angle = -1.8125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_134]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_132]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 12]],  
              Position = {
                y = -1319.1875,  
                x = 28832.5,  
                InstanceId = [[Client1_135]],  
                Class = [[Position]],  
                z = 60.03125
              },  
              Angle = -1.8125,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_138]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_136]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 13]],  
              Position = {
                y = -1330.90625,  
                x = 28846.39063,  
                InstanceId = [[Client1_139]],  
                Class = [[Position]],  
                z = 56.328125
              },  
              Angle = -3.015625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_142]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_140]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 14]],  
              Position = {
                y = -1334.46875,  
                x = 28848.23438,  
                InstanceId = [[Client1_143]],  
                Class = [[Position]],  
                z = 54.890625
              },  
              Angle = -0.875,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_146]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_144]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 15]],  
              Position = {
                y = -1351.140625,  
                x = 28837.29688,  
                InstanceId = [[Client1_147]],  
                Class = [[Position]],  
                z = 52.796875
              },  
              Angle = -0.0625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_150]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_148]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 16]],  
              Position = {
                y = -1351.640625,  
                x = 28832.8125,  
                InstanceId = [[Client1_151]],  
                Class = [[Position]],  
                z = 52.75
              },  
              Angle = 1.5,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_170]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_168]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fire 17]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_171]],  
                x = 28821.42188,  
                y = -1343.71875,  
                z = 53.6875
              },  
              Angle = -2.140625,  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_174]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_172]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[fog 1]],  
              Position = {
                y = -1335.421875,  
                x = 28833.39063,  
                InstanceId = [[Client1_175]],  
                Class = [[Position]],  
                z = 51.921875
              },  
              Angle = -2.5625,  
              Base = [[palette.entities.botobjects.fx_ju_solbirtha]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 1]],  
              InstanceId = [[Client1_343]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_345]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1138.125,  
                    x = 28824.53125,  
                    InstanceId = [[Client1_346]],  
                    Class = [[Position]],  
                    z = 60.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_348]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1150.859375,  
                    x = 28837.70313,  
                    InstanceId = [[Client1_349]],  
                    Class = [[Position]],  
                    z = 59.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_351]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1167.171875,  
                    x = 28838.82813,  
                    InstanceId = [[Client1_352]],  
                    Class = [[Position]],  
                    z = 60.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_354]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1179.15625,  
                    x = 28829.07813,  
                    InstanceId = [[Client1_355]],  
                    Class = [[Position]],  
                    z = 60.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_357]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1178.46875,  
                    x = 28808.23438,  
                    InstanceId = [[Client1_358]],  
                    Class = [[Position]],  
                    z = 61.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_360]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1155.453125,  
                    x = 28804.26563,  
                    InstanceId = [[Client1_361]],  
                    Class = [[Position]],  
                    z = 61.9375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_342]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 1]],  
              InstanceId = [[Client1_407]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_409]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1393.671875,  
                    x = 28811.34375,  
                    InstanceId = [[Client1_410]],  
                    Class = [[Position]],  
                    z = 82.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_412]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1361.5625,  
                    x = 28822.42188,  
                    InstanceId = [[Client1_413]],  
                    Class = [[Position]],  
                    z = 58.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_415]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1347.8125,  
                    x = 28825.10938,  
                    InstanceId = [[Client1_416]],  
                    Class = [[Position]],  
                    z = 52.890625
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_406]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 2]],  
              InstanceId = [[Client1_422]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_424]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1277.53125,  
                    x = 28722.25,  
                    InstanceId = [[Client1_425]],  
                    Class = [[Position]],  
                    z = 77.203125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_427]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1299.859375,  
                    x = 28773.6875,  
                    InstanceId = [[Client1_428]],  
                    Class = [[Position]],  
                    z = 61.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_430]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1324.28125,  
                    x = 28818.23438,  
                    InstanceId = [[Client1_431]],  
                    Class = [[Position]],  
                    z = 58.6875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_421]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_437]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_439]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1257.65625,  
                    x = 28876.75,  
                    InstanceId = [[Client1_440]],  
                    Class = [[Position]],  
                    z = 80.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_442]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1271.765625,  
                    x = 28865.125,  
                    InstanceId = [[Client1_443]],  
                    Class = [[Position]],  
                    z = 78.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_445]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1314.9375,  
                    x = 28840.84375,  
                    InstanceId = [[Client1_446]],  
                    Class = [[Position]],  
                    z = 64.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_448]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1324.125,  
                    x = 28838.84375,  
                    InstanceId = [[Client1_449]],  
                    Class = [[Position]],  
                    z = 59.125
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_436]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 4]],  
              InstanceId = [[Client1_461]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_463]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1407.84375,  
                    x = 28932.125,  
                    InstanceId = [[Client1_464]],  
                    Class = [[Position]],  
                    z = 88.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_466]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1407.84375,  
                    x = 28932.125,  
                    InstanceId = [[Client1_467]],  
                    Class = [[Position]],  
                    z = 88.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_469]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1365.5625,  
                    x = 28886.35938,  
                    InstanceId = [[Client1_470]],  
                    Class = [[Position]],  
                    z = 56.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_472]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1344.34375,  
                    x = 28843.4375,  
                    InstanceId = [[Client1_473]],  
                    Class = [[Position]],  
                    z = 52.34375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_460]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_478]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_476]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              Position = {
                y = -1269,  
                x = 28771.07813,  
                InstanceId = [[Client1_479]],  
                Class = [[Position]],  
                z = 63.8125
              },  
              Angle = -0.390625,  
              Base = [[palette.entities.botobjects.watch_tower]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 5]],  
              InstanceId = [[Client1_485]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_487]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1278.296875,  
                    x = 28785.04688,  
                    InstanceId = [[Client1_488]],  
                    Class = [[Position]],  
                    z = 60.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_490]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1278.296875,  
                    x = 28785.04688,  
                    InstanceId = [[Client1_491]],  
                    Class = [[Position]],  
                    z = 60.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_493]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1318.40625,  
                    x = 28824.92188,  
                    InstanceId = [[Client1_494]],  
                    Class = [[Position]],  
                    z = 59.9375
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_484]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Route 7]],  
              InstanceId = [[Client2_118]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_123]],  
                  Class = [[WayPoint]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_124]],  
                    x = 28821.375,  
                    y = -1343.65625,  
                    z = 53.671875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_117]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InstanceId = [[Client1_608]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_606]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[hut 1]],  
              Position = {
                y = -1152.984375,  
                x = 28810.5625,  
                InstanceId = [[Client1_609]],  
                Class = [[Position]],  
                z = 61.3125
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.botobjects.hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_616]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_614]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[3 jars 1]],  
              Position = {
                y = -1153.234375,  
                x = 28816.21875,  
                InstanceId = [[Client1_617]],  
                Class = [[Position]],  
                z = 61.03125
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.botobjects.jar_3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_620]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_618]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[camp fire 1]],  
              Position = {
                y = -1165.015625,  
                x = 28819.21875,  
                InstanceId = [[Client1_621]],  
                Class = [[Position]],  
                z = 60.578125
              },  
              Angle = -1.609375,  
              Base = [[palette.entities.botobjects.campfire]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[KitinAttackPlace]],  
              InstanceId = [[Client2_191]],  
              Class = [[Road]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client2_199]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1341.96875,  
                    x = 28841.67188,  
                    InstanceId = [[Client2_200]],  
                    Class = [[Position]],  
                    z = 51.671875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client2_190]],  
                Class = [[Position]],  
                z = 0
              }
            },  
            {
              InheritPos = 1,  
              Name = [[KitinWinArea]],  
              InstanceId = [[Client1_623]],  
              Class = [[Region]],  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_625]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1335.578125,  
                    x = 28844.03125,  
                    InstanceId = [[Client1_626]],  
                    Class = [[Position]],  
                    z = 53.546875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_628]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1326.265625,  
                    x = 28834.78125,  
                    InstanceId = [[Client1_629]],  
                    Class = [[Position]],  
                    z = 56.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_631]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1336.796875,  
                    x = 28819.96875,  
                    InstanceId = [[Client1_632]],  
                    Class = [[Position]],  
                    z = 54.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_634]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1346.46875,  
                    x = 28835.10938,  
                    InstanceId = [[Client1_635]],  
                    Class = [[Position]],  
                    z = 51.421875
                  }
                }
              },  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_622]],  
                Class = [[Position]],  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      InstanceId = [[Client1_4]],  
      Events = {
      }
    },  
    {
      Cost = 3,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_362]],  
        [[Client1_364]],  
        [[Client1_366]]
      },  
      Title = [[Act 1]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              MorphTarget8 = 1,  
              InstanceId = [[Client1_332]],  
              ActivitiesId = {
                [[Client1_364]]
              },  
              HairType = 3118,  
              TrouserColor = 4,  
              MorphTarget5 = 5,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 0,  
              GabaritBreastSize = 10,  
              GabaritHeight = 9,  
              HairColor = 3,  
              EyesColor = 5,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 7,  
              HandsColor = 0,  
              MorphTarget1 = 6,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_330]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_364]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_365]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_378]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_379]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_380]],  
                            Who = r2.RefId([[Client1_332]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_381]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Mercenary says Ther...]]
                      },  
                      {
                        Time = 6,  
                        InstanceId = [[Client1_382]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_383]],  
                            Who = r2.RefId([[Client1_332]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_384]]
                          }
                        },  
                        Name = [[Chat 2 : (after  6sec) Mercenary says I ca...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 3,  
              FeetModel = 5605422,  
              Angle = -1.921875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 1,  
              MorphTarget3 = 4,  
              MorphTarget7 = 2,  
              Sex = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 7,  
              MorphTarget2 = 6,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Mercenary]],  
              Position = {
                y = -1165.109375,  
                x = 28818.85938,  
                InstanceId = [[Client1_333]],  
                Class = [[Position]],  
                z = 60.609375
              },  
              JacketModel = 5606446,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_male.creature]]
            },  
            {
              MorphTarget8 = 3,  
              InstanceId = [[Client1_336]],  
              ActivitiesId = {
                [[Client1_366]]
              },  
              HairType = 2350,  
              TrouserColor = 0,  
              MorphTarget5 = 5,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 14,  
              HandsModel = 0,  
              FeetColor = 3,  
              GabaritBreastSize = 5,  
              GabaritHeight = 8,  
              HairColor = 4,  
              EyesColor = 6,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 2,  
              HandsColor = 0,  
              MorphTarget1 = 5,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_334]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_366]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_367]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_385]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_386]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_387]],  
                            Who = r2.RefId([[Client1_336]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_388]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Mercenary says What...]]
                      },  
                      {
                        Time = 6,  
                        InstanceId = [[Client1_389]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_390]],  
                            Who = r2.RefId([[Client1_336]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_391]]
                          }
                        },  
                        Name = [[Chat 2 : (after  6sec) Mercenary says And ...]]
                      },  
                      {
                        Time = 9,  
                        InstanceId = [[Client1_392]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_393]],  
                            Who = r2.RefId([[Client1_336]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_394]]
                          }
                        },  
                        Name = [[Chat 3 : (after  9sec) Mercenary says The ...]]
                      },  
                      {
                        Time = 12,  
                        InstanceId = [[Client1_396]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_397]],  
                            Who = r2.RefId([[Client1_336]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_398]]
                          }
                        },  
                        Name = [[Chat 4 : (after  12sec) Mercenary says Very...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Angle = -1.921875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 10,  
              MorphTarget3 = 4,  
              MorphTarget7 = 1,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 2,  
              MorphTarget2 = 1,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Mercenary]],  
              Position = {
                y = -1158.40625,  
                x = 28825.85938,  
                InstanceId = [[Client1_337]],  
                Class = [[Position]],  
                z = 60.359375
              },  
              JacketModel = 5606446,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              MorphTarget8 = 4,  
              InstanceId = [[Client1_340]],  
              ActivitiesId = {
                [[Client1_362]]
              },  
              HairType = 5621806,  
              TrouserColor = 5,  
              MorphTarget5 = 4,  
              MorphTarget6 = 0,  
              GabaritArmsWidth = 13,  
              HandsModel = 5605678,  
              FeetColor = 1,  
              GabaritBreastSize = 8,  
              GabaritHeight = 2,  
              HairColor = 0,  
              EyesColor = 4,  
              TrouserModel = 5605934,  
              GabaritLegsWidth = 6,  
              HandsColor = 0,  
              MorphTarget1 = 2,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_338]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_362]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_363]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_368]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_369]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_370]],  
                            Who = r2.RefId([[Client1_340]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_374]]
                          }
                        },  
                        Name = [[Chat 1 : (after  3sec) Mercenary says Ahh....]]
                      },  
                      {
                        Time = 5,  
                        InstanceId = [[Client1_375]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_376]],  
                            Who = r2.RefId([[Client1_340]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_377]]
                          }
                        },  
                        Name = [[Chat 2 : (after  5sec) Mercenary says I ho...]]
                      }
                    }
                  }
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 6,  
              FeetModel = 5605422,  
              Angle = -1.921875,  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Tattoo = 18,  
              MorphTarget3 = 4,  
              MorphTarget7 = 6,  
              Sex = 1,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Mercenary]],  
              Position = {
                y = -1168.265625,  
                x = 28824.48438,  
                InstanceId = [[Client1_341]],  
                Class = [[Position]],  
                z = 60.328125
              },  
              JacketModel = 5606446,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          InstanceId = [[Client1_329]]
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_328]],  
      States = {
      }
    },  
    {
      Cost = 15,  
      Class = [[Act]],  
      ActivitiesIds = {
        [[Client1_450]],  
        [[Client1_454]],  
        [[Client1_474]],  
        [[Client1_495]],  
        [[Client2_54]],  
        [[Client2_56]],  
        [[Client2_62]],  
        [[Client1_545]],  
        [[Client2_68]],  
        [[Client2_71]],  
        [[Client1_571]],  
        [[Client1_573]],  
        [[Client1_575]],  
        [[Client1_600]],  
        [[Client1_602]],  
        [[Client1_604]],  
        [[Client2_170]],  
        [[Client2_177]],  
        [[Client1_642]]
      },  
      Title = [[Crossing the Fire line]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_404]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_402]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_454]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_455]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client2_62]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_63]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_407]]),  
                        Name = [[Activity 1 : Follow Route Route 1 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_64]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_24]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 1 : Follow Route Route 5 without time limit' event of Mu-Cho, Kami Priest]],  
                    InstanceId = [[Client2_67]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_66]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 1]],  
              Position = {
                y = -1401.921875,  
                x = 28799.75,  
                InstanceId = [[Client1_405]],  
                Class = [[Position]],  
                z = 82.671875
              },  
              Angle = 1.015625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
                [[Client1_454]],  
                [[Client2_62]]
              }
            },  
            {
              InstanceId = [[Client1_419]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_417]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_54]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_55]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client2_56]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_57]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_422]]),  
                        Name = [[Activity 1 : Follow Route Route 2 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_58]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_24]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 1 : Follow Route Route 5 without time limit' event of Mu-Cho, Kami Priest]],  
                    InstanceId = [[Client2_61]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_60]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 2]],  
              Position = {
                y = -1277.640625,  
                x = 28709.0625,  
                InstanceId = [[Client1_420]],  
                Class = [[Position]],  
                z = 79.625
              },  
              Angle = -0.390625,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
                [[Client2_54]],  
                [[Client2_56]]
              }
            },  
            {
              InstanceId = [[Client1_434]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_432]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_450]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_451]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client2_68]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_69]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_437]]),  
                        Name = [[Activity 1 : Follow Route Route 3 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_70]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_24]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 1 : Follow Route Route 5 without time limit' event of Mu-Cho, Kami Priest]],  
                    InstanceId = [[Client2_76]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_75]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 3]],  
              Position = {
                y = -1256.65625,  
                x = 28886.26563,  
                InstanceId = [[Client1_435]],  
                Class = [[Position]],  
                z = 79.90625
              },  
              Angle = 3.46875,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
                [[Client1_450]],  
                [[Client2_68]]
              }
            },  
            {
              InstanceId = [[Client1_458]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_456]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_474]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_475]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Inactive without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Inactive]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client2_71]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_72]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_461]]),  
                        Name = [[Activity 1 : Follow Route Route 4 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_73]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 4 without time limit' event]],  
                    InstanceId = [[Client2_174]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client2_173]],  
                      Value = r2.RefId([[Client2_72]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_175]],  
                          Value = r2.RefId([[Client2_177]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_176]],  
                        Entity = r2.RefId([[Client2_157]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  },  
                  {
                    Name = [[Event 2 : on 'death' event]],  
                    InstanceId = [[Client1_639]],  
                    Conditions = {
                      {
                        InstanceId = [[Client1_645]],  
                        Entity = r2.RefId([[Client1_404]]),  
                        Class = [[ConditionStep]],  
                        Condition = {
                          Type = [[]],  
                          InstanceId = [[Client1_644]],  
                          Value = r2.RefId([[]]),  
                          Class = [[ConditionType]]
                        }
                      }
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client1_638]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_640]],  
                          Value = r2.RefId([[Client1_642]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_641]],  
                        Entity = r2.RefId([[Client2_157]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_24]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 1 : Follow Route Route 5 without time limit' event of Mu-Cho, Kami Priest]],  
                    InstanceId = [[Client2_79]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_78]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Mossy Colossus 4]],  
              Position = {
                y = -1421.71875,  
                x = 28934.65625,  
                InstanceId = [[Client1_459]],  
                Class = [[Position]],  
                z = 85.21875
              },  
              Angle = 2.09375,  
              Base = [[palette.entities.npcs.kami.kami_guardian_b]],  
              ActivitiesId = {
                [[Client1_474]],  
                [[Client2_71]]
              }
            },  
            {
              MorphTarget8 = 0,  
              InstanceId = [[Client1_482]],  
              ActivitiesId = {
                [[Client1_495]]
              },  
              HairType = 9006,  
              TrouserColor = 2,  
              MorphTarget5 = 2,  
              MorphTarget6 = 5,  
              GabaritArmsWidth = 7,  
              HandsModel = 5617966,  
              FeetColor = 3,  
              GabaritBreastSize = 1,  
              GabaritHeight = 0,  
              HairColor = 3,  
              EyesColor = 1,  
              TrouserModel = 5618222,  
              GabaritLegsWidth = 3,  
              HandsColor = 1,  
              MorphTarget1 = 4,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_480]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_495]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_496]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_497]]),  
                        ActivityZoneId = r2.RefId([[Client1_485]]),  
                        Name = [[Activity 1 : Follow Route Route 5 without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_52]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[Chat1]],  
                    InstanceId = [[Client1_497]],  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 0,  
                        InstanceId = [[Client1_498]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_499]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_501]]
                          }
                        },  
                        Name = [[Chat 1 : (after  0sec) Mu-Cho, Kami Priest says *run...]]
                      },  
                      {
                        Time = 10,  
                        InstanceId = [[Client1_502]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_503]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_504]]
                          }
                        },  
                        Name = [[Chat 2 : (after  10sec) Mu-Cho, Kami Priest says Hey!...]]
                      },  
                      {
                        Time = 13,  
                        InstanceId = [[Client1_505]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_506]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_507]]
                          }
                        },  
                        Name = [[Chat 3 : (after  13sec) Mu-Cho, Kami Priest says Are ...]]
                      },  
                      {
                        Time = 17,  
                        InstanceId = [[Client1_508]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_509]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_510]]
                          }
                        },  
                        Name = [[Chat 4 : (after  17sec) Mu-Cho, Kami Priest says You'...]]
                      },  
                      {
                        Time = 20,  
                        InstanceId = [[Client1_512]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_513]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_514]]
                          }
                        },  
                        Name = [[Chat 5 : (after  20sec) Mu-Cho, Kami Priest says This...]]
                      },  
                      {
                        Time = 25,  
                        InstanceId = [[Client1_515]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_516]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_517]]
                          }
                        },  
                        Name = [[Chat 6 : (after  25sec) Mu-Cho, Kami Priest says *bre...]]
                      },  
                      {
                        Time = 28,  
                        InstanceId = [[Client1_518]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_519]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_520]]
                          }
                        },  
                        Name = [[Chat 7 : (after  28sec) Mu-Cho, Kami Priest says This...]]
                      },  
                      {
                        Time = 30,  
                        InstanceId = [[Client1_521]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_522]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_523]]
                          }
                        },  
                        Name = [[Chat 8 : (after  30sec) Mu-Cho, Kami Priest says But ...]]
                      },  
                      {
                        Time = 26,  
                        InstanceId = [[Client1_524]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_525]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_526]]
                          }
                        },  
                        Name = [[Chat 9 : (after  26sec) Mu-Cho, Kami Priest says Go b...]]
                      },  
                      {
                        Time = 40,  
                        InstanceId = [[Client1_527]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_528]],  
                            Who = r2.RefId([[Client1_482]]),  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_529]]
                          }
                        },  
                        Name = [[Chat 10 : (after  40sec) Mu-Cho, Kami Priest says And ...]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[Event 1 : on 'end of activity step Activity 1 : Follow Route Route 5 without time limit' event]],  
                    InstanceId = [[Client2_24]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client2_23]],  
                      Value = r2.RefId([[Client1_496]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_59]],  
                          Value = r2.RefId([[Client2_56]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_60]],  
                        Entity = r2.RefId([[Client1_419]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_65]],  
                          Value = r2.RefId([[Client2_62]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_66]],  
                        Entity = r2.RefId([[Client1_404]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_74]],  
                          Value = r2.RefId([[Client2_68]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_75]],  
                        Entity = r2.RefId([[Client1_434]]),  
                        Class = [[ActionStep]]
                      },  
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_77]],  
                          Value = r2.RefId([[Client2_71]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_78]],  
                        Entity = r2.RefId([[Client1_458]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Reactions = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 0,  
              MorphTarget4 = 6,  
              FeetModel = 5617710,  
              Angle = -0.390625,  
              Base = [[palette.entities.npcs.civils.z_civil_20]],  
              Tattoo = 20,  
              MorphTarget3 = 2,  
              MorphTarget7 = 2,  
              Sex = 0,  
              WeaponRightHand = 0,  
              GabaritTorsoWidth = 4,  
              MorphTarget2 = 7,  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]],  
              InheritPos = 1,  
              ArmColor = 0,  
              Name = [[Mu-Cho, Kami Priest]],  
              Position = {
                y = -1273.625,  
                x = 28780.01563,  
                InstanceId = [[Client1_483]],  
                Class = [[Position]],  
                z = 61.609375
              },  
              JacketModel = 0,  
              WeaponLeftHand = 0,  
              ArmModel = 0,  
              SheetClient = [[basic_zorai_male.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_654]],  
              Base = [[palette.entities.npcs.guards.t_guard_45]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_655]],  
                x = 29025.35938,  
                y = -1310.71875,  
                z = 77.03125
              },  
              Angle = -0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_652]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 0,  
              GabaritTorsoWidth = 3,  
              GabaritArmsWidth = 9,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 12,  
              HairType = 5612590,  
              HairColor = 5,  
              Tattoo = 23,  
              EyesColor = 2,  
              MorphTarget1 = 7,  
              MorphTarget2 = 4,  
              MorphTarget3 = 1,  
              MorphTarget4 = 5,  
              MorphTarget5 = 5,  
              MorphTarget6 = 2,  
              MorphTarget7 = 2,  
              MorphTarget8 = 7,  
              Sex = 0,  
              JacketModel = 5613358,  
              TrouserModel = 5612846,  
              FeetModel = 5612078,  
              HandsModel = 5612334,  
              ArmModel = 5613102,  
              JacketColor = 5,  
              ArmColor = 5,  
              HandsColor = 5,  
              TrouserColor = 5,  
              FeetColor = 5,  
              WeaponRightHand = 5601070,  
              WeaponLeftHand = 0,  
              Name = [[lakeland guard 1]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_tryker_male.creature]],  
              Sheet = [[ring_guard_melee_tank_slash_b3.creature]]
            }
          },  
          InstanceId = [[Client1_401]]
        },  
        {
          InstanceId = [[Client1_599]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_598]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client1_549]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_547]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_575]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_576]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_343]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_602]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_603]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_343]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Karavan Scout]],  
              Position = {
                y = -1170.265625,  
                x = 28808.79688,  
                InstanceId = [[Client1_550]],  
                Class = [[Position]],  
                z = 61.4375
              },  
              Angle = -1.984375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_b]],  
              ActivitiesId = {
                [[Client1_575]],  
                [[Client1_602]]
              }
            },  
            {
              InstanceId = [[Client1_595]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_593]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Karavan Guard]],  
              Position = {
                y = -1170.5,  
                x = 28811.09375,  
                InstanceId = [[Client1_596]],  
                Class = [[Position]],  
                z = 61.109375
              },  
              Angle = 0.15625,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_h_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_591]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_589]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_600]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_601]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_343]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[prote]],  
              Position = {
                y = -1168.296875,  
                x = 28810.14063,  
                InstanceId = [[Client1_592]],  
                Class = [[Position]],  
                z = 61.28125
              },  
              Angle = 0.15625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_b]],  
              ActivitiesId = {
                [[Client1_600]]
              }
            },  
            {
              InstanceId = [[Client1_587]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_585]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Karavan Guard]],  
              Position = {
                y = -1163.984375,  
                x = 28814.75,  
                InstanceId = [[Client1_588]],  
                Class = [[Position]],  
                z = 60.890625
              },  
              Angle = 0.15625,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_579]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_577]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Karavan Guard]],  
              Position = {
                y = -1169.15625,  
                x = 28818.15625,  
                InstanceId = [[Client1_580]],  
                Class = [[Position]],  
                z = 60.53125
              },  
              Angle = 0.796875,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_583]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_581]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_604]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_605]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_343]]),  
                        Name = [[Activity 1 : Wander Place 1 for 20sec]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Karavan Guard]],  
              Position = {
                y = -1165.484375,  
                x = 28823.35938,  
                InstanceId = [[Client1_584]],  
                Class = [[Position]],  
                z = 60.375
              },  
              Angle = 0.796875,  
              Base = [[palette.entities.npcs.karavan.karavan_guard_k_f_b]],  
              ActivitiesId = {
                [[Client1_604]]
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_597]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          InstanceId = [[Client2_169]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client2_168]],  
            Class = [[Position]],  
            z = 0
          },  
          Cost = 0,  
          Components = {
            {
              InstanceId = [[Client2_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_155]],  
                Class = [[Behavior]],  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client2_170]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_171]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 1 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq2]],  
                    InstanceId = [[Client2_177]],  
                    Repeating = 0,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client2_178]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_191]]),  
                        Name = [[Activity 1 : Follow Route KitinAttackPlace without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Follow Route]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_636]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[Activity 2 : Stand Still without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[Seq3]],  
                    InstanceId = [[Client1_642]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_643]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_623]]),  
                        Name = [[Activity 1 : Wander KitinWinArea without time limit]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                  {
                    LogicEntityAction = [[Client2_174]],  
                    Name = [[Trigger 1 : Begin activity Seq2 on 'End of activity step Activity 1 : Follow Route Route 4 without time limit' event of Mossy Colossus 4]],  
                    InstanceId = [[Client2_172]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_176]]
                  },  
                  {
                    LogicEntityAction = [[Client1_639]],  
                    Name = [[Trigger 2 : Begin activity Seq3 on 'Death' event of Mossy Colossus 4]],  
                    InstanceId = [[Client1_637]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_641]]
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kizarak]],  
              Position = {
                y = -1479.625,  
                x = 29007.57813,  
                InstanceId = [[Client2_158]],  
                Class = [[Position]],  
                z = 61.953125
              },  
              Angle = 2.765625,  
              Base = [[palette.entities.creatures.ckcif2]],  
              ActivitiesId = {
                [[Client2_170]],  
                [[Client2_177]],  
                [[Client1_642]]
              }
            },  
            {
              InstanceId = [[Client2_161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_159]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_162]],  
                x = 29009.28125,  
                y = -1487.921875,  
                z = 61.5
              },  
              Angle = -2.953125,  
              Base = [[palette.entities.creatures.ckaif2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_163]],  
                Class = [[Behavior]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Power Overlord Kidinak]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_166]],  
                x = 29018.375,  
                y = -1482.5625,  
                z = 60.546875
              },  
              Angle = 1.696979523,  
              Base = [[palette.entities.creatures.ckaif2]],  
              ActivitiesId = {
              }
            }
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client2_167]],  
            Class = [[Behavior]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Events = {
      },  
      InstanceId = [[Client1_400]],  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
      {
        Count = 33,  
        InstanceId = [[Client1_371]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_372]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_373]],  
        Class = [[TextManagerEntry]],  
        Text = [[]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client1_374]],  
        Class = [[TextManagerEntry]],  
        Text = [[Ahh... Finally, we arrived here!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_377]],  
        Class = [[TextManagerEntry]],  
        Text = [[I hope this is really the place where we'll find the source; it has been a long journey to come here.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_381]],  
        Class = [[TextManagerEntry]],  
        Text = [[There is something wrong with this place...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_384]],  
        Class = [[TextManagerEntry]],  
        Text = [[I can feel it.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_388]],  
        Class = [[TextManagerEntry]],  
        Text = [[What are these noises ?]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_391]],  
        Class = [[TextManagerEntry]],  
        Text = [[And there is a Kami flag there!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_394]],  
        Class = [[TextManagerEntry]],  
        Text = [[The misterious messenger who told us about this source didn't mentionned it was a Kami place...]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_395]],  
        Class = [[TextManagerEntry]],  
        Text = [[The misterious messenger who told us about this source didn't mentionned it was a Kami place...]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_398]],  
        Class = [[TextManagerEntry]],  
        Text = [[Very strange!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_399]],  
        Class = [[TextManagerEntry]],  
        Text = [[Very strange!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_500]],  
        Class = [[TextManagerEntry]],  
        Text = [[*runs of the tower and looks toward the ]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_501]],  
        Class = [[TextManagerEntry]],  
        Text = [[*runs of the tower and looks toward the source*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_504]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hey! You!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_507]],  
        Class = [[TextManagerEntry]],  
        Text = [[Are you crazy?]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client1_510]],  
        Class = [[TextManagerEntry]],  
        Text = [[You've crossed the line and entered the protected area!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_511]],  
        Class = [[TextManagerEntry]],  
        Text = [[You've crossed the line and entered the protected area!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_514]],  
        Class = [[TextManagerEntry]],  
        Text = [[This has raised the alarm! *looks at the approaching Kamis*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_517]],  
        Class = [[TextManagerEntry]],  
        Text = [[*breathe deeply*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_520]],  
        Class = [[TextManagerEntry]],  
        Text = [[This is an old alarm, meant to protect the source in case somebody tried to harm it. The Kamis won't do any harm to you, since it seems you have no will to do something bad here.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_523]],  
        Class = [[TextManagerEntry]],  
        Text = [[But this alarm could have been intercepted by the Karavan; this would be a disaster! If they come here, that's the end!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_526]],  
        Class = [[TextManagerEntry]],  
        Text = [[Go back on the continent, as soon as possible! Gather some friends, these few Kamis couldn't hold a massive Karavan attack.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_529]],  
        Class = [[TextManagerEntry]],  
        Text = [[And don't try to fight the Karavan if you find them on your way back to the continent. You need to spread the word first!]]
      },  
      {
        Count = 3,  
        InstanceId = [[Client2_132]],  
        Class = [[TextManagerEntry]],  
        Text = [[*dies*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_133]],  
        Class = [[TextManagerEntry]],  
        Text = [[*dies*]]
      },  
      {
        Count = 5,  
        InstanceId = [[Client2_137]],  
        Class = [[TextManagerEntry]],  
        Text = [[waa I burned my tail *bark*]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_149]],  
        Class = [[TextManagerEntry]],  
        Text = [[waa I burned my tail *bark*
]]
      }
    },  
    InstanceId = [[Client1_2]]
  }
}