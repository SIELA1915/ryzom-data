scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_22]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_24]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    Position = 0,  
    Region = 0,  
    Road = 0,  
    WayPoint = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 0,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_25]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_31]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_32]],  
                x = 30626.82813,  
                y = -2299.8125,  
                z = 78.109375
              },  
              Angle = -1.453125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_29]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_35]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_36]],  
                x = 30610.96875,  
                y = -2301.375,  
                z = 77.484375
              },  
              Angle = 0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_33]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[runic circle 1]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_246]],  
              Name = [[Y0]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_248]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_249]],  
                    x = 30830.48438,  
                    y = -2350.15625,  
                    z = 66.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_251]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_252]],  
                    x = 30830.375,  
                    y = -1800.515625,  
                    z = 79.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_254]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_255]],  
                    x = 30831.84375,  
                    y = -1800.6875,  
                    z = 79.390625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_245]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_258]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_259]],  
                x = 30831.40625,  
                y = -2240.1875,  
                z = 65.046875
              },  
              Angle = 3.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_256]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 2]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_262]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_263]],  
                x = 30831.03125,  
                y = -2130.625,  
                z = 70.359375
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_260]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 3]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_266]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_267]],  
                x = 30830.98438,  
                y = -2020.71875,  
                z = 74.921875
              },  
              Angle = 3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_264]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 4]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_270]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_271]],  
                x = 30831.09375,  
                y = -1910.953125,  
                z = 79.09375
              },  
              Angle = 3.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_268]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 5]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_273]],  
              Name = [[X5]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_275]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_276]],  
                    x = 30830.53125,  
                    y = -1800.546875,  
                    z = 79.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_278]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_279]],  
                    x = 30375.34375,  
                    y = -1800.5,  
                    z = 45.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_281]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_282]],  
                    x = 30373.40625,  
                    y = -1795.296875,  
                    z = 45.703125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_272]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_285]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_286]],  
                x = 30466.29688,  
                y = -1808.203125,  
                z = 51.453125
              },  
              Angle = -1.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_283]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 6]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_289]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_290]],  
                x = 30557.5625,  
                y = -1800,  
                z = 58.421875
              },  
              Angle = -1.59375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_287]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 7]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_293]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_294]],  
                x = 30648.54688,  
                y = -1800.109375,  
                z = 67.453125
              },  
              Angle = -1.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_291]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 8]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_297]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_298]],  
                x = 30739.10938,  
                y = -1800.28125,  
                z = 72.125
              },  
              Angle = -1.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_295]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros focus sign 9]]
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_300]],  
              Name = [[Y1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_302]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_303]],  
                    x = 30739.15625,  
                    y = -1800.515625,  
                    z = 72.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_305]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_306]],  
                    x = 30739.54688,  
                    y = -2360.140625,  
                    z = 75.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_308]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_309]],  
                    x = 30740.98438,  
                    y = -2359.015625,  
                    z = 74.71875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_299]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_311]],  
              Name = [[X0]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_313]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_314]],  
                    x = 30830.45313,  
                    y = -2350.1875,  
                    z = 66.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_316]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_317]],  
                    x = 30374.9375,  
                    y = -2350.71875,  
                    z = 54.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_319]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_320]],  
                    x = 30376.25,  
                    y = -2350.046875,  
                    z = 54.28125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_310]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_322]],  
              Name = [[Y5]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_324]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_325]],  
                    x = 30374.90625,  
                    y = -2350.84375,  
                    z = 54.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_327]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_328]],  
                    x = 30375.375,  
                    y = -1800.5625,  
                    z = 45.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_330]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_331]],  
                    x = 30374.45313,  
                    y = -1800.4375,  
                    z = 45.5625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_321]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_333]],  
              Name = [[Y4]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_335]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_336]],  
                    x = 30466.57813,  
                    y = -1805.234375,  
                    z = 53.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_338]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_339]],  
                    x = 30466.6875,  
                    y = -2350.640625,  
                    z = 65.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_341]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_342]],  
                    x = 30465.92188,  
                    y = -2348.90625,  
                    z = 65.953125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_332]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_344]],  
              Name = [[Y3]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_346]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_347]],  
                    x = 30557.5625,  
                    y = -1800.5,  
                    z = 58.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_349]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_350]],  
                    x = 30557.39063,  
                    y = -2350.453125,  
                    z = 79.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_352]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_353]],  
                    x = 30559.8125,  
                    y = -2351.546875,  
                    z = 79.03125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_343]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_355]],  
              Name = [[Y2]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_357]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_358]],  
                    x = 30648.59375,  
                    y = -1800.53125,  
                    z = 67.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_360]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_361]],  
                    x = 30648.65625,  
                    y = -2350.375,  
                    z = 78.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_363]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_364]],  
                    x = 30649.98438,  
                    y = -2352.03125,  
                    z = 79.3125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_354]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_366]],  
              Name = [[X4]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_368]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_369]],  
                    x = 30830.32813,  
                    y = -1910.9375,  
                    z = 79.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_371]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_372]],  
                    x = 30375.29688,  
                    y = -1910.765625,  
                    z = 49.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_374]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_375]],  
                    x = 30372.59375,  
                    y = -1909.78125,  
                    z = 49.3125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_365]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_377]],  
              Name = [[X3]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_379]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_380]],  
                    x = 30830.35938,  
                    y = -2020.75,  
                    z = 74.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_382]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_383]],  
                    x = 30375.17188,  
                    y = -2020.109375,  
                    z = 47.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_385]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_386]],  
                    x = 30372.10938,  
                    y = -2021.671875,  
                    z = 46.796875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_376]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_388]],  
              Name = [[X2]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_390]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_391]],  
                    x = 30830.29688,  
                    y = -2130.75,  
                    z = 70.359375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_393]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_394]],  
                    x = 30375.125,  
                    y = -2130.140625,  
                    z = 53.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_396]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_397]],  
                    x = 30371.5,  
                    y = -2132.734375,  
                    z = 53.265625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_387]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_399]],  
              Name = [[X1]],  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_401]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_402]],  
                    x = 30830.5,  
                    y = -2240.359375,  
                    z = 65.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_404]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_405]],  
                    x = 30375.0625,  
                    y = -2239.90625,  
                    z = 55.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_407]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_408]],  
                    x = 30372.09375,  
                    y = -2241.359375,  
                    z = 55.578125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_398]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_410]],  
              Name = [[AA]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_412]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_413]],  
                    x = 30829.85938,  
                    y = -2349.859375,  
                    z = 66.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_415]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_416]],  
                    x = 30740.42188,  
                    y = -2349.84375,  
                    z = 74.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_418]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_419]],  
                    x = 30739.71875,  
                    y = -2240.765625,  
                    z = 67.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_421]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_422]],  
                    x = 30829.25,  
                    y = -2240.8125,  
                    z = 65.375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_409]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_424]],  
              Name = [[BA]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_426]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_427]],  
                    x = 30829.25,  
                    y = -2239.75,  
                    z = 65.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_429]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_430]],  
                    x = 30830,  
                    y = -2131.578125,  
                    z = 70.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_432]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_433]],  
                    x = 30740.8125,  
                    y = -2131.15625,  
                    z = 73.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_435]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_436]],  
                    x = 30739.92188,  
                    y = -2238.15625,  
                    z = 68.71875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_423]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_438]],  
              Name = [[CA]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_440]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_441]],  
                    x = 30740.07813,  
                    y = -2129.5,  
                    z = 74.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_443]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_444]],  
                    x = 30829.125,  
                    y = -2130,  
                    z = 70.5625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_446]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_447]],  
                    x = 30829.79688,  
                    y = -2022.234375,  
                    z = 74.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_449]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_450]],  
                    x = 30741.46875,  
                    y = -2021.03125,  
                    z = 77.421875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_437]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_452]],  
              Name = [[DA]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_454]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_455]],  
                    x = 30741.03125,  
                    y = -2019.6875,  
                    z = 77.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_457]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_458]],  
                    x = 30740.14063,  
                    y = -1912.640625,  
                    z = 76.1875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_460]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_461]],  
                    x = 30828.6875,  
                    y = -1911.640625,  
                    z = 79.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_463]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_464]],  
                    x = 30829.45313,  
                    y = -2018.671875,  
                    z = 75.3125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_451]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_466]],  
              Name = [[EA]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_468]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_469]],  
                    x = 30829.07813,  
                    y = -1910.34375,  
                    z = 79.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_471]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_472]],  
                    x = 30740.78125,  
                    y = -1910.375,  
                    z = 76.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_474]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_475]],  
                    x = 30739.84375,  
                    y = -1801.515625,  
                    z = 71.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_477]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_478]],  
                    x = 30828.84375,  
                    y = -1801.21875,  
                    z = 78.984375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_465]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_503]],  
              Name = [[EB]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_505]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_506]],  
                    x = 30738.17188,  
                    y = -1909.796875,  
                    z = 76.109375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_508]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_509]],  
                    x = 30650.40625,  
                    y = -1910.109375,  
                    z = 62.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_511]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_512]],  
                    x = 30649.20313,  
                    y = -1801.3125,  
                    z = 67.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_514]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_515]],  
                    x = 30737.5625,  
                    y = -1801.46875,  
                    z = 72.09375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_502]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_517]],  
              Name = [[EC]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_519]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_520]],  
                    x = 30647.57813,  
                    y = -1909.96875,  
                    z = 61.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_522]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_523]],  
                    x = 30559.28125,  
                    y = -1910.125,  
                    z = 58.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_525]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_526]],  
                    x = 30558.15625,  
                    y = -1801.90625,  
                    z = 58.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_528]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_529]],  
                    x = 30647.32813,  
                    y = -1801.390625,  
                    z = 67.25
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_516]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_531]],  
              Name = [[ED]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_533]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_534]],  
                    x = 30556.90625,  
                    y = -1909.8125,  
                    z = 58.8125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_536]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_537]],  
                    x = 30468.5625,  
                    y = -1910.15625,  
                    z = 51.921875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_539]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_540]],  
                    x = 30467.29688,  
                    y = -1805.328125,  
                    z = 53.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_542]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_543]],  
                    x = 30556.59375,  
                    y = -1801.328125,  
                    z = 58.296875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_530]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_545]],  
              Name = [[EE]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_547]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_548]],  
                    x = 30465.01563,  
                    y = -1910.109375,  
                    z = 51.453125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_550]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_551]],  
                    x = 30376.75,  
                    y = -1910.046875,  
                    z = 49.265625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_553]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_554]],  
                    x = 30376.14063,  
                    y = -1802.09375,  
                    z = 45.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_556]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_557]],  
                    x = 30465.14063,  
                    y = -1805.40625,  
                    z = 53.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_544]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_559]],  
              Name = [[DE]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_561]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_562]],  
                    x = 30376.25,  
                    y = -1912.015625,  
                    z = 49.390625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_564]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_565]],  
                    x = 30465.09375,  
                    y = -1911.34375,  
                    z = 51.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_567]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_568]],  
                    x = 30466.14063,  
                    y = -2019.125,  
                    z = 55.828125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_570]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_571]],  
                    x = 30377.26563,  
                    y = -2019.4375,  
                    z = 47.828125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_558]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_573]],  
              Name = [[CE]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_575]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_576]],  
                    x = 30376.5625,  
                    y = -2021.046875,  
                    z = 47.65625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_578]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_579]],  
                    x = 30375.65625,  
                    y = -2128.546875,  
                    z = 53.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_581]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_582]],  
                    x = 30464.59375,  
                    y = -2129.9375,  
                    z = 55.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_584]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_585]],  
                    x = 30465.875,  
                    y = -2021.6875,  
                    z = 55.53125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_572]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_587]],  
              Name = [[BE]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_589]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_590]],  
                    x = 30376.20313,  
                    y = -2131.1875,  
                    z = 53.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_592]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_593]],  
                    x = 30375.42188,  
                    y = -2237.75,  
                    z = 56.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_595]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_596]],  
                    x = 30463.85938,  
                    y = -2239.390625,  
                    z = 66.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_598]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_599]],  
                    x = 30464.98438,  
                    y = -2131.875,  
                    z = 54.984375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_586]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_601]],  
              Name = [[AE]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_603]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_604]],  
                    x = 30375.73438,  
                    y = -2241.453125,  
                    z = 55.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_606]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_607]],  
                    x = 30376.375,  
                    y = -2347.4375,  
                    z = 54.421875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_609]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_610]],  
                    x = 30463.10938,  
                    y = -2349.71875,  
                    z = 65.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_612]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_613]],  
                    x = 30465.29688,  
                    y = -2242.859375,  
                    z = 66.765625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_600]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_615]],  
              Name = [[AD]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_617]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_618]],  
                    x = 30468.14063,  
                    y = -2241.140625,  
                    z = 66.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_620]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_621]],  
                    x = 30556.03125,  
                    y = -2240.578125,  
                    z = 70.140625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_623]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_624]],  
                    x = 30556.5,  
                    y = -2348.734375,  
                    z = 79.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_626]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_627]],  
                    x = 30467.78125,  
                    y = -2349.90625,  
                    z = 65.96875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_614]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_629]],  
              Name = [[AC Spawn]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_631]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_632]],  
                    x = 30558.375,  
                    y = -2241.015625,  
                    z = 69.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_634]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_635]],  
                    x = 30647.35938,  
                    y = -2240.828125,  
                    z = 68.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_637]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_638]],  
                    x = 30647.92188,  
                    y = -2349.171875,  
                    z = 78.71875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_640]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_641]],  
                    x = 30558.59375,  
                    y = -2349.453125,  
                    z = 79.21875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_628]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_643]],  
              Name = [[AB]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_645]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_646]],  
                    x = 30649.6875,  
                    y = -2241.171875,  
                    z = 68.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_648]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_649]],  
                    x = 30738.1875,  
                    y = -2240.828125,  
                    z = 67.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_651]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_652]],  
                    x = 30738.96875,  
                    y = -2349.5625,  
                    z = 74.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_654]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_655]],  
                    x = 30649.59375,  
                    y = -2349.84375,  
                    z = 78.53125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_642]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_26]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[]]
    },  
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_27]],  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_39]],  
              Base = [[palette.entities.creatures.ccbdc7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_40]],  
                x = 30418.10938,  
                y = -1842.5,  
                z = 53.25
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_37]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Clopperketh]]
            }
          },  
          InstanceId = [[Client1_28]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Title = [[Act 1]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_23]]
  }
}