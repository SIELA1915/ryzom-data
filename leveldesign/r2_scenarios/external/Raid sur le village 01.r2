scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      InstanceId = [[Client1_14]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts13]],  
      Time = 0,  
      Name = [[Dunes of Temperance (Desert 13)]],  
      Season = [[fall]],  
      ManualSeason = 1,  
      EntryPoint = [[uiR2SouthEntryPoint]]
    }
  },  
  InstanceId = [[Client1_5]],  
  UserComponents = {
  },  
  Class = [[Scenario]],  
  Behavior = {
    InstanceId = [[Client1_3]],  
    Class = [[LogicEntityBehavior]],  
    Actions = {
    },  
    Reactions = {
    }
  },  
  Name = [[New scenario]],  
  InheritPos = 1,  
  PlotItems = {
  },  
  VersionName = [[0.1.0]],  
  Position = {
    y = 0,  
    x = 0,  
    InstanceId = [[Client1_4]],  
    Class = [[Position]],  
    z = 0
  },  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 2,  
    Act = 4,  
    Behavior = 0,  
    TextManager = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    Region = 0,  
    ZoneTrigger = 0,  
    Position = 0,  
    Location = 0,  
    NpcCustom = 0,  
    LogicEntityBehavior = 0,  
    DefaultFeature = 0
  },  
  Description = {
    ShortDescription = [[]],  
    OptimalNumberOfPlayer = 0,  
    Title = [[]],  
    LevelId = 2,  
    Class = [[MapDescription]],  
    LocationId = 49,  
    InstanceId = [[Client1_1]]
  },  
  Acts = {
    {
      Cost = 1,  
      Behavior = {
        InstanceId = [[Client1_6]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 1,  
      InstanceId = [[Client1_8]],  
      ManualWeather = 0,  
      LocationId = [[]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_7]],  
        Class = [[Position]],  
        z = 0
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_230]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_228]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kami hut 1]],  
              Position = {
                y = -1923.1875,  
                x = 28949.23438,  
                InstanceId = [[Client1_231]],  
                Class = [[Position]],  
                z = 74.84375
              },  
              Angle = 2.625,  
              Base = [[palette.entities.botobjects.kami_hut]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_234]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_232]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Hote toi de la que je m'y mette]],  
              Position = {
                y = -1920.765625,  
                x = 28944.89063,  
                InstanceId = [[Client1_235]],  
                Class = [[Position]],  
                z = 74.796875
              },  
              Angle = 2.625,  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_9]]
        }
      },  
      Counters = {
      },  
      Version = 4,  
      Name = [[Permanent]],  
      Title = [[]]
    },  
    {
      Cost = 2,  
      Behavior = {
        InstanceId = [[Client1_10]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ShortDescription = [[]],  
      ActivitiesIds = {
      },  
      ExportList = {
      },  
      StaticCost = 0,  
      InstanceId = [[Client1_12]],  
      ManualWeather = 1,  
      LocationId = [[Client1_14]],  
      WeatherValue = 949,  
      InheritPos = 1,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_11]],  
        Class = [[Position]],  
        z = 0
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_255]],  
              Base = [[palette.entities.npcs.civils.f_civil_20]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_256]],  
                x = 28949.25,  
                y = -1982.453125,  
                z = 74.015625
              },  
              Angle = 2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_253]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_257]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 12,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 7,  
              GabaritLegsWidth = 2,  
              GabaritBreastSize = 12,  
              HairType = 3118,  
              HairColor = 1,  
              Tattoo = 19,  
              EyesColor = 3,  
              MorphTarget1 = 4,  
              MorphTarget2 = 5,  
              MorphTarget3 = 7,  
              MorphTarget4 = 0,  
              MorphTarget5 = 3,  
              MorphTarget6 = 5,  
              MorphTarget7 = 7,  
              MorphTarget8 = 5,  
              JacketModel = 0,  
              TrouserModel = 5605934,  
              FeetModel = 5605422,  
              HandsModel = 5605678,  
              ArmModel = 0,  
              JacketColor = 3,  
              ArmColor = 1,  
              HandsColor = 0,  
              TrouserColor = 4,  
              FeetColor = 2,  
              Speed = 0,  
              Level = 0,  
              Sex = 1,  
              WeaponRightHand = 0,  
              WeaponLeftHand = 0,  
              Name = [[Hotesse d'Accueil]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_civil_light_melee_blunt_b2.creature]]
            }
          },  
          InstanceId = [[Client1_13]]
        },  
        {
          InstanceId = [[Client1_236]],  
          Behavior = {
            InstanceId = [[Client1_237]],  
            Class = [[LogicEntityBehavior]],  
            Actions = {
            },  
            Reactions = {
            }
          },  
          Cyclic = 0,  
          InheritPos = 1,  
          _Zone = [[Client1_240]],  
          Name = [[TZ]],  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_238]],  
            x = 28940.82813,  
            y = -1918.625,  
            z = 74.796875
          },  
          Base = [[palette.entities.botobjects.campfire]],  
          Active = 1,  
          Components = {
            {
              Class = [[Region]],  
              InstanceId = [[Client1_240]],  
              Name = [[Zone d'arrivée]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_239]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Deletable = 0,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_242]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_243]],  
                    x = 6.421875,  
                    y = 2.109375,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_245]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_246]],  
                    x = -1.21875,  
                    y = 5.890625,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_248]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_249]],  
                    x = -5.84375,  
                    y = -2.15625,  
                    z = 0
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_251]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_252]],  
                    x = 1.6875,  
                    y = -6.546875,  
                    z = -0.046875
                  },  
                  InheritPos = 1,  
                  Deletable = 0
                }
              }
            }
          },  
          Class = [[ZoneTrigger]]
        }
      },  
      Counters = {
      },  
      Version = 4,  
      Name = [[Act 1:Act 1]],  
      Title = [[]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_2]]
  }
}