scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_1132]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 18,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_1134]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    TextManager = 0,  
    Position = 0,  
    ActivityStep = 0,  
    NpcCustom = 0,  
    Region = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 40,  
      Class = [[Act]],  
      WeatherValue = 1022,  
      Title = [[Ceremony]],  
      ActivitiesIds = {
        [[Client1_1438]]
      },  
      Events = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1141]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1142]],  
                x = 27940.65625,  
                y = -1967.8125,  
                z = -3.140625
              },  
              Angle = 1.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1139]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Home Exit Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1145]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1146]],  
                x = 27899.29688,  
                y = -1851.296875,  
                z = -13.546875
              },  
              Angle = -2.953125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1143]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1149]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1150]],  
                x = 27895.9375,  
                y = -1851.203125,  
                z = -13.875
              },  
              Angle = -2.203125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1147]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1153]],  
              Base = [[palette.entities.botobjects.tower_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1154]],  
                x = 27931.39063,  
                y = -1850.9375,  
                z = -14.5
              },  
              Angle = -1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1151]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1157]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1158]],  
                x = 27922.95313,  
                y = -1844.8125,  
                z = -13.4375
              },  
              Angle = -0.890625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1155]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1161]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1162]],  
                x = 27937.125,  
                y = -1853.234375,  
                z = -12.9375
              },  
              Angle = 2.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1159]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1165]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1166]],  
                x = 27892.96875,  
                y = -1838.734375,  
                z = -14.640625
              },  
              Angle = -1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1163]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1169]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1170]],  
                x = 27937.21875,  
                y = -1840.8125,  
                z = -14.734375
              },  
              Angle = -2.421875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1167]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1173]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1174]],  
                x = 27930.85938,  
                y = -1856.328125,  
                z = -13.578125
              },  
              Angle = -1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1171]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1177]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1178]],  
                x = 27898.125,  
                y = -1853.578125,  
                z = -13.796875
              },  
              Angle = -0.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1175]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1181]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1182]],  
                x = 27942.53125,  
                y = -1887.59375,  
                z = -7.96875
              },  
              Angle = -1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1179]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1185]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1186]],  
                x = 27941.15625,  
                y = -1901.703125,  
                z = -7.15625
              },  
              Angle = 1.78125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1183]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn House 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1189]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1190]],  
                x = 27943.17188,  
                y = -1896.890625,  
                z = -7.53125
              },  
              Angle = 3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1187]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1193]],  
              Base = [[palette.entities.botobjects.fx_fy_feu_foret]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1194]],  
                x = 27939.875,  
                y = -1900.859375,  
                z = -7.34375
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1191]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Fire 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1197]],  
              Base = [[palette.entities.botobjects.merchant_focus_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1198]],  
                x = 27915.09375,  
                y = -1878.25,  
                z = -10.640625
              },  
              Angle = 1.625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1195]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Home Spawn Point]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1209]],  
              Base = [[palette.entities.botobjects.runic_circle]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1210]],  
                x = 27895.6875,  
                y = -1877.34375,  
                z = -13.0625
              },  
              Angle = -1.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1207]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Home Circle 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1213]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1214]],  
                x = 27964.14063,  
                y = -1893.28125,  
                z = -13.71875
              },  
              Angle = 2.859375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1211]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Burn Goo 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1216]],  
              Name = [[Home Region]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1218]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1219]],  
                    x = 27932.96875,  
                    y = -1906.75,  
                    z = -7.75
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1221]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1222]],  
                    x = 27932.32813,  
                    y = -1881.984375,  
                    z = -9.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1224]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1225]],  
                    x = 27935.84375,  
                    y = -1863.34375,  
                    z = -11.625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1228]],  
                    x = 27890.98438,  
                    y = -1861.46875,  
                    z = -14.546875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1231]],  
                    x = 27883.375,  
                    y = -1869.015625,  
                    z = -13.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1233]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1234]],  
                    x = 27887.78125,  
                    y = -1903.015625,  
                    z = -10.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1236]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1237]],  
                    x = 27898.15625,  
                    y = -1901.34375,  
                    z = -7.859375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1215]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1263]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1264]],  
                x = 28058.39063,  
                y = -2034.015625,  
                z = 9.828125
              },  
              Angle = 0.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1261]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1267]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1268]],  
                x = 28140.42188,  
                y = -1974.828125,  
                z = 15.875
              },  
              Angle = -1.25,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1265]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1271]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1272]],  
                x = 28223.21875,  
                y = -1946.765625,  
                z = 32.015625
              },  
              Angle = 0.515625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1269]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[watch tower 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1359]],  
              Base = [[palette.entities.botobjects.kami_hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1360]],  
                x = 28334.4375,  
                y = -1838.203125,  
                z = 33.25
              },  
              Angle = -2.921875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1357]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kami hut 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1363]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1364]],  
                x = 28311.25,  
                y = -1830.140625,  
                z = 33.375
              },  
              Angle = -1.546875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1361]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kami watchtower 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1367]],  
              Base = [[palette.entities.botobjects.kami_watchtower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1368]],  
                x = 28312.84375,  
                y = -1849.75,  
                z = 33.796875
              },  
              Angle = 2.46875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1365]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[kami watchtower 2]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1391]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1392]],  
                x = 28310.42188,  
                y = -1838.375,  
                z = 32.59375
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1389]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1395]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1396]],  
                x = 28310.21875,  
                y = -1839.640625,  
                z = 32.6875
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1393]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 0,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1399]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1400]],  
                x = 28309.82813,  
                y = -1840.984375,  
                z = 32.75
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1397]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1403]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1404]],  
                x = 28309.20313,  
                y = -1841.65625,  
                z = 32.90625
              },  
              Angle = -3.09375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1401]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1407]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1408]],  
                x = 28308.75,  
                y = -1842.703125,  
                z = 33.140625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1405]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 3,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard]],  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1411]],  
              Base = [[palette.entities.npcs.bandits.t_melee_dd_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1412]],  
                x = 28306.70313,  
                y = -1839.953125,  
                z = 32.640625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1409]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1438]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1439]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_1425]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 7,  
              GabaritArmsWidth = 14,  
              GabaritLegsWidth = 14,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 5,  
              Tattoo = 18,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6728750,  
              TrouserModel = 6727726,  
              FeetModel = 6726702,  
              HandsModel = 6727214,  
              ArmModel = 6728238,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6767150,  
              WeaponLeftHand = 0,  
              Name = [[Security Guard Leader]],  
              ActivitiesId = {
                [[Client1_1438]]
              },  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_male.creature]],  
              Sheet = [[ring_melee_damage_dealer_slash_f3.creature]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1415]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1416]],  
                x = 28314.59375,  
                y = -1838.515625,  
                z = 32.796875
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1413]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Security Kami]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1419]],  
              Base = [[palette.entities.npcs.kami.kami_guardian_4_f]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1420]],  
                x = 28313.84375,  
                y = -1843.03125,  
                z = 32.875
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1417]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Security Kami]]
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_1425]],  
              Name = [[Place 1]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1427]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1428]],  
                    x = 28310.14063,  
                    y = -1828.4375,  
                    z = 33.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1430]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1431]],  
                    x = 28335.92188,  
                    y = -1825.09375,  
                    z = 34.796875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1433]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1434]],  
                    x = 28338.23438,  
                    y = -1855.75,  
                    z = 34.890625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_1436]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_1437]],  
                    x = 28311.59375,  
                    y = -1851.625,  
                    z = 34.484375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1424]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1446]],  
              Base = [[palette.entities.botobjects.stele]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1447]],  
                x = 28189.9375,  
                y = -1901.8125,  
                z = 38.21875
              },  
              Angle = 0.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1444]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[stele 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1454]],  
              Base = [[palette.entities.botobjects.spot_goo]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1455]],  
                x = 28168.67188,  
                y = -1909.078125,  
                z = 24.125
              },  
              Angle = 0.5,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1452]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo spot 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1458]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1459]],  
                x = 28171.23438,  
                y = -1907,  
                z = 27.375
              },  
              Angle = 0.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1456]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[goo smoke 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1462]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1463]],  
                x = 28247.25,  
                y = -1756.09375,  
                z = 28.78125
              },  
              Angle = -2.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1460]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1466]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1467]],  
                x = 28236.39063,  
                y = -1752.5,  
                z = 27.875
              },  
              Angle = -1.234375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1464]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[hut 1]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1470]],  
              Base = [[palette.entities.botobjects.wind_turbine]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1471]],  
                x = 28208.625,  
                y = -1816.8125,  
                z = 37.34375
              },  
              Angle = 2.005622864,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1468]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[wind turbine 1]]
            }
          },  
          InstanceId = [[Client1_1136]]
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1260]],  
          Name = [[Ceremony Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1240]],  
              Base = [[palette.entities.npcs.kami.kami_preacher_4_b]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1241]],  
                x = 27908.625,  
                y = -1876.734375,  
                z = -11.28125
              },  
              Angle = 0,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1238]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[Ceremony Master]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1256]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1257]],  
                x = 27907.17188,  
                y = -1874.671875,  
                z = -11.9375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1254]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6736942,  
              TrouserModel = 6735918,  
              FeetModel = 6734894,  
              HandsModel = 6735406,  
              ArmModel = 6736430,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6936622,  
              Name = [[Ceremony Guard]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            },  
            {
              Class = [[NpcCustom]],  
              InstanceId = [[Client1_1252]],  
              Base = [[palette.entities.npcs.bandits.z_mage_damage_dealer_220]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1253]],  
                x = 27907.75,  
                y = -1878.078125,  
                z = -10.984375
              },  
              Angle = -0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1250]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              GabaritHeight = 14,  
              GabaritTorsoWidth = 0,  
              GabaritArmsWidth = 0,  
              GabaritLegsWidth = 0,  
              GabaritBreastSize = 14,  
              HairType = 5621550,  
              HairColor = 3,  
              Tattoo = 12,  
              EyesColor = 5,  
              MorphTarget1 = 3,  
              MorphTarget2 = 5,  
              MorphTarget3 = 3,  
              MorphTarget4 = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              MorphTarget7 = 4,  
              MorphTarget8 = 2,  
              Sex = 1,  
              JacketModel = 6736942,  
              TrouserModel = 6735918,  
              FeetModel = 6734894,  
              HandsModel = 6735406,  
              ArmModel = 6736430,  
              JacketColor = 4,  
              ArmColor = 4,  
              HandsColor = 4,  
              TrouserColor = 4,  
              FeetColor = 4,  
              WeaponRightHand = 6936622,  
              Name = [[Ceremony Guard]],  
              WeaponLeftHand = 0,  
              ActivitiesId = {
              },  
              Sheet = [[ring_magic_damage_dealer_electricity_f2.creature]],  
              InheritPos = 1,  
              SheetClient = [[basic_fyros_female.creature]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1259]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1258]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1423]],  
          Name = [[Group 1]],  
          Components = {
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1422]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1421]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 1,  
      InstanceId = [[Client1_1135]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_1133]],  
    Texts = {
    }
  }
}