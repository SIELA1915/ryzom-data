scenario = {
  AccessRules = [[liberal]],  
  Locations = {
    {
      Time = 0,  
      InstanceId = [[Client4_12]],  
      Season = [[fall]],  
      Class = [[Location]],  
      IslandName = [[uiR2_Deserts33]],  
      EntryPoint = 0
    }
  },  
  InstanceId = [[Client4_3]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Description = {
    InstanceId = [[Client4_1]],  
    Class = [[MapDescription]],  
    LevelId = 3,  
    LocationId = 70,  
    OptimalNumberOfPlayer = 0,  
    EntryPointId = 0,  
    MaxEntities = 50,  
    RuleId = 0,  
    ShortDescription = [[A nice little town that's being invaded by evil bandits and kitins.]],  
    Title = [[Outnumbered by Kitins]],  
    MaxPlayers = 100
  },  
  Versions = {
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    TextManagerEntry = 0,  
    Npc = 0,  
    MapDescription = 0,  
    RegionVertex = 0,  
    TextManager = 0,  
    Region = 0,  
    Position = 0,  
    Location = 0,  
    Road = 0,  
    WayPoint = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 17,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_4]],  
      ActivitiesIds = {
        [[Client1_138]],  
        [[Client1_163]],  
        [[Client1_166]],  
        [[Client1_358]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_30]],  
              Base = [[palette.entities.botobjects.barrier]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_31]],  
                x = 29704.92188,  
                y = -2218.90625,  
                z = -17.859375
              },  
              Angle = 2.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_28]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kitin Cave]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_34]],  
              Base = [[palette.entities.botobjects.fx_goo_smoke]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_35]],  
                x = 29676.78125,  
                y = -2123.0625,  
                z = -16.984375
              },  
              Angle = -1.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_32]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Goo Show]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_38]],  
              Base = [[palette.entities.botobjects.roadsign]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_39]],  
                x = 29649.26563,  
                y = -2176.5625,  
                z = -16.8125
              },  
              Angle = -0.484375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_36]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Homin Camp]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_46]],  
              Base = [[palette.entities.botobjects.hut]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_47]],  
                x = 29551.48438,  
                y = -2051.046875,  
                z = -22.125
              },  
              Angle = -2.9375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_44]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[hut 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_50]],  
              Base = [[palette.entities.botobjects.paddock]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_51]],  
                x = 29476.76563,  
                y = -2057.046875,  
                z = -22.375
              },  
              Angle = 0.53125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_48]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[paddock 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_54]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_55]],  
                x = 29518.67188,  
                y = -2093.921875,  
                z = -23.9375
              },  
              Angle = -4.75,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_52]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Bandit Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_58]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_59]],  
                x = 29488.76563,  
                y = -2026.875,  
                z = -19.3125
              },  
              Angle = 0.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_56]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1882]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1883]],  
                x = 29498.64063,  
                y = -2010.75,  
                z = -18.765625
              },  
              Angle = -1.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1880]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1886]],  
              Base = [[palette.entities.botobjects.tent_fyros]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1887]],  
                x = 29517.92188,  
                y = -2017.03125,  
                z = -20.125
              },  
              Angle = -2.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1884]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[fyros tent 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_141]],  
              Name = [[Town]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_140]],  
                x = -0.59375,  
                y = 0.578125,  
                z = 0.109375
              },  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_143]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_144]],  
                    x = 29508.5,  
                    y = -2057.015625,  
                    z = -21.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_146]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_147]],  
                    x = 29508.5,  
                    y = -2057.015625,  
                    z = -21.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_149]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_150]],  
                    x = 29469.6875,  
                    y = -2040.40625,  
                    z = -22.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_152]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_153]],  
                    x = 29504.9375,  
                    y = -2023.03125,  
                    z = -20.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_155]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_156]],  
                    x = 29532.5625,  
                    y = -2021.5,  
                    z = -20.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_158]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_159]],  
                    x = 29549.25,  
                    y = -2068.90625,  
                    z = -22.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_161]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_162]],  
                    x = 29523.15625,  
                    y = -2063.609375,  
                    z = -22.296875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_191]],  
              Name = [[Bandit Place]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_193]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_194]],  
                    x = 29534.01563,  
                    y = -2242.34375,  
                    z = -20.046875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_196]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_197]],  
                    x = 29533.96875,  
                    y = -2242.546875,  
                    z = -20.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_199]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_200]],  
                    x = 29527.03125,  
                    y = -2212.703125,  
                    z = -19.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_202]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_203]],  
                    x = 29503.76563,  
                    y = -2216.140625,  
                    z = -18.59375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_205]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_206]],  
                    x = 29506.0625,  
                    y = -2240.953125,  
                    z = -19.265625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_190]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_295]],  
              Base = [[palette.entities.botobjects.house_ruin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_296]],  
                x = 29519.23438,  
                y = -2243.34375,  
                z = -22.078125
              },  
              Angle = 1.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_293]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_300]],  
              Name = [[Kitin Camp]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_302]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_303]],  
                    x = 29903.57813,  
                    y = -2225.984375,  
                    z = -9.203125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_305]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_306]],  
                    x = 29777.8125,  
                    y = -2203.046875,  
                    z = -16.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_308]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_309]],  
                    x = 29777.375,  
                    y = -2127.296875,  
                    z = -17.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_311]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_312]],  
                    x = 29905.3125,  
                    y = -2142.65625,  
                    z = -14.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_299]],  
                x = -0.03125,  
                y = -0.265625,  
                z = 0.796875
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_348]],  
              Name = [[Somewhere to Leader]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_350]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_351]],  
                    x = 29495.92188,  
                    y = -2017.796875,  
                    z = -18.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_353]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_354]],  
                    x = 29497.125,  
                    y = -2019.984375,  
                    z = -19.25
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_356]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_357]],  
                    x = 29500.01563,  
                    y = -2023.578125,  
                    z = -19.9375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_347]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_414]],  
              Name = [[Tower to Leader]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_413]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_416]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_417]],  
                    x = 29520.78125,  
                    y = -2073.828125,  
                    z = -24.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_419]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_420]],  
                    x = 29518.3125,  
                    y = -2046.453125,  
                    z = -21.84375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_422]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_423]],  
                    x = 29500.82813,  
                    y = -2024.75,  
                    z = -20.171875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_447]],  
              Name = [[Leader to Bandits]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_446]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_449]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_450]],  
                    x = 29509.17188,  
                    y = -2073.53125,  
                    z = -23
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_452]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_453]],  
                    x = 29506.60938,  
                    y = -2090.1875,  
                    z = -22.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_455]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_456]],  
                    x = 29484.26563,  
                    y = -2112.890625,  
                    z = -22.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_458]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_459]],  
                    x = 29495.40625,  
                    y = -2194.328125,  
                    z = -17.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_461]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_462]],  
                    x = 29497.625,  
                    y = -2189.015625,  
                    z = -16.96875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_464]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_465]],  
                    x = 29497.1875,  
                    y = -2187.765625,  
                    z = -17.046875
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_475]],  
              Name = [[Bandits To Leader]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_477]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_478]],  
                    x = 29485,  
                    y = -2111.4375,  
                    z = -22.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_480]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_481]],  
                    x = 29507.39063,  
                    y = -2088.078125,  
                    z = -22.40625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_483]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_484]],  
                    x = 29500.82813,  
                    y = -2024.75,  
                    z = -20.15625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_474]],  
                x = 0,  
                y = 0,  
                z = 0.015625
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_529]],  
              Base = [[palette.entities.botobjects.tr_s2_bamboo_a]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_530]],  
                x = 29466.95313,  
                y = -2102.796875,  
                z = -23.90625
              },  
              Angle = -0.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_527]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kami Background]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_537]],  
              Base = [[palette.entities.botobjects.watch_tower]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_538]],  
                x = 29621.89063,  
                y = -2186.8125,  
                z = -17.65625
              },  
              Angle = 0.828125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_535]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[Kitin Tower]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_541]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_542]],  
                x = 29673.1875,  
                y = -2208.171875,  
                z = -18
              },  
              Angle = 0.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_539]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_545]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_546]],  
                x = 29679.0625,  
                y = -2222.78125,  
                z = -19.484375
              },  
              Angle = 1.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_543]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_549]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_550]],  
                x = 29690.01563,  
                y = -2226.046875,  
                z = -18.390625
              },  
              Angle = 0.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_547]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 3]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_553]],  
              Base = [[palette.entities.botobjects.landslide_desert]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_554]],  
                x = 29680.32813,  
                y = -2111.59375,  
                z = -17.15625
              },  
              Angle = -1.734375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_551]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[desert landslide 4]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_573]],  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_574]],  
                x = 29747.34375,  
                y = -2132.53125,  
                z = -20.328125
              },  
              Angle = -1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_571]],  
                Type = [[]],  
                ZoneId = [[]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              InheritPos = 1,  
              Name = [[kitin totem 1]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client1_18940]],  
              Name = [[Boss Zone]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18942]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18943]],  
                    x = 29906.3125,  
                    y = -2143.09375,  
                    z = -14.09375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18945]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18946]],  
                    x = 29927.65625,  
                    y = -2147.359375,  
                    z = -17.53125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18948]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18949]],  
                    x = 29927.0625,  
                    y = -2170.078125,  
                    z = -18.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client1_18951]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18952]],  
                    x = 29906.45313,  
                    y = -2175.078125,  
                    z = -17.546875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18939]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_18968]],  
              Name = [[Kitins To Town]],  
              InheritPos = 1,  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_18967]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18970]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18971]],  
                    x = 29748.5625,  
                    y = -2172.953125,  
                    z = -18.984375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18973]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18974]],  
                    x = 29744.26563,  
                    y = -2205.25,  
                    z = -17.578125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18976]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18977]],  
                    x = 29731.60938,  
                    y = -2217.875,  
                    z = -18.4375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18979]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18980]],  
                    x = 29720.89063,  
                    y = -2220.671875,  
                    z = -17.125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18982]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18983]],  
                    x = 29701.6875,  
                    y = -2221.8125,  
                    z = -17.078125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18985]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18986]],  
                    x = 29701.67188,  
                    y = -2217.59375,  
                    z = -18.171875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18988]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18989]],  
                    x = 29677.60938,  
                    y = -2194.390625,  
                    z = -19.21875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18991]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18992]],  
                    x = 29648.35938,  
                    y = -2179.296875,  
                    z = -17.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18994]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18995]],  
                    x = 29634.65625,  
                    y = -2177.78125,  
                    z = -17.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_18997]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_18998]],  
                    x = 29616.59375,  
                    y = -2174.6875,  
                    z = -17.296875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19000]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19001]],  
                    x = 29597.875,  
                    y = -2174.796875,  
                    z = -18.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19003]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19004]],  
                    x = 29562.25,  
                    y = -2212.109375,  
                    z = -17.765625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19006]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19007]],  
                    x = 29545.04688,  
                    y = -2204.859375,  
                    z = -19.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19009]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19010]],  
                    x = 29514.78125,  
                    y = -2163.078125,  
                    z = -18.6875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19012]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19013]],  
                    x = 29484.65625,  
                    y = -2112.15625,  
                    z = -22.671875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19015]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19016]],  
                    x = 29509.125,  
                    y = -2086.71875,  
                    z = -23.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19018]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19019]],  
                    x = 29519.90625,  
                    y = -2051.796875,  
                    z = -21.609375
                  },  
                  InheritPos = 1
                }
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19021]],  
              Name = [[Kitin Tower To Town]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19023]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19024]],  
                    x = 29627.9375,  
                    y = -2177.421875,  
                    z = -17.15625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19026]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19027]],  
                    x = 29614.51563,  
                    y = -2174.25,  
                    z = -17.515625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19029]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19030]],  
                    x = 29596.90625,  
                    y = -2174.015625,  
                    z = -18.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19032]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19033]],  
                    x = 29562,  
                    y = -2212.546875,  
                    z = -17.859375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19035]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19036]],  
                    x = 29544.51563,  
                    y = -2204.84375,  
                    z = -19.78125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19038]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19039]],  
                    x = 29513.32813,  
                    y = -2159.625,  
                    z = -19.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19041]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19042]],  
                    x = 29483.89063,  
                    y = -2111.46875,  
                    z = -23.015625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19044]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19045]],  
                    x = 29506.73438,  
                    y = -2092.875,  
                    z = -22.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19047]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19048]],  
                    x = 29517.8125,  
                    y = -2094.21875,  
                    z = -23.8125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19020]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19167]],  
              Name = [[Walk In Tower]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19169]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19170]],  
                    x = 29507.29688,  
                    y = -2092.859375,  
                    z = -23
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19172]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19173]],  
                    x = 29517.76563,  
                    y = -2094.140625,  
                    z = -23.875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19175]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19176]],  
                    x = 29517.95313,  
                    y = -2092.234375,  
                    z = -24.21875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19166]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19178]],  
              Name = [[TalkLeader Route 1]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19180]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19181]],  
                    x = 29501.89063,  
                    y = -2029.984375,  
                    z = -20.953125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19183]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19184]],  
                    x = 29500.375,  
                    y = -2026.234375,  
                    z = -20.34375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19186]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19187]],  
                    x = 29500.35938,  
                    y = -2025.21875,  
                    z = -20.203125
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19177]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19189]],  
              Name = [[TalkLeader Route 2]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19191]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19192]],  
                    x = 29503.6875,  
                    y = -2029.3125,  
                    z = -20.90625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19194]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19195]],  
                    x = 29502.20313,  
                    y = -2025.0625,  
                    z = -20.328125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19197]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19198]],  
                    x = 29501.3125,  
                    y = -2024.5625,  
                    z = -20.1875
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19188]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            },  
            {
              Class = [[Road]],  
              InstanceId = [[Client1_19222]],  
              Name = [[Walk in Kitin Tower]],  
              InheritPos = 1,  
              Points = {
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19224]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19225]],  
                    x = 29628.73438,  
                    y = -2178.328125,  
                    z = -17.234375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19227]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19228]],  
                    x = 29622.98438,  
                    y = -2186.40625,  
                    z = -17.734375
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[WayPoint]],  
                  InstanceId = [[Client1_19230]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client1_19231]],  
                    x = 29621.79688,  
                    y = -2185.875,  
                    z = -17.84375
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_19221]],  
                x = 0,  
                y = 0,  
                z = 0
              }
            }
          },  
          InstanceId = [[Client1_5]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Events = {
      },  
      States = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_2]],  
    Texts = {
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_221]],  
        Count = 35,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_222]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_223]],  
        Count = 1,  
        Text = [[]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_224]],  
        Count = 4,  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_225]],  
        Count = 1,  
        Text = [[Some bandits appeared near our cute little town! Omgwth?? KILL THEM ALL!!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_407]],  
        Count = 7,  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_408]],  
        Count = 1,  
        Text = [[Good morning to everyone! What a great day today!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_430]],  
        Count = 11,  
        Text = [[Bad news!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_431]],  
        Count = 1,  
        Text = [[Bad news!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_434]],  
        Count = 2,  
        Text = [[What's wrong? Did something bad happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_437]],  
        Count = 2,  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_438]],  
        Count = 1,  
        Text = [[There's a group of bandit's that just arrived near our camp, they will probly try to take over this place!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_443]],  
        Count = 1,  
        Text = [[We must do something immediately!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_444]],  
        Count = 2,  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_445]],  
        Count = 1,  
        Text = [[Everyone! Please, go find this group of bandits, and kill them!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_469]],  
        Count = 3,  
        Text = [[Follow me!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_470]],  
        Count = 1,  
        Text = [[Follow me!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_489]],  
        Count = 3,  
        Text = [[The bandits are gone!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_492]],  
        Count = 3,  
        Text = [[Thank you all for helping us out with this! You were of great help for us!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_495]],  
        Count = 1,  
        Text = [[I will see you all later. Goodbye.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_509]],  
        Count = 2,  
        Text = [[Well Done!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_513]],  
        Count = 4,  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_514]],  
        Count = 1,  
        Text = [[This is the bandit's camp.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_517]],  
        Count = 2,  
        Text = [[Well Done! You killed their leader!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_522]],  
        Count = 4,  
        Text = [[All you will die right here and now! GET THEM!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_18841]],  
        Count = 1,  
        Text = [[Muahahahahahaaaa!!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19057]],  
        Count = 3,  
        Text = [[Anyone here? There might be a problem!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19058]],  
        Count = 4,  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19059]],  
        Count = 1,  
        Text = [[Yes, I'm coming! What's wrong?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19062]],  
        Count = 5,  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19063]],  
        Count = 1,  
        Text = [[The Kitins! They're acting strange again!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19066]],  
        Count = 5,  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19067]],  
        Count = 1,  
        Text = [[We should go see the Town Leader.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19073]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19074]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19075]],  
        Count = 1,  
        Text = [[I'm here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19076]],  
        Count = 2,  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19077]],  
        Count = 1,  
        Text = [[Hello there, did anything happen?]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19080]],  
        Count = 2,  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19081]],  
        Count = 1,  
        Text = [[We're here to report some very bad news.]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19084]],  
        Count = 2,  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19085]],  
        Count = 1,  
        Text = [[The Kitin are showing strange behavior again, they might be here soon!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19088]],  
        Count = 3,  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19089]],  
        Count = 1,  
        Text = [[That's terrible! Please, everyone, be very careful! If they come here, they have to be killed immediately!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19202]],  
        Count = 2,  
        Text = [[Oh my god! People are dieing!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19203]],  
        Count = 1,  
        Text = [[Oh my god! People are dieing!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19255]],  
        Count = 4,  
        Text = [[Well done, all! You killed their leader!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19256]],  
        Count = 1,  
        Text = [[Well done, all! You killed their leader!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19272]],  
        Count = 5,  
        Text = [[The Kitin are gone!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19273]],  
        Count = 1,  
        Text = [[The Kitin are gone!]]
      },  
      {
        Class = [[TextManagerEntry]],  
        InstanceId = [[Client1_19297]],  
        Count = 3,  
        Text = [[Send a huge thank you to everyone who helped!]]
      }
    }
  }
}