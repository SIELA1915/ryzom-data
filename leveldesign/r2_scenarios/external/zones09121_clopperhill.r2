scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_22]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_24]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    NpcCustom = 0,  
    MapDescription = 0,  
    DefaultFeature = 0,  
    Region = 0,  
    ActivityStep = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    Position = 0,  
    TextManager = 0,  
    Road = 0,  
    WayPoint = 0,  
    Npc = 0
  },  
  Acts = {
    {
      Cost = 50,  
      Class = [[Act]],  
      WeatherValue = 0,  
      Events = {
      },  
      ActivitiesIds = {
        [[Client1_1336]],  
        [[Client1_1521]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Y0]],  
              InstanceId = [[Client1_246]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_245]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_248]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.15625,  
                    x = 30830.48438,  
                    InstanceId = [[Client1_249]],  
                    Class = [[Position]],  
                    z = 66.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_251]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.515625,  
                    x = 30830.375,  
                    InstanceId = [[Client1_252]],  
                    Class = [[Position]],  
                    z = 79.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_254]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.6875,  
                    x = 30831.84375,  
                    InstanceId = [[Client1_255]],  
                    Class = [[Position]],  
                    z = 79.390625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X5]],  
              InstanceId = [[Client1_273]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_272]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_275]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.546875,  
                    x = 30830.53125,  
                    InstanceId = [[Client1_276]],  
                    Class = [[Position]],  
                    z = 79.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_278]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5,  
                    x = 30375.34375,  
                    InstanceId = [[Client1_279]],  
                    Class = [[Position]],  
                    z = 45.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_281]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1795.296875,  
                    x = 30373.40625,  
                    InstanceId = [[Client1_282]],  
                    Class = [[Position]],  
                    z = 45.703125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y1]],  
              InstanceId = [[Client1_300]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_299]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_302]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.515625,  
                    x = 30739.15625,  
                    InstanceId = [[Client1_303]],  
                    Class = [[Position]],  
                    z = 72.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_305]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2360.140625,  
                    x = 30739.54688,  
                    InstanceId = [[Client1_306]],  
                    Class = [[Position]],  
                    z = 75.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_308]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2359.015625,  
                    x = 30740.98438,  
                    InstanceId = [[Client1_309]],  
                    Class = [[Position]],  
                    z = 74.71875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X0]],  
              InstanceId = [[Client1_311]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_310]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_313]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.1875,  
                    x = 30830.45313,  
                    InstanceId = [[Client1_314]],  
                    Class = [[Position]],  
                    z = 66.671875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_316]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.71875,  
                    x = 30374.9375,  
                    InstanceId = [[Client1_317]],  
                    Class = [[Position]],  
                    z = 54.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_319]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.046875,  
                    x = 30376.25,  
                    InstanceId = [[Client1_320]],  
                    Class = [[Position]],  
                    z = 54.28125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y5]],  
              InstanceId = [[Client1_322]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_321]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_324]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.84375,  
                    x = 30374.90625,  
                    InstanceId = [[Client1_325]],  
                    Class = [[Position]],  
                    z = 54.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_327]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5625,  
                    x = 30375.375,  
                    InstanceId = [[Client1_328]],  
                    Class = [[Position]],  
                    z = 45.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_330]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.4375,  
                    x = 30374.45313,  
                    InstanceId = [[Client1_331]],  
                    Class = [[Position]],  
                    z = 45.5625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y4]],  
              InstanceId = [[Client1_333]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_332]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_335]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1805.234375,  
                    x = 30466.57813,  
                    InstanceId = [[Client1_336]],  
                    Class = [[Position]],  
                    z = 53.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_338]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.640625,  
                    x = 30466.6875,  
                    InstanceId = [[Client1_339]],  
                    Class = [[Position]],  
                    z = 65.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_341]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2348.90625,  
                    x = 30465.92188,  
                    InstanceId = [[Client1_342]],  
                    Class = [[Position]],  
                    z = 65.953125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y3]],  
              InstanceId = [[Client1_344]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_343]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_346]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.5,  
                    x = 30557.5625,  
                    InstanceId = [[Client1_347]],  
                    Class = [[Position]],  
                    z = 58.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_349]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.453125,  
                    x = 30557.39063,  
                    InstanceId = [[Client1_350]],  
                    Class = [[Position]],  
                    z = 79.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_352]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2351.546875,  
                    x = 30559.8125,  
                    InstanceId = [[Client1_353]],  
                    Class = [[Position]],  
                    z = 79.03125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Y2]],  
              InstanceId = [[Client1_355]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_354]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_357]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1800.53125,  
                    x = 30648.59375,  
                    InstanceId = [[Client1_358]],  
                    Class = [[Position]],  
                    z = 67.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_360]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2350.375,  
                    x = 30648.65625,  
                    InstanceId = [[Client1_361]],  
                    Class = [[Position]],  
                    z = 78.90625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_363]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2352.03125,  
                    x = 30649.98438,  
                    InstanceId = [[Client1_364]],  
                    Class = [[Position]],  
                    z = 79.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X4]],  
              InstanceId = [[Client1_366]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_365]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_368]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1910.9375,  
                    x = 30830.32813,  
                    InstanceId = [[Client1_369]],  
                    Class = [[Position]],  
                    z = 79.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_371]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1910.765625,  
                    x = 30375.29688,  
                    InstanceId = [[Client1_372]],  
                    Class = [[Position]],  
                    z = 49.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_374]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1909.78125,  
                    x = 30372.59375,  
                    InstanceId = [[Client1_375]],  
                    Class = [[Position]],  
                    z = 49.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X3]],  
              InstanceId = [[Client1_377]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_376]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_379]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2020.75,  
                    x = 30830.35938,  
                    InstanceId = [[Client1_380]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_382]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2020.109375,  
                    x = 30375.17188,  
                    InstanceId = [[Client1_383]],  
                    Class = [[Position]],  
                    z = 47.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_385]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2021.671875,  
                    x = 30372.10938,  
                    InstanceId = [[Client1_386]],  
                    Class = [[Position]],  
                    z = 46.796875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X2]],  
              InstanceId = [[Client1_388]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_387]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_390]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2130.75,  
                    x = 30830.29688,  
                    InstanceId = [[Client1_391]],  
                    Class = [[Position]],  
                    z = 70.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_393]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2130.140625,  
                    x = 30375.125,  
                    InstanceId = [[Client1_394]],  
                    Class = [[Position]],  
                    z = 53.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_396]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2132.734375,  
                    x = 30371.5,  
                    InstanceId = [[Client1_397]],  
                    Class = [[Position]],  
                    z = 53.265625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[X1]],  
              InstanceId = [[Client1_399]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_398]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_401]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2240.359375,  
                    x = 30830.5,  
                    InstanceId = [[Client1_402]],  
                    Class = [[Position]],  
                    z = 65.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_404]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2239.90625,  
                    x = 30375.0625,  
                    InstanceId = [[Client1_405]],  
                    Class = [[Position]],  
                    z = 55.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_407]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2241.359375,  
                    x = 30372.09375,  
                    InstanceId = [[Client1_408]],  
                    Class = [[Position]],  
                    z = 55.578125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AA]],  
              InstanceId = [[Client1_410]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_409]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_412]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.859375,  
                    x = 30829.85938,  
                    InstanceId = [[Client1_413]],  
                    Class = [[Position]],  
                    z = 66.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_415]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.84375,  
                    x = 30740.42188,  
                    InstanceId = [[Client1_416]],  
                    Class = [[Position]],  
                    z = 74.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_418]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.765625,  
                    x = 30739.71875,  
                    InstanceId = [[Client1_419]],  
                    Class = [[Position]],  
                    z = 67.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_421]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.8125,  
                    x = 30829.25,  
                    InstanceId = [[Client1_422]],  
                    Class = [[Position]],  
                    z = 65.375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BA]],  
              InstanceId = [[Client1_424]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_423]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_426]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.75,  
                    x = 30829.25,  
                    InstanceId = [[Client1_427]],  
                    Class = [[Position]],  
                    z = 65.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_429]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.578125,  
                    x = 30830,  
                    InstanceId = [[Client1_430]],  
                    Class = [[Position]],  
                    z = 70.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_432]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.15625,  
                    x = 30740.8125,  
                    InstanceId = [[Client1_433]],  
                    Class = [[Position]],  
                    z = 73.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_435]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.15625,  
                    x = 30739.92188,  
                    InstanceId = [[Client1_436]],  
                    Class = [[Position]],  
                    z = 68.71875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CA]],  
              InstanceId = [[Client1_438]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_437]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_440]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.5,  
                    x = 30740.07813,  
                    InstanceId = [[Client1_441]],  
                    Class = [[Position]],  
                    z = 74.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_443]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130,  
                    x = 30829.125,  
                    InstanceId = [[Client1_444]],  
                    Class = [[Position]],  
                    z = 70.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_446]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.234375,  
                    x = 30829.79688,  
                    InstanceId = [[Client1_447]],  
                    Class = [[Position]],  
                    z = 74.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_449]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.03125,  
                    x = 30741.46875,  
                    InstanceId = [[Client1_450]],  
                    Class = [[Position]],  
                    z = 77.421875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DA]],  
              InstanceId = [[Client1_452]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_451]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_454]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.6875,  
                    x = 30741.03125,  
                    InstanceId = [[Client1_455]],  
                    Class = [[Position]],  
                    z = 77.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_457]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.640625,  
                    x = 30740.14063,  
                    InstanceId = [[Client1_458]],  
                    Class = [[Position]],  
                    z = 76.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_460]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.640625,  
                    x = 30828.6875,  
                    InstanceId = [[Client1_461]],  
                    Class = [[Position]],  
                    z = 79.328125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_463]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2018.671875,  
                    x = 30829.45313,  
                    InstanceId = [[Client1_464]],  
                    Class = [[Position]],  
                    z = 75.3125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EA]],  
              InstanceId = [[Client1_466]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_465]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_468]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.34375,  
                    x = 30829.07813,  
                    InstanceId = [[Client1_469]],  
                    Class = [[Position]],  
                    z = 79.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_471]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.375,  
                    x = 30740.78125,  
                    InstanceId = [[Client1_472]],  
                    Class = [[Position]],  
                    z = 76.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_474]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.515625,  
                    x = 30739.84375,  
                    InstanceId = [[Client1_475]],  
                    Class = [[Position]],  
                    z = 71.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_477]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.21875,  
                    x = 30828.84375,  
                    InstanceId = [[Client1_478]],  
                    Class = [[Position]],  
                    z = 78.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EB]],  
              InstanceId = [[Client1_503]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_502]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_505]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.796875,  
                    x = 30738.17188,  
                    InstanceId = [[Client1_506]],  
                    Class = [[Position]],  
                    z = 76.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_508]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.109375,  
                    x = 30650.40625,  
                    InstanceId = [[Client1_509]],  
                    Class = [[Position]],  
                    z = 62.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_511]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.3125,  
                    x = 30649.20313,  
                    InstanceId = [[Client1_512]],  
                    Class = [[Position]],  
                    z = 67.09375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_514]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.46875,  
                    x = 30737.5625,  
                    InstanceId = [[Client1_515]],  
                    Class = [[Position]],  
                    z = 72.09375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EC]],  
              InstanceId = [[Client1_517]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_516]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_519]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.96875,  
                    x = 30647.57813,  
                    InstanceId = [[Client1_520]],  
                    Class = [[Position]],  
                    z = 61.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_522]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.125,  
                    x = 30559.28125,  
                    InstanceId = [[Client1_523]],  
                    Class = [[Position]],  
                    z = 58.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_525]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.90625,  
                    x = 30558.15625,  
                    InstanceId = [[Client1_526]],  
                    Class = [[Position]],  
                    z = 58.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_528]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.390625,  
                    x = 30647.32813,  
                    InstanceId = [[Client1_529]],  
                    Class = [[Position]],  
                    z = 67.25
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[ED]],  
              InstanceId = [[Client1_531]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_530]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_533]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1909.8125,  
                    x = 30556.90625,  
                    InstanceId = [[Client1_534]],  
                    Class = [[Position]],  
                    z = 58.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_536]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.15625,  
                    x = 30468.5625,  
                    InstanceId = [[Client1_537]],  
                    Class = [[Position]],  
                    z = 51.921875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_539]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.328125,  
                    x = 30467.29688,  
                    InstanceId = [[Client1_540]],  
                    Class = [[Position]],  
                    z = 53.171875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_542]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1801.328125,  
                    x = 30556.59375,  
                    InstanceId = [[Client1_543]],  
                    Class = [[Position]],  
                    z = 58.296875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[EE]],  
              InstanceId = [[Client1_545]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_544]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_547]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.109375,  
                    x = 30465.01563,  
                    InstanceId = [[Client1_548]],  
                    Class = [[Position]],  
                    z = 51.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_550]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1910.046875,  
                    x = 30376.75,  
                    InstanceId = [[Client1_551]],  
                    Class = [[Position]],  
                    z = 49.265625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_553]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1802.09375,  
                    x = 30376.14063,  
                    InstanceId = [[Client1_554]],  
                    Class = [[Position]],  
                    z = 45.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_556]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1805.40625,  
                    x = 30465.14063,  
                    InstanceId = [[Client1_557]],  
                    Class = [[Position]],  
                    z = 53.15625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DE]],  
              InstanceId = [[Client1_559]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_558]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_561]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.015625,  
                    x = 30376.25,  
                    InstanceId = [[Client1_562]],  
                    Class = [[Position]],  
                    z = 49.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_564]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.34375,  
                    x = 30465.09375,  
                    InstanceId = [[Client1_565]],  
                    Class = [[Position]],  
                    z = 51.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_567]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.125,  
                    x = 30466.14063,  
                    InstanceId = [[Client1_568]],  
                    Class = [[Position]],  
                    z = 55.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_570]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.4375,  
                    x = 30377.26563,  
                    InstanceId = [[Client1_571]],  
                    Class = [[Position]],  
                    z = 47.828125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CE]],  
              InstanceId = [[Client1_573]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_572]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_575]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.046875,  
                    x = 30376.5625,  
                    InstanceId = [[Client1_576]],  
                    Class = [[Position]],  
                    z = 47.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_578]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.546875,  
                    x = 30375.65625,  
                    InstanceId = [[Client1_579]],  
                    Class = [[Position]],  
                    z = 53.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_581]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.9375,  
                    x = 30464.59375,  
                    InstanceId = [[Client1_582]],  
                    Class = [[Position]],  
                    z = 55.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_584]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.6875,  
                    x = 30465.875,  
                    InstanceId = [[Client1_585]],  
                    Class = [[Position]],  
                    z = 55.53125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BE]],  
              InstanceId = [[Client1_587]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_586]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_589]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.1875,  
                    x = 30376.20313,  
                    InstanceId = [[Client1_590]],  
                    Class = [[Position]],  
                    z = 53.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_592]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2237.75,  
                    x = 30375.42188,  
                    InstanceId = [[Client1_593]],  
                    Class = [[Position]],  
                    z = 56.15625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_595]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.390625,  
                    x = 30463.85938,  
                    InstanceId = [[Client1_596]],  
                    Class = [[Position]],  
                    z = 66.796875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_598]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.875,  
                    x = 30464.98438,  
                    InstanceId = [[Client1_599]],  
                    Class = [[Position]],  
                    z = 54.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AE]],  
              InstanceId = [[Client1_601]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_600]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_603]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.453125,  
                    x = 30375.73438,  
                    InstanceId = [[Client1_604]],  
                    Class = [[Position]],  
                    z = 55.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_606]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2347.4375,  
                    x = 30376.375,  
                    InstanceId = [[Client1_607]],  
                    Class = [[Position]],  
                    z = 54.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_609]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.71875,  
                    x = 30463.10938,  
                    InstanceId = [[Client1_610]],  
                    Class = [[Position]],  
                    z = 65.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_612]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2242.859375,  
                    x = 30465.29688,  
                    InstanceId = [[Client1_613]],  
                    Class = [[Position]],  
                    z = 66.765625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AD]],  
              InstanceId = [[Client1_615]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_614]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_617]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.140625,  
                    x = 30468.14063,  
                    InstanceId = [[Client1_618]],  
                    Class = [[Position]],  
                    z = 66.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_620]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.578125,  
                    x = 30556.03125,  
                    InstanceId = [[Client1_621]],  
                    Class = [[Position]],  
                    z = 70.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_623]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2348.734375,  
                    x = 30556.5,  
                    InstanceId = [[Client1_624]],  
                    Class = [[Position]],  
                    z = 79.25
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_626]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.90625,  
                    x = 30467.78125,  
                    InstanceId = [[Client1_627]],  
                    Class = [[Position]],  
                    z = 65.96875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AC Spawn]],  
              InstanceId = [[Client1_629]],  
              Class = [[Region]],  
              Position = {
                y = -0.0625,  
                x = -0.09375,  
                InstanceId = [[Client1_628]],  
                Class = [[Position]],  
                z = -1.234375
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_631]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.015625,  
                    x = 30558.375,  
                    InstanceId = [[Client1_632]],  
                    Class = [[Position]],  
                    z = 69.6875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_634]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.828125,  
                    x = 30647.35938,  
                    InstanceId = [[Client1_635]],  
                    Class = [[Position]],  
                    z = 68.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_637]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.171875,  
                    x = 30647.92188,  
                    InstanceId = [[Client1_638]],  
                    Class = [[Position]],  
                    z = 78.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_640]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.453125,  
                    x = 30558.59375,  
                    InstanceId = [[Client1_641]],  
                    Class = [[Position]],  
                    z = 79.21875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[AB]],  
              InstanceId = [[Client1_643]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_642]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_645]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2241.171875,  
                    x = 30649.6875,  
                    InstanceId = [[Client1_646]],  
                    Class = [[Position]],  
                    z = 68.734375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_648]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2240.828125,  
                    x = 30738.1875,  
                    InstanceId = [[Client1_649]],  
                    Class = [[Position]],  
                    z = 67.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_651]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.5625,  
                    x = 30738.96875,  
                    InstanceId = [[Client1_652]],  
                    Class = [[Position]],  
                    z = 74.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_654]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.84375,  
                    x = 30649.59375,  
                    InstanceId = [[Client1_655]],  
                    Class = [[Position]],  
                    z = 78.53125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BB]],  
              InstanceId = [[Client1_657]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_656]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_659]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.578125,  
                    x = 30738.76563,  
                    InstanceId = [[Client1_660]],  
                    Class = [[Position]],  
                    z = 68.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_662]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.640625,  
                    x = 30738.73438,  
                    InstanceId = [[Client1_663]],  
                    Class = [[Position]],  
                    z = 73.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_665]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130.96875,  
                    x = 30649.625,  
                    InstanceId = [[Client1_666]],  
                    Class = [[Position]],  
                    z = 73.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_668]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.09375,  
                    x = 30649.57813,  
                    InstanceId = [[Client1_669]],  
                    Class = [[Position]],  
                    z = 68.90625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BC]],  
              InstanceId = [[Client1_671]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_670]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_673]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.234375,  
                    x = 30647.51563,  
                    InstanceId = [[Client1_674]],  
                    Class = [[Position]],  
                    z = 69.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_676]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.953125,  
                    x = 30558.64063,  
                    InstanceId = [[Client1_677]],  
                    Class = [[Position]],  
                    z = 69.578125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_679]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.828125,  
                    x = 30558.21875,  
                    InstanceId = [[Client1_680]],  
                    Class = [[Position]],  
                    z = 65.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_682]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2131.140625,  
                    x = 30647.20313,  
                    InstanceId = [[Client1_683]],  
                    Class = [[Position]],  
                    z = 73.15625
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[BD Clopper Hill]],  
              InstanceId = [[Client1_711]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_710]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_713]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2238.59375,  
                    x = 30556.73438,  
                    InstanceId = [[Client1_714]],  
                    Class = [[Position]],  
                    z = 69.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_716]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2239.109375,  
                    x = 30468.29688,  
                    InstanceId = [[Client1_717]],  
                    Class = [[Position]],  
                    z = 65.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_719]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2132.078125,  
                    x = 30467.54688,  
                    InstanceId = [[Client1_720]],  
                    Class = [[Position]],  
                    z = 54.515625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_722]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2130.984375,  
                    x = 30555.9375,  
                    InstanceId = [[Client1_723]],  
                    Class = [[Position]],  
                    z = 64.8125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CD]],  
              InstanceId = [[Client1_725]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_724]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_727]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.703125,  
                    x = 30467.48438,  
                    InstanceId = [[Client1_728]],  
                    Class = [[Position]],  
                    z = 54.140625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_730]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.84375,  
                    x = 30467.10938,  
                    InstanceId = [[Client1_731]],  
                    Class = [[Position]],  
                    z = 55.484375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_733]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.1875,  
                    x = 30556.4375,  
                    InstanceId = [[Client1_734]],  
                    Class = [[Position]],  
                    z = 59.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_736]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.640625,  
                    x = 30556.625,  
                    InstanceId = [[Client1_737]],  
                    Class = [[Position]],  
                    z = 64.875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CC]],  
              InstanceId = [[Client1_739]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_738]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_741]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2128.875,  
                    x = 30558.03125,  
                    InstanceId = [[Client1_742]],  
                    Class = [[Position]],  
                    z = 65.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_744]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.140625,  
                    x = 30647.15625,  
                    InstanceId = [[Client1_745]],  
                    Class = [[Position]],  
                    z = 73.421875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_747]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.171875,  
                    x = 30647.51563,  
                    InstanceId = [[Client1_748]],  
                    Class = [[Position]],  
                    z = 61.1875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_750]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.515625,  
                    x = 30558.78125,  
                    InstanceId = [[Client1_751]],  
                    Class = [[Position]],  
                    z = 59.984375
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[CB]],  
              InstanceId = [[Client1_753]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_752]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_755]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.046875,  
                    x = 30649.46875,  
                    InstanceId = [[Client1_756]],  
                    Class = [[Position]],  
                    z = 73.84375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_758]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2129.484375,  
                    x = 30737.10938,  
                    InstanceId = [[Client1_759]],  
                    Class = [[Position]],  
                    z = 74.640625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_761]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2021.5625,  
                    x = 30738.3125,  
                    InstanceId = [[Client1_762]],  
                    Class = [[Position]],  
                    z = 77.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_764]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2022.15625,  
                    x = 30650.51563,  
                    InstanceId = [[Client1_765]],  
                    Class = [[Position]],  
                    z = 61.5
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DB]],  
              InstanceId = [[Client1_793]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_792]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_795]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.921875,  
                    x = 30737.98438,  
                    InstanceId = [[Client1_796]],  
                    Class = [[Position]],  
                    z = 77.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_798]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.515625,  
                    x = 30738.35938,  
                    InstanceId = [[Client1_799]],  
                    Class = [[Position]],  
                    z = 76.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_801]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.875,  
                    x = 30651.09375,  
                    InstanceId = [[Client1_802]],  
                    Class = [[Position]],  
                    z = 62.75
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_804]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.609375,  
                    x = 30649.48438,  
                    InstanceId = [[Client1_805]],  
                    Class = [[Position]],  
                    z = 61.1875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DC]],  
              InstanceId = [[Client1_807]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_806]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_809]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.0625,  
                    x = 30648.07813,  
                    InstanceId = [[Client1_810]],  
                    Class = [[Position]],  
                    z = 60.984375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_812]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.734375,  
                    x = 30558.875,  
                    InstanceId = [[Client1_813]],  
                    Class = [[Position]],  
                    z = 59.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_815]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1912.171875,  
                    x = 30558.17188,  
                    InstanceId = [[Client1_816]],  
                    Class = [[Position]],  
                    z = 58.59375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_818]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.734375,  
                    x = 30647.5625,  
                    InstanceId = [[Client1_819]],  
                    Class = [[Position]],  
                    z = 62.046875
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[DD]],  
              InstanceId = [[Client1_821]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_820]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_823]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.703125,  
                    x = 30556.71875,  
                    InstanceId = [[Client1_824]],  
                    Class = [[Position]],  
                    z = 58.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_826]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1911.703125,  
                    x = 30469.03125,  
                    InstanceId = [[Client1_827]],  
                    Class = [[Position]],  
                    z = 52.359375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_829]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2018.671875,  
                    x = 30466.92188,  
                    InstanceId = [[Client1_830]],  
                    Class = [[Position]],  
                    z = 55.859375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_832]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2019.453125,  
                    x = 30556.23438,  
                    InstanceId = [[Client1_833]],  
                    Class = [[Position]],  
                    z = 59.453125
                  }
                }
              }
            },  
            {
              InheritPos = 1,  
              Name = [[XF Extra]],  
              InstanceId = [[Client1_1092]],  
              Class = [[Region]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_1091]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1094]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2349.125,  
                    x = 30374.1875,  
                    InstanceId = [[Client1_1095]],  
                    Class = [[Position]],  
                    z = 54.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1097]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2351.09375,  
                    x = 30280.79688,  
                    InstanceId = [[Client1_1098]],  
                    Class = [[Position]],  
                    z = 41.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1100]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2062.859375,  
                    x = 30290.78125,  
                    InstanceId = [[Client1_1101]],  
                    Class = [[Position]],  
                    z = 49.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_1103]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -1979.765625,  
                    x = 30372.90625,  
                    InstanceId = [[Client1_1104]],  
                    Class = [[Position]],  
                    z = 48.4375
                  }
                }
              }
            }
          },  
          InstanceId = [[Client1_26]]
        },  
        {
          InstanceId = [[Client1_1335]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Guard Group]],  
          Position = {
            y = 0,  
            x = 0,  
            InstanceId = [[Client1_1334]],  
            Class = [[Position]],  
            z = 0
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_1333]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1323]],  
              ActivitiesId = {
                [[Client1_1336]]
              },  
              HairType = 6700590,  
              TrouserColor = 1,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 6,  
              HandsModel = 6700078,  
              FeetColor = 1,  
              GabaritBreastSize = 14,  
              GabaritHeight = 0,  
              HairColor = 1,  
              EyesColor = 0,  
              TrouserModel = 6701102,  
              GabaritLegsWidth = 6,  
              HandsColor = 1,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1321]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                  {
                    Name = [[Seq1]],  
                    InstanceId = [[Client1_1336]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_1337]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_629]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 1,  
              MorphTarget4 = 4,  
              FeetModel = 6699566,  
              Angle = 1.1875,  
              Base = [[palette.entities.npcs.guards.f_guard_245]],  
              Sheet = [[ring_guard_melee_tank_slash_f3.creature]],  
              SheetClient = [[basic_fyros_male.creature]],  
              InheritPos = 1,  
              ArmModel = 6701614,  
              WeaponLeftHand = 0,  
              GabaritTorsoWidth = 3,  
              MorphTarget2 = 5,  
              JacketModel = 6702126,  
              WeaponRightHand = 6756910,  
              ArmColor = 1,  
              Name = [[Guard Leader]],  
              Position = {
                y = -2322.859375,  
                x = 30611.75,  
                InstanceId = [[Client1_1324]],  
                Class = [[Position]],  
                z = 79.921875
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1315]],  
              ActivitiesId = {
              },  
              HairType = 5621550,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 5,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1313]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.1875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              SheetClient = [[basic_fyros_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Position = {
                y = -2324.671875,  
                x = 30612,  
                InstanceId = [[Client1_1316]],  
                Class = [[Position]],  
                z = 79.921875
              },  
              Sex = 0,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1327]],  
              ActivitiesId = {
              },  
              HairType = 8238,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1325]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.328125,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              SheetClient = [[basic_zorai_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 3,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Position = {
                y = -2324.734375,  
                x = 30611.17188,  
                InstanceId = [[Client1_1328]],  
                Class = [[Position]],  
                z = 80
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1307]],  
              ActivitiesId = {
              },  
              HairType = 5166,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 5,  
              EyesColor = 3,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1305]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.1875,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              SheetClient = [[basic_matis_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Position = {
                y = -2324.34375,  
                x = 30610.40625,  
                InstanceId = [[Client1_1308]],  
                Class = [[Position]],  
                z = 80.046875
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            },  
            {
              MorphTarget8 = 2,  
              InstanceId = [[Client1_1331]],  
              ActivitiesId = {
              },  
              HairType = 5623086,  
              TrouserColor = 4,  
              MorphTarget5 = 2,  
              MorphTarget6 = 4,  
              GabaritArmsWidth = 0,  
              HandsModel = 6716462,  
              FeetColor = 4,  
              GabaritBreastSize = 14,  
              GabaritHeight = 14,  
              HairColor = 4,  
              EyesColor = 3,  
              TrouserModel = 6716974,  
              GabaritLegsWidth = 0,  
              HandsColor = 4,  
              MorphTarget1 = 3,  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1329]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[NpcCustom]],  
              JacketColor = 4,  
              MorphTarget4 = 4,  
              FeetModel = 6715950,  
              Angle = 1.328125,  
              Base = [[palette.entities.npcs.bandits.m_light_melee_220]],  
              Sheet = [[ring_light_melee_pierce_f3.creature]],  
              SheetClient = [[basic_tryker_female.creature]],  
              InheritPos = 1,  
              ArmModel = 6717486,  
              WeaponLeftHand = 6758446,  
              GabaritTorsoWidth = 0,  
              MorphTarget2 = 5,  
              JacketModel = 6717998,  
              WeaponRightHand = 6758446,  
              ArmColor = 4,  
              Name = [[Guard]],  
              Position = {
                y = -2323.65625,  
                x = 30610.03125,  
                InstanceId = [[Client1_1332]],  
                Class = [[Position]],  
                z = 80.0625
              },  
              Sex = 1,  
              MorphTarget7 = 4,  
              MorphTarget3 = 3,  
              Tattoo = 0
            }
          },  
          Cost = 0
        },  
        {
          Class = [[NpcGrpFeature]],  
          InstanceId = [[Client1_1520]],  
          Name = [[Clopper Group]],  
          Components = {
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1340]],  
              Base = [[palette.entities.creatures.ccbdc7]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1341]],  
                x = 30520.34375,  
                y = -2182.59375,  
                z = 78.40625
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1338]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client1_1521]],  
                    Name = [[Seq1]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client1_1522]],  
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_711]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
                [[Client1_1521]]
              },  
              InheritPos = 1,  
              Name = [[Clopperketh]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1360]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1361]],  
                x = 30510.3125,  
                y = -2190.90625,  
                z = 77.328125
              },  
              Angle = -2.5625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1358]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1364]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1365]],  
                x = 30508.6875,  
                y = -2184.796875,  
                z = 78.5
              },  
              Angle = 3.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1362]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1356]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1357]],  
                x = 30512.40625,  
                y = -2195.34375,  
                z = 76.21875
              },  
              Angle = -0.84375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1354]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1344]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1345]],  
                x = 30518.09375,  
                y = -2193.875,  
                z = 76.265625
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1342]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1348]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1349]],  
                x = 30525.21875,  
                y = -2192.65625,  
                z = 76.4375
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1346]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1352]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1353]],  
                x = 30531.10938,  
                y = -2191.21875,  
                z = 76.9375
              },  
              Angle = -1.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1350]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1388]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1389]],  
                x = 30531.625,  
                y = -2186.5625,  
                z = 77.90625
              },  
              Angle = 0.15625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1386]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1384]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1385]],  
                x = 30529.96875,  
                y = -2180.640625,  
                z = 78.96875
              },  
              Angle = 0.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1382]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1380]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1381]],  
                x = 30524.04688,  
                y = -2175.859375,  
                z = 78.1875
              },  
              Angle = 0.046875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1378]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1376]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1377]],  
                x = 30516.875,  
                y = -2175.796875,  
                z = 78.078125
              },  
              Angle = 1.71875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1374]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1372]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1373]],  
                x = 30510.9375,  
                y = -2177.21875,  
                z = 78.421875
              },  
              Angle = 1.640625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1370]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1368]],  
              Base = [[palette.entities.creatures.ccbdc4]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1369]],  
                x = 30507.21875,  
                y = -2179.5,  
                z = 78.375
              },  
              Angle = 2.8125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1366]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Menacing Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1504]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1505]],  
                x = 30496.9375,  
                y = -2188.65625,  
                z = 73.765625
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1502]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1500]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1501]],  
                x = 30497.14063,  
                y = -2184.3125,  
                z = 73.953125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1498]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1496]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1497]],  
                x = 30498.25,  
                y = -2180.046875,  
                z = 74.03125
              },  
              Angle = -3.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1494]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1492]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1493]],  
                x = 30500.5625,  
                y = -2174.125,  
                z = 73.296875
              },  
              Angle = 2.90625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1490]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1488]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1489]],  
                x = 30502.875,  
                y = -2170.359375,  
                z = 73.40625
              },  
              Angle = 3.03125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1486]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1484]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1485]],  
                x = 30510.85938,  
                y = -2168.46875,  
                z = 75.015625
              },  
              Angle = 1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1482]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1480]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1481]],  
                x = 30514.40625,  
                y = -2168.703125,  
                z = 75.53125
              },  
              Angle = 1.765625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1478]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1476]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1477]],  
                x = 30519.96875,  
                y = -2168.125,  
                z = 75.578125
              },  
              Angle = 2.125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1474]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1472]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1473]],  
                x = 30525.03125,  
                y = -2167.234375,  
                z = 75.625
              },  
              Angle = 1.375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1470]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1468]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1469]],  
                x = 30530.0625,  
                y = -2166.4375,  
                z = 75.765625
              },  
              Angle = 1.296875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1466]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1516]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1517]],  
                x = 30498.875,  
                y = -2200.9375,  
                z = 72.875
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1514]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1464]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1465]],  
                x = 30533.76563,  
                y = -2166.734375,  
                z = 75.234375
              },  
              Angle = 1.3125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1462]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1420]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1421]],  
                x = 30533.40625,  
                y = -2205.609375,  
                z = 76.5
              },  
              Angle = -1.0625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1418]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1432]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1433]],  
                x = 30538.48438,  
                y = -2195.203125,  
                z = 74
              },  
              Angle = 0.015625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1430]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1424]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1425]],  
                x = 30536.40625,  
                y = -2202.65625,  
                z = 74.71875
              },  
              Angle = -0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1422]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1416]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1417]],  
                x = 30530.8125,  
                y = -2208.578125,  
                z = 78.640625
              },  
              Angle = -0.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1414]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1452]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1453]],  
                x = 30542.17188,  
                y = -2175.734375,  
                z = 73.390625
              },  
              Angle = -0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1450]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1412]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1413]],  
                x = 30526.79688,  
                y = -2209.984375,  
                z = 79.328125
              },  
              Angle = -0.671875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1410]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1456]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1457]],  
                x = 30539.60938,  
                y = -2171.53125,  
                z = 74.078125
              },  
              Angle = 1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1454]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1436]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1437]],  
                x = 30539.15625,  
                y = -2191.15625,  
                z = 74.296875
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1434]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1428]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1429]],  
                x = 30537.45313,  
                y = -2199.234375,  
                z = 74.09375
              },  
              Angle = -0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1426]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1460]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1461]],  
                x = 30536.89063,  
                y = -2168.84375,  
                z = 74.6875
              },  
              Angle = 1.078125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1458]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1404]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1405]],  
                x = 30518.8125,  
                y = -2209.734375,  
                z = 77.8125
              },  
              Angle = -1.65625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1402]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1448]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1449]],  
                x = 30541.54688,  
                y = -2179.53125,  
                z = 74.15625
              },  
              Angle = -0.109375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1446]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1396]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1397]],  
                x = 30508.53125,  
                y = -2206.328125,  
                z = 74.640625
              },  
              Angle = -2.609375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1394]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1440]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1441]],  
                x = 30539.95313,  
                y = -2188.359375,  
                z = 74.390625
              },  
              Angle = -0.28125,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1438]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1444]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1445]],  
                x = 30540.98438,  
                y = -2183.515625,  
                z = 74.390625
              },  
              Angle = 0.265625,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1442]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1508]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1509]],  
                x = 30497.03125,  
                y = -2191.75,  
                z = 73.46875
              },  
              Angle = 3,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1506]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1408]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1409]],  
                x = 30521.4375,  
                y = -2210.265625,  
                z = 78.5625
              },  
              Angle = -2.34375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1406]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1400]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1401]],  
                x = 30515.5,  
                y = -2208.53125,  
                z = 76.578125
              },  
              Angle = -1.171875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1398]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1512]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1513]],  
                x = 30497.07813,  
                y = -2196.71875,  
                z = 72.71875
              },  
              Angle = -2.96875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1510]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client1_1392]],  
              Base = [[palette.entities.creatures.ccbdc3]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_1393]],  
                x = 30505.34375,  
                y = -2204.734375,  
                z = 74.046875
              },  
              Angle = -1.984375,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client1_1390]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dangerous Clopper]]
            }
          },  
          ActivitiesId = {
          },  
          Cost = 0,  
          InheritPos = 1,  
          Position = {
            Class = [[Position]],  
            InstanceId = [[Client1_1519]],  
            x = 0,  
            y = 0,  
            z = 0
          },  
          Behavior = {
            Class = [[Behavior]],  
            InstanceId = [[Client1_1518]],  
            Type = [[]],  
            ZoneId = [[]],  
            Activities = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Reactions = {
            }
          }
        }
      },  
      Counters = {
      },  
      Version = 1,  
      ManualWeather = 0,  
      InstanceId = [[Client1_25]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_23]],  
    Texts = {
    }
  }
}