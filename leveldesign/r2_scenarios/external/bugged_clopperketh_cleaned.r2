scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_22]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 8,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    Title = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    ShortDescription = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_24]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.1]],  
  Versions = {
    Scenario = 0,  
    Act = 1,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    Position = 0,  
    TextManager = 0,  
    DefaultFeature = 0
  },  
  Acts = {
    {
      Cost = 1,  
      Class = [[Act]],  
      WeatherValue = 0,  
      InstanceId = [[Client1_25]],  
      ActivitiesIds = {
        [[Client1_1336]]
      },  
      Title = [[]],  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_1340]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_1338]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Clopperketh]],  
              Position = {
                y = -2318.6875,  
                x = 30656.23438,  
                InstanceId = [[Client1_1341]],  
                Class = [[Position]],  
                z = 75.53125
              },  
              Angle = -1.265625,  
              Base = [[palette.entities.creatures.ccbdc7]],  
              ActivitiesId = {
              }
            }
          },  
          InstanceId = [[Client1_26]]
        }
      },  
      Counters = {
      },  
      ManualWeather = 0,  
      Version = 1,  
      Events = {
      }
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    Texts = {
    },  
    InstanceId = [[Client1_23]]
  }
}