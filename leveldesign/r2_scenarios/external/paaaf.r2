scenario = {
  AccessRules = [[]],  
  Description = {
    InstanceId = [[Client1_12]],  
    Class = [[MapDescription]],  
    LevelId = 0,  
    LocationId = 19,  
    OptimalNumberOfPlayer = 10,  
    EntryPointId = 0,  
    ShortDescription = [[]],  
    RuleId = 0,  
    MaxEntities = 50,  
    Title = [[]],  
    MaxPlayers = 100
  },  
  InstanceId = [[Client1_14]],  
  Class = [[Scenario]],  
  VersionName = [[0.0.4]],  
  Versions = {
    ActivitySequence = 0,  
    Scenario = 1,  
    Act = 3,  
    Behavior = 0,  
    Npc = 0,  
    MapDescription = 0,  
    LogicEntityAction = 0,  
    TextManagerEntry = 0,  
    TextManager = 0,  
    ConditionType = 0,  
    DefaultFeature = 0,  
    ConditionStep = 0,  
    ChatAction = 0,  
    ActivityStep = 1,  
    Region = 0,  
    Road = 0,  
    LogicEntityReaction = 0,  
    ActionStep = 0,  
    EventType = 0,  
    RegionVertex = 0,  
    NpcGrpFeature = 0,  
    ChatStep = 0,  
    Position = 0,  
    ChatSequence = 0,  
    ActionType = 0,  
    LogicEntityBehavior = 0,  
    WayPoint = 0
  },  
  Acts = {
    {
      Cost = 30,  
      Behavior = {
        InstanceId = [[Client1_15]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Permanent]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Permanent]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_16]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InheritPos = 1,  
              Name = [[Route 3]],  
              InstanceId = [[Client1_77]],  
              Class = [[Road]],  
              Position = {
                y = 0,  
                x = 0,  
                InstanceId = [[Client1_76]],  
                Class = [[Position]],  
                z = 0
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_79]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2112.0625,  
                    x = 25202.375,  
                    InstanceId = [[Client1_80]],  
                    Class = [[Position]],  
                    z = -16.0625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_82]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2054.21875,  
                    x = 25205.3125,  
                    InstanceId = [[Client1_83]],  
                    Class = [[Position]],  
                    z = -20.875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_85]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2010.84375,  
                    x = 25244.125,  
                    InstanceId = [[Client1_86]],  
                    Class = [[Position]],  
                    z = -17.296875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_88]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -1996.84375,  
                    x = 25322.14063,  
                    InstanceId = [[Client1_89]],  
                    Class = [[Position]],  
                    z = -14.53125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_91]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2055.875,  
                    x = 25302.04688,  
                    InstanceId = [[Client1_92]],  
                    Class = [[Position]],  
                    z = -17.125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_94]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2071.515625,  
                    x = 25369.40625,  
                    InstanceId = [[Client1_95]],  
                    Class = [[Position]],  
                    z = -17.78125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_97]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2119.265625,  
                    x = 25356.8125,  
                    InstanceId = [[Client1_98]],  
                    Class = [[Position]],  
                    z = -16.234375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_100]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2152.359375,  
                    x = 25416.03125,  
                    InstanceId = [[Client1_101]],  
                    Class = [[Position]],  
                    z = -14.3125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_103]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2169.328125,  
                    x = 25410.71875,  
                    InstanceId = [[Client1_104]],  
                    Class = [[Position]],  
                    z = -11.953125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_106]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2159.796875,  
                    x = 25431.79688,  
                    InstanceId = [[Client1_107]],  
                    Class = [[Position]],  
                    z = -16.65625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_109]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2187.015625,  
                    x = 25518.5625,  
                    InstanceId = [[Client1_110]],  
                    Class = [[Position]],  
                    z = -16.109375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_112]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2251.046875,  
                    x = 25521.3125,  
                    InstanceId = [[Client1_113]],  
                    Class = [[Position]],  
                    z = -19.625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_115]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2271.953125,  
                    x = 25576.57813,  
                    InstanceId = [[Client1_116]],  
                    Class = [[Position]],  
                    z = -20.28125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_118]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2331.90625,  
                    x = 25560.875,  
                    InstanceId = [[Client1_119]],  
                    Class = [[Position]],  
                    z = -16.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_121]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2311.640625,  
                    x = 25639.5625,  
                    InstanceId = [[Client1_122]],  
                    Class = [[Position]],  
                    z = -19.390625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_124]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2334.171875,  
                    x = 25673.67188,  
                    InstanceId = [[Client1_125]],  
                    Class = [[Position]],  
                    z = -20.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_127]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2365.171875,  
                    x = 25683.07813,  
                    InstanceId = [[Client1_128]],  
                    Class = [[Position]],  
                    z = -20.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_130]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2359.1875,  
                    x = 25712.26563,  
                    InstanceId = [[Client1_131]],  
                    Class = [[Position]],  
                    z = -19.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_133]],  
                  Class = [[WayPoint]],  
                  Position = {
                    y = -2321.671875,  
                    x = 25721.76563,  
                    InstanceId = [[Client1_134]],  
                    Class = [[Position]],  
                    z = -19.28125
                  }
                }
              }
            },  
            {
              InstanceId = [[Client1_157]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_155]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 1]],  
              Position = {
                y = -2099.125,  
                x = 25202.375,  
                InstanceId = [[Client1_158]],  
                Class = [[Position]],  
                z = -17.921875
              },  
              Angle = -1.703125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_161]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_159]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 2]],  
              Position = {
                y = -2065.0625,  
                x = 25209.54688,  
                InstanceId = [[Client1_162]],  
                Class = [[Position]],  
                z = -19.984375
              },  
              Angle = -1.703125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_165]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_163]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 1]],  
              Position = {
                y = -2073.125,  
                x = 25194.98438,  
                InstanceId = [[Client1_166]],  
                Class = [[Position]],  
                z = -21.25
              },  
              Angle = -1.703125,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_169]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_167]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 2]],  
              Position = {
                y = -2016.84375,  
                x = 25237.04688,  
                InstanceId = [[Client1_170]],  
                Class = [[Position]],  
                z = -17.390625
              },  
              Angle = -2.296875,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_173]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_171]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 3]],  
              Position = {
                y = -2004.0625,  
                x = 25286.45313,  
                InstanceId = [[Client1_174]],  
                Class = [[Position]],  
                z = -18.34375
              },  
              Angle = -2.828125,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_3]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_1]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 3]],  
              Position = {
                y = -2108.140625,  
                x = 25202.95313,  
                InstanceId = [[Client2_4]],  
                Class = [[Position]],  
                z = -16.40625
              },  
              Angle = 3.046875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_7]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_5]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 4]],  
              Position = {
                y = -2013.3125,  
                x = 25258.48438,  
                InstanceId = [[Client2_8]],  
                Class = [[Position]],  
                z = -16.28125
              },  
              Angle = -2.671875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_11]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_9]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 5]],  
              Position = {
                y = -1978.046875,  
                x = 25306.8125,  
                InstanceId = [[Client2_12]],  
                Class = [[Position]],  
                z = -14.1875
              },  
              Angle = -1.484375,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_19]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_17]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 6]],  
              Position = {
                y = -2058.921875,  
                x = 25314.73438,  
                InstanceId = [[Client2_20]],  
                Class = [[Position]],  
                z = -14.328125
              },  
              Angle = -3.125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_23]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_21]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 7]],  
              Position = {
                y = -2062.75,  
                x = 25319,  
                InstanceId = [[Client2_24]],  
                Class = [[Position]],  
                z = -13.828125
              },  
              Angle = -3.125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_31]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_29]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 9]],  
              Position = {
                y = -2109.265625,  
                x = 25337.21875,  
                InstanceId = [[Client2_32]],  
                Class = [[Position]],  
                z = -16.421875
              },  
              Angle = 0.515625,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_35]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_33]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 10]],  
              Position = {
                y = -2154.40625,  
                x = 25415.14063,  
                InstanceId = [[Client2_36]],  
                Class = [[Position]],  
                z = -13.875
              },  
              Angle = 2.671875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_39]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_37]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 11]],  
              Position = {
                y = -2164.5625,  
                x = 25515.6875,  
                InstanceId = [[Client2_40]],  
                Class = [[Position]],  
                z = -16.765625
              },  
              Angle = -2.046875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_43]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_41]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 12]],  
              Position = {
                y = -2219.203125,  
                x = 25543.40625,  
                InstanceId = [[Client2_44]],  
                Class = [[Position]],  
                z = -17.15625
              },  
              Angle = 1.8125,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_47]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_45]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 13]],  
              Position = {
                y = -2266.109375,  
                x = 25558.84375,  
                InstanceId = [[Client2_48]],  
                Class = [[Position]],  
                z = -18.828125
              },  
              Angle = 2.875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_55]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_53]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 4 2]],  
              Position = {
                y = -2267.796875,  
                x = 25558.125,  
                InstanceId = [[Client2_56]],  
                Class = [[Position]],  
                z = -19
              },  
              Angle = 2.875,  
              Base = [[palette.entities.botobjects.tomb_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_59]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_57]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 4]],  
              Position = {
                y = -2333.953125,  
                x = 25555.04688,  
                InstanceId = [[Client2_60]],  
                Class = [[Position]],  
                z = -16.296875
              },  
              Angle = 1.28125,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_67]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_65]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 2 5]],  
              Position = {
                y = -2333.578125,  
                x = 25671.26563,  
                InstanceId = [[Client2_68]],  
                Class = [[Position]],  
                z = -19.875
              },  
              Angle = 2.53125,  
              Base = [[palette.entities.botobjects.tomb_2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_71]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_69]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 14]],  
              Position = {
                y = -2356.140625,  
                x = 25713.29688,  
                InstanceId = [[Client2_72]],  
                Class = [[Position]],  
                z = -18.71875
              },  
              Angle = 1.171875,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_75]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_73]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[tomb stone 1 15]],  
              Position = {
                y = -2363.75,  
                x = 25686.34375,  
                InstanceId = [[Client2_76]],  
                Class = [[Position]],  
                z = -20.40625
              },  
              Angle = 0.140625,  
              Base = [[palette.entities.botobjects.tomb_1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_79]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_77]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[giant skull 1]],  
              Position = {
                y = -2351,  
                x = 25530.51563,  
                InstanceId = [[Client2_80]],  
                Class = [[Position]],  
                z = -16.859375
              },  
              Angle = 1,  
              Base = [[palette.entities.botobjects.giant_skull]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_83]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_81]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[house ruin 1]],  
              Position = {
                y = -2061.734375,  
                x = 25516.53125,  
                InstanceId = [[Client2_84]],  
                Class = [[Position]],  
                z = -15.21875
              },  
              Angle = 2.125,  
              Base = [[palette.entities.botobjects.house_ruin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_87]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_85]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[wall ruin 1 1]],  
              Position = {
                y = -2162.3125,  
                x = 25526.70313,  
                InstanceId = [[Client2_88]],  
                Class = [[Position]],  
                z = -17.609375
              },  
              Angle = 1.59375,  
              Base = [[palette.entities.botobjects.ruin_wall]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_91]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_89]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[kitin totem 1]],  
              Position = {
                y = -2132.390625,  
                x = 25403.625,  
                InstanceId = [[Client2_92]],  
                Class = [[Position]],  
                z = -14.765625
              },  
              Angle = -0.46875,  
              Base = [[palette.entities.botobjects.totem_kitin]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_95]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_93]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pachyderm totem 1]],  
              Position = {
                y = -2106.421875,  
                x = 25327.10938,  
                InstanceId = [[Client2_96]],  
                Class = [[Position]],  
                z = -15.640625
              },  
              Angle = -0.015625,  
              Base = [[palette.entities.botobjects.totem_pachyderm]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_99]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_97]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[runic circle 1]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_100]],  
                x = 25356.67188,  
                y = -1987.765625,  
                z = -14.875
              },  
              Angle = -0.5625,  
              Base = [[palette.entities.botobjects.runic_circle]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_107]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_105]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo II 1]],  
              Position = {
                y = -2045,  
                x = 25310.3125,  
                InstanceId = [[Client2_108]],  
                Class = [[Position]],  
                z = -16.5625
              },  
              Angle = 2.5,  
              Base = [[palette.entities.botobjects.tr_s2_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_111]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_109]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[bamboo I 1]],  
              Position = {
                y = -2012.234375,  
                x = 25312.73438,  
                InstanceId = [[Client2_112]],  
                Class = [[Position]],  
                z = -17.078125
              },  
              Angle = -1.984375,  
              Base = [[palette.entities.botobjects.tr_s1_bamboo_a]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_115]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_113]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 4 1]],  
              Position = {
                y = -2021,  
                x = 25257.01563,  
                InstanceId = [[Client2_116]],  
                Class = [[Position]],  
                z = -16.390625
              },  
              Angle = 0.984375,  
              Base = [[palette.entities.botobjects.pack_4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client2_119]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client2_117]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[pack 3 1]],  
              Position = {
                y = -2021.625,  
                x = 25261.39063,  
                InstanceId = [[Client2_120]],  
                Class = [[Position]],  
                z = -16.0625
              },  
              Angle = 0.984375,  
              Base = [[palette.entities.botobjects.pack_3]],  
              ActivitiesId = {
              }
            },  
            {
              InheritPos = 1,  
              Name = [[Place 2]],  
              InstanceId = [[Client1_823]],  
              Class = [[Region]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client1_822]],  
                x = -0.96875,  
                y = 1.53125,  
                z = -0.28125
              },  
              Points = {
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_825]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2301.171875,  
                    x = 25658.95313,  
                    InstanceId = [[Client1_826]],  
                    Class = [[Position]],  
                    z = -18.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_828]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2296.828125,  
                    x = 25661.10938,  
                    InstanceId = [[Client1_829]],  
                    Class = [[Position]],  
                    z = -17.5625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_831]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2287,  
                    x = 25698.89063,  
                    InstanceId = [[Client1_832]],  
                    Class = [[Position]],  
                    z = -19.4375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_834]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2289.34375,  
                    x = 25709.95313,  
                    InstanceId = [[Client1_835]],  
                    Class = [[Position]],  
                    z = -18.8125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_837]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2302.546875,  
                    x = 25723.10938,  
                    InstanceId = [[Client1_838]],  
                    Class = [[Position]],  
                    z = -18.46875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_840]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2311.71875,  
                    x = 25726.65625,  
                    InstanceId = [[Client1_841]],  
                    Class = [[Position]],  
                    z = -17.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_843]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2331.734375,  
                    x = 25724.8125,  
                    InstanceId = [[Client1_844]],  
                    Class = [[Position]],  
                    z = -17.046875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_846]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2356.71875,  
                    x = 25725.5,  
                    InstanceId = [[Client1_847]],  
                    Class = [[Position]],  
                    z = -18.015625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_849]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2369.625,  
                    x = 25724.60938,  
                    InstanceId = [[Client1_850]],  
                    Class = [[Position]],  
                    z = -18.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_852]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2366.59375,  
                    x = 25707.28125,  
                    InstanceId = [[Client1_853]],  
                    Class = [[Position]],  
                    z = -18.34375
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_855]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2364.03125,  
                    x = 25671.23438,  
                    InstanceId = [[Client1_856]],  
                    Class = [[Position]],  
                    z = -19.453125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_858]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2355.953125,  
                    x = 25704.95313,  
                    InstanceId = [[Client1_859]],  
                    Class = [[Position]],  
                    z = -19.21875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_861]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2340.3125,  
                    x = 25715.96875,  
                    InstanceId = [[Client1_862]],  
                    Class = [[Position]],  
                    z = -19.828125
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_864]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2317.21875,  
                    x = 25718.01563,  
                    InstanceId = [[Client1_865]],  
                    Class = [[Position]],  
                    z = -18.765625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_867]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2297.734375,  
                    x = 25716.85938,  
                    InstanceId = [[Client1_868]],  
                    Class = [[Position]],  
                    z = -18.71875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_870]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2293.1875,  
                    x = 25712.23438,  
                    InstanceId = [[Client1_871]],  
                    Class = [[Position]],  
                    z = -18.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_873]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2290.640625,  
                    x = 25696.71875,  
                    InstanceId = [[Client1_874]],  
                    Class = [[Position]],  
                    z = -18.96875
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_876]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2295.359375,  
                    x = 25677.70313,  
                    InstanceId = [[Client1_877]],  
                    Class = [[Position]],  
                    z = -18.40625
                  }
                },  
                {
                  InheritPos = 1,  
                  InstanceId = [[Client1_879]],  
                  Class = [[RegionVertex]],  
                  Position = {
                    y = -2300.625,  
                    x = 25669.78125,  
                    InstanceId = [[Client1_880]],  
                    Class = [[Position]],  
                    z = -16.8125
                  }
                }
              }
            },  
            {
              Class = [[Region]],  
              InstanceId = [[Client2_165]],  
              Name = [[Place 3]],  
              Points = {
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_167]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_168]],  
                    x = 25375.625,  
                    y = -1968.9375,  
                    z = -14.5
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_170]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_171]],  
                    x = 25393.95313,  
                    y = -1970.46875,  
                    z = -17.03125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_173]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_174]],  
                    x = 25414.98438,  
                    y = -1957.0625,  
                    z = -20.0625
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_176]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_177]],  
                    x = 25392.3125,  
                    y = -1947.359375,  
                    z = -19.46875
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_179]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_180]],  
                    x = 25375.64063,  
                    y = -1958.375,  
                    z = -16.703125
                  },  
                  InheritPos = 1
                },  
                {
                  Class = [[RegionVertex]],  
                  InstanceId = [[Client2_182]],  
                  Position = {
                    Class = [[Position]],  
                    InstanceId = [[Client2_183]],  
                    x = 25375.60938,  
                    y = -1968.90625,  
                    z = -14.515625
                  },  
                  InheritPos = 1
                }
              },  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_164]],  
                x = 0,  
                y = 0,  
                z = 0
              },  
              InheritPos = 1
            }
          },  
          InstanceId = [[Client1_18]]
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_17]]
    },  
    {
      Cost = 15,  
      Behavior = {
        InstanceId = [[Client1_19]],  
        Class = [[LogicEntityBehavior]],  
        Actions = {
        },  
        Reactions = {
        }
      },  
      Class = [[Act]],  
      Season = 0,  
      ActivitiesIds = {
      },  
      Events = {
      },  
      Title = [[Act 1]],  
      WeatherValue = 0,  
      InheritPos = 1,  
      Name = [[Act 1]],  
      Version = 3,  
      Position = {
        y = 0,  
        x = 0,  
        InstanceId = [[Client1_20]],  
        Class = [[Position]],  
        z = 0
      },  
      Counters = {
      },  
      Features = {
        {
          Class = [[DefaultFeature]],  
          Components = {
            {
              InstanceId = [[Client1_25]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_23]],  
                Class = [[Behavior]],  
                Reactions = {
                  {
                    LogicEntityAction = [[Client1_147]],  
                    Name = [[]],  
                    InstanceId = [[Client1_150]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client1_149]]
                  },  
                  {
                    LogicEntityAction = [[Client2_150]],  
                    Name = [[]],  
                    InstanceId = [[Client2_148]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_152]]
                  },  
                  {
                    LogicEntityAction = [[Client2_155]],  
                    Name = [[]],  
                    InstanceId = [[Client2_153]],  
                    Class = [[LogicEntityReaction]],  
                    ActionStep = [[Client2_157]]
                  }
                },  
                ChatSequences = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_135]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client1_136]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_137]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_138]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_139]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_140]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_141]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client1_142]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client1_143]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client1_145]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_125]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client2_126]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_127]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_128]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_129]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_130]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_131]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_132]],  
                    Repeating = 1,  
                    Class = [[ChatSequence]],  
                    Components = {
                      {
                        Time = 3,  
                        InstanceId = [[Client2_133]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_134]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_135]]
                          }
                        },  
                        Name = [[]]
                      },  
                      {
                        Time = 3,  
                        InstanceId = [[Client2_137]],  
                        Class = [[ChatStep]],  
                        Actions = {
                          {
                            Emote = [[]],  
                            InstanceId = [[Client2_138]],  
                            Who = [[Client1_25]],  
                            Class = [[ChatAction]],  
                            Facing = r2.RefId([[]]),  
                            Says = [[Client2_139]]
                          }
                        },  
                        Name = [[]]
                      }
                    }
                  }
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_147]],  
                    Conditions = {
                    },  
                    Class = [[LogicEntityAction]],  
                    Event = {
                      Type = [[end of activity step]],  
                      InstanceId = [[Client1_146]],  
                      Value = r2.RefId([[Client1_152]]),  
                      Class = [[EventType]]
                    },  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client1_148]],  
                          Value = r2.RefId([[Client1_153]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client1_149]],  
                        Entity = r2.RefId([[Client1_25]]),  
                        Class = [[ActionStep]]
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_151]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[10]],  
                        InstanceId = [[Client1_152]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client1_135]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_153]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[1200]],  
                        InstanceId = [[Client1_154]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_77]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Follow Route]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_140]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_141]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_125]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_142]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[Non Repeating]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client2_143]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[Client2_132]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Stand Still]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[voice of Jena]],  
              Position = {
                y = -2116.625,  
                x = 25203.34375,  
                InstanceId = [[Client1_26]],  
                Class = [[Position]],  
                z = -15.5
              },  
              Angle = -1.234375,  
              Base = [[palette.entities.npcs.karavan.karavan_emissary_f_b]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_3]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_1]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dreaded Kincher]],  
              Position = {
                y = -2065.296875,  
                x = 25327.35938,  
                InstanceId = [[Client3_4]],  
                Class = [[Position]],  
                z = -14.0625
              },  
              Angle = 2.78125,  
              Base = [[palette.entities.creatures.ckddd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_7]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_5]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dreaded Kincher]],  
              Position = {
                y = -2057.71875,  
                x = 25333.07813,  
                InstanceId = [[Client3_8]],  
                Class = [[Position]],  
                z = -15.453125
              },  
              Angle = 2.78125,  
              Base = [[palette.entities.creatures.ckddd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_11]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_9]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dreaded Kincher]],  
              Position = {
                y = -2062.03125,  
                x = 25343.375,  
                InstanceId = [[Client3_12]],  
                Class = [[Position]],  
                z = -15.15625
              },  
              Angle = 2.78125,  
              Base = [[palette.entities.creatures.ckddd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_15]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_13]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Dreaded Kincher]],  
              Position = {
                y = -2059.359375,  
                x = 25324.10938,  
                InstanceId = [[Client3_16]],  
                Class = [[Position]],  
                z = -14.5625
              },  
              Angle = 2.78125,  
              Base = [[palette.entities.creatures.ckddd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_23]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_21]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Seasoned Cute]],  
              Position = {
                y = -2186.375,  
                x = 25461.09375,  
                InstanceId = [[Client3_24]],  
                Class = [[Position]],  
                z = -19.03125
              },  
              Angle = 1.453125,  
              Base = [[palette.entities.creatures.cdald2]],  
              ActivitiesId = {
              }
            },  
            {
              Class = [[Npc]],  
              InstanceId = [[Client2_162]],  
              Base = [[palette.entities.creatures.ccfld5]],  
              Position = {
                Class = [[Position]],  
                InstanceId = [[Client2_163]],  
                x = 25382.6875,  
                y = -1963.015625,  
                z = -16.125
              },  
              Angle = -2.875,  
              Behavior = {
                Class = [[Behavior]],  
                InstanceId = [[Client2_160]],  
                Type = [[]],  
                ZoneId = [[]],  
                Activities = {
                  {
                    Class = [[ActivitySequence]],  
                    InstanceId = [[Client2_184]],  
                    Name = [[]],  
                    Repeating = 1,  
                    Components = {
                      {
                        Class = [[ActivityStep]],  
                        InstanceId = [[Client2_185]],  
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        EventsIds = {
                        },  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client2_165]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Reactions = {
                }
              },  
              ActivitiesId = {
              },  
              InheritPos = 1,  
              Name = [[Dgambi]]
            }
          },  
          InstanceId = [[Client1_22]]
        },  
        {
          InstanceId = [[Client1_738]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 1]],  
          Position = {
            y = -22.015625,  
            x = 23.578125,  
            InstanceId = [[Client1_737]],  
            Class = [[Position]],  
            z = -4.5625
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client1_736]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client1_722]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_720]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_150]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_149]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_151]],  
                          Value = r2.RefId([[Client2_140]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_152]],  
                        Entity = r2.RefId([[Client1_25]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                    }
                  },  
                  {
                    Name = [[]],  
                    InstanceId = [[Client2_155]],  
                    Event = {
                      Type = [[death]],  
                      InstanceId = [[Client2_154]],  
                      Value = r2.RefId([[]]),  
                      Class = [[EventType]]
                    },  
                    Class = [[LogicEntityAction]],  
                    Actions = {
                      {
                        Action = {
                          Type = [[begin activity sequence]],  
                          InstanceId = [[Client2_156]],  
                          Value = r2.RefId([[Client2_142]]),  
                          Class = [[ActionType]]
                        },  
                        InstanceId = [[Client2_157]],  
                        Entity = r2.RefId([[Client1_25]]),  
                        Class = [[ActionStep]]
                      }
                    },  
                    Conditions = {
                      {
                        InstanceId = [[Client2_159]],  
                        Entity = r2.RefId([[Client1_25]]),  
                        Class = [[ConditionStep]],  
                        Condition = {
                          Type = [[is in activity sequence]],  
                          InstanceId = [[Client2_158]],  
                          Value = r2.RefId([[Client1_153]]),  
                          Class = [[ConditionType]]
                        }
                      }
                    }
                  }
                },  
                Activities = {
                  {
                    Name = [[]],  
                    InstanceId = [[Client1_753]],  
                    Repeating = 1,  
                    Class = [[ActivitySequence]],  
                    Components = {
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_754]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[]],  
                        InstanceId = [[Client1_817]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[]]),  
                        Name = [[]],  
                        TimeLimit = [[No Limit]],  
                        Activity = [[Stand Still]]
                      },  
                      {
                        Type = [[None]],  
                        TimeLimitValue = [[20]],  
                        InstanceId = [[Client1_881]],  
                        EventsIds = {
                        },  
                        Class = [[ActivityStep]],  
                        Chat = r2.RefId([[]]),  
                        ActivityZoneId = r2.RefId([[Client1_823]]),  
                        Name = [[]],  
                        TimeLimit = [[Few Sec]],  
                        Activity = [[Wander]]
                      }
                    }
                  }
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Freneq]],  
              Position = {
                y = -2291,  
                x = 25699.0625,  
                InstanceId = [[Client1_723]],  
                Class = [[Position]],  
                z = -14.96875
              },  
              Angle = 2.859375,  
              Base = [[palette.entities.creatures.ccajd5]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_734]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_732]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Roaring Gingo]],  
              Position = {
                y = -2342.65625,  
                x = 25684.48438,  
                InstanceId = [[Client1_735]],  
                Class = [[Position]],  
                z = -15.328125
              },  
              Angle = 2.6875,  
              Base = [[palette.entities.creatures.ccajd1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client1_820]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client1_818]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Roaring Gingo]],  
              Position = {
                y = -2276.109375,  
                x = 25644.9375,  
                InstanceId = [[Client1_821]],  
                Class = [[Position]],  
                z = -12.578125
              },  
              Angle = 0.921875,  
              Base = [[palette.entities.creatures.ccajd1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        },  
        {
          InstanceId = [[Client3_39]],  
          ActivitiesId = {
          },  
          Class = [[NpcGrpFeature]],  
          InheritPos = 1,  
          Name = [[Group 2]],  
          Position = {
            y = 0.90625,  
            x = 0.890625,  
            InstanceId = [[Client3_38]],  
            Class = [[Position]],  
            z = -0.046875
          },  
          Behavior = {
            Type = [[]],  
            ZoneId = [[]],  
            InstanceId = [[Client3_37]],  
            Class = [[Behavior]],  
            Reactions = {
            },  
            ChatSequences = {
            },  
            Actions = {
            },  
            Activities = {
            }
          },  
          Components = {
            {
              InstanceId = [[Client3_31]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_29]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Seasoned Cute]],  
              Position = {
                y = -2183.453125,  
                x = 25463.4375,  
                InstanceId = [[Client3_32]],  
                Class = [[Position]],  
                z = -19.703125
              },  
              Angle = 1.453125,  
              Base = [[palette.entities.creatures.cdald2]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_42]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_40]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Lurking Cute]],  
              Position = {
                y = -2159.6875,  
                x = 25429.5625,  
                InstanceId = [[Client3_43]],  
                Class = [[Position]],  
                z = -16.4375
              },  
              Angle = -0.375,  
              Base = [[palette.entities.creatures.cdald4]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_35]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_33]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Veteran Cute]],  
              Position = {
                y = -2186.453125,  
                x = 25463.1875,  
                InstanceId = [[Client3_36]],  
                Class = [[Position]],  
                z = -19.28125
              },  
              Angle = 1.453125,  
              Base = [[palette.entities.creatures.cdald3]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_27]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_25]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Savage Cute]],  
              Position = {
                y = -2184.09375,  
                x = 25466.84375,  
                InstanceId = [[Client3_28]],  
                Class = [[Position]],  
                z = -19.578125
              },  
              Angle = 1.453125,  
              Base = [[palette.entities.creatures.cdald1]],  
              ActivitiesId = {
              }
            },  
            {
              InstanceId = [[Client3_19]],  
              Behavior = {
                Type = [[]],  
                ZoneId = [[]],  
                InstanceId = [[Client3_17]],  
                Class = [[Behavior]],  
                Reactions = {
                },  
                ChatSequences = {
                },  
                Actions = {
                },  
                Activities = {
                }
              },  
              Class = [[Npc]],  
              InheritPos = 1,  
              Name = [[Savage Cute]],  
              Position = {
                y = -2186.65625,  
                x = 25466.09375,  
                InstanceId = [[Client3_20]],  
                Class = [[Position]],  
                z = -18.953125
              },  
              Angle = 1.453125,  
              Base = [[palette.entities.creatures.cdald1]],  
              ActivitiesId = {
              }
            }
          },  
          Cost = 0
        }
      },  
      ManualWeather = 0,  
      InstanceId = [[Client1_21]]
    }
  },  
  Texts = {
    Class = [[TextManager]],  
    InstanceId = [[Client1_13]],  
    Texts = {
      {
        Count = 2,  
        InstanceId = [[Client1_138]],  
        Class = [[TextManagerEntry]],  
        Text = [[Hello homins, Your mission is to kill KarPot before i reach it.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_141]],  
        Class = [[TextManagerEntry]],  
        Text = [[You must find the tomb stones, they will show you the path to follow.]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_144]],  
        Class = [[TextManagerEntry]],  
        Text = [[So what are you expecting ! Go and follow him]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client1_145]],  
        Class = [[TextManagerEntry]],  
        Text = [[So what are you expecting ! Go and follow him !]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_128]],  
        Class = [[TextManagerEntry]],  
        Text = [[Felicitation! Good Job!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_131]],  
        Class = [[TextManagerEntry]],  
        Text = [[Jena give yor her Benediction]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_135]],  
        Class = [[TextManagerEntry]],  
        Text = [[Too Late Homin!]]
      },  
      {
        Count = 1,  
        InstanceId = [[Client2_136]],  
        Class = [[TextManagerEntry]],  
        Text = [[Too Late Homin!]]
      },  
      {
        Count = 2,  
        InstanceId = [[Client2_139]],  
        Class = [[TextManagerEntry]],  
        Text = [[You dont merit Jena Benediction!]]
      }
    }
  }
}