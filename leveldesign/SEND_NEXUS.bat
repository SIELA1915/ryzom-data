
rmdir /q /s Z:\SSD\Ryzom\1\user\nexus
rmdir /q /s R:\server\ R:\server\nexus_ig\ R:\server\nexus_pacs\ R:\server\nexus_zones R:client\ R:\client\nexus_lighted

mkdir R:\server\ R:\server\nexus_ig\ R:\server\nexus_pacs\ R:\server\nexus_zones R:client\ R:\client\nexus_lighted
copy R:\pipeline\export\continents\nexus\ai_wmap\nexus_0.cwmap2 R:\server\

copy R:\pipeline\export\continents\nexus\ig_land\* R:\server\nexus_ig\
copy R:\pipeline\export\continents\nexus\zone_lighted\* R:\server\nexus_zones\
copy R:\pipeline\export\continents\nexus\rbank_output\* R:\server\nexus_pacs\

copy R:\pipeline\export\continents\nexus\zone_lighted\* R:\client\nexus_lighted\

scp -r R:\server\nexus_0.cwmap2 R:\server\nexus_ig\ R:\server\nexus_pacs\ nevrax@gingo.ryzom.com:/home/nevrax/shard/data/collisions/
scp -r R:\server\* nevrax@gingo.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/

mkdir Z:\SSD\Ryzom\2\user\nexus\

rmdir /q /s Z:\SSD\Ryzom\2\user\nexus\nexus_pacs
rmdir /q /s Z:\SSD\Ryzom\2\user\nexus\nexus_ig
rmdir /q /s Z:\SSD\Ryzom\2\user\nexus\nexus_zones

mkdir Z:\SSD\Ryzom\2\user\nexus\nexus_pacs
mkdir Z:\SSD\Ryzom\2\user\nexus\nexus_ig
mkdir Z:\SSD\Ryzom\2\user\nexus\nexus_zones



copy R:\server\nexus_pacs\*.* Z:\SSD\Ryzom\2\user\nexus\nexus_pacs
copy R:\server\nexus_ig\*.* Z:\SSD\Ryzom\2\user\nexus\nexus_ig
copy R:\client\nexus_lighted\*.* Z:\SSD\Ryzom\2\user\nexus\nexus_zones
pause
