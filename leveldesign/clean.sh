#!/bin/bash

source ../scripts/utils.sh
#/r/scripts/pipeline_setup.sh

echo -e "\e[36mCleaning...\e[0m"

rm /r/pipeline/export/continents/$1/ai_wmap/*
rm /r/pipeline/export/continents/$1/ai_wmap_tag/*
rm /r/pipeline/export/continents/$1/ig_land*/*
rm /r/pipeline/export/continents/$1/ig_elev_land*/*
rm /r/pipeline/export/continents/$1/ligo_ig_land/*
rm /r/pipeline/export/continents/$1/ligo_zones/*
rm /r/pipeline/export/continents/$1/rbank_output/*
rm /r/pipeline/export/continents/$1/rbank_smooth/*
rm /r/pipeline/export/continents/$1/zone*/*

echo -e "\e[32mDone !\e[0m"

