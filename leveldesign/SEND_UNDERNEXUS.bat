mkdir R:\server\ R:\server\undernexus_ig\ R:\server\undernexus_pacs\ R:\server\undernexus_zones R:client\ R:\client\undernexus_lighted
copy R:\pipeline\export\continents\undernexus\ai_wmap\undernexus_0.cwmap2 R:\server\

copy R:\pipeline\export\continents\undernexus\ig_land\* R:\server\undernexus_ig\
copy R:\pipeline\export\continents\undernexus\zone_lighted\* R:\server\undernexus_zones\
copy R:\pipeline\export\continents\undernexus\rbank_output\* R:\server\undernexus_pacs\
copy R:\pipeline\export\continents\undernexus\zone_lighted\* R:\client\undernexus_lighted\

scp -r R:\server\undernexus_0.cwmap2 R:\server\undernexus_ig\ R:\server\undernexus_pacs\ nevrax@gingo.ryzom.com:/home/nevrax/shard/data/collisions/
scp -r R:\server\* nevrax@gingo.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/

mkdir Z:\SSD\Ryzom\2\user\undernexus\
mkdir Z:\SSD\Ryzom\2\user\undernexus\undernexus_pacs
mkdir Z:\SSD\Ryzom\2\user\undernexus\undernexus_ig
mkdir Z:\SSD\Ryzom\2\user\undernexus\undernexus_zones
copy R:\server\undernexus_pacs\*.* Z:\SSD\Ryzom\2\user\undernexus\undernexus_pacs
copy R:\server\undernexus_ig\*.* Z:\SSD\Ryzom\2\user\undernexus\undernexus_ig
copy R:\client\undernexus_lighted\*.* Z:\SSD\Ryzom\2\user\undernexus\undernexus_zones
pause
