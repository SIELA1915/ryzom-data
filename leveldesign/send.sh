continent=$1
server=$2
RYZOM_USER=/z/home/ulukyn/.local/share/Ryzom/2/user

echo -e "\e[36mCreating local files...\e[0m"

mkdir -p /r/server/ /r/server/${continent}_ig/ /r/server/${continent}_pacs/ /rclient/ /r/client/${continent}_zones

echo -e "\e[36m\tIG\e[0m"
cp /r/pipeline/export/continents/${continent}/ig_land/* /r/server/${continent}_ig/

echo -e "\e[36m\tRbank\e[0m"
cp /r/pipeline/export/continents/${continent}/rbank_output/* /r/server/${continent}_pacs/

echo -e "\e[36m\tZones\e[0m"
cp /r/pipeline/export/continents/${continent}/zone_lighted/* /r/client/${continent}_zones/


echo -e "\e[36mCopying CW MAP to $server server data...\e[0m"
scp -i /c/Users/uluky/.ssh/id_rsa -r /r/pipeline/export/continents/${continent}/ai_wmap/${continent}_0.cwmap2 nevrax@${server}.ryzom.com:/home/nevrax/shard/data/collisions/

echo -e "\e[36mCopying IG to $server server data...\e[0m"
scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_ig nevrax@${server}.ryzom.com:/home/nevrax/shard/data/collisions/

echo -e "\e[36mCopying PACS to $server server data...\e[0m"
scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_pacs nevrax@${server}.ryzom.com:/home/nevrax/shard/data/collisions/

if [[ "$1" == "r2_"* ]]
then
	echo -e "\e[36mCopying IG to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_ig nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/${continent}2

	echo -e "\e[36mCopying PACS to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_pacs/* nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/${continent}2/

	echo -e "\e[36mCopying ZONES to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/client/${continent}_zones nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/${continent}
else
	echo -e "\e[36mCopying IG to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_ig nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/

	echo -e "\e[36mCopying PACS to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/server/${continent}_pacs nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/

	echo -e "\e[36mCopying ZONES to $server client data...\e[0m"
	scp -i /c/Users/uluky/.ssh/id_rsa -r /r/client/${continent}_zones nevrax@${server}.ryzom.com:/home/nevrax/repos/ryzom-data/final_bnps/
fi

echo -e "\e[36mcping to client...\e[0m"
mkdir -p $RYZOM_USER/${continent}/
rm -rf  $RYZOM_USER/${continent}/${continent}*
mkdir $RYZOM_USER/${continent}/${continent}_pacs
mkdir $RYZOM_USER/${continent}/${continent}_ig
mkdir $RYZOM_USER/${continent}/${continent}_zones
cp /r/server/${continent}_pacs/* $RYZOM_USER/${continent}/${continent}_pacs
cp /r/server/${continent}_ig/* $RYZOM_USER/${continent}/${continent}_ig
cp /r/client/${continent}_zones/* $RYZOM_USER/${continent}/${continent}_zones

echo -e "\e[32mDone !\e[0m"
