#!/bin/bash

source ../scripts/utils.sh
#../scripts/pipeline_setup.sh

echo -e "\e[36mUpdating workspace...\e[0m"
rsync -auvr /d/repos/ryzom-data/leveldesign/workspace/* /r/leveldesign/workspace/

bash CLEAN_NEXUS.sh

cp /d/repos/ryzom-data/leveldesign/landscape/jungle/nexus.land /r/graphics/landscape/ligo/jungle
cp /d/repos/ryzom-private-data/primitives/primeroots/flora_nexus_minor.primitive /R/graphics/primitive/nexus
cp /d/repos/ryzom-private-data/primitives/primeroots/nexus_land_fx.primitive /R/graphics/primitive_microlife/nexus

cd /r/code/nel/tools/build_gamedata
echo $SCRIPT_DIR
"$SCRIPT_DIR"/call_python.sh 2_build.py -ipj continents/nexus
#"$SCRIPT_DIR"/call_python.sh 2_build.py -ipj continents/nexus  -ipc ai_wmap
read



/d/repos/ryzom-data/pipeline/export/ecosystems/jungle /r/pipeline/export/ecosystems/jungle