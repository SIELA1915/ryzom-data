#!/bin/bash

source ../scripts/utils.sh
#/r/scripts/pipeline_setup.sh

cd /r/code/nel/tools/build_gamedata
"$SCRIPT_DIR"/call_python.sh 1_export.py -ipj ecosystems/jungle -ipc ligo
"$SCRIPT_DIR"/call_python.sh 2_build.py -ipj ecosystems/jungle -ipc ligo
read