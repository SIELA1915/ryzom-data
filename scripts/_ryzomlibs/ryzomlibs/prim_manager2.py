import os
import sys
import json
import xml.etree.ElementTree as ET

class PrimNode():

	def __init__(self, node):
		self.node = node

	def _getChilds(self, node, nclass):
		if not node:
			return

		for child in node.findall("*"):
			if child.tag == "CHILD":
				if child.get("TYPE") == "CPrimNode":
					if nclass :
						for prop in child.findall("PROPERTY"):
							if prop.find("NAME").text == "class" and prop.find("STRING").text == nclass:
								self.elementsByName.append(PrimNode(child))
					else:
						self.elementsByName.append(PrimNode(child))
				else:
					self._getChilds(child, nclass)


	def getChilds(self, nclass=""):
		self.elementsByName = []
		self._getChilds(self.node, nclass)
		return self.elementsByName

	def getName(self):
		for prop in self.node.findall("PROPERTY"):
			if prop.find("NAME").text == "name":
				return prop.find("STRING").text

	def setName(self, name):
		for prop in self.node.findall("PROPERTY"):
			if prop.find("NAME").text == "name":
				prop.find("STRING").text = name

	def getText(self):
		texts = []
		strings = self.node.findall("STRING")
		for string in strings:
			if string.text:
				texts.append(string.text)
			else:
				texts.append("")
		return "\n".join(texts)

	def setText(self, text):
		texts = text.split("\n")

		strings = self.node.findall("STRING")
		ftail = "\n"
		ltail = "\n"
		for string in strings:
			if ftail == "\n":
				ftail = string.tail
			ltail = string.tail
			self.node.remove(string)

		for text in texts:
			e = ET.SubElement(self.node, "STRING")
			e.text = text
			e.tail = ftail
		e.tail = ltail

	def getProp(self, name):
		for prop in self.node.findall("PROPERTY"):
			if prop.find("NAME").text == name:
				return PrimNode(prop)

class PrimManager2():

	def __init__(self, filename):
		self.tree = ET.parse(filename)
		self.root = self.tree.getroot()

	def write(self, filename):
		with open(filename, "w") as f:
			f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+ET.tostring(self.root).decode().replace("\" />", "\"/>").replace("<STRING />", "<STRING></STRING>"))

	def toXml(self, node):
		print(ET.tostring(node))

	def _getElementsByClass(self, classname, node):
		pass
		#todo

	def _getElementsByName(self, elname, node):
		if not node:
			return

		for child in node.findall("*"):
			if child.tag == "CHILD":
				self._getElementsByName(elname, child)
			elif child.tag == "PROPERTY":
				if child.get("TYPE") == "string":
					name = child.find("NAME")
					if name.text == "name":
						value = child.find("STRING")
						if value.text == elname:
							self.elementsByName.append(PrimNode(node))


	def getElementsByName(self, elname, node=None):
		self.elementsByName = []
		if not node:
			node = self.root.find("ROOT_PRIMITIVE")
		self._getElementsByName(elname, node)
		return self.elementsByName

