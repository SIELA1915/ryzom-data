#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_WDIR=$(cygpath.exe  -w -a "$SCRIPT_DIR/..")
ESC_SCRIPT_WDIR=$(echo "$SCRIPT_WDIR" | sed s/\\\\/\\\\\\//g)
"$SCRIPT_DIR"/r_check.sh

if [[ ! -f /r/pipeline/build_gamedata/configuration/buildsite_local2.py ]]
then
	cp "$SCRIPT_DIR"/buildsite_local.py /r/pipeline/build_gamedata/configuration/
	sed -i "s/%%%PATH%%%/$ESC_SCRIPT_WDIR/g" /r/pipeline/build_gamedata/configuration/buildsite_local.py
fi

if [[ ! -f /r/pipeline/build_gamedata/configuration/buildsite2.py ]]
then
	cp "$SCRIPT_DIR"/buildsite.py /r/pipeline/build_gamedata/configuration/
	sed -i "s/%%%PATH%%%/$ESC_SCRIPT_WDIR/g" /r/pipeline/build_gamedata/configuration/buildsite.py
fi

cd /r/pipeline/build_gamedata/

"$SCRIPT_DIR"/call_python.sh 0_setup.py -p 
