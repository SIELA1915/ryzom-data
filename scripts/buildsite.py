#!/usr/bin/python
# 
# \file site.py
# \brief Site configuration
# \date 2021-09-17-18-01-GMT
# \author Jan Boon (Kaetemi)
# Python port of game data build pipeline.
# Site configuration.
# 
# NeL - MMORPG Framework <http://dev.ryzom.com/projects/nel/>
# Copyright (C) 2009-2014  by authors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 


# *** SITE INSTALLATION ***

# Use '/' in path name, not ''
# Don't put '/' at the end of a directory name


# Quality option for this site (1 for BEST, 0 for DRAFT)
BuildQuality = 1

RemapLocalFrom = "R:"
RemapLocalTo = "%%%PATH%%%"

ToolDirectories = ['R:/distribution/nel_tools_win_x64', 'R:/distribution/ryzom_tools_win_x64']
ToolSuffix = ".exe"

# Build script directory
ScriptDirectory = "R:/code/nel/tools/build_gamedata"
WorkspaceDirectory = "R:/leveldesign/workspace"

# Data build directories
DatabaseDirectory = "R:/graphics"
SoundDirectory = "R:/sound"
SoundDfnDirectory = "R:/sound/DFN"
ExportBuildDirectory = "R:/pipeline/export"

# Install directories
InstallDirectory = "R:/pipeline/install"
ClientDevDirectory = "R:/pipeline/client_dev"
ClientDevLiveDirectory = "R:/pipeline/client_dev_live"
ClientPatchDirectory = "R:/pipeline/client_patch"
ClientInstallDirectory = "R:/pipeline/client_install"
ShardInstallDirectory = "R:/pipeline/shard"
ShardDevDirectory = "R:/pipeline/shard_dev"
WorldEditInstallDirectory = "R:/pipeline/worldedit"

# Utility directories
WorldEditorFilesDirectory = "R:/code/ryzom/common/data_leveldesign/leveldesign/world_editor_files"

# Leveldesign directories
LeveldesignDirectory = "R:/leveldesign"
LeveldesignDfnDirectory = "R:/leveldesign/DFN"
LeveldesignWorldDirectory = "R:/leveldesign/world"
PrimitivesDirectory = "R:/leveldesign/primitives"
LeveldesignDataCommonDirectory = "R:/leveldesign/common"
LeveldesignDataShardDirectory = "R:/leveldesign/shard"
TranslationDirectory = "R:/leveldesign/translation"

# Misc data directories
GamedevDirectory = "R:/code/ryzom/client/data/gamedev"
DataCommonDirectory = "R:/code/ryzom/common/data_common"
DataShardDirectory = "R:/code/ryzom/server/data_shard"
WindowsExeDllCfgDirectories = ["", 'R:/build/fv_x64/bin/Release', 'R:/distribution/external_x64', 'R:/code/ryzom/client', '', '', '']
LinuxServiceExecutableDirectory = "R:/build/server_gcc/bin"
LinuxClientExecutableDirectory = "R:/build/client_gcc/bin"
PatchmanDevDirectory = "R:/patchman/terminal_dev"
PatchmanCfgAdminDirectory = "R:/patchman/admin_install"
PatchmanCfgDefaultDirectory = "R:/patchman/default"
PatchmanBridgeServerDirectory = "R:/pipeline/bridge_server"

# Sign tool
SignToolExecutable = "C:/Program Files/Microsoft SDKs/Windows/v6.0A/Bin/signtool.exe"
SignToolSha1 = ""
SignToolTimestamp = "http://timestamp.comodoca.com/authenticode"

# 3dsMax directives
MaxAvailable = 0
MaxDirectory = "C:/Program Files/Autodesk/3ds Max 2018"
MaxUserDirectory = "C:/Users/uluky/AppData/Local/Autodesk/3dsMax/2018 - 64bit/ENU"
MaxExecutable = "3dsmax.exe"


# end of file
