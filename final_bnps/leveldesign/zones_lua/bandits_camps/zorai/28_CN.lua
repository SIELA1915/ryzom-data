-- GROUP: spleen_busters
-- fe821c28499c4a2a41908c37ba262b57b914d710
SceneEditor:spawnShape("ge_mission_coffre.shape", 10458.007, -4323.048, -77.016, 0, 0, -12.59, '{"scale z":1.14,"col size x":1.262,"col size y":0.667,"col orientation":0,"scale y":1,"col pos x":10458.007,"col size z":0.762,"scale x":1.14,"col pos y":-4323.048}')

-- cee7883682a0713a1d162ca9ec67dca16b270672
SceneEditor:spawnShape("ju_s3_plante.shape", 10461.502, -4327.297, -76.979, 0, 0, -1.11, '{"scale z":1,"col size x":5.976,"col size y":5.849,"col orientation":0,"scale y":1,"col pos x":0,"col size z":3.21,"scale x":1,"col pos y":-4327.297}')

-- dd2cc78bc0efee0988c1f084088d90b93dd88bf4
SceneEditor:spawnShape("ju_s3_bush_tree.shape", 10451.766, -4325.818, -76.992, 0, 0, 0, '{"scale z":1,"col size x":5.048,"col size y":4.998,"col orientation":-0.95,"scale y":1,"col pos x":0,"col size z":4.612,"scale x":1,"col pos y":-4325.818}')

-- 0a65441ddb420df9ee9c3cd1da0906e37fb1c576
SceneEditor:spawnShape("ge_mission_barriere.shape", 10451.033, -4324.732, -77.016, 0, 0, -3.49, '{"scale z":1,"col size x":3.424,"col size y":0.188,"col orientation":-3.51,"scale y":1,"col pos x":10451.033,"col size z":2.303,"scale x":1,"col pos y":-4324.732}')

-- 73ed9d692f1982ee681df1a8453fae91440ae6b8
SceneEditor:spawnShape("zo_wea_dague.shape", 10458.323, -4324.926, -75.938, 0, 0, 0, '{"scale z":1,"col size x":0.109,"col size y":0.617,"col orientation":0,"scale y":1,"col pos x":10458.323,"col size z":0.04,"scale x":1,"col pos y":-4324.926}')

-- 1d3889dcfeb4a766042b1af3dcfff43227fc8f17
SceneEditor:spawnShape("zo_wea_dague.shape", 10459.577, -4323.605, -75.433, -1.07, -0.92, 0, '{"scale z":1,"col size x":0.109,"col size y":0.617,"col orientation":-0.744,"scale y":1,"col pos x":10459.577,"col size z":0.04,"scale x":1,"col pos y":-4323.605}')

-- cfc9dac309d208c6e981dad60276d3c694ddd481
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10461.168, -4323.411, -76.938, 0, 0, -0.35, '{"scale z":0.833,"col size x":1.173,"col size y":1.078,"col orientation":2.253,"scale y":0.833,"col pos x":10461.168,"col size z":1.192,"scale x":0.833,"col pos y":-4323.411}')

-- 1c65c7a6680b6891aff1c220f1942c8fa2b7cdcb
SceneEditor:spawnShape("ge_mission_tente.shape", 10464.096, -4320.91, -77.003, 0, 0, -5.02, '{"scale z":1,"col size x":6.438,"col size y":5.775,"col orientation":-0.93,"scale y":1,"col pos x":10464.096,"col size z":5.034,"scale x":1,"col pos y":-4320.91}')

-- b313900e419585643ff2f33b442126780e8d662f
SceneEditor:spawnShape("ge_mission_1_caisse_flyb.shape", 10460.526, -4324.69, -77.695, 0, 0, -1.17, '{"scale z":1,"col size x":1.044,"col size y":1.044,"col orientation":0,"scale y":1,"col pos x":10460.526,"col size z":1,"scale x":1,"col pos y":-4324.69}')

-- b2a94731f70cf25159fdc5649d2653969b29badf
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10462.126, -4323.523, -76.965, 0, 0, 0, '{"scale z":1,"col size x":1.054,"col size y":1.054,"col orientation":0,"scale y":1,"col pos x":10462.126,"col size z":1.241,"scale x":1,"col pos y":-4323.523}')

-- 4b6266f6e90448855a2f6e8efe339c1fe140ddea
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 10461.137, -4324.378, -76.948, 0, 0, -0.44, '{"scale z":1,"col size x":1.044,"col size y":1.044,"col orientation":5.67,"scale y":1,"col pos x":10461.137,"col size z":1,"scale x":1,"col pos y":-4324.378}')

-- 6f4ff55277fe80646252073eba1ec72228e7e673
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 10461.783, -4322.872, -76.934, 0, 0, 0, '{"scale z":1,"col size x":0.648,"col size y":0.652,"col orientation":1.906,"scale y":1,"col pos x":10461.783,"col size z":0.935,"scale x":1,"col pos y":-4322.872}')

-- 8d564ee5fa1f612afbc255de674cb4a51b2ebc52
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10459.607, -4323.535, -76.939, 0, 0, 0, '{"scale z":1,"col size x":1.054,"col size y":1.054,"col orientation":0,"scale y":1,"col pos x":10459.607,"col size z":1.241,"scale x":1,"col pos y":-4323.535}')

-- d783d83714523594cf3b2e8b7610b7d8ea52d736
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10459.232, -4324.893, -76.954, 0, 0, 0, '{"scale z":1,"col size x":2.532,"col size y":1.299,"col orientation":0,"scale y":1,"col pos x":10459.232,"col size z":2,"scale x":1,"col pos y":-4324.893}')


