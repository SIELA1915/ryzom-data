-- GROUP: lake_loafers
-- bb031317e160ea1f9fe8c392f6386e12ba24d500
SceneEditor:spawnShape("ge_feudecamp.ps", 10750.521, -3818.443, -72.541, 0, 0, 0, '{"col pos y":-3818.443,"col orientation":0.439,"col size x":2.491,"col size y":2.608,"col pos x":0,"scale z":1,"scale x":1,"scale y":1,"col size z":5.716}')

-- ac7b80d1cde1d3f70b316bdeef1de9b308f6aaca
SceneEditor:spawnShape("ge_mission_tente.shape", 10745.902, -3817.536, -72.389, 0, 0, -0.94, '{"col pos y":-3817.536,"col orientation":-0.97,"col size x":6.438,"col size y":5.775,"col pos x":10745.902,"scale z":1,"scale x":1,"scale y":1,"col size z":5.034}')

-- 94d8d5556362ad6a61b1105403bc2b43b6fad4cc
SceneEditor:spawnShape("ge_mission_sac_a.shape", 10743.577, -3814.247, -72.189, 0, 0, -2.11, '{"col pos y":-3814.247,"col orientation":-1.989,"col size x":0.967,"col size y":1.165,"col pos x":10743.577,"scale z":1,"scale x":1,"scale y":1,"col size z":0.404}')

-- bd91e614181c5f5aeb86bcc32b79c2f9f4b23850
SceneEditor:spawnShape("ge_mission_sac_a.shape", 10743.548, -3812.23, -71.987, 0, 0, 0.42, '{"col pos y":-3812.23,"col orientation":2.554,"col size x":0.967,"col size y":1.165,"col pos x":10743.548,"scale z":1,"scale x":1,"scale y":1,"col size z":0.404}')

-- e4463986cfa146354b0f5bcc0317243000955cde
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10742.107, -3813.547, -72.348, 0, 0, 0, '{"col pos y":-3813.547,"col orientation":0,"col size x":1.7,"col size y":1.62,"col pos x":10742.107,"scale z":1,"scale x":1,"scale y":1,"col size z":0.968}')

-- 66ff55cf028992972e4c49cfd25ea959f64409ea
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10741.885, -3812.328, -72.307, 0, 0, -1.17, '{"col pos y":-3812.328,"col orientation":0,"col size x":1.7,"col size y":1.62,"col pos x":10741.885,"scale z":1,"scale x":1,"scale y":1,"col size z":0.968}')

-- 28d5c1cfcc784976278a8a9a214c90f2a0c306d4
SceneEditor:spawnShape("ge_mission_stand.shape", 10742.358, -3813.209, -72.277, 0, 0, 1.63, '{"col pos y":-3813.209,"col orientation":-1.492,"col size x":4.258,"col size y":4.775,"col pos x":10742.358,"scale z":0.644,"scale x":1.088,"scale y":0.938,"col size z":6.313}')

-- fd27f6ee3c89afe29ea6f6545ce2fb71078dcbce
SceneEditor:spawnShape("ju_s3_fantree.shape", 10750.985, -3827.623, -74.939, 0, 0, 0, '{"col pos y":-3827.623,"col orientation":0.657,"col size x":2.466,"col size y":2.199,"col pos x":10750.985,"scale z":1,"scale x":1,"scale y":1,"col size z":8.502}')

-- a8ebfea73d1542ffc9bcc8bbf1a5298df4865bec
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 10762.895, -3828.339, -74.842, 0, 0, 0.32, '{"col pos y":-3828.339,"col orientation":0,"col size x":2.685,"col size y":5.184,"col pos x":10762.895,"scale z":0.769,"scale x":0.769,"scale y":0.769,"col size z":2.147}')

-- 954e10b0261c459a2479e8d2b551718a60d1d3ca
SceneEditor:spawnShape("ge_mission_barriere.shape", 10740.502, -3809.348, -72.632, 0, 0, -1.8, '{"col pos y":-3809.348,"col orientation":-1.882,"col size x":3.424,"col size y":0.188,"col pos x":10740.502,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 501945c817a7ba7b9c2ddcd3d4ba1ac8294aa8a3
SceneEditor:spawnShape("ge_mission_barriere.shape", 10739.963, -3813.051, -72.745, 0, 0, -1.64, '{"col pos y":-3813.051,"col orientation":-1.64,"col size x":3.424,"col size y":0.188,"col pos x":10739.963,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 4528b941874248d2d7f5d6456751d775f8157a1b
SceneEditor:spawnShape("ge_mission_barriere.shape", 10740.858, -3816.43, -72.87, 0, 0, -1.01, '{"col pos y":-3816.43,"col orientation":-1.05,"col size x":3.424,"col size y":0.188,"col pos x":10740.858,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 8ad8c07c7dbed5454f6a90f6da1c58b0fbf8e325
SceneEditor:spawnShape("ge_mission_barriere.shape", 10742.606, -3819.537, -73.107, 0, 0, -1.03, '{"col pos y":-3819.537,"col orientation":-1.09,"col size x":3.424,"col size y":0.188,"col pos x":10742.606,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- d5e2372ddd97dd54a1e5a67808a00b7966b2e70d
SceneEditor:spawnShape("ge_mission_barriere.shape", 10745.374, -3821.114, -73.217, 0, 0, 0, '{"col pos y":-3821.114,"col orientation":-0,"col size x":3.424,"col size y":0.188,"col pos x":10745.374,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- f3e00590300c98e17de7868d353c6b84b0bee9b9
SceneEditor:spawnShape("ge_mission_barriere.shape", 10748.771, -3821.901, -73.357, 0, 0, -0.46, '{"col pos y":-3821.901,"col orientation":-0.53,"col size x":3.424,"col size y":0.188,"col pos x":10748.771,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 42ed7e60b73b9fc29b6954a3d97e4ee276d4d7d0
SceneEditor:spawnShape("ge_mission_maison_ruine.shape", 10756.822, -3832.749, -76.163, 0, 0, -0.32, '{"col pos y":-3832.749,"col orientation":-0.27,"col size x":14.393,"col size y":10.759,"col pos x":10756.822,"scale z":1,"scale x":1,"scale y":1,"col size z":5.193}')

-- 4895a934e90a1cb8cb3262927d80682c9316e12c
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10766.913, -3821.44, -73.272, 0, 0, 0, '{"col pos y":-3821.44,"col orientation":0,"col size x":1.7,"col size y":1.62,"col pos x":10766.913,"scale z":1,"scale x":1,"scale y":1,"col size z":0.968}')

-- 4311d9af60393c7995ee0ffa27c1bfc98f34ae69
SceneEditor:spawnShape("ge_mission_barriere.shape", 10767.58, -3822.119, -73.229, 0, 0, -2.34, '{"col pos y":-3822.119,"col orientation":-2.27,"col size x":3.424,"col size y":0.188,"col pos x":10767.58,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 988fcdcd72e4e717d3f4e790a78b8591b89b3c3e
SceneEditor:spawnShape("ge_mission_barriere.shape", 10768.65, -3818.961, -73.078, 0, 0, -1.55, '{"col pos y":-3818.961,"col orientation":1.65,"col size x":3.424,"col size y":0.188,"col pos x":10768.65,"scale z":1,"scale x":1,"scale y":1,"col size z":2.303}')

-- 8619cf0af659ca6d9a2f2deb6bef474ebdba173d
SceneEditor:spawnShape("ge_mission_tente.shape", 10766.778, -3825.639, -73.878, 0, 0, -4.84, '{"col pos y":-3825.639,"col orientation":-0.69,"col size x":6.438,"col size y":5.775,"col pos x":10766.778,"scale z":1,"scale x":1,"scale y":1,"col size z":5.034}')

-- 5b3133790259e1b589a8fca8d291f28c9d3fe3ee
SceneEditor:spawnShape("ge_mission_tente.shape", 10753.023, -3823.088, -73.942, 0, 0, -1.75, '{"col pos y":-3823.088,"col orientation":-1.53,"col size x":6.438,"col size y":5.775,"col pos x":10753.023,"scale z":1,"scale x":1,"scale y":1,"col size z":5.034}')


