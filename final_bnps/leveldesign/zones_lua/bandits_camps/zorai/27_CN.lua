-- GROUP: spleen_busters
-- d5fc9ace2a7ffbff0f794731510f3849f3edc885
SceneEditor:spawnShape("ge_mission_sac_a.shape", 10445.005, -4310.483, -77, 0, 0, 0, '{"scale z":1,"col size x":0.967,"col size y":1.165,"col orientation":1.011,"scale y":1,"col pos x":10445.005,"col size z":0.404,"scale x":1,"col pos y":-4310.483}')

-- ee0149b9ecc07db87f17973dec7f60503041e252
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10445.551, -4309.007, -77.036, 0, 0, 0, '{"scale z":1,"col size x":1.7,"col size y":1.62,"col orientation":2.46,"scale y":1,"col pos x":10445.551,"col size z":0.968,"scale x":1,"col pos y":-4309.007}')

-- 08638cf1f5562e25500434ea6356320b355dbd16
SceneEditor:spawnShape("ge_mission_tonneau_broke.shape", 10444.805, -4307.329, -77.044, 0, 0, 0, '{"scale z":1,"col size x":1.054,"col size y":1.054,"col orientation":-0.65,"scale y":1,"col pos x":10444.805,"col size z":1.241,"scale x":1,"col pos y":-4307.329}')

-- 1c950cea9cd7cfe58fe0058f4c42bc670546aebe
SceneEditor:spawnShape("ge_mission_charette.shape", 10456.969, -4306.612, -77.008, 0, 0, 0.4, '{"scale z":0.769,"col size x":2.325,"col size y":3.934,"col orientation":-2.707,"scale y":0.769,"col pos x":10456.947,"col size z":2.592,"scale x":0.769,"col pos y":-4306.719}')

-- 4412d3d88ddae70b31a161c840d1b5f659bb292a
SceneEditor:spawnShape("ju_s3_bamboo.shape", 10461.978, -4314.718, -77.02, 0, 0, 0, '{"scale z":1,"col size x":4.404,"col size y":4.612,"col orientation":0,"scale y":1,"col pos x":0,"col size z":6.611,"scale x":1,"col pos y":-4314.718}')

-- 008810439f5c43315ff9879d4b34038f322cda47
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 10462.074, -4315.727, -76.973, 0, 0, -4.37, '{"scale z":1,"col size x":5.411,"col size y":1.112,"col orientation":-7.444,"scale y":1,"col pos x":10462.815,"col size z":2.376,"scale x":1,"col pos y":-4314.812}')

-- 121dee7895eb4faf84233f61e7f2bd633e693aec
SceneEditor:spawnShape("ge_mission_barriere.shape", 10458.792, -4307.529, -76.998, 0, 0, -1.25, '{"scale z":1,"col size x":3.424,"col size y":0.188,"col orientation":-1.25,"scale y":1,"col pos x":10458.792,"col size z":2.303,"scale x":1,"col pos y":-4307.529}')

-- 781412a262f468f15436946e3ab23c0de4ace317
SceneEditor:spawnShape("ge_feudecamp.ps", 10457.375, -4318.766, -76.999, 0, 0, 0, '{"scale z":1,"col size x":2.491,"col size y":2.608,"col orientation":1.404,"scale y":1,"col pos x":-0.086,"col size z":5.716,"scale x":1,"col pos y":-4318.766}')


