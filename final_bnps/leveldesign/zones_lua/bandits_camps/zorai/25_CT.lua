-- GROUP: dare_doers
-- 48714aaae8489606d7379be70653addfb081b170
SceneEditor:spawnShape("ge_feudecamp.ps", 11406.95, -3998.451, -76.987, 0, 0, 0, '{"col size z":5.716,"scale z":1,"col pos y":-3998.451,"col size x":2.491,"col pos x":0,"col size y":2.608,"col orientation":0,"scale y":1,"scale x":1}')

-- bd04cbfb84e3f29635541e0271f28611e8ae43c4
SceneEditor:spawnShape("ju_s3_fantree.shape", 11387.012, -3994.799, -76.973, 0, 0, 0, '{"col size z":8.502,"scale z":1,"col pos y":-3994.799,"col size x":2.466,"col pos x":11387.012,"col size y":2.199,"col orientation":0.284,"scale y":1,"scale x":1}')

-- 164292e5386fd7132dd46ed6f4b7ba19df9f2616
SceneEditor:spawnShape("ju_s3_bush_tree.shape", 11416.668, -3996.15, -76.934, 0, 0, 0, '{"col size z":4.612,"scale z":1,"col pos y":-3996.15,"col size x":5.048,"col pos x":11416.668,"col size y":4.998,"col orientation":3.12,"scale y":1,"scale x":1}')

-- fbff72745f7140f6eead4f1f78e0630bc1ab3a9a
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 11399.265, -3987.179, -77.043, 0, 0, 0, '{"col size z":3.99,"scale z":1,"col pos y":-3987.179,"col size x":4.765,"col pos x":0,"col size y":4.346,"col orientation":0,"scale y":1,"scale x":1}')

-- 6cefbd0a178b5e3d273103bf2da86ce8b318f208
SceneEditor:spawnShape("ge_mission_barriere.shape", 11419.905, -3998.727, -76.977, 0, 0, -1.55, '{"col size z":2.303,"scale z":1,"col pos y":-3998.727,"col size x":3.424,"col pos x":11419.905,"col size y":0.188,"col orientation":-1.56,"scale y":1,"scale x":1}')

-- 5af7e48521f7e1eae601d7cc605084df8641fd2a
SceneEditor:spawnShape("ge_mission_barriere.shape", 11419.088, -3995.385, -76.964, 0, 0, -1.1, '{"col size z":2.303,"scale z":1,"col pos y":-3995.385,"col size x":3.424,"col pos x":11419.088,"col size y":0.188,"col orientation":-1.11,"scale y":1,"scale x":1}')

-- 81f411b033efa26a37309e93cf1cabe65f486e3c
SceneEditor:spawnShape("ge_mission_barriere.shape", 11417.442, -3992.279, -76.984, 0, 0, -1.07, '{"col size z":2.303,"scale z":1,"col pos y":-3992.279,"col size x":3.424,"col pos x":11417.442,"col size y":0.188,"col orientation":-1.03,"scale y":1,"scale x":1}')

-- fdefb5ce7c49a5f82adb1bfc6acc4e2593dd2923
SceneEditor:spawnShape("ge_mission_barriere.shape", 11415.703, -3989.225, -76.977, 0, 0, -1.02, '{"col size z":2.303,"scale z":1,"col pos y":-3989.225,"col size x":3.424,"col pos x":11415.703,"col size y":0.188,"col orientation":2.127,"scale y":1,"scale x":1}')

-- d817cf6be8c7b387158d75dfbbb7dfb1af4cbe76
SceneEditor:spawnShape("ge_mission_barriere.shape", 11413.826, -3986.141, -77.002, 0, 0, -1.01, '{"col size z":2.303,"scale z":1,"col pos y":-3986.141,"col size x":3.424,"col pos x":11413.826,"col size y":0.188,"col orientation":-1.02,"scale y":1,"scale x":1}')

-- e3c7a33518d756f65aa7cdffc146d7f2f6df876e
SceneEditor:spawnShape("ge_mission_barriere.shape", 11411.22, -3984.48, -76.987, 0, 0, 0, '{"col size z":2.303,"scale z":1,"col pos y":-3984.48,"col size x":3.424,"col pos x":11411.22,"col size y":0.188,"col orientation":0,"scale y":1,"scale x":1}')

-- 36a10ff39a57dc967fa4516e24b905d9df1a00aa
SceneEditor:spawnShape("ge_mission_barriere.shape", 11407.668, -3984.442, -76.978, 0, 0, 0, '{"col size z":2.303,"scale z":1,"col pos y":-3984.442,"col size x":3.424,"col pos x":11407.668,"col size y":0.188,"col orientation":0,"scale y":1,"scale x":1}')


