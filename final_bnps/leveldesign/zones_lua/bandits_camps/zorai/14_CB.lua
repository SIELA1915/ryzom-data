-- GROUP: zora_springers
-- bc41103e0042acef51dcd2315ae2fad8ae8030d7
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 8481.674, -2108.132, -77.002, 0, 0, 1.27, '{"col size y":4.365,"col orientation":0,"col pos y":-2108.132,"col size x":3.835,"scale x":1,"col size z":3.046,"scale y":1,"scale z":1,"col pos x":8481.674}')

-- 602e28b15f5e11f2b341df50d99e0bba29fcf66d
SceneEditor:spawnShape("ge_mission_charette.shape", 8482.753, -2079.744, -77.023, 0, 0, -1.6, '{"col size y":3.909,"col orientation":-1.6,"col pos y":-2079.744,"col size x":1.599,"scale x":0.769,"col size z":0.769,"scale y":0.769,"scale z":0.769,"col pos x":8482.753}')

-- fc11173789cf2a42d0d0b6ac5e67f5e49213ea66
SceneEditor:spawnShape("ge_mission_barriere.shape", 8498.737, -2085.541, -76.971, 0, 0, -1.43, '{"col size y":0.188,"col orientation":-1.44,"col pos y":-2085.541,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8498.737}')

-- 63fd839df94b72db26dc842023ca429f7a4a0d88
SceneEditor:spawnShape("ge_mission_barriere.shape", 8497.165, -2082.66, -76.962, 0, 0, -0.68, '{"col size y":0.188,"col orientation":-0.683,"col pos y":-2082.66,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8497.165}')

-- 80c6056fb1ad9cf30eaf210973e2a5648dad509c
SceneEditor:spawnShape("ge_mission_barriere.shape", 8494.451, -2080.412, -76.933, 0, 0, -0.66, '{"col size y":0.188,"col orientation":-0.66,"col pos y":-2080.412,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8494.451}')

-- 0cca8281f7311f5e28f7419e193a36833a465dd6
SceneEditor:spawnShape("ge_mission_barriere.shape", 8497.224, -2102.244, -76.781, 0, 0, -1.78, '{"col size y":0.188,"col orientation":-1.8,"col pos y":-2102.244,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8497.224}')

-- b3561e181b435da30c535a9652c5d58b822b7e20
SceneEditor:spawnShape("ge_mission_barriere.shape", 8495.488, -2105.13, -76.718, 0, 0, 0.72, '{"col size y":0.188,"col orientation":0.73,"col pos y":-2105.13,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8495.488}')

-- dc60fb741b28cd24be3b0d39e193c05af05f1f3c
SceneEditor:spawnShape("ge_mission_barriere.shape", 8492.833, -2107.484, -76.789, 0, 0, 0.75, '{"col size y":0.188,"col orientation":0.76,"col pos y":-2107.484,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8492.833}')

-- 621f391dca4608d00ec424e7c42415ff0b41a444
SceneEditor:spawnShape("ge_mission_barriere.shape", 8490.233, -2109.812, -76.959, 0, 0, 0.72, '{"col size y":0.188,"col orientation":0.72,"col pos y":-2109.812,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8490.233}')

-- d4b955b18f82e88944303717450a66829c606504
SceneEditor:spawnShape("ge_mission_barriere.shape", 8487.183, -2110.958, -77.055, 0, 0, 0, '{"col size y":0.188,"col orientation":0,"col pos y":-2110.958,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8487.183}')

-- 799fafa662c2c2d82f9ba73dc794e9fdb4d42a26
SceneEditor:spawnShape("ge_mission_barriere.shape", 8483.43, -2111.21, -77.017, 0, 0, 0, '{"col size y":0.188,"col orientation":3.17,"col pos y":-2111.21,"col size x":3.424,"scale x":1,"col size z":2.303,"scale y":1,"scale z":1,"col pos x":8483.43}')


