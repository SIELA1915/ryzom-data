-- GROUP: zoras_thorn
-- 05815169097647cd5f70a6a1c28c4db41de5d89b
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 8148.798, -3153.216, -76.839, 0, 0, 0, '{"col pos x":8148.798,"col pos y":-3153.216,"scale y":1,"col size y":-0.897,"col size x":0.811,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- 350f3baeefb3231871358b5663c0f03c8dbcdea8
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 8150.157, -3115.746, -76.551, 0, 0, 0, '{"col pos x":8150.157,"col pos y":-3115.746,"scale y":1,"col size y":-1.027,"col size x":0.681,"scale z":1,"col orientation":-2.52,"col size z":5.674,"scale x":1}')

-- 10b091f7ec91a4a35657345df5ed39e69c4bc01f
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 8142.826, -3114.973, -76.68, 0, 0, 0, '{"col pos x":8142.826,"col pos y":-3114.973,"scale y":1,"col size y":-0.827,"col size x":0.881,"scale z":1,"col orientation":0,"col size z":5.674,"scale x":1}')

-- 4cb31e69f75d5bdc2fbbcb4204b1991fb5bba5af
SceneEditor:spawnShape("ge_feudecamp.ps", 8153.583, -3128.191, -77.053, 0, 0, 0, '{"col pos x":0,"col pos y":-3128.191,"scale y":1,"col size y":2.608,"col size x":2.491,"scale z":1,"col orientation":0,"col size z":5.716,"scale x":1}')

-- 7169010a5ff9c7228379d2f8db3511e0aa6dc173
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 8140.37, -3150.35, -76.837, 0, 0, 1.94, '{"col pos x":8140.37,"col pos y":-3150.35,"scale y":1,"col size y":0.822,"col size x":0.793,"scale z":1,"col orientation":0.5,"col size z":2.124,"scale x":1}')

-- 1a2f9d9b8d50f38b6ca5cdfb9a1d366e37b81031
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 8139.746, -3148.897, -76.897, 0, 0, -1.2, '{"col pos x":8139.746,"col pos y":-3148.897,"scale y":1,"col size y":0.539,"col size x":1.334,"scale z":1,"col orientation":-1.24,"col size z":1.192,"scale x":1}')

-- 2abeaa6cb73e4a96afbe49d91aa62d576c80f193
SceneEditor:spawnShape("ge_mission_objet_pack_2.shape", 8141.175, -3127.594, -77.568, 0, 0, 0, '{"col pos x":8141.175,"col pos y":-3127.594,"scale y":1,"col size y":3.263,"col size x":2.321,"scale z":1,"col orientation":0,"col size z":2.243,"scale x":1}')

-- fe350b7ad5aa7dc1ca2adffbbb06017dad297907
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 8138.007, -3132.687, -77.838, 0, 0, 0, '{"col pos x":8138.007,"col pos y":-3134.258,"scale y":0.833,"col size y":1.078,"col size x":1.173,"scale z":0.833,"col orientation":2.889,"col size z":1.192,"scale x":0.833}')

-- d14437cdaa971d8438eb41d8e580399872530984
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 8137.928, -3133.874, -77.84, 0, 0, 0, '{"col pos x":8137.928,"col pos y":-3133.874,"scale y":1,"col size y":1.299,"col size x":2.532,"scale z":1,"col orientation":3.284,"col size z":2,"scale x":1}')

-- 7aa4135d042c8547e8bea53f8f40a54d7a2c3c4e
SceneEditor:spawnShape("ge_mission_barriere.shape", 8158.563, -3126.259, -76.996, 0, 0, -0.24, '{"col pos x":8158.563,"col pos y":-3126.259,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.28,"col size z":2.303,"scale x":1}')

-- bc17ae004e07bd9467d4ddd849591b7c11079ee6
SceneEditor:spawnShape("ge_mission_barriere.shape", 8155.486, -3124.749, -77.014, 0, 0, -0.67, '{"col pos x":8155.486,"col pos y":-3124.749,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.71,"col size z":2.303,"scale x":1}')

-- 8409535db57d17a0e177f22b6861261c70d6f8e4
SceneEditor:spawnShape("ge_mission_barriere.shape", 8141.25, -3141.432, -77.373, 0, 0, -0.85, '{"col pos x":8141.25,"col pos y":-3141.432,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-0.85,"col size z":2.303,"scale x":1}')

-- 892c29811a44115f143f9a4900693365e8201361
SceneEditor:spawnShape("ge_mission_barriere.shape", 8139.144, -3116.419, -76.959, 0, 0, -2.56, '{"col pos x":8139.144,"col pos y":-3116.419,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0.6,"col size z":2.303,"scale x":1}')

-- 4719c36f4db5423424a2599f283a3bfa71095712
SceneEditor:spawnShape("ge_mission_barriere.shape", 8137.078, -3131.283, -77.898, 0, 0, -1.65, '{"col pos x":8137.078,"col pos y":-3131.283,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.65,"col size z":2.303,"scale x":1}')

-- 2e20717a750ab6b4e9b44d15309c1ffca2060da9
SceneEditor:spawnShape("ge_mission_barriere.shape", 8137.309, -3127.78, -77.768, 0, 0, -1.65, '{"col pos x":8137.309,"col pos y":-3127.78,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":-1.572,"col size z":2.303,"scale x":1}')

-- 400747315d1427ee3ee29d760bbee5eb761803f0
SceneEditor:spawnShape("ge_mission_barriere.shape", 8138.607, -3124.702, -77.582, 0, 0, 0.81, '{"col pos x":8138.607,"col pos y":-3124.702,"scale y":1,"col size y":0.188,"col size x":3.424,"scale z":1,"col orientation":0.744,"col size z":2.303,"scale x":1}')


