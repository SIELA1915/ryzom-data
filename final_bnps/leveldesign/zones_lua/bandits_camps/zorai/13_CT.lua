-- GROUP: goo_grapplers
-- 59fe2fcdfe6c40a4dd2cd67f0c47b2e56fedc8c9
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 11463.721, -1974.588, -10.321, 0, 0, 0, '{"col size y":4.365,"col orientation":-3.91,"scale x":1,"scale y":1,"scale z":1,"col pos x":11463.721,"col size z":3.046,"col size x":3.835,"col pos y":-1974.588}')

-- 178d0bb66209ceb66c402044fd5d515d1c8e6bce
SceneEditor:spawnShape("ju_s2_young_tree.shape", 11479.742, -2008.966, -9.77, 0, 0, 0, '{"col size y":13.482,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":-196.587,"col size z":16.447,"col size x":10.921,"col pos y":-2008.966}')


-- GROUP: underwood_rebels
-- 1af0668188446f18efca2db11f58f6dbc93a384c
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 11464.268, -2005.518, -9.235, 0, 0, 0, '{"col pos x":11464.268,"col size y":-0.867,"col size x":0.841,"scale y":1,"col size z":5.674,"col orientation":0,"col pos y":-2005.518,"scale z":1,"scale x":1}')

-- 54f680739d6932aaa49927955a6bc8e4892a9c7b
SceneEditor:spawnShape("ge_mission_reverbere_blue.ps", 11456.88, -2000.581, -8.461, 0, 0, 0, '{"col pos x":11456.88,"col size y":-0.687,"col size x":1.021,"scale y":1,"col size z":5.674,"col orientation":0,"col pos y":-2000.581,"scale z":1,"scale x":1}')

-- 93d92728d020f93232e923e8c1a07bea8631ace9
SceneEditor:spawnShape("ge_mission_barriere.shape", 11464.667, -2004.487, -9.511, 0, 0.13, -0.58, '{"col pos x":11464.667,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"col orientation":-0.59,"col pos y":-2004.487,"scale z":1,"scale x":1}')

-- 8e963d105b21840041d9a88e5ca7ca9c75748868
SceneEditor:spawnShape("ge_mission_barriere.shape", 11458.355, -2000.162, -8.639, 0, 0.08, -0.57, '{"col pos x":11458.355,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"col orientation":-0.58,"col pos y":-2000.162,"scale z":1,"scale x":1}')

-- 09aa9f70f40be04311166a5b852f025389c32ae3
SceneEditor:spawnShape("ge_mission_barriere.shape", 11461.439, -2002.337, -8.97, 0, 0.12, -0.6, '{"col pos x":11461.439,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"col orientation":2.49,"col pos y":-2002.337,"scale z":1,"scale x":1}')

-- 94dc427524438313fecb3c9500503311476f007f
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 11468.248, -1987.725, -10.818, 0, 0, 1.7, '{"col pos x":11468.248,"col size y":5.184,"col size x":2.685,"scale y":0.769,"col size z":2.147,"col orientation":-4.631,"col pos y":-1987.725,"scale z":0.769,"scale x":0.769}')

-- 2252bbe953fdb7f26c92d82cd15269b03bc6f5da
SceneEditor:spawnShape("ge_mission_sac_b.shape", 11480.292, -2002.626, -10.188, 0, 0, 0, '{"col pos x":11480.292,"col size y":1.62,"col size x":1.7,"scale y":1,"col size z":0.968,"col orientation":-2.525,"col pos y":-2002.626,"scale z":1,"scale x":1}')

-- d8b346f6476db98c11357bde857674795568872d
SceneEditor:spawnShape("ge_mission_sac_a.shape", 11467.788, -1998.103, -10.217, 1.03, 0.22, 0, '{"col pos x":11467.788,"col size y":1.165,"col size x":0.967,"scale y":1,"col size z":0.404,"col orientation":2.333,"col pos y":-1998.103,"scale z":1,"scale x":1}')

-- 7c923c4749e69dfa803c84d1089be6b104a5f26c
SceneEditor:spawnShape("ju_s3_bamboo.shape", 11451.068, -1994.867, -9.151, 0, 0, 0, '{"col pos x":0,"col size y":4.612,"col size x":4.404,"scale y":1,"col size z":6.611,"col orientation":0,"col pos y":-1994.867,"scale z":1,"scale x":1}')

-- 966561ab0fc5d8e4e28cff5a57a4b9aadb87acbb
SceneEditor:spawnShape("ge_mission_tombe_a.shape", 11477.206, -2012.98, -9.325, 0, 0, 0.15, '{"col pos x":11477.206,"col size y":0.965,"col size x":1.494,"scale y":1,"col size z":1.76,"col orientation":0,"col pos y":-2012.98,"scale z":1,"scale x":1}')

-- 9feb6b6804ebca1c2f99e4cecc090cf1ed6e79c0
SceneEditor:spawnShape("ge_mission_tombe_c.shape", 11474.957, -2012.303, -9.134, 0, 0, 0, '{"col pos x":11474.957,"col size y":0.57,"col size x":0.902,"scale y":1,"col size z":1.564,"col orientation":-3.009,"col pos y":-2012.303,"scale z":1,"scale x":1}')

-- 82ecfa7d7a8f813fd3482f9bf00939874f5ee3b3
SceneEditor:spawnShape("ge_mission_tombe_d.shape", 11472.824, -2012.382, -8.874, 0, 0, -0.46, '{"col pos x":11472.824,"col size y":0.822,"col size x":0.793,"scale y":1,"col size z":2.124,"col orientation":0,"col pos y":-2012.382,"scale z":1,"scale x":1}')

-- 78106c24a452ed9fe29cdafb50fab34e4ea7030c
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 11471.096, -2011.483, -8.84, 0, 0, -0.33, '{"col pos x":11470.379,"col size y":0.539,"col size x":1.334,"scale y":1,"col size z":1.192,"col orientation":1.145,"col pos y":-2017.073,"scale z":1,"scale x":1}')

-- 11fb7007a597e34184abfd4e41797a9934997074
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 11466.838, -1997.91, -10.362, 0, 0, 0, '{"col pos x":11466.838,"col size y":0.652,"col size x":0.648,"scale y":1,"col size z":0.935,"col orientation":0,"col pos y":-1997.91,"scale z":1,"scale x":1}')

-- 83dd4a821abd174e52702e23fbb0b2084c71047b
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 11466.619, -1997.22, -10.271, 0, 0, 0, '{"col pos x":11466.619,"col size y":1.078,"col size x":1.173,"scale y":0.833,"col size z":1.192,"col orientation":0,"col pos y":-1997.22,"scale z":0.833,"scale x":0.833}')

-- 0dfbb6216c8676b28a42027ad6b80426cb18c2e9
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 11463.305, -1974.746, -10.38, 0, 0, 0, '{"col pos x":11463.305,"col size y":4.365,"col size x":3.835,"scale y":1,"col size z":3.046,"col orientation":0,"col pos y":-1974.746,"scale z":1,"scale x":1}')

-- 5c44128b7cd5943dcaee323ec0868e226f546355
SceneEditor:spawnShape("ju_s3_dead_tree.shape", 11486.558, -1998.571, -9.594, 0, 0, 0, '{"col pos x":11486.558,"col size y":5.665,"col size x":5.319,"scale y":1,"col size z":4.997,"col orientation":2.885,"col pos y":-1998.571,"scale z":1,"scale x":1}')

-- 6b0222250ba6621bf55967801569de5302b276b0
SceneEditor:spawnShape("ge_mission_tente.shape", 11460.166, -1979.311, -10.488, 0, 0, -3.13, '{"col pos x":11460.166,"col size y":5.775,"col size x":6.438,"scale y":1,"col size z":5.034,"col orientation":0,"col pos y":-1979.311,"scale z":1,"scale x":1}')

-- 8a0f66949425bb753f4135138daf11ba24db8d73
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 11493.294, -1997.761, -7.709, 0, -0.4, 1.54, '{"col pos x":11493.552,"col size y":5.258,"col size x":5.361,"scale y":1,"col size z":3.102,"col orientation":-2.095,"col pos y":-1996.789,"scale z":1,"scale x":1}')

-- e876bc9c1061ffe03c5515bf322e2a762fde7e88
SceneEditor:spawnShape("ge_mission_barriere.shape", 11489.593, -2001.554, -9.655, 0, 0.16, -2.07, '{"col pos x":11489.593,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"col orientation":-2.03,"col pos y":-2001.554,"scale z":1,"scale x":1}')

-- 15d2382f4a51166f46a3c46fddc8de522d7e6a79
SceneEditor:spawnShape("ge_mission_barriere.shape", 11491.377, -1998.344, -8.871, 0, -0.29, 1.05, '{"col pos x":11491.377,"col size y":0.188,"col size x":3.424,"scale y":1,"col size z":2.303,"col orientation":0.97,"col pos y":-1998.344,"scale z":1,"scale x":1}')

-- 6141bfeca847e7c37c33c44032238b8bfb94b7c3
SceneEditor:spawnShape("ge_mission_totem_pachyderm.shape", 11453.028, -1966.941, -10.658, 0, 0, -3.11, '{"col pos x":11452.393,"col size y":6.933,"col size x":4.529,"scale y":1,"col size z":13.237,"col orientation":-0.048,"col pos y":-1965.704,"scale z":1,"scale x":1}')


