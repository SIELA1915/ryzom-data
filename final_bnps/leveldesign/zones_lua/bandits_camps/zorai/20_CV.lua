-- GROUP: plain_scourers
-- 12d7faa179b89c1821edb3b772f95660179e4d0c
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11691.917, -3196.716, -76.871, 0, 0, 0, '{"col size y":1.453,"col orientation":-1.78,"scale x":1,"scale y":1,"scale z":1,"col pos x":11691.917,"col size z":5.674,"col size x":3.161,"col pos y":-3196.716}')

-- 6f499e5543f855615a5838433c16ecbaa5bf16c5
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 11681.698, -3178.754, -76.946, 0, 0, -2.03, '{"col size y":-1.027,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":11681.698,"col size z":5.674,"col size x":0.681,"col pos y":-3178.754}')

-- 1c261bd2b44b175105894341c78b389505294c32
SceneEditor:spawnShape("ju_s3_bush_tree.shape", 11693.46, -3195.299, -76.779, 0, 0, 0, '{"col size y":4.998,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":11693.46,"col size z":4.612,"col size x":5.048,"col pos y":-3195.299}')

-- 5bdce9f5d154ea6508fcf79f5b950854e1183b02
SceneEditor:spawnShape("ge_mission_barriere.shape", 11682.148, -3178.034, -76.939, 0, 0, -0.41, '{"col size y":0.188,"col orientation":-0.42,"scale x":1,"scale y":1,"scale z":1,"col pos x":11682.148,"col size z":2.303,"col size x":3.424,"col pos y":-3178.034}')

-- f0084ac8078eeecaafea188052469536cdbd848c
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 11687.596, -3183.777, -76.583, 0, 0, 0, '{"col size y":2.837,"col orientation":2.359,"scale x":1,"scale y":1,"scale z":1,"col pos x":11687.596,"col size z":2.556,"col size x":2.339,"col pos y":-3183.777}')

-- 60252d5ca4fcf8ebc06f63cb2f20e4a3eb51e57b
SceneEditor:spawnShape("ge_mission_objet_pack_2.shape", 11686.504, -3181.458, -76.549, 0, 0, 0, '{"col size y":3.263,"col orientation":-2.662,"scale x":1,"scale y":1,"scale z":1,"col pos x":11686.504,"col size z":2.243,"col size x":2.321,"col pos y":-3181.458}')

-- 31c9b2dbe50bf542091ad43846f39ecf5eb94c7a
SceneEditor:spawnShape("ge_mission_barriere.shape", 11685.743, -3179.052, -76.58, 0, -0.15, -0.14, '{"col size y":0.188,"col orientation":-0.15,"scale x":1,"scale y":1,"scale z":1,"col pos x":11685.743,"col size z":2.303,"col size x":3.424,"col pos y":-3179.052}')

-- 2b1c4a6631d67a56b6de6e5854f732242614831a
SceneEditor:spawnShape("ge_mission_barriere.shape", 11688.278, -3180.858, -76.311, 0, 0, -1.04, '{"col size y":0.188,"col orientation":2.07,"scale x":1,"scale y":1,"scale z":1,"col pos x":11688.278,"col size z":2.303,"col size x":3.424,"col pos y":-3180.858}')

-- ab7609508286a800a7349377ea305ee14048f684
SceneEditor:spawnShape("ge_mission_barriere.shape", 11689.045, -3184.323, -76.413, 0, 0, -1.64, '{"col size y":0.188,"col orientation":1.49,"scale x":1,"scale y":1,"scale z":1,"col pos x":11689.045,"col size z":2.303,"col size x":3.424,"col pos y":-3184.323}')

-- 1b9a6853975fa4bf3d8c5e66e97d152ac373c759
SceneEditor:spawnShape("ge_mission_barriere.shape", 11689.89, -3190.818, -76.694, 0, 0, -0.64, '{"col size y":0.188,"col orientation":-0.65,"scale x":1,"scale y":1,"scale z":1,"col pos x":11689.89,"col size z":2.303,"col size x":3.424,"col pos y":-3190.818}')

-- dd84012927675329e9fea4254f5d11a5b1a1c82f
SceneEditor:spawnShape("ge_mission_barriere.shape", 11692.741, -3193.026, -76.681, 0, 0, -0.68, '{"col size y":0.188,"col orientation":-0.68,"scale x":1,"scale y":1,"scale z":1,"col pos x":11692.741,"col size z":2.303,"col size x":3.424,"col pos y":-3193.026}')

-- ed10319fe217a4432a97523e4811ed124321112f
SceneEditor:spawnShape("ge_mission_barriere.shape", 11695.473, -3195.362, -76.756, 0, 0, -0.7, '{"col size y":0.188,"col orientation":-3.86,"scale x":1,"scale y":1,"scale z":1,"col pos x":11695.473,"col size z":2.303,"col size x":3.424,"col pos y":-3195.362}')


