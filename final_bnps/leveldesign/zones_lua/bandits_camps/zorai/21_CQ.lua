-- GROUP: shadow_bandits
-- 2157b5548d1fa9d6dd74be3ba342fb30fe650e3b
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 10951.465, -3235.557, -78.609, 0, 0, -0.8, '{"col orientation":-0.82,"col pos y":-3235.557,"scale x":0.769,"col size x":2.065,"col size y":4.564,"scale y":0.769,"col pos x":10951.465,"scale z":0.769,"col size z":2.147}')

-- 19658389dce3412265565d956826b4b37364304e
SceneEditor:spawnShape("ge_mission_charette.shape", 10951.982, -3239.096, -78.643, 0, 0, -1.77, '{"col orientation":-1.8,"col pos y":-3239.096,"scale x":0.769,"col size x":1.905,"col size y":4.584,"scale y":0.769,"col pos x":10951.982,"scale z":0.769,"col size z":2.592}')

-- f8350dc7dba81064820342ed600dbac63deb803e
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 10968.706, -3241.165, -80.305, 0, 0, 0, '{"col orientation":2.502,"col pos y":-3241.165,"scale x":1,"col size x":0.891,"col size y":-0.817,"scale y":1,"col pos x":10968.706,"scale z":1,"col size z":5.674}')

-- 2f92ab4b94548666398b110def82e542f5e59aed
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 10966.001, -3249.818, -79.21, 0, 0, 0, '{"col orientation":0,"col pos y":-3249.818,"scale x":1,"col size x":0.891,"col size y":-0.817,"scale y":1,"col pos x":10966.001,"scale z":1,"col size z":5.674}')

-- cfd692be258bae4d62d1780a172ab466827d99ff
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 10962.24, -3222.939, -79.761, 0, 0, 0, '{"col orientation":0,"col pos y":-3222.939,"scale x":1,"col size x":-0.559,"col size y":-0.607,"scale y":1,"col pos x":10962.24,"scale z":1,"col size z":5.674}')

-- bc09fe430ef5d9b69c90686dc47f423cafb60d43
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 10952.796, -3223.469, -79.141, 0, 0, 0, '{"col orientation":0,"col pos y":-3223.469,"scale x":1,"col size x":0.841,"col size y":-0.867,"scale y":1,"col pos x":10952.796,"scale z":1,"col size z":5.674}')

-- 1c4fb04851df4480b1cce7ca06bd27951f2eaaff
SceneEditor:spawnShape("ge_feudecamp.ps", 10956.448, -3249.151, -77.983, 0, 0, 0, '{"col orientation":0,"col pos y":-3249.151,"scale x":1,"col size x":2.491,"col size y":2.608,"scale y":1,"col pos x":0,"scale z":1,"col size z":5.716}')

-- dc64de0e7a1efadab20ee0a5403bad1770a77cb0
SceneEditor:spawnShape("ge_mission_enclos.shape", 10966.002, -3229.926, -79.996, 0, 0, 2.35, '{"col orientation":-2.341,"col pos y":-3229.926,"scale x":1,"col size x":8.401,"col size y":7.043,"scale y":1,"col pos x":10966.002,"scale z":1,"col size z":6.629}')

-- 299857cb34c54e25b47ab7e2e98f2ac9661bc842
SceneEditor:spawnShape("ge_mission_mur_ruine.shape", 10967.714, -3254.599, -77.866, 0, 0, -1.57, '{"col orientation":-0.45,"col pos y":-3254.599,"scale x":1,"col size x":6.7,"col size y":6.848,"scale y":1,"col pos x":10967.714,"scale z":1,"col size z":5.325}')

-- b7f8f219da9d54840281c6d0203df45c4220354f
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 10953.199, -3254.614, -77.275, 0, 0, 0, '{"col orientation":-2.104,"col pos y":-3254.614,"scale x":1,"col size x":4.765,"col size y":4.346,"scale y":1,"col pos x":10953.199,"scale z":1,"col size z":3.99}')

-- 2e971dd5dc675049b059c2b891dccee67a783965
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 10994.416, -3233.782, -80.796, 0, 0, -0.82, '{"col orientation":-3.036,"col pos y":-3233.782,"scale x":1,"col size x":0.648,"col size y":0.652,"scale y":1,"col pos x":10994.416,"scale z":1,"col size z":0.935}')

-- 2c8948943626878c8f2a178dfd3da0f5e16cc68f
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 10993.896, -3233.399, -80.712, 0, 0, -2.36, '{"col orientation":2.45,"col pos y":-3233.399,"scale x":1,"col size x":0.648,"col size y":0.652,"scale y":1,"col pos x":10993.896,"scale z":1,"col size z":0.935}')

-- 2d51cf589ba55791543466de0e8cef3bd101e691
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 10994.434, -3233.307, -80.53, 0, 0, 0.7, '{"col orientation":3.131,"col pos y":-3233.307,"scale x":1,"col size x":0.648,"col size y":0.652,"scale y":1,"col pos x":10994.434,"scale z":1,"col size z":0.935}')

-- 9a13d6e69fb9b739aca1f087fdda5e8509689ea4
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10976.297, -3242.189, -81.848, 0, 0, -0.36, '{"col orientation":2.685,"col pos y":-3242.189,"scale x":1,"col size x":2.145,"col size y":1.103,"scale y":1,"col pos x":10976.297,"scale z":1,"col size z":2.481}')

-- 9cc78e55a92187d9b1149f821f5f2e676c9690c1
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10992.947, -3244.624, -82.334, 0, 0, -2.41, '{"col orientation":0.012,"col pos y":-3244.624,"scale x":1,"col size x":2.532,"col size y":1.299,"scale y":1,"col pos x":10992.947,"scale z":1,"col size z":2}')

-- d8910a249d4afe08f41a5d143ba2194675bc0ef7
SceneEditor:spawnShape("ge_mission_tente.shape", 10951.189, -3250.998, -77.243, 0, 0, -0.85, '{"col orientation":2.052,"col pos y":-3250.998,"scale x":1,"col size x":6.438,"col size y":5.775,"scale y":1,"col pos x":10951.189,"scale z":1,"col size z":5.034}')

-- f5e294768490a1a236e7c88736b493adc9b040ca
SceneEditor:spawnShape("ge_mission_tente.shape", 10993.171, -3236.34, -82.185, 0, 0, -4.14, '{"col orientation":0,"col pos y":-3236.34,"scale x":1,"col size x":6.438,"col size y":5.775,"scale y":1,"col pos x":10993.171,"scale z":1,"col size z":5.034}')

-- 4f991bbfe3d24e75bd1b02104154c7ba5309fbd9
SceneEditor:spawnShape("ge_mission_tente.shape", 10957.384, -3254.751, -77.477, 0, 0, 0.04, '{"col orientation":3.198,"col pos y":-3254.751,"scale x":1,"col size x":6.438,"col size y":5.775,"scale y":1,"col pos x":10957.384,"scale z":1,"col size z":5.034}')

-- e51cca03595cd2ebd42058f863e5217760b9478d
SceneEditor:spawnShape("ge_mission_barriere.shape", 10970.663, -3241.407, -80.547, 0, 0.2, -0.19, '{"col orientation":-0.21,"col pos y":-3241.407,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10970.663,"scale z":1,"col size z":2.303}')

-- d296213fb17c0c489e051acf6932cf3a33ca2534
SceneEditor:spawnShape("ge_mission_barriere.shape", 10973.942, -3242.215, -81.262, 0, 0.26, -0.32, '{"col orientation":-0.33,"col pos y":-3242.215,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10973.942,"scale z":1,"col size z":2.303}')

-- f8fdf81044878316b28820ce4d513610c1c2950c
SceneEditor:spawnShape("ge_mission_barriere.shape", 10977.194, -3243.594, -82.16, 0, 0.21, -0.51, '{"col orientation":-0.5,"col pos y":-3243.594,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10977.194,"scale z":1,"col size z":2.303}')

-- c79481314cf3826b5d7b4857df77c7347387067f
SceneEditor:spawnShape("ge_mission_barriere.shape", 10980.557, -3245.31, -82.648, 0, 0, -0.43, '{"col orientation":-3.592,"col pos y":-3245.31,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10980.557,"scale z":1,"col size z":2.303}')

-- 1cb109e456d43eb49d51f32419325c1e9e280598
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 10995.436, -3234.287, -80.827, 0, -0.39, -11.19, '{"col orientation":-1.61,"col pos y":-3234.287,"scale x":1,"col size x":5.411,"col size y":2.832,"scale y":1,"col pos x":10995.436,"scale z":1,"col size z":2.376}')

-- fc972e53832a3510bc4564a2ec77e170b21e7ee0
SceneEditor:spawnShape("ge_mission_barriere.shape", 10963.349, -3223.618, -79.697, 0, 0, 0, '{"col orientation":0,"col pos y":-3223.618,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10963.349,"scale z":1,"col size z":2.303}')

-- c78921cb306859a6fc3d643982c2a9d9509314f9
SceneEditor:spawnShape("ge_mission_barriere.shape", 10983.785, -3246.939, -82.687, 0, 0, -0.51, '{"col orientation":-0.51,"col pos y":-3246.939,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10983.785,"scale z":1,"col size z":2.303}')

-- 7e40b5cd30233447293ffae238ad4bf4f021c6d4
SceneEditor:spawnShape("ge_mission_barriere.shape", 10965.026, -3252.088, -78.318, 0, -0.35, -2.02, '{"col orientation":-2.02,"col pos y":-3252.088,"scale x":1,"col size x":4.15,"col size y":0.33,"scale y":1,"col pos x":10965.026,"scale z":1,"col size z":1}')

-- cd9c7c54dd67d5059224698d2f60c590d3587d45
SceneEditor:spawnShape("ge_mission_barriere.shape", 10963.439, -3255.04, -77.225, 0, -0.28, -2.06, '{"col orientation":-2.054,"col pos y":-3255.04,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10963.439,"scale z":1,"col size z":2.303}')

-- 7ce1f57316194da4b493d352ad62ff735c6dcfd0
SceneEditor:spawnShape("ge_mission_barriere.shape", 10987.095, -3247.799, -82.618, 0, 0, 0, '{"col orientation":0,"col pos y":-3247.799,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10987.095,"scale z":1,"col size z":2.303}')

-- daf49a6944cb6bb710053fd3b9371ed7b4f198f7
SceneEditor:spawnShape("ge_mission_barriere.shape", 10990.463, -3247.09, -82.536, 0, 0, 0.41, '{"col orientation":-2.73,"col pos y":-3247.09,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10990.463,"scale z":1,"col size z":2.303}')

-- 711da3c397c2f3b277e35f91c6da62f53c7a7d09
SceneEditor:spawnShape("ge_mission_barriere.shape", 10993.299, -3245.494, -82.24, 0, 0, -2.51, '{"col orientation":-2.514,"col pos y":-3245.494,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10993.299,"scale z":1,"col size z":2.303}')

-- 54c845098999caf037a75256f9ef061223975453
SceneEditor:spawnShape("ge_mission_barriere.shape", 10995.239, -3242.696, -82.369, 0, 0, -1.81, '{"col orientation":1.322,"col pos y":-3242.696,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10995.239,"scale z":1,"col size z":2.303}')

-- e03e8c98ba7aa83ab377d97ffcec9aae5a14e507
SceneEditor:spawnShape("ge_mission_barriere.shape", 10995.917, -3239.051, -82.301, 0, 0.16, -1.72, '{"col orientation":-1.72,"col pos y":-3239.051,"scale x":1,"col size x":3.88,"col size y":0.51,"scale y":1,"col pos x":10995.917,"scale z":1,"col size z":1}')

-- 29c539b2a894e7149f01c6e5d4e6e59765012b21
SceneEditor:spawnShape("ge_mission_barriere.shape", 10952.077, -3224.103, -78.789, 0, 0.13, 0, '{"col orientation":0,"col pos y":-3224.103,"scale x":1,"col size x":3.24,"col size y":0.33,"scale y":1,"col pos x":10952.077,"scale z":1,"col size z":1}')

-- ab216a70f43d9a285d147b833e12c6caf9f6a056
SceneEditor:spawnShape("ge_mission_barriere.shape", 10948.514, -3224.086, -77.974, 0, 0.24, 0, '{"col orientation":3.07,"col pos y":-3224.086,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10948.514,"scale z":1,"col size z":2.303}')

-- faedada62835d19ad826cf55f8948c950f326f72
SceneEditor:spawnShape("ge_mission_barriere.shape", 10967.031, -3223.609, -79.25, 0, -0.17, 0, '{"col orientation":-3.15,"col pos y":-3223.609,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10967.031,"scale z":1,"col size z":2.303}')

-- f83ed013ad9b8def2ff5ad9fdb4e0697d14c1940
SceneEditor:spawnShape("ge_mission_barriere.shape", 10970.647, -3223.588, -78.349, 0, -0.4, 0, '{"col orientation":0,"col pos y":-3223.588,"scale x":1,"col size x":3.424,"col size y":0.188,"scale y":1,"col pos x":10970.647,"scale z":1,"col size z":2.303}')


