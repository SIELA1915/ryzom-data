-- GROUP: birlds_of_omen
-- 1be79fd1b4e0e276dd8f026e6c0b23ca63b90db9
SceneEditor:spawnShape("ge_mission_barriere.shape", 10044.653, -3688.108, -76.925, 0, 0.12, -0.74, '{"col orientation":-0.78,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3688.108,"scale y":1,"scale x":1,"col pos x":10044.653,"scale z":1,"context":null,"url":null}')

-- 5dbfd42ef16bc6bfad357e2d1b56ae623deb6b23
SceneEditor:spawnShape("ge_mission_barriere.shape", 10055.418, -3686.407, -77.148, 0, 0, -2.39, '{"col orientation":-2.366,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3686.407,"scale y":1,"scale x":1,"col pos x":10055.418,"scale z":1,"context":null,"url":null}')

-- ea71051d9de804f3a313591da962ba73b0d04edc
SceneEditor:spawnShape("ge_mission_barriere.shape", 10057.374, -3683.698, -77.173, 0, 0, -2.01, '{"col orientation":-1.985,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3683.698,"scale y":1,"scale x":1,"col pos x":10057.374,"scale z":1,"context":null,"url":null}')

-- 5d3d3b5493bf460cd51e4542097ac21fd98c5525
SceneEditor:spawnShape("ge_mission_barriere.shape", 10040.002, -3683.083, -76.547, 0, 0.05, -0.86, '{"col orientation":-0.86,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3683.037,"scale y":1,"scale x":1,"col pos x":10040.021,"scale z":1,"context":null,"url":null}')

-- b72cb4e971ea7784bee239a561b33c7ba121cc8f
SceneEditor:spawnShape("ge_mission_barriere.shape", 10042.253, -3685.701, -76.655, 0, 0.06, -0.86, '{"col orientation":-0.84,"col size y":0.188,"col size x":3.424,"col size z":2.303,"col pos y":-3685.667,"scale y":1,"scale x":1,"col pos x":10042.277,"scale z":1,"context":null,"url":null}')

-- 2c4fcd381eab76880a6f2f8fd1e97a47362be545
SceneEditor:spawnShape("ge_mission_barriere.shape", 10047.903, -3689.212, -77.207, 0, 0.04, 0, '{"col orientation":0,"col size y":-0.32,"col size x":3.56,"col size z":1,"col pos y":-3689.212,"scale y":1,"scale x":1,"col pos x":10047.903,"scale z":1,"context":null,"url":null}')

-- a881c612a8c4191cacdfc4a8f3dfbc74c1e8feba
SceneEditor:spawnShape("ge_mission_tente.shape", 10036.671, -3680.219, -76.229, 0, 0, -1.21, '{"col orientation":0.55,"col size y":4.825,"col size x":5.488,"col size z":5.034,"col pos y":-3680.219,"scale y":1,"scale x":1,"col pos x":10036.671,"scale z":1,"context":null,"url":null}')

-- e93bcebcdcf3f4275156cc2ae82550252cc7aeae
SceneEditor:spawnShape("ge_mission_tente.shape", 10052.088, -3688.808, -77.302, 0, 0, 0.65, '{"col orientation":2.056,"col size y":5.775,"col size x":6.438,"col size z":5.034,"col pos y":-3688.808,"scale y":1,"scale x":1,"col pos x":10052.088,"scale z":1,"context":null,"url":null}')


