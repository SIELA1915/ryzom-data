-- GROUP: vilains_of_the_void
-- dbcefb58a10ebcd761b020c2685e53540c0aa158
SceneEditor:spawnShape("ju_s3_plante.shape", 10919.196, -2363.175, -79.315, 0, 0, 0, '{"col size y":5.849,"col orientation":0,"col pos y":-2363.175,"col size x":5.976,"scale x":1,"col size z":3.21,"scale y":1,"scale z":1,"col pos x":10919.196}')

-- c742f7aaacf06ff1a8d2dd2a57597a442b983bdf
SceneEditor:spawnShape("ge_mission_objet_pack_4.shape", 10922.766, -2361.823, -79.437, 0, 0, -1.75, '{"col size y":4.345,"col orientation":-0.961,"col pos y":-2361.823,"col size x":3.046,"scale x":1,"col size z":2.048,"scale y":1,"scale z":1,"col pos x":10922.766}')

-- 27469c52d26cd7ebcab4a2b515f7ae93d4499150
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 10918.361, -2364.925, -79.315, 0, 0, -0.8, '{"col size y":4.365,"col orientation":0.202,"col pos y":-2364.925,"col size x":3.835,"scale x":1,"col size z":3.046,"scale y":1,"scale z":1,"col pos x":10918.361}')

-- c517c3fb754df2c34db3dff5854a36244fcd83b0
SceneEditor:spawnShape("ge_mission_crane.shape", 10928.117, -2370.907, -79.96, 0, 0, -5.12, '{"col size y":5.782,"col orientation":0.202,"col pos y":-2370.907,"col size x":5.192,"scale x":1,"col size z":6.826,"scale y":1,"scale z":1,"col pos x":10928.117}')

-- 3286b0738b34cbb64c6aa71994ac3e853cd533e1
SceneEditor:spawnShape("ju_s3_bamboo.shape", 10949.227, -2393.336, -77.303, 0, 0, 0, '{"col size y":4.612,"col orientation":0,"col pos y":-2393.336,"col size x":4.404,"scale x":1,"col size z":6.611,"scale y":1,"scale z":1,"col pos x":0}')

-- 1da263335188cbc18397d11bbf0f3765dc4279dd
SceneEditor:spawnShape("ju_s3_bamboo.shape", 10935.028, -2360.011, -79.313, 0, 0, 0, '{"col size y":4.612,"col orientation":-2.519,"col pos y":-2360.011,"col size x":4.404,"scale x":1,"col size z":6.611,"scale y":1,"scale z":1,"col pos x":0}')

-- 4880068eff396db2f17a5f0f8068ecdcd7e0c2fa
SceneEditor:spawnShape("ju_s3_bamboo.shape", 10915.361, -2372.221, -79.135, 0, 0, 0, '{"col size y":4.612,"col orientation":-0.49,"col pos y":-2372.221,"col size x":4.404,"scale x":1,"col size z":6.611,"scale y":1,"scale z":1,"col pos x":0}')

-- 0acab33580996102023d27586164dfebe7a6372c
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 10933.99, -2383.478, -79.033, 0, 0, 0, '{"col size y":4.365,"col orientation":-2.298,"col pos y":-2383.478,"col size x":3.835,"scale x":1,"col size z":3.046,"scale y":1,"scale z":1,"col pos x":10933.99}')

-- 1a850bb5eed3dbee90c2d3bab0137d9b83781bc9
SceneEditor:spawnShape("ge_mission_charette.shape", 10940.444, -2372.58, -79.591, 0, 0, 0.74, '{"col size y":5.184,"col orientation":0,"col pos y":-2372.58,"col size x":2.505,"scale x":0.769,"col size z":2.592,"scale y":0.769,"scale z":0.769,"col pos x":10940.444}')

-- 309ec801dffde0d3240da2efa8bc2a64395f2c4c
SceneEditor:spawnShape("ge_mission_tente.shape", 10918.547, -2379.615, -78.981, 0, 0, -1.09, '{"col size y":5.775,"col orientation":0.215,"col pos y":-2379.615,"col size x":6.438,"scale x":1,"col size z":5.034,"scale y":1,"scale z":1,"col pos x":10918.547}')

-- 44354f0076e3362835851f1149ddc944ddde6ff5
SceneEditor:spawnShape("ge_mission_tente.shape", 10922.946, -2383.665, -78.728, 0, 0, -0.37, '{"col size y":5.775,"col orientation":0,"col pos y":-2383.665,"col size x":6.438,"scale x":1,"col size z":5.034,"scale y":1,"scale z":1,"col pos x":10922.946}')

-- 9a03df59b15784f49ef382e448f2076fccdb900e
SceneEditor:spawnShape("ge_mission_tente.shape", 10937.963, -2367.146, -79.237, 0, 0, -4.44, '{"col size y":5.775,"col orientation":0.48,"col pos y":-2367.146,"col size x":6.438,"scale x":1,"col size z":5.034,"scale y":1,"scale z":1,"col pos x":10937.963}')


