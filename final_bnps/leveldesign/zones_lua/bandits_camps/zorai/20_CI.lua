-- GROUP: flights_of_fury
-- 5f2ca19cd52f99fc33856da0f9f638a156c64723
SceneEditor:spawnShape("ge_mission_charette.shape", 9674.778, -3102.267, -75.503, -0.13, 0.4, 0, '{"scale y":0.769,"col size x":1.595,"col pos y":-3102.2,"col orientation":-0.026,"col size z":2.592,"scale x":0.769,"col size y":3.394,"col pos x":9675.084,"scale z":0.769}')

-- e76cf99f6bcdb7552b97c882d2b65de844019248
SceneEditor:spawnShape("ju_s2_young_tree.shape", 9672.866, -3105.812, -74.149, 0, 0, 0, '{"scale y":1,"col size x":10.921,"col pos y":-3116.319,"col orientation":0,"col size z":16.447,"scale x":1,"col size y":13.672,"col pos x":0,"scale z":1}')

-- b7aba357cdc2cfdc2438ab40ebb98a79cf928648
SceneEditor:spawnShape("ju_s3_fantree.shape", 9695.75, -3098.798, -79.274, 0, 0, 0, '{"scale y":1,"col size x":2.466,"col pos y":-3098.798,"col orientation":0,"col size z":8.502,"scale x":1,"col size y":2.199,"col pos x":9695.75,"scale z":1}')

-- c05ed6ca1404aae775128516cc2b4d551cffa31d
SceneEditor:spawnShape("ge_mission_sac_a.shape", 9670.852, -3099.455, -74.127, 0, 0, 0, '{"scale y":1,"col size x":0.967,"col pos y":-3099.455,"col orientation":-2.003,"col size z":0.404,"scale x":1,"col size y":1.165,"col pos x":9670.852,"scale z":1}')

-- 5f8051450df8b0f2f1f12cb89806154987b1f362
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 9670.19, -3099.151, -73.801, 0, 0, 0, '{"scale y":1,"col size x":1.054,"col pos y":-3099.151,"col orientation":0.003,"col size z":1.241,"scale x":1,"col size y":1.054,"col pos x":9670.19,"scale z":1}')

-- 4e8b9342441c7bafaa12c558c675c7a02e1dafa8
SceneEditor:spawnShape("ju_s3_banana_tree.shape", 9670.769, -3090.406, -72.999, 0, 0, 0, '{"scale y":1,"col size x":4.551,"col pos y":-3090.406,"col orientation":-0.77,"col size z":3.102,"scale x":1,"col size y":4.448,"col pos x":0,"scale z":1}')

-- 15c03e77f1cd12b2cb51c9075ef4985236163bbf
SceneEditor:spawnShape("ge_mission_barriere.shape", 9684.188, -3086.567, -74.325, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3086.567,"col orientation":-3.13,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9684.188,"scale z":1}')

-- 02d770332abe4c6cde9cad7b56f418959987414d
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 9687.083, -3092.279, -75.578, 0, 0.42, -7.7, '{"scale y":1,"col size x":5.411,"col pos y":-3092.279,"col orientation":1.7,"col size z":2.376,"scale x":1,"col size y":2.832,"col pos x":9687.083,"scale z":1}')

-- fc9b5b5870a77ab7da4c32a1975e95b6158cddab
SceneEditor:spawnShape("ge_mission_barriere.shape", 9685.691, -3088.13, -74.172, 0, 0, -1.8, '{"scale y":1,"col size x":3.424,"col pos y":-3088.13,"col orientation":-1.747,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9685.691,"scale z":1}')

-- cb5dfe25242e063f2f55266cb554625fd114eaac
SceneEditor:spawnShape("ge_mission_barriere.shape", 9673.846, -3087.224, -72.964, 0, 0, 0.16, '{"scale y":1,"col size x":3.424,"col pos y":-3087.224,"col orientation":0.16,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9673.846,"scale z":1}')

-- 2d7ddd2f4ad37dcf2cbe38fbd40e066518f8a3bb
SceneEditor:spawnShape("ge_mission_mur_ruine_b.shape", 9665.433, -3096.495, -72.555, 0, 0, -3.47, '{"scale y":1,"col size x":8.742,"col pos y":-3096.495,"col orientation":0.57,"col size z":6.018,"scale x":1,"col size y":7.983,"col pos x":9665.433,"scale z":1}')

-- a9feb0178df4ec21a7918861f6dc8914ea0746c2
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 9684.608, -3088.076, -74.401, 0, 0, 0, '{"scale y":1,"col size x":2.339,"col pos y":-3088.076,"col orientation":0.032,"col size z":2.556,"scale x":1,"col size y":2.837,"col pos x":9684.608,"scale z":1}')

-- c96450c534e59fe0a5484164bbf1ee3e51e3b489
SceneEditor:spawnShape("ge_mission_feu_off.shape", 9691.438, -3083.712, -71.633, 0, 0, 0, '{"scale y":1,"col size x":2.551,"col pos y":-3083.712,"col orientation":0,"col size z":0.642,"scale x":1,"col size y":2.735,"col pos x":9691.438,"scale z":1}')

-- bd0f1605280d404be45dd1f04cccda5bde991550
SceneEditor:spawnShape("ge_mission_sac_b.shape", 9688.7, -3094.563, -76.957, 0, 0, 0, '{"scale y":1,"col size x":1.7,"col pos y":-3094.563,"col orientation":0.103,"col size z":0.968,"scale x":1,"col size y":1.62,"col pos x":9688.7,"scale z":1}')

-- 3564bb0dca8b85e0c92c3864b11d5fe261d69a04
SceneEditor:spawnShape("ge_mission_barriere.shape", 9685.029, -3107.317, -77.434, 0, 0, -2.04, '{"scale y":1,"col size x":3.424,"col pos y":-3107.317,"col orientation":1.1,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9685.029,"scale z":1}')

-- ffd6d7db12dfbde97a0e7ec022f3dd63111aee88
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9681.548, -3107.498, -76.857, 0, 0, 0, '{"scale y":0.833,"col size x":1.173,"col pos y":-3107.498,"col orientation":0,"col size z":1.192,"scale x":0.833,"col size y":1.078,"col pos x":9681.548,"scale z":0.833}')

-- eb0ff4ce5a9ed6691b5c9fd4d10bc74676177d3f
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 9682.889, -3108.747, -76.784, 0, 0, 0, '{"scale y":1,"col size x":2.532,"col pos y":-3108.747,"col orientation":0,"col size z":2,"scale x":1,"col size y":1.299,"col pos x":9682.889,"scale z":1}')

-- 7ebfb4fde560e7a3d11422fea099eabaca25a536
SceneEditor:spawnShape("ge_mission_tente.shape", 9677.552, -3106.46, -76.191, 0, 0, -0.58, '{"scale y":1,"col size x":6.438,"col pos y":-3106.46,"col orientation":-2.216,"col size z":5.034,"scale x":1,"col size y":6.705,"col pos x":9677.552,"scale z":1}')

-- c0ad0309e8090a042349b7ebf8af4cc3c3a93f7e
SceneEditor:spawnShape("ge_mission_objet_pack_5.shape", 9688.342, -3097.098, -77.77, 0, 0, 0, '{"scale y":1,"col size x":3.71,"col pos y":-3096.877,"col orientation":0,"col size z":2.53,"scale x":1,"col size y":4.8,"col pos x":9687.986,"scale z":1}')

-- 24d8232458860a24af8d1cf26713030ee4d0b612
SceneEditor:spawnShape("ge_mission_barriere.shape", 9671.272, -3101.15, -74.401, 0, 0.19, -0.82, '{"scale y":1,"col size x":3.56,"col pos y":-3101.15,"col orientation":-3.94,"col size z":2.36,"scale x":1,"col size y":0.28,"col pos x":9671.272,"scale z":1}')

-- 5c8a02673325c51d8883f3bbac8c97e052a081b7
SceneEditor:spawnShape("ge_mission_barriere.shape", 9673.808, -3104, -75.142, 0, 0.2, -0.83, '{"scale y":1,"col size x":3.424,"col pos y":-3104,"col orientation":-0.81,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9673.808,"scale z":1}')

-- ff418a0b34df99a7e43e77691d6f2b8312fa0ade
SceneEditor:spawnShape("ge_mission_barriere.shape", 9667.225, -3089.988, -72.162, 0, 0, -2.36, '{"scale y":1,"col size x":3.424,"col pos y":-3089.988,"col orientation":-2.383,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9667.225,"scale z":1}')

-- 3a86ce90c0baf2dacc4b17f4a0465bd170f2fd13
SceneEditor:spawnShape("ge_mission_barriere.shape", 9670.31, -3088.116, -72.287, 0, 0, -2.77, '{"scale y":1,"col size x":3.424,"col pos y":-3088.116,"col orientation":-2.768,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9670.31,"scale z":1}')


