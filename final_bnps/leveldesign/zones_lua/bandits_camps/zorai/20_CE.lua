-- GROUP: zora_kovans
-- 6cc5da43d778a330b12e4aac1f0fd2d2956f0cc5
SceneEditor:spawnShape("ge_mission_1_caisse.shape", 9045.309, -3075.056, -80.649, 0, 0, 0, '{"scale y":1,"col size x":1.044,"col pos y":-3075.096,"col orientation":-3.117,"col size z":1,"scale x":1,"col size y":1.044,"col pos x":9045.287,"scale z":1}')

-- a2c4554e49fc1d6e8bd674acdebad07ab04637b2
SceneEditor:spawnShape("ge_mission_tente.shape", 9052.851, -3077.553, -80.494, 0, 0, -10.49, '{"scale y":1,"col size x":6.438,"col pos y":-3077.553,"col orientation":0.538,"col size z":5.034,"scale x":1,"col size y":5.775,"col pos x":9052.851,"scale z":1}')

-- 5a4f4c443e1b92a7f9daadd9e31c22a2e07bb70d
SceneEditor:spawnShape("ge_mission_jarre_couchee.shape", 9039.47, -3094.347, -80.366, 0, 0, -1.17, '{"scale y":1,"col size x":0.648,"col pos y":-3094.347,"col orientation":-0.492,"col size z":0.935,"scale x":1,"col size y":0.652,"col pos x":9039.47,"scale z":1}')

-- 15f3781be4af7c215a71ee8c15fef58cd76c812c
SceneEditor:spawnShape("ju_s3_bamboo.shape", 9046.099, -3094.387, -80.381, 0, 0, 0, '{"scale y":1,"col size x":4.404,"col pos y":-3094.387,"col orientation":-0.517,"col size z":6.611,"scale x":1,"col size y":4.612,"col pos x":9046.099,"scale z":1}')

-- e4691e638480e47244aab1137d69820dfc9e13c9
SceneEditor:spawnShape("ju_s3_plantegrasse.shape", 9034.059, -3095.341, -81.604, 0, 0, -0.14, '{"scale y":1,"col size x":1,"col pos y":-3095.341,"col orientation":-0.14,"col size z":1,"scale x":1,"col size y":1,"col pos x":0,"scale z":1}')

-- 15e77c035b9afb5b124023826bbfea699f5b9fa4
SceneEditor:spawnShape("ge_mission_borne.shape", 9035.729, -3083.339, -80.978, 0, 0, -2.14, '{"scale y":1,"col size x":0.3,"col pos y":-3083.36,"col orientation":-0.438,"col size z":0.821,"scale x":1,"col size y":0.3,"col pos x":9035.742,"scale z":1}')

-- 6e056c433313085eab07f6e40e7d97d64bb76e23
SceneEditor:spawnShape("ge_mission_borne.shape", 9036.808, -3080.441, -80.974, 0, 0, 0, '{"scale y":1,"col size x":0.3,"col pos y":-3080.441,"col orientation":-0.428,"col size z":0.821,"scale x":1,"col size y":0.3,"col pos x":9036.808,"scale z":1}')

-- 211c0bb96cf8b135bfb4b579af5380b2aa960988
SceneEditor:spawnShape("ge_mission_charette_ok.shape", 9031.475, -3091.798, -81.899, 0, 0, 0, '{"scale y":0.769,"col size x":-3.465,"col pos y":-3092.26,"col orientation":-0.035,"col size z":1.937,"scale x":0.769,"col size y":3.474,"col pos x":9032.034,"scale z":0.769}')

-- cbd417ddaad44aaff023b6346306c4fa144446ab
SceneEditor:spawnShape("ge_feudecamp.ps", 9044.65, -3082.838, -80.574, 0, 0, 0, '{"scale y":1,"col size x":2.491,"col pos y":-3082.838,"col orientation":0,"col size z":5.716,"scale x":1,"col size y":2.608,"col pos x":-0.678,"scale z":1}')

-- cfb1df5995927baccc6b1704484b2067ef62e252
SceneEditor:spawnShape("ge_mission_barriere.shape", 9038.08, -3095.139, -80.899, 0, -0.05, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3095.139,"col orientation":-3.102,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9038.08,"scale z":1}')

-- d90f5e9149eef5df11940f6193dcd56a388cd900
SceneEditor:spawnShape("ge_mission_barriere.shape", 9036.64, -3083.899, -81.118, 0, 0, -2.11, '{"scale y":1,"col size x":3.424,"col pos y":-3083.799,"col orientation":1.04,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9036.695,"scale z":1}')

-- 6728ed24a6b6148c8c7f12d2a02b102d42f64013
SceneEditor:spawnShape("ge_mission_barriere.shape", 9037.736, -3080.37, -81.06, 0, 0, -1.64, '{"scale y":1,"col size x":3.424,"col pos y":-3080.416,"col orientation":-4.77,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9037.704,"scale z":1}')

-- 63199e6adfaf210858d82821bab7d5a5d236c8dd
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 9033.597, -3093.427, -81.317, 0, 0, -4.06, '{"scale y":1,"col size x":5.201,"col pos y":-3092.726,"col orientation":-0.92,"col size z":2.376,"scale x":1,"col size y":-0.418,"col pos x":9034.748,"scale z":1}')

-- c4fb54064aef826e69e1af1f9dae78ebd5f05d7f
SceneEditor:spawnShape("ge_mission_tente.shape", 9042.555, -3094.628, -80.261, 0, 0, -5.84, '{"scale y":1,"col size x":6.438,"col pos y":-3094.301,"col orientation":0.285,"col size z":5.034,"scale x":1,"col size y":5.775,"col pos x":9042.91,"scale z":1}')

-- 9f3c11d2eff253b15f9957b9909c43ee66d298fb
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9049.711, -3075.288, -80.53, 0, 0, -1.03, '{"scale y":0.833,"col size x":1.173,"col pos y":-3075.357,"col orientation":0,"col size z":1.192,"scale x":0.833,"col size y":1.078,"col pos x":9049.813,"scale z":0.833}')

-- a333a4eb9bbdf8b297746c70959359fe32f583c5
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 9041.973, -3076.429, -80.772, 0, 0, -1.65, '{"scale y":1,"col size x":2.145,"col pos y":-3076.382,"col orientation":-1.7,"col size z":2.481,"scale x":1,"col size y":1.103,"col pos x":9041.82,"scale z":1}')

-- bb9c1e3d393e1cbcffddc370bc3d4da5fd4bf73a
SceneEditor:spawnShape("ge_mission_barriere.shape", 9052.865, -3082.834, -80.573, 0, 0, -1.55, '{"scale y":1,"col size x":3.424,"col pos y":-3082.955,"col orientation":1.607,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9052.926,"scale z":1}')

-- cf53cf8157582df599b3cf6c05a9d15b3be75f16
SceneEditor:spawnShape("ge_mission_barriere.shape", 9047.217, -3074.411, -80.573, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3074.411,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9047.217,"scale z":1}')

-- 013f62d76884a3108056eced0219bee8e37930a7
SceneEditor:spawnShape("ge_mission_barriere.shape", 9043.377, -3074.412, -80.717, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3074.379,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9043.402,"scale z":1}')

-- 1efc402380465620cd8c98307025ca63b0cc045c
SceneEditor:spawnShape("ge_mission_charette.shape", 9055.769, -3081.561, -80.521, 0, 0, -0.7, '{"scale y":0.769,"col size x":2.005,"col pos y":-3081.667,"col orientation":5.573,"col size z":2.432,"scale x":0.769,"col size y":3.794,"col pos x":9055.615,"scale z":0.769}')

-- 612ca757568ac4ae1b7d30bb2ac3a5fcccec149f
SceneEditor:spawnShape("zo_wea_masse2m.shape", 9050.295, -3088.915, -80.536, -0.06, 0, 0, '{"scale y":1,"col size x":0.079,"col pos y":-3088.915,"col orientation":0,"col size z":0.083,"scale x":1,"col size y":1.286,"col pos x":0,"scale z":1}')

-- 7d3a5a540bc352efee707548546ab575859dc5ff
SceneEditor:spawnShape("zo_wea_hache1m.shape", 9050.384, -3092.01, -80.489, -0.07, -0.38, -3.66, '{"scale y":1,"col size x":0.296,"col pos y":-3092.01,"col orientation":-2.585,"col size z":0.047,"scale x":1,"col size y":0.97,"col pos x":0,"scale z":1}')

-- b406393dc1691c056c02a2f11aedf9e26fb62f6a
SceneEditor:spawnShape("zo_wea_grand_bouclier.shape", 9050.111, -3092.209, -80.347, 0.03, 1.16, -0.32, '{"scale y":1,"col size x":0.446,"col pos y":-3092.209,"col orientation":-1.639,"col size z":0.255,"scale x":1,"col size y":0.869,"col pos x":0,"scale z":1}')

-- 5521d66d00f85c38bf9ea6d2b30f279d8b2ea0cd
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9046.82, -3088.302, -80.485, 0, 0, 0, '{"scale y":0.833,"col size x":1.503,"col pos y":-3089.009,"col orientation":0.08,"col size z":1.192,"scale x":0.833,"col size y":2.448,"col pos x":9047.207,"scale z":0.833}')

-- 7fb40b8b53616dffe371c1b6e6011e9d2e57f2ab
SceneEditor:spawnShape("ge_mission_tombe_e.shape", 9049.831, -3091.642, -80.536, 0, 0, -1.51, '{"scale y":1,"col size x":1.334,"col pos y":-3091.642,"col orientation":-0.414,"col size z":1.192,"scale x":1,"col size y":0.539,"col pos x":9049.831,"scale z":1}')

-- 6d50bd182c9dd4b7588e7965a17307a739381c97
SceneEditor:spawnShape("ge_mission_tombe_b.shape", 9049.733, -3088.698, -80.552, 0, 0, -1.44, '{"scale y":1,"col size x":1.029,"col pos y":-3088.698,"col orientation":1.173,"col size z":1.93,"scale x":1,"col size y":0.515,"col pos x":9049.733,"scale z":1}')

-- a89b3c1ea0e830f5e5450ca4326fe4f6c52a2bb1
SceneEditor:spawnShape("ge_mission_barriere.shape", 9053.432, -3093.497, -80.537, 0, 0, 0, '{"scale y":1,"col size x":3.424,"col pos y":-3093.497,"col orientation":0.005,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9053.432,"scale z":1}')

-- 471f847a6509f6f843c64722af3358d5791058bc
SceneEditor:spawnShape("ge_mission_barriere.shape", 9052.781, -3084.881, -80.556, 0, 0, 0, '{"scale y":1,"col size x":3.544,"col pos y":-3084.86,"col orientation":-3.15,"col size z":2.303,"scale x":1,"col size y":-0.212,"col pos x":9052.631,"scale z":1}')

-- 595657809620f4839319dca54d87c0c6e9f1c2f1
SceneEditor:spawnShape("ge_mission_barriere.shape", 9049.333, -3085.922, -80.571, 0, 0, -2.53, '{"scale y":1,"col size x":3.424,"col pos y":-3085.922,"col orientation":0.61,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9049.333,"scale z":1}')

-- 57ab8af50e0cf51b30c838c939247d69ce8f85ef
SceneEditor:spawnShape("ge_mission_barriere.shape", 9048.092, -3088.83, -80.437, 0, 0, -1.5, '{"scale y":1,"col size x":3.424,"col pos y":-3088.83,"col orientation":4.781,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9048.092,"scale z":1}')

-- 0bc70abd89930f3eb78e405cd5823a6861a9c0fb
SceneEditor:spawnShape("ge_mission_barriere_t.shape", 9048.31, -3092.234, -80.521, 0, 0, 0, '{"scale y":1,"col size x":5.411,"col pos y":-3092.234,"col orientation":0,"col size z":2.376,"scale x":1,"col size y":2.832,"col pos x":9048.31,"scale z":1}')


