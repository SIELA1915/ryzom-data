-- GROUP: plain_scourers
-- a8412238707698a5b9e6fe19a16bb92193569092
SceneEditor:spawnShape("ju_s3_tree.shape", 11674.812, -3183.769, -76.901, 0, 0, 0, '{"col size y":17.185,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":-1.518,"col size z":19.556,"col size x":14.295,"col pos y":-3183.769}')

-- 474a3ecc322d4e539fe89da6cbf5959ac0dda89d
SceneEditor:spawnShape("ge_mission_barriere.shape", 11674.833, -3179.48, -76.823, 0, 0, -2.41, '{"col size y":0.188,"col orientation":0.73,"scale x":1,"scale y":1,"scale z":1,"col pos x":11674.833,"col size z":2.303,"col size x":3.424,"col pos y":-3179.48}')

-- 474d931128bc88e3c0267a30878dfb2e7ab17995
SceneEditor:spawnShape("ge_mission_barriere.shape", 11672.744, -3196.523, -76.815, 0, 0, -1.8, '{"col size y":0.188,"col orientation":-4.95,"scale x":1,"scale y":1,"scale z":1,"col pos x":11672.744,"col size z":2.303,"col size x":3.424,"col pos y":-3196.523}')

-- 32ffa3fa1ee9bfe4bd592223d1efa5d573eaa73b
SceneEditor:spawnShape("ge_mission_barriere.shape", 11673.254, -3182.487, -76.824, 0, 0, -1.73, '{"col size y":0.188,"col orientation":-1.75,"scale x":1,"scale y":1,"scale z":1,"col pos x":11673.254,"col size z":2.303,"col size x":3.424,"col pos y":-3182.487}')

-- 8b6a36814d1e1241601d5d5f40a27cf1aeaf9380
SceneEditor:spawnShape("ge_mission_barriere.shape", 11672.879, -3186.129, -76.861, 0, 0, -1.67, '{"col size y":0.188,"col orientation":0,"scale x":1,"scale y":1,"scale z":1,"col pos x":2488.176,"col size z":2.303,"col size x":3.424,"col pos y":-3186.129}')

-- 4a6b2b7d9aecd6ef6ec94a9ac17fc0c609bbfbd8
SceneEditor:spawnShape("ge_mission_barriere.shape", 11673.279, -3189.791, -76.873, 0, 0, -1.24, '{"col size y":0.188,"col orientation":-1.187,"scale x":1,"scale y":1,"scale z":1,"col pos x":11673.279,"col size z":2.303,"col size x":3.424,"col pos y":-3189.791}')

-- ab8576a9079482533f7ffe48d02a6ef6f0898173
SceneEditor:spawnShape("ge_mission_tente.shape", 11675.566, -3193.818, -76.969, 0, 0, -2.08, '{"col size y":5.775,"col orientation":-0.7,"scale x":1,"scale y":1,"scale z":1,"col pos x":11675.566,"col size z":5.034,"col size x":6.438,"col pos y":-3193.818}')


