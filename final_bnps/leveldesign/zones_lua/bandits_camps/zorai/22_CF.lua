-- GROUP: eyes_of_intuition
-- efde4f49dc6b7a2a69fbbb6b5ae0039a14dc0438
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 9175.842, -3469.192, -77.198, 0, 0, -4.74, '{"scale y":1,"col size x":2.145,"col pos y":-3469.192,"col orientation":0,"col size z":2.481,"scale x":1,"col size y":1.103,"col pos x":9175.842,"scale z":1}')

-- 29b0045cee3a3449fb7586880f3424ddf4aae0c7
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 9184.131, -3476.45, -77.48, 0, 0, 0, '{"scale y":0.833,"col size x":1.173,"col pos y":-3476.45,"col orientation":-1.13,"col size z":1.192,"scale x":0.833,"col size y":1.078,"col pos x":9184.131,"scale z":0.833}')

-- 5cd504096d22e8218643005779391305e64a7d1b
SceneEditor:spawnShape("ge_feudecamp.shape", 9184.703, -3456.526, -73.571, 0, 0, 0, '{"scale y":1,"col size x":2.002,"col pos y":-3456.526,"col orientation":0,"col size z":0.522,"scale x":1,"col size y":2.094,"col pos x":0,"scale z":1}')

-- f80616281e7278dbf18c6d9598a860c4f8af2948
SceneEditor:spawnShape("ge_feudecamp.ps", 9183.505, -3468.192, -76.913, 0, 0, 0, '{"scale y":1,"col size x":2.491,"col pos y":-3468.192,"col orientation":0,"col size z":5.716,"scale x":1,"col size y":2.608,"col pos x":0,"scale z":1}')

-- a4d1f27924a3244ee91907455e10e56c093ab434
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9181.455, -3444.744, -71.497, 0, 0, 0, '{"scale y":1,"col size x":0.911,"col pos y":-3444.744,"col orientation":0,"col size z":5.674,"scale x":1,"col size y":-0.797,"col pos x":9181.455,"scale z":1}')

-- db46eed5a98f7172614e66bde9d5aa8947860412
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9174.813, -3448.162, -72.88, 0, 0, 0, '{"scale y":1,"col size x":0.941,"col pos y":-3448.162,"col orientation":0,"col size z":5.674,"scale x":1,"col size y":-0.767,"col pos x":9174.813,"scale z":1}')

-- 230208d866d4e4b005fa5dc666ead5e6e615537e
SceneEditor:spawnShape("ge_mission_barriere.shape", 9201.638, -3459.7, -74.888, 0, 0.23, -0.78, '{"scale y":1,"col size x":3.424,"col pos y":-3459.7,"col orientation":5.541,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9201.638,"scale z":1}')

-- 758ae650316b08a2ac21eaeb19e214eaef11074d
SceneEditor:spawnShape("ge_mission_tente.shape", 9203.733, -3463.68, -76.018, 0, 0, -4.5, '{"scale y":1,"col size x":6.438,"col pos y":-3463.68,"col orientation":0,"col size z":5.034,"scale x":1,"col size y":5.775,"col pos x":9203.733,"scale z":1}')

-- 845d64f39d8ed66618210ef595d4e43f228873e7
SceneEditor:spawnShape("ge_mission_hut.shape", 9194.404, -3450.477, -73.218, 0.04, -0.13, -3.64, '{"scale y":1,"col size x":8.037,"col pos y":-3450.477,"col orientation":0.98,"col size z":4.669,"scale x":1,"col size y":9.64,"col pos x":9194.404,"scale z":1}')

-- 87f093efabb68b8260e42e0c877c3128cddd72f9
SceneEditor:spawnShape("ge_mission_barriere.shape", 9177.008, -3472.966, -77.216, 0, 0, -0.97, '{"scale y":1,"col size x":3.424,"col pos y":-3472.966,"col orientation":2.09,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9177.008,"scale z":1}')

-- 66de26dcdc78b060d602cc1eedeb8261731a053b
SceneEditor:spawnShape("ge_mission_barriere.shape", 9199.438, -3456.987, -73.841, 0, -0.18, -4.23, '{"scale y":1,"col size x":3.424,"col pos y":-3456.987,"col orientation":2.02,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9199.438,"scale z":1}')

-- d5ce23196dfa98356eb59fed07dc25215ed920c4
SceneEditor:spawnShape("ge_mission_barriere.shape", 9197.799, -3453.764, -73.497, 0, 0, -1.21, '{"scale y":1,"col size x":3.424,"col pos y":-3453.764,"col orientation":-1.21,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9197.799,"scale z":1}')

-- c78c95911a3e5b1244303191b24e2f15f6a46e6e
SceneEditor:spawnShape("ge_mission_barriere.shape", 9207.334, -3467.691, -77.304, 0, 0, -1.21, '{"scale y":1,"col size x":3.424,"col pos y":-3467.691,"col orientation":-1.2,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9207.334,"scale z":1}')

-- 2767142ffbe4e68789b1b2646f8df0c8d3ab83bb
SceneEditor:spawnShape("ge_mission_barriere.shape", 9182.731, -3445.027, -71.483, 0, 0, -2.63, '{"scale y":1,"col size x":3.424,"col pos y":-3445.027,"col orientation":3.664,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9182.731,"scale z":1}')

-- 4b0227d237b2bd19340c0a5a7306e5a146bc9a2e
SceneEditor:spawnShape("ge_mission_barriere.shape", 9174.4, -3449.388, -73.044, 0, 0.21, -2.7, '{"scale y":1,"col size x":3.424,"col pos y":-3449.388,"col orientation":0.462,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9174.4,"scale z":1}')

-- a7f5259d59da0c04f0185043bec57d473c54d6cb
SceneEditor:spawnShape("ge_mission_barriere.shape", 9193.966, -3472.13, -79.12, 0, 0, -2.77, '{"scale y":1,"col size x":3.424,"col pos y":-3472.13,"col orientation":-2.78,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9193.966,"scale z":1}')

-- d9e2c0584a6f0bc6e01df7bba84a30459dfc7533
SceneEditor:spawnShape("ge_mission_barriere.shape", 9205.273, -3469.688, -78.663, 0, 0, -0.25, '{"scale y":1,"col size x":3.424,"col pos y":-3469.688,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9205.273,"scale z":1}')

-- 469753ba62aacc94eedb4e3b2a747792f62574fc
SceneEditor:spawnShape("ge_mission_barriere.shape", 9201.688, -3469.445, -78.876, 0, 0, 0.09, '{"scale y":1,"col size x":3.424,"col pos y":-3469.445,"col orientation":0,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9201.688,"scale z":1}')

-- dd7a67c9052fe97f6fc07d822e509cb4017a2c9d
SceneEditor:spawnShape("ge_mission_carapace_bull.shape", 9204.673, -3471.425, -80.057, 0, -0.58, -5.16, '{"scale y":1,"col size x":3.087,"col pos y":-3471.425,"col orientation":0,"col size z":2.576,"scale x":1,"col size y":4.358,"col pos x":9204.673,"scale z":1}')

-- c968c13f610dbae5018057f61c0010230f127481
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9200.967, -3470.218, -79.388, 0, 0, 0, '{"scale y":1,"col size x":0.961,"col pos y":-3470.218,"col orientation":0.236,"col size z":5.674,"scale x":1,"col size y":-0.747,"col pos x":9200.967,"scale z":1}')

-- 3db57bcefaa99a9c38ab10adbc2d589c50d7b3dc
SceneEditor:spawnShape("ge_mission_reverbere_red.ps", 9195.256, -3472.428, -79.509, 0, 0, 0, '{"scale y":1,"col size x":0.931,"col pos y":-3472.428,"col orientation":-1.192,"col size z":5.674,"scale x":1,"col size y":-0.777,"col pos x":9195.256,"scale z":1}')

-- f8e168b6cd85d0b51cd54c449002bbdac31f36d2
SceneEditor:spawnShape("ge_mission_objet_pack_1.shape", 9188.319, -3460.946, -75.287, 0.4, -0.11, 0.52, '{"scale y":1,"col size x":2.339,"col pos y":-3460.946,"col orientation":0,"col size z":2.556,"scale x":1,"col size y":2.837,"col pos x":9188.319,"scale z":1}')

-- 581f75f788f5448f1bc8b48435641059efa7661d
SceneEditor:spawnShape("ge_mission_tonneau_broke.shape", 9188.41, -3463.652, -76.502, 0, 0, 0, '{"scale y":1,"col size x":1.054,"col pos y":-3463.652,"col orientation":0,"col size z":1.241,"scale x":1,"col size y":1.054,"col pos x":9188.41,"scale z":1}')

-- cb850e07db3b7996230cf9a50c0fa2cb7f47d13d
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 9186.909, -3462.07, -76.123, 0, 0, -0.26, '{"scale y":1,"col size x":1,"col pos y":-3462.07,"col orientation":-0.26,"col size z":1,"scale x":1,"col size y":1,"col pos x":9186.909,"scale z":1}')

-- 6e0993e1adf9162efc8204d482cc74ad82b575cc
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 9192.876, -3475.079, -78.957, 0, 0, -1.73, '{"scale y":1,"col size x":2.532,"col pos y":-3475.079,"col orientation":-1.12,"col size z":2,"scale x":1,"col size y":1.299,"col pos x":9192.876,"scale z":1}')

-- 54347deafa4c3ea2d6bd08ffba042218d8a31a9d
SceneEditor:spawnShape("ge_mission_barriere.shape", 9179.032, -3475.985, -77.297, 0, 0, -0.97, '{"scale y":1,"col size x":3.424,"col pos y":-3475.985,"col orientation":2.175,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9179.032,"scale z":1}')

-- 8e8854ea97bc1e1303796f9a154fc43548703b25
SceneEditor:spawnShape("ge_mission_barriere.shape", 9174.782, -3469.77, -77.323, 0, 0, -0.9, '{"scale y":1,"col size x":3.424,"col pos y":-3469.77,"col orientation":-0.92,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9174.782,"scale z":1}')

-- de4a2b751df2044b0ee933172c7f82a59a52c0c3
SceneEditor:spawnShape("ge_mission_barriere.shape", 9173.989, -3461.139, -75.748, -0.04, -0.35, -4.44, '{"scale y":1,"col size x":3.424,"col pos y":-3461.139,"col orientation":1.81,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9173.989,"scale z":1}')

-- 8c801b788031007bcca642f5677b4e9ff8a001c4
SceneEditor:spawnShape("ge_mission_barriere.shape", 9172.789, -3458.151, -74.529, 0, -0.28, -4.3, '{"scale y":1,"col size x":3.424,"col pos y":-3458.151,"col orientation":1.97,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9172.789,"scale z":1}')

-- df6c2d045825961ee72269ebe9918c57479a5e22
SceneEditor:spawnShape("ge_mission_barriere.shape", 9171.966, -3454.871, -73.747, 0, 0.17, -1.42, '{"scale y":1,"col size x":3.424,"col pos y":-3454.871,"col orientation":-1.42,"col size z":2.303,"scale x":1,"col size y":0.188,"col pos x":9171.966,"scale z":1}')

-- 38aa38cdbf75daf50f657455dc678360f9a1891e
SceneEditor:spawnShape("ge_mission_tente.shape", 9186.526, -3449.64, -72.235, 0, 0, -3.42, '{"scale y":1,"col size x":6.438,"col pos y":-3449.64,"col orientation":0.06,"col size z":5.034,"scale x":1,"col size y":5.775,"col pos x":9186.526,"scale z":1}')

-- 3c8cc9ab05b67e11c8010a6642df73679f8a70af
SceneEditor:spawnShape("ge_mission_enclos.shape", 9188.838, -3475.181, -77.972, 0, 0, -5.85, '{"scale y":1,"col size x":6.741,"col pos y":-3475.226,"col orientation":-1.17,"col size z":6.629,"scale x":1,"col size y":8.623,"col pos x":9188.183,"scale z":1}')


