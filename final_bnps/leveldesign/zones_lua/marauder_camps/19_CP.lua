---- ZONE : 19_CP
-- GROUP: campfire
-- 31cc6930068bb2dd7a8af38c513215561e8fa91e
SceneEditor:spawnShape("ge_feudecamp.ps", 10841.048, -3069.01, -76.96, 0, 0, 0, '{"col size x":2.491,"col orientation":1.777,"scale z":1,"col size y":2.608,"col size z":5.716,"scale y":1,"scale x":1,"col pos x":10841.048,"col pos y":-3069.01}')


-- GROUP: crates
-- 5f23706c48cfbf346e5402d444cdaf6147324260
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10860.236, -3077.263, -78.6, 0, 0, 0, '{"col size x":1.054,"col orientation":0,"scale z":1,"col size y":1.054,"col size z":1.241,"scale y":1,"scale x":1,"col pos x":10860.236,"col pos y":-3077.263}')

-- 0a8ca998429a33702673aeefe2dfed18d443f7a1
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10857.309, -3079.71, -78.683, 0, 0, 0, '{"col size x":1.054,"col orientation":0,"scale z":1,"col size y":1.054,"col size z":1.241,"scale y":1,"scale x":1,"col pos x":10857.309,"col pos y":-3079.71}')

-- 578fbc1e2c006f0463edfed302d7008fa6ff6355
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10857.483, -3078.746, -78.637, 0, 0, 0, '{"col size x":1.173,"col orientation":0,"scale z":0.833,"col size y":1.078,"col size z":1.192,"scale y":0.833,"scale x":0.833,"col pos x":10857.483,"col pos y":-3078.746}')

-- fa7da0c381309f3de1fc29c01da1507c99ecacfb
SceneEditor:spawnShape("ge_mission_3_jarres.shape", 10858.063, -3077.919, -78.573, 0, 0, 0, '{"col size x":1.173,"col orientation":0,"scale z":0.833,"col size y":1.078,"col size z":1.192,"scale y":0.833,"scale x":0.833,"col pos x":10858.063,"col pos y":-3077.919}')

-- 71162864c557b7510a79a32e17a661bee68b72bd
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10859.856, -3078.389, -78.491, 0, 0, -0.54, '{"col size x":2.532,"col orientation":-0.54,"scale z":1,"col size y":1.299,"col size z":2,"scale y":1,"scale x":1,"col pos x":10859.856,"col pos y":-3078.389}')

-- e04beef9af72f101dea706bccd7d9331b97507eb
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10859.584, -3080.48, -78.593, 0, 0, -2, '{"col size x":2.532,"col orientation":-1.92,"scale z":1,"col size y":1.299,"col size z":2,"scale y":1,"scale x":1,"col pos x":10859.584,"col pos y":-3080.48}')

-- 316753ccd4efe6f3ff13336e80331ffd9d200f89
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10858.418, -3079.742, -78.436, 0, 0, -1.89, '{"col size x":2.532,"col orientation":1.25,"scale z":1,"col size y":1.299,"col size z":2,"scale y":1,"scale x":1,"col pos x":10858.418,"col pos y":-3079.742}')

-- 9c35f46539ca65725be86bbf0c28fc7022b5bc1c
SceneEditor:spawnShape("ge_mission_coffre_old.shape", 10854.679, -3072.336, -77.585, 0, 0, -0.49, '{"col size x":1.429,"col orientation":0,"scale z":1,"col size y":0.712,"col size z":0.84,"scale y":1,"scale x":1,"col pos x":10854.679,"col pos y":-3072.336}')

-- 924f634428730ac9cb4879e3ac2328c4718d7ec2
SceneEditor:spawnShape("ge_mission_coffre_old.shape", 10850.791, -3075.97, -77.407, 0, 0, -0.43, '{"col size x":1.429,"col orientation":0,"scale z":1,"col size y":0.712,"col size z":0.84,"scale y":1,"scale x":1,"col pos x":10850.791,"col pos y":-3075.97}')


-- GROUP: packers
-- e7445391e3d679dcc513ee06588b3bb9249808eb
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10859.71, -3040.043, -78.655, 0, 0, 0, '{"col size x":1.7,"col pos x":10859.71,"col pos y":-3040.043,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')


-- GROUP: tents
-- 58f7d90b103cc01a6021eb5a9fa14980c8ebf140
SceneEditor:spawnShape("ge_mission_tente_zo.shape", 10854.827, -3068.243, -77.527, 0, -0.02, 1.09, '{"col size x":5.572,"col orientation":-0.48,"scale z":1.08,"col size y":5.572,"col size z":5.994,"scale y":1.08,"scale x":1.08,"col pos x":10854.827,"col pos y":-3068.243}')

-- 68cc1ee669d6b88e14f68086fb6290a7fa7aaf3f
SceneEditor:spawnShape("ge_mission_tente_zo.shape", 10849.556, -3079.045, -77.616, -0.01, 0.03, 1.16, '{"col size x":6.482,"col orientation":-0.52,"scale z":1.07,"col size y":6.482,"col size z":5.994,"scale y":1.07,"scale x":1.07,"col pos x":10849.556,"col pos y":-3079.045}')


