
---- ZONE : 18_CP
-- GROUP: packers
-- a2c410f7be132a4a9e6e4c0fa1f1d840dd032386
SceneEditor:spawnShape("ge_mission_sac_a.shape", 10836.62, -3030.885, -78.013, 0, 0, 0, '{"col size x":0.967,"col pos x":10836.62,"col pos y":-3030.885,"col orientation":0,"col size z":0.404,"scale z":1,"scale x":1,"scale y":1,"col size y":1.165}')

-- 27cf943c9791443b72c74062d786b0d9e5a288fd
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10835.653, -3030.791, -77.997, 0, 0, 0, '{"col size x":1.7,"col pos x":10835.653,"col pos y":-3030.791,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')

-- d3053035332711c5bbccdec80c9fb29ecd31799e
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10840.896, -3030.399, -78.061, 0, 0, 0, '{"col size x":1.7,"col pos x":10840.896,"col pos y":-3030.399,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')

-- c74510761268c335792355948cd361aaf14fcdc1
SceneEditor:spawnShape("ge_mission_barriere.shape", 10836.061, -3031.509, -78.006, 0, 0, 0, '{"col size x":3.424,"col pos x":10836.061,"col pos y":-3031.509,"col orientation":0,"col size z":2.303,"scale z":1,"scale x":1,"scale y":1,"col size y":0.188}')

-- c93aa06a1d2026e8474bc25c58d76ed047a62042
SceneEditor:spawnShape("ge_mission_barriere.shape", 10840.996, -3031.303, -78.108, 0, 0, 0, '{"col size x":3.424,"col pos x":10840.996,"col pos y":-3031.303,"col orientation":-0,"col size z":2.303,"scale z":1,"scale x":1,"scale y":1,"col size y":0.188}')

-- 05155bd12cf8ea2f40a547bdf82af83759b7eb89
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10862.143, -3038.961, -78.589, 0, 0, 0, '{"col size x":2.145,"col pos x":10862.143,"col pos y":-3038.961,"col orientation":0,"col size z":2.481,"scale z":1,"scale x":1,"scale y":1,"col size y":1.103}')

-- 58d893216db9bb709f67a04effce40bf12ec2b56
SceneEditor:spawnShape("ge_mission_3_tonneaux.shape", 10861.369, -3038.104, -78.576, 0, 0, 0, '{"col size x":2.145,"col pos x":10861.369,"col pos y":-3038.104,"col orientation":0,"col size z":2.481,"scale z":1,"scale x":1,"scale y":1,"col size y":1.103}')

-- 7b2f61b3a5772591ad0841c4654afd52a560be94
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10860.477, -3039.987, -78.65, 0, 0, 0, '{"col size x":1.7,"col pos x":10860.477,"col pos y":-3039.987,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')

-- 2b958038c274fd9931aa885baf6f1cae2df360db
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10859.006, -3039.337, -78.605, 0, 0, 0, '{"col size x":1.7,"col pos x":10859.006,"col pos y":-3039.337,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')

-- 6ebdc3988fcf60d6afc6eec4058319d154ce74e4
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10860.312, -3039.188, -78.661, 0, 0, 0, '{"col size x":1.7,"col pos x":7.306,"col pos y":3.114,"col orientation":0,"col size z":0.968,"scale z":1,"scale x":1,"scale y":1,"col size y":1.62}')

-- f35f8426cec95b737696d37825e5cdfca1a75ed5
SceneEditor:spawnShape("ge_mission_enclos.shape", 10858.543, -3034.281, -78.302, 0, 0, -4.17, '{"col size x":8.401,"col pos x":10858.025,"col pos y":-3034.937,"col orientation":-1.04,"col size z":6.629,"scale z":1,"scale x":1,"scale y":1,"col size y":7.043}')

