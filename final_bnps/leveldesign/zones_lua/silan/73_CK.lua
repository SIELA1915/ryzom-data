-- GROUP: deco
-- 24bff4d245ce4091f5dbdd4b4db8e4811e061dbd
SceneEditor:spawnShape("ge_mission_3_caisses.shape", 10043.973, -11798.486, -15.501, 0, 0, 0.47, '{"col size z":2,"col size x":2.532,"col pos x":10043.973,"col orientation":0,"col pos y":-11798.486,"scale y":1,"col size y":1.299,"scale z":1,"scale x":1}')

-- f3c2efeb572667a269e738bf92ecce21b482340c
SceneEditor:spawnShape("ge_mission_barriere.shape", 10038.628, -11801.443, -15.591, 0, 0, 0, '{"col size z":2.303,"col size x":3.424,"col pos x":10038.628,"col orientation":0,"col pos y":-11801.443,"scale y":1,"col size y":0.188,"scale z":1,"scale x":1}')

-- 72cf97ee0d248e4bd9d0d3636b57dd70c19a2b13
SceneEditor:spawnShape("ge_mission_barriere.shape", 10042.082, -11800.496, -15.674, 0, 0, 0.47, '{"col size z":2.303,"col size x":3.424,"col pos x":10042.082,"col orientation":0.31,"col pos y":-11800.496,"scale y":1,"col size y":0.188,"scale z":1,"scale x":1}')

-- 0c061a640456f1393f1f86ee2bec5aa30d6b46cf
SceneEditor:spawnShape("ge_mission_barriere.shape", 10045.527, -11798.551, -15.512, 0, 0, 0.48, '{"col size z":2.303,"col size x":3.424,"col pos x":10045.527,"col orientation":0.52,"col pos y":-11798.551,"scale y":1,"col size y":0.188,"scale z":1,"scale x":1}')

-- 5b930f49c6c9685f02015fa8a99e1f99e03cf420
SceneEditor:spawnShape("gen_mission_tonneau_broke.shape", 10042.991, -11797.048, -15.106, 0, 0, 0, '{"col size z":1.241,"col size x":1.044,"col pos x":10042.991,"col orientation":0,"col pos y":-11797.048,"scale y":1,"col size y":1.044,"scale z":1,"scale x":1}')

-- 4d8b4576b38e44686c7a0030ab20fffa29d72637
SceneEditor:spawnShape("ge_mission_sac_b.shape", 10020.969, -11794.96, -14.074, 0, 0, 0, '{"col size z":0.968,"col size x":1.7,"col pos x":10020.969,"col orientation":-0.334,"col pos y":-11794.96,"scale y":1,"col size y":1.62,"scale z":1,"scale x":1}')

-- 33af71b3305d70f80957ae5fa978686de2dc8675
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10027.998, -11796.003, -14.14, 0, 0, 0, '{"col size z":1.241,"col size x":1.054,"col pos x":10027.998,"col orientation":0,"col pos y":-11796.003,"scale y":1,"col size y":1.054,"scale z":1,"scale x":1}')

-- e70db27b1bb436c24f5818514a9c897962417d27
SceneEditor:spawnShape("ge_mission_1_tonneau.shape", 10027.005, -11795.012, -13.985, 0, 0, 0, '{"col size z":1.241,"col size x":1.054,"col pos x":10027.005,"col orientation":0,"col pos y":-11795.012,"scale y":1,"col size y":1.054,"scale z":1,"scale x":1}')

-- dd9076afc79ff4a28d48dae5f47fd0ef80d22820
SceneEditor:spawnShape("ge_mission_enclos.shape", 10023.999, -11797.965, -14.221, 0, 0, 0.18, '{"col size z":6.629,"col size x":7.881,"col pos x":10024.025,"col orientation":9.55,"col pos y":-11799.397,"scale y":1,"col size y":4.263,"scale z":1,"scale x":1}')


