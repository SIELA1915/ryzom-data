#!/bin/sh

rm *.csv
cd bnps

for a in *
do
	event=$(echo $a | cut -d"_" -f2)
	out="../${event}_enable_events_bnp.csv"
	bnp="${a%.*}"
	if [ ! -f $out ]
	then
		echo "${bnp}.bnpe,${bnp}.bnp" > $out
	else
		echo "${bnp}.bnpe,${bnp}.bnp" >> $out
	fi
	echo "$event: $bnp"
done
