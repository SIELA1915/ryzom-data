-- GROUP: stable_zora
-- 0327f2da28d32f3157f6deee8ab6413e53e52133
SceneEditor:spawnShape("xmas_stable2022.ps", 8746.503, -2986.903, -77.011, 0, 0, -2.78, '{"col pos y":9333.097,"col size x":26.321,"scale z":1,"col size z":9.389,"col orientation":0,"scale x":1,"col size y":9.577,"scale y":1,"col pos x":-24318.506}')

-- GROUP: zora_deco
-- 747fdbd25029dd02e01a24adbc24bc1649b1af53
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 8717.598, -3007.558, -77.177, 0, 0, 0, '{"col orientation":-0.923,"col size z":11.357,"scale z":1,"col pos y":9312.442,"col size y":8.525,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":8.581}')

-- 0d4c29de8f4d16e6598fb5a280b5e5f055dd784e
SceneEditor:spawnShape("ge_mission_snowman.ps", 8757.685, -2952.515, -76.831, 0, 0, -1.96, '{"col orientation":0,"col size z":3.66,"scale z":1,"col pos y":9367.485,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')

-- 08d7c138b5bcce83f9d1f864b7e910dbcd01ef46
SceneEditor:spawnShape("Ge_mission_Gift.shape", 8762.02, -2951.092, -76.934, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9368.908,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- d7ebbeb5d62eca8b64281c8e5ba3845942dc5b9f
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 8762.413, -2949.56, -76.908, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9370.44,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 925c42ef79059bc2637e8599a9f8028c8011a8b1
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 8761.034, -2948.412, -76.889, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9371.588,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 473a979d5aa5055e2081576703ce5e1117de46e9
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 8773.384, -2949.419, -77.03, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9370.581,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- e2f1a83789cf58da5feb5ce058667f2d042c65ad
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 8771.141, -2948.134, -77.007, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"scale z":1,"col pos y":9371.866,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 2d443e130de09c7d22d1f0a09b80e4af901187c1
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 8771.9, -2950.171, -77.017, 0, 0, 0, '{"col orientation":-0.192,"col size z":0.943,"scale z":1,"col pos y":9369.829,"col size y":0.704,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":0.704}')

-- 9dbae1930635d8a019f3f4fd4ab7c7941420ad87
SceneEditor:spawnShape("ge_mission_snowman.ps", 8730.728, -3019.03, -77.362, 0, 0, -3.37, '{"col orientation":-1.828,"col size z":3.66,"scale z":1,"col pos y":9300.97,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')

-- a421635f3bd892b1e0de4b1d257edfc192f72685
SceneEditor:spawnShape("ge_mission_snowman.ps", 8713.349, -3019.602, -77.335, 0, 0, -3.25, '{"col orientation":0,"col size z":3.66,"scale z":0.99,"col pos y":9300.398,"col size y":1.601,"col pos x":-24320,"scale y":1,"scale x":1,"col size x":1.527}')