-- GROUP: stable_fh
-- 7d83c5d3c18465945f386bafc3305787bd9e2964
SceneEditor:spawnShape("xmas_stable2022.ps", 17269.717, -32941.965, -2.1, 0, 0, 3.14, '{"scale y":1,"col pos y":-20621.965,"col size y":9.577,"col pos x":-15795.292,"scale z":1,"scale x":1,"col orientation":0,"col size z":9.389,"col size x":26.321}')


-- GROUP: fh_deco
-- 8474960931467816e34c2cf7bdacaafab9899228
SceneEditor:spawnShape("Ge_mission_Gift.shape", 17218.037, -32902.801, -2.755, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.963,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20582.801,"scale x":1}')

-- bbf5390f2aeac5581b448a9eb15062812c989b73
SceneEditor:spawnShape("Ge_mission_Gift.shape", 17218.293, -32901.203, -2.751, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.707,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20581.203,"scale x":1}')

-- 697412b396579abc7e2574dab9646549433a5e0a
SceneEditor:spawnShape("Ge_mission_Gift-turquoise.shape", 17220.957, -32902.109, -2.546, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.043,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20582.109,"scale x":1}')

-- fb8811964acca886ee2c810a6fe2a848e7a50303
SceneEditor:spawnShape("Ge_mission_Gift-red.shape", 17220.281, -32900.988, -2.591, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.719,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20580.988,"scale x":1}')

-- c0606c7c6df8ce0665699f0fd277d7e40fb0ec01
SceneEditor:spawnShape("Ge_mission_Gift-purple.shape", 17220.166, -32903.008, -2.585, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7099.834,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20583.008,"scale x":1}')

-- f202c8e3efebc026b28dfc4b9177eb665ef41215
SceneEditor:spawnShape("Ge_mission_Gift-green.shape", 17218.957, -32900.383, -2.703, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7101.043,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20580.383,"scale x":1}')

-- 01c096e7c3f0fefbd13722e601cc0cddc57ee3a3
SceneEditor:spawnShape("Ge_mission_Gift-brown.shape", 17219.074, -32903.594, -2.653, 0, 0, 0, '{"col orientation":0,"col size z":0.943,"col pos x":-7100.926,"scale z":1,"scale y":1,"col size y":0.704,"col size x":0.704,"col pos y":-20583.594,"scale x":1}')

-- 1452ad6b4327c717f8424f6edafbe1ba39c35103
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 17236.945, -32857.77, 0.094, 0, 0, 0, '{"col orientation":0,"col size z":11.357,"col pos x":-24320,"scale z":1,"scale y":1,"col size y":8.525,"col size x":8.581,"col pos y":-20537.77,"scale x":1}')

-- 6f47dde424196e71f9f09545dd53c4adb7efc8dd
SceneEditor:spawnShape("ge_mission_snowman.ps", 17222.766, -32929.422, -1.556, 0, 0, 0, '{"col orientation":0,"col size z":3.66,"col pos x":-24320,"scale z":1,"scale y":1,"col size y":1.601,"col size x":1.527,"col pos y":-20609.422,"scale x":1}')