-- GROUP: stable_pyr
-- c18038c3af20958dc44389a7107814b81e13f96f
SceneEditor:spawnShape("xmas_stable2022.ps", 18963.371, -24408.867, -1.956, 0, 0, -1.45, '{"col pos y":-12088.867,"col pos x":-24319.701,"col orientation":0,"col size z":9.389,"scale z":1,"scale x":1,"col size x":26.321,"scale y":1,"col size y":9.577}')

-- GROUP: pyr_deco
-- d7010e9e237b9a8d45d2a8b273db459fc7fbd146
SceneEditor:spawnShape("ge_mission_snowman.ps", 18919.635, -24371.617, -1.326, 0, 0, -2.91, '{"col size z":3.66,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12051.617,"col pos x":-24320,"col size y":1.601,"col size x":1.527,"scale x":1}')

-- d157ef4a83d4745046a9066250775486b26c32aa
SceneEditor:spawnShape("ge_mission_snowman.ps", 18939.143, -24379.072, -0.933, 0, 0, -3.08, '{"col size z":3.66,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12059.072,"col pos x":-24319.479,"col size y":1.601,"col size x":1.527,"scale x":1}')

-- 7e13cf00feb0d0a7ee3779dccffe1719798eb0a2
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 18960.266, -24395.252, -1.91, 0, 0, 0, '{"col size z":11.357,"scale y":1,"scale z":1,"col orientation":0,"col pos y":-12075.252,"col pos x":-24320,"col size y":8.525,"col size x":8.581,"scale x":1}')