-- GROUP: stable_1
-- 97d6812ab11bd1ac15f061db231321a082935bcd
SceneEditor:spawnShape("xmas_stable2022.ps", 4712.073, -3211.028, -9.489, 0, 0, -0.83, '{"scale y":1,"col pos y":9108.972,"scale x":1,"scale z":1,"col size z":9.389,"col pos x":-36178.329,"col size y":9.577,"col orientation":0,"col size x":26.321}')

-- GROUP: yrk_deco
-- eef4cc65d98e369121641f774d6d4ee662a4428f
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4724.864, -3263.736, -7.397, 0, 0, 0, '{"col pos x":-24320,"scale z":0.8,"col orientation":0,"col size y":8.525,"scale y":0.8,"col pos y":9056.264,"col size z":11.357,"col size x":8.581,"scale x":0.8}')

-- 8e97bc24187b3aebb5d48905d34a5ca687cb4b94
SceneEditor:spawnShape("ge_mission_snowman.ps", 4704.283, -3208.374, -9.485, 0, 0, -3.64, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9111.626,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 01c44a5d9774c7358b437a192daa85ac24ed27b7
SceneEditor:spawnShape("ge_mission_snowman.ps", 4781.434, -3234.561, -9.45, 0, 0, -2.69, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9085.439,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- aeef85562950281a83911414529e9b0039f545c0
SceneEditor:spawnShape("ge_mission_snowman.ps", 4785.906, -3244.528, -9.497, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":1.601,"scale y":1,"col pos y":9075.472,"col size z":3.66,"col size x":1.527,"scale x":1}')

-- 0b2d3a752065f20f0d30b7af65c2c97b49858ad2
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4646.815, -3226.276, -9.472, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":9093.724,"col size z":11.357,"col size x":8.581,"scale x":1}')

-- 42a161db07a159b9ef47b1c4d9ce4cd20fd576b8
SceneEditor:spawnShape("ge_mission_xmas_tree.ps", 4659.424, -3203.465, -9.483, 0, 0, 0, '{"col pos x":-24320,"scale z":1,"col orientation":0,"col size y":8.525,"scale y":1,"col pos y":9116.535,"col size z":11.357,"col size x":8.581,"scale x":1}')